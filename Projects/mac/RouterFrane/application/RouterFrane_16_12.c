/**************************************************************************************************
  Filename:       msa.c
  Revised:        $Date: 2011-09-16 11:36:37 -0700 (Fri, 16 Sep 2011) $
  Revision:       $Revision: 27599 $

  Description:    This file contains the sample application that can be use to test
                  the functionality of the MAC, HAL and low level.


  Copyright 2006-2011 Texas Instruments Incorporated. All rights reserved.

  IMPORTANT: Your use of this Software is limited to those specific rights
  granted under the terms of a software license agreement between the user
  who downloaded the software, his/her employer (which must be your employer)
  and Texas Instruments Incorporated (the "License").  You may not use this
  Software unless you agree to abide by the terms of the License. The License
  limits your use, and you acknowledge, that the Software may not be modified,
  copied or distributed unless embedded on a Texas Instruments microcontroller
  or used solely and exclusively in conjunction with a Texas Instruments radio
  frequency transceiver, which is integrated into your product.  Other than for
  the foregoing purpose, you may not use, reproduce, copy, prepare derivative
  works of, modify, distribute, perform, display or sell this Software and/or
  its documentation for any purpose.

  YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE
  PROVIDED �AS IS� WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
  INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, TITLE,
  NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL
  TEXAS INSTRUMENTS OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER CONTRACT,
  NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER
  LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
  INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE
  OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT
  OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
  (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.

  Should you have any questions regarding your right to use this Software,
  contact Texas Instruments Incorporated at www.TI.com.
**************************************************************************************************/

/**************************************************************************************************

    Description:

                                KEY UP (or S1)
                              - Non Beacon
                              - First board in the network will be setup as the coordinator
                              - Any board after the first one will be setup as the device
                                 |
          KEY LEFT               |      KEY RIGHT(or S2)
                             ----+----- - Start transmitting
                                 |
                                 |
                                KEY DOWN



**************************************************************************************************/


/**************************************************************************************************
 *                                           Includes
 **************************************************************************************************/

/* Hal Driver includes */
#include "hal_types.h"
#include "hal_key.h"
#include "hal_timer.h"
#include "hal_drivers.h"
#include "hal_led.h"
#include "hal_adc.h"


/* OS includes */
#include "OSAL.h"
#include "OSAL_Tasks.h"
#include "OSAL_PwrMgr.h"

/* Application Includes */
#include "OnBoard.h"

/* For Random numbers */
#include "mac_radio_defs.h"

/* MAC Application Interface */
#include "mac_api.h"
#include "mac_main.h"
#include "mac_radio.h"

#ifdef MAC_SECURITY
#include "mac_spec.h"

/* MAC Sceurity */
#include "mac_security_pib.h"
#include "mac_security.h"
#endif /* MAC_SECURITY */

/* Application */
#include "RouterFrane.h"

/* WinetAQ */
#include "hal_i2c.h"
#include "astronomic.h"
#include "eeprom.h"

#if defined COUNTER
#include "counter.h"
#endif
#if defined MODEM
#include "modem.h"
#endif

#include "hal_uart.h"


#include "OSAL_Nv.h"
#include "hal_flash.h"
#include "sensors.h"




/**************************************************************************************************
 *                                           Constant
 **************************************************************************************************/

#define MSA_MAC_MAX_RESULTS       5             /* Maximun number of scan result that will be accepted */
#define MSA_MAX_DEVICE_NUM        50            /* Maximun number of devices can associate with the coordinator */

#if defined (HAL_BOARD_CC2420DB)
  #define MSA_HAL_ADC_CHANNEL     HAL_ADC_CHANNEL_0             /* AVR - Channel 0 and Resolution 10 */
  #define MSA_HAL_ADC_RESOLUTION  HAL_ADC_RESOLUTION_10
#elif defined (HAL_BOARD_DZ1611) || defined (HAL_BOARD_DZ1612) || defined (HAL_BOARD_DRFG4618) || defined (HAL_BOARD_F2618)
  #define MSA_HAL_ADC_CHANNEL     HAL_ADC_CHANNEL_0             /* DZ1611 and DZ1612 - Channel 0 and Resolution 12 */
  #define MSA_HAL_ADC_RESOLUTION  HAL_ADC_RESOLUTION_12
#else
  #define MSA_HAL_ADC_CHANNEL     HAL_ADC_CHANNEL_7             /* CC2430 EB & DB - Channel 7 and Resolution 14 */
  #define MSA_HAL_ADC_RESOLUTION  HAL_ADC_RESOLUTION_14
#endif

#define MSA_EBR_PERMITJOINING    TRUE
#define MSA_EBR_LINKQUALITY      1
#define MSA_EBR_PERCENTFILTER    0xFF

/* Size table for MAC structures */
const CODE uint8 msa_cbackSizeTable [] =
{
  0,                                   /* unused */
  sizeof(macMlmeAssociateInd_t),       /* MAC_MLME_ASSOCIATE_IND */
  sizeof(macMlmeAssociateCnf_t),       /* MAC_MLME_ASSOCIATE_CNF */
  sizeof(macMlmeDisassociateInd_t),    /* MAC_MLME_DISASSOCIATE_IND */
  sizeof(macMlmeDisassociateCnf_t),    /* MAC_MLME_DISASSOCIATE_CNF */
  sizeof(macMlmeBeaconNotifyInd_t),    /* MAC_MLME_BEACON_NOTIFY_IND */
  sizeof(macMlmeOrphanInd_t),          /* MAC_MLME_ORPHAN_IND */
  sizeof(macMlmeScanCnf_t),            /* MAC_MLME_SCAN_CNF */
  sizeof(macMlmeStartCnf_t),           /* MAC_MLME_START_CNF */
  sizeof(macMlmeSyncLossInd_t),        /* MAC_MLME_SYNC_LOSS_IND */
  sizeof(macMlmePollCnf_t),            /* MAC_MLME_POLL_CNF */
  sizeof(macMlmeCommStatusInd_t),      /* MAC_MLME_COMM_STATUS_IND */
  sizeof(macMcpsDataCnf_t),            /* MAC_MCPS_DATA_CNF */
  sizeof(macMcpsDataInd_t),            /* MAC_MCPS_DATA_IND */
  sizeof(macMcpsPurgeCnf_t),           /* MAC_MCPS_PURGE_CNF */
  sizeof(macEventHdr_t)                /* MAC_PWR_ON_CNF */
};

/**************************************************************************************************
 *                                        Local Variables
 **************************************************************************************************/


/* Device information */
uint16        msa_PanId = MSA_PAN_ID; //PAN ID: se non corrisponde a quella delle rete non viene effettuata l'associazione 
uint16        msa_CoordShortAddr = MSA_COORD_SHORT_ADDR; //Indirizzo del nodo padre al momento dell'associazione
uint16        msa_DevShortAddr   = 0x0000; // indirizzo SHORT del nodo
uint16        fatherAdd; // indirizzo del nodo padre che potrebbe essere diverso da quello definito durante l'associazione nel caso di Fault Tolerance

/*varibili che gesticono la fase di Fault Tolerance: numOfReply serve per cercare un nodo padre tra quelli che possono essere presenti
su differenti livelli, stato gestisce la macchina a stati secondo il meccanismo definito nel caso di Fault Tolerance
ATTENZIONE: la variabile stato deve essere inizializzata a 0*/
uint8         numOfReply=0,stato=0; 

/* Indice utilizzato per inviare un solo pacchetto con l'orario al nodo figlio che invia pi� pacchetti dati */
uint8         msa_NumOfDevices = 0;

#ifdef MAC_SECURITY
  /* Current number of devices (including coordinator) communicating to this device */
  uint8       msa_NumOfSecuredDevices = 0;
#endif /* MAC_SECURITY */

/* Vettore utilizzato per salvare i dati in memoria e per inviare il pacchetto dati */
uint8         msa_Data1[MSA_PACKET_LENGTH];

/* Vettore utilizzato per inviare il pacchetto ai figli con l'orario (8byte) e i secondi in cui il nodo si risveglia (1byte) */
uint8         msa_Data2[9];

/* TRUE and FALSE value */
bool          msa_MACTrue = TRUE;
bool          msa_MACFalse = FALSE;

/* Beacon payload, this is used to determine if the device is not zigbee device */
uint8         msa_BeaconPayload[] = {0x22, 0x33, 0x44};
uint8         msa_BeaconPayloadLen = 3;

/* Contains pan descriptor results from scan */
macPanDesc_t  msa_PanDesc[MSA_MAC_MAX_RESULTS];

/* flags used in the application */
bool          msa_IsSampleBeacon = FALSE;   /* True if the beacon payload match with the predefined */
bool          msa_IsDirectMsg    = TRUE;   /* True if the messages will be sent as direct messages */

/* Variabile che consente di definire gli stati di funzionamento del nodo: MSA_ASSOC_STATE e MSA_SEND_STATE
La variabile viene inizializzata a MSA_START_STATE in modo tale che nello SLEEP_EVENT il nodo vada subito in sleep*/
uint8         msa_State = MSA_START_STATE;   

/* Structure that used for association request */
macMlmeAssociateReq_t msa_AssociateReq;

/* Structure that used for association response */
macMlmeAssociateRsp_t msa_AssociateRsp;

/* Vettore che consente di gestire l'invio dell'orario una sola volta al figlio che trasmette pi� pacchetti */
uint16 msa_DeviceRecord[MSA_MAX_DEVICE_NUM];

uint8 msa_SuperFrameOrder;
uint8 msa_BeaconOrder;

/* Task ID */
uint8 MSA_TaskId;


#ifdef MAC_SECURITY
/**************************************************************************************************
 *                                  MAC security related constants
 **************************************************************************************************/
#define MSA_KEY_TABLE_ENTRIES         1
#define MSA_KEY_ID_LOOKUP_ENTRIES     1
#define MSA_KEY_DEVICE_TABLE_ENTRIES  8
#define MSA_KEY_USAGE_TABLE_ENTRIES   1
#define MSA_DEVICE_TABLE_ENTRIES      0 /* can grow up to MAX_DEVICE_TABLE_ENTRIES */
#define MSA_SECURITY_LEVEL_ENTRIES    1
#define MSA_MAC_SEC_LEVEL             MAC_SEC_LEVEL_ENC_MIC_32
#define MSA_MAC_KEY_ID_MODE           MAC_KEY_ID_MODE_1
#define MSA_MAC_KEY_SOURCE            {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}
#define MSA_MAC_DEFAULT_KEY_SOURCE    {0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33}
#define MSA_MAC_KEY_INDEX             3  /* cannot be zero for implicit key identifier */


/* Default MSA Security Parameters for outgoing frames */
bool  macSecurity       = TRUE;
uint8 msa_securityLevel = MSA_MAC_SEC_LEVEL;
uint8 msa_keyIdMode     = MSA_MAC_KEY_ID_MODE;
uint8 msa_keySource[]   = MSA_MAC_KEY_SOURCE;
uint8 msa_keyIndex      = MSA_MAC_KEY_INDEX;


/**************************************************************************************************
 *                                 Security PIBs for outgoing frames
 **************************************************************************************************/
const keyIdLookupDescriptor_t msa_keyIdLookupList[] =
{
  {
    {0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x03}, 0x01 /* index 3, 9 octets */
  }
};

/* Key device list can be modified at run time
 */
keyDeviceDescriptor_t msa_keyDeviceList[] =
{
  {0x00, false, false},
  {0x00, false, false},
  {0x00, false, false},
  {0x00, false, false},
  {0x00, false, false},
  {0x00, false, false},
  {0x00, false, false},
  {0x00, false, false}
};

const keyUsageDescriptor_t msa_keyUsageList[] =
{
  {MAC_FRAME_TYPE_DATA, MAC_DATA_REQ_FRAME}
};

const keyDescriptor_t msa_keyTable[] =
{
  {
    (keyIdLookupDescriptor_t *)msa_keyIdLookupList, MSA_KEY_ID_LOOKUP_ENTRIES,
    (keyDeviceDescriptor_t *)msa_keyDeviceList, MSA_KEY_DEVICE_TABLE_ENTRIES,
    (keyUsageDescriptor_t *)msa_keyUsageList, MSA_KEY_USAGE_TABLE_ENTRIES,
    {0x12, 0x34, 0x56, 0x78, 0x9a, 0xbc, 0xde, 0xf0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}
  }
};

const uint8 msa_keyDefaultSource[] = MSA_MAC_DEFAULT_KEY_SOURCE;


/**************************************************************************************************
 *                                 Security PIBs for incoming frames
 **************************************************************************************************/

/* Device table can be modified at run time. */
deviceDescriptor_t msa_deviceTable[] =
{
  {0, 0xFFFF, 0, 0, FALSE},
  {0, 0xFFFF, 0, 0, FALSE},
  {0, 0xFFFF, 0, 0, FALSE},
  {0, 0xFFFF, 0, 0, FALSE},
  {0, 0xFFFF, 0, 0, FALSE},
  {0, 0xFFFF, 0, 0, FALSE},
  {0, 0xFFFF, 0, 0, FALSE},
  {0, 0xFFFF, 0, 0, FALSE}
};

const securityLevelDescriptor_t msa_securityLevelTable[] =
{
  {MAC_FRAME_TYPE_DATA, MAC_DATA_REQ_FRAME, MAC_SEC_LEVEL_NONE, FALSE}
};
#else /* MAC_SECURITY */
uint8 msa_securityLevel = MAC_SEC_LEVEL_NONE;
uint8 msa_keyIdMode     = MAC_KEY_ID_MODE_NONE;
uint8 msa_keySource[]   = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
uint8 msa_keyIndex      = 0;
#endif /* MAC_SECURITY */


/**************************************************************************************************
 *                                     Local Function Prototypes
 **************************************************************************************************/
/* Setup routines */
void MSA_DeviceStartup(void);

/* MAC related routines */
void MSA_AssociateReq(void);
void MSA_AssociateRsp(macCbackEvent_t* pMsg);
void MSA_McpsDataReq(uint8* data, uint8 dataLength, bool directMsg, uint16 dstShortAddr,bool ack);
void MSA_ScanReq(uint8 scanType, uint8 scanDuration);

/* Support */
bool MSA_BeaconPayLoadCheck(uint8* pSdu);

uint8 aSecExtendedAddress[8]; //variabile che consente di salvare l'indirizzo secondario esteso del nodo da cui ricaviamo anche il numero/tipo di sensori collegati

/*ReadBuffer viene utilizzato per leggere e scrivere l'astronomico (la dimensione deve diventare 9 se si effettua la calibrazione)
matchBuffer viene utilizzato per calcolare i tempi di attesa del nodo in fase attiva confrontando l'orario letto quanto si entra nella 
fase di SEND_STATE con l'orario letto in una qualunque altra fase
timeStampBuffer viene usato per verificare il tempo in cui il nodo � rimasto attivo e gestire in maniera corretta l'interrupt di risveglio*/
uint8 readBuffer[8], matchBuffer[8], timeStampBuffer[8],activeBuffer[8];


/* Sleep - funzioni e variabili di supporto */
void incrementAstro(uint8 min); //funzione che consente di settare l'orario di risveglio del nodo
void SleepAstro(void); //funzione che manda in sleep il nodo
void checkMeseUp(uint8 mese); //funzione che consente di gestire i mesi per il settaggio dell'orario e dell'interrupt di risveglio
void defineDate(void); // funzione che consente di definire la data per il settaggio dell'orario e dell'interrupt di risveglio
uint8 preparePacket(void); //funzione che consente di preparare il pacchetto dati da inviare, restituisce la lunghezza del pacchetto

/*funzione utilizzata per gestire la Fault Tolerance, passando come parametro lo short address del padre che non risponde pi� 
fornisce in uscita il numero di tentativi da effettuare per cercare un nuovo padre*/
void faultTolerance(void); 

//funzione che calcola una differenza di tempi
uint16 calcOsalTimerPeriod(void);
//funzione che riattiva le porte al risveglio del chip
void InputOutputWakeUp(void);
//funzione che spegne le porte prima della fase di sleep
void InputOutputSleep(void);
//funzione che consente di accendere e spegnare la sezione radio
void setMessageTipology(uint8 val);
//funzione che effettua il salvataggio dei dati ricevuti
void saveData(void);
//funzione che consente la lettura dei dati da eprom
uint8 readData(uint16 len);
//funzione che effettua la cancellazione da Eprom dei dati inviati correttamente
void updateEpromAddres(uint16 updateLen);

uint8 diffTimestamp(void);
//bool timePermitAssociation(uint8 mode);
bool timePermitAssociation(void);
uint8 countSens(uint16 id);
uint8 timeBuffer[8]; // buffer dove memorizzo secondi,minuti,ore...
uint8 newGiorno,newMese,newMinuti,newOre,newAnno,newSecondi,newDecCent,newdayWeek; // per settaggio allarme
uint8 retControl;

/* ADC */
uint16 getAdcChannel(uint8 channel);
uint16 getBattery(void);

/*Rete funzioni e variabili di supporto*/
uint8 minWakeUp=0; //variabile che indica il valori di secondi nei quali si deve svegliare il nodo
uint8 minWakeUpOld=0xFF; //il settaggio iniziale serve per far partire la sincronizzazione Matteo 3 Agosto
uint8 diffMinWakeUp=0; //Matteo 3 Agosto
uint8 minToSleep;
uint16 assocShortAddress=0xFFFF;
uint16 broadShortAddress=0xFFFF;
uint8 assocExtAdd[2];
bool txToCoord=FALSE;
uint8 countPck=0;
bool enableAssociation=TRUE; //settaggio indifferente, viene messo FALSE al primo risveglio
uint8 rssi;
uint8 dataLen;
uint16 writeAddress=MSA_READ_ADDRESS;
uint16 lenOfMyPack=0,lenOfSon=0;
uint16 byteEpromTx=0;
uint8 numSensori;
bool orphan=FALSE;
bool present;
uint16 winRx,osalTimerValue;
uint16 addrFree=0, windowStar=0; //Matteo 11 Ottobre
bool time=FALSE;
bool msa_IsStarted= FALSE;   /* Variabile da mantenere per evitare che se un nodo si resetta altri nodi cambino padre
settando la variabile fatherAdd nell'evento BEACON_NOTIFY_IND*/
bool sinch=FALSE;


 
uint8 countLevel;

/*Prova FaultToleracen Teo*/
uint8 indexChannel=0;
/*Da riportare in SaveData*/
 uint16 totLenght1;


#ifdef MAC_SECURITY
/**************************************************************************************************
 *
 * @fn          MSA_SecurityInit
 *
 * @brief       Initialize the security part of the application
 *
 * @param       none
 *
 * @return      none
 *
 **************************************************************************************************/
static void MSA_SecurityInit(void)
{
  uint8   keyTableEntries      = MSA_KEY_TABLE_ENTRIES;
  uint8   autoRequestSecLevel  = MAC_SEC_LEVEL_NONE;
  uint8   securityLevelEntries = MSA_SECURITY_LEVEL_ENTRIES;

  /* Write key table PIBs */
  MAC_MlmeSetSecurityReq(MAC_KEY_TABLE, (void *)msa_keyTable);
  MAC_MlmeSetSecurityReq(MAC_KEY_TABLE_ENTRIES, &keyTableEntries );

  /* Write default key source to PIB */
  MAC_MlmeSetSecurityReq(MAC_DEFAULT_KEY_SOURCE, (void *)msa_keyDefaultSource);

  /* Write security level table to PIB */
  MAC_MlmeSetSecurityReq(MAC_SECURITY_LEVEL_TABLE, (void *)msa_securityLevelTable);
  MAC_MlmeSetSecurityReq(MAC_SECURITY_LEVEL_TABLE_ENTRIES, &securityLevelEntries);

  /* TBD: MAC_AUTO_REQUEST is true on by default
   * need to set auto request security PIBs.
   * dieable auto request security for now
   */
  MAC_MlmeSetSecurityReq(MAC_AUTO_REQUEST_SECURITY_LEVEL, &autoRequestSecLevel);

  /* Turn on MAC security */
  MAC_MlmeSetReq(MAC_SECURITY_ENABLED, &macSecurity);
}


/**************************************************************************************************
 *
 * @fn          MSA_SecuredDeviceTableUpdate
 *
 * @brief       Update secured device table and key device descriptor handle
 *
 * @param       panID - Secured network PAN ID
 * @param       shortAddr - Other device's short address
 * @param       extAddr - Other device's extended address
 * @param       pNumOfSecuredDevices - pointer to number of secured devices
 *
 * @return      none
 *
 **************************************************************************************************/
static void MSA_SecuredDeviceTableUpdate(uint16 panID,
                                         uint16 shortAddr,
                                         sAddrExt_t extAddr,
                                         uint8 *pNumOfSecuredDevices)
{
  /* Update key device list */
  msa_keyDeviceList[*pNumOfSecuredDevices].deviceDescriptorHandle = *pNumOfSecuredDevices;

  /* Update device table */
  msa_deviceTable[*pNumOfSecuredDevices].panID = panID;
  msa_deviceTable[*pNumOfSecuredDevices].shortAddress = shortAddr;
  sAddrExtCpy(msa_deviceTable[*pNumOfSecuredDevices].extAddress, extAddr);
  MAC_MlmeSetSecurityReq(MAC_DEVICE_TABLE, msa_deviceTable);

  /* Increase the number of secured devices */
  (*pNumOfSecuredDevices)++;
  MAC_MlmeSetSecurityReq(MAC_DEVICE_TABLE_ENTRIES, pNumOfSecuredDevices);
}
#endif /* MAC_SECURITY */


/**************************************************************************************************
 *
 * @fn          MSA_Init
 *
 * @brief       Initialize the application
 *
 * @param       taskId - taskId of the task after it was added in the OSAL task queue
 *
 * @return      none
 *
 **************************************************************************************************/
void MSA_Init(uint8 taskId)
{

  /* Initialize the task id */
  MSA_TaskId = taskId;

  /* initialize MAC features */
  MAC_InitDevice();
  MAC_InitCoord();

  /* Initialize MAC beacon */
  MAC_InitBeaconDevice();
  MAC_InitBeaconCoord();

  /* Reset the MAC */
  MAC_MlmeResetReq(TRUE);
  
  TXPOWER=MAC_TX_POWER;
  
  /* Imposta l'indirizzo IEEE Secondario */
  readIEEEAddressFromNv();
  
#ifdef MAC_SECURITY
  /* Initialize the security part of the application */
  MSA_SecurityInit();
#endif /* MAC_SECURITY */
  
  for(int i=0; i<MSA_PACKET_LENGTH; i++) 
    msa_Data1[i]=0;

  minToSleep=SLEEP_TIME;
  
  msa_BeaconOrder = MSA_MAC_BEACON_ORDER;
  msa_SuperFrameOrder = MSA_MAC_SUPERFRAME_ORDER;
 
    MSA_ScanReq(MAC_SCAN_ACTIVE, 3);

}

/**************************************************************************************************
 *
 * @fn          MSA_ProcessEvent
 *
 * @brief       This routine handles events
 *
 * @param       taskId - ID of the application task when it registered with the OSAL
 *              events - Events for this task
 *
 * @return      16bit - Unprocessed events
 *
 **************************************************************************************************/
uint16 MSA_ProcessEvent(uint8 taskId, uint16 events)
{
  uint8* pMsg;
  macCbackEvent_t* pData;
 


#ifdef MAC_SECURITY
  uint16       panID;
  uint16       panCoordShort;
  sAddrExt_t   panCoordExtAddr;
#endif /* MAC_SECURITY */

  if (events & SYS_EVENT_MSG)
  {
    while ((pMsg = osal_msg_receive(MSA_TaskId)) != NULL)
    {
      switch ( *pMsg )
      {
        case MAC_MLME_ASSOCIATE_IND:
      
          /* Matteo 18 Ottobre
          if (enableAssociation){
            
            enableAssociation=FALSE;
         
          MSA_AssociateRsp((macCbackEvent_t*)pMsg);}
          
          else {
            
            if(timePermitAssociation(0)) 
                MSA_AssociateRsp((macCbackEvent_t*)pMsg);
                
      }*/
                    
          if (enableAssociation){
            
            enableAssociation=FALSE;
         
            if (timePermitAssociation())
            MSA_AssociateRsp((macCbackEvent_t*)pMsg);
          }
          
          break;

        case MAC_MLME_ASSOCIATE_CNF:
          /* Retrieve the message */
          pData = (macCbackEvent_t *) pMsg;

          if ((!msa_IsStarted) && (pData->associateCnf.hdr.status == MAC_SUCCESS))
          {
            
            msa_IsStarted = TRUE;
             /* Retrieve MAC_SHORT_ADDRESS */

            msa_DevShortAddr = pData->associateCnf.assocShortAddress;
            
            /* Setup MAC_SHORT_ADDRESS - obtained from Association */
            MAC_MlmeSetReq(MAC_SHORT_ADDRESS, &msa_DevShortAddr);

        #ifdef MAC_SECURITY
            /* Add the coordinator to device table and key device table for security */
            MAC_MlmeGetReq(MAC_PAN_ID, &panID);
            MAC_MlmeGetSecurityReq(MAC_PAN_COORD_SHORT_ADDRESS, &panCoordShort);
            MAC_MlmeGetSecurityReq(MAC_PAN_COORD_EXTENDED_ADDRESS, &panCoordExtAddr);
            MSA_SecuredDeviceTableUpdate(panID, panCoordShort,
                                         panCoordExtAddr, &msa_NumOfSecuredDevices);
       #endif /* MAC_SECURITY */

            macMlmeStartReq_t   startReq;
            startReq.startTime = 0;
            startReq.panId = msa_PanId;
            startReq.logicalChannel = MSA_MAC_CHANNEL;
            startReq.beaconOrder = msa_BeaconOrder;
            startReq.superframeOrder = msa_SuperFrameOrder;
            startReq.panCoordinator = TRUE;
            startReq.batteryLifeExt = FALSE;
            startReq.coordRealignment = FALSE;
            startReq.realignSec.securityLevel = FALSE;
            startReq.beaconSec.securityLevel = FALSE;
          
            /* Call start request to start the device as a coordinator */
            MAC_MlmeStartReq(&startReq);
            
          }
          
          else MSA_ScanReq(MAC_SCAN_ACTIVE, 3);
          break;

        case MAC_MLME_COMM_STATUS_IND:
          
          osal_start_timerEx(MSA_TaskId, MSA_READ_EVENT, 1);
          
          break;

        case MAC_MLME_BEACON_NOTIFY_IND:
          /* Retrieve the message */
          pData = (macCbackEvent_t *) pMsg;
          
          uint8 LQI;
          uint16 readPermit;
          uint16 BeaconPanID;
          
          LQI=pData->beaconNotifyInd.pPanDesc->linkQuality;
          readPermit=pData->beaconNotifyInd.pPanDesc->superframeSpec;
          BeaconPanID=pData->beaconNotifyInd.pPanDesc->coordPanId;
          
          /* Check for correct beacon payload */
          if ( (LQI>20) && ((readPermit & 0x8000)==0x8000) && (BeaconPanID==msa_PanId) && (!msa_IsStarted))
          {
           
            countPck++;
            /*Effettuo un controllo sul pacchetto di beacon ricevuto che deve contenere un payload definito*/
            msa_IsSampleBeacon = MSA_BeaconPayLoadCheck(pData->beaconNotifyInd.pSdu);

            /* If it's the correct beacon payload, retrieve the data for association req */
            if (msa_IsSampleBeacon)
            {
              msa_AssociateReq.logicalChannel = MSA_MAC_CHANNEL;
              msa_AssociateReq.coordAddress.addrMode = SADDR_MODE_SHORT;
              msa_AssociateReq.coordAddress.addr.shortAddr = pData->beaconNotifyInd.pPanDesc->coordAddress.addr.shortAddr;
              
              msa_CoordShortAddr=pData->beaconNotifyInd.pPanDesc->coordAddress.addr.shortAddr;
              fatherAdd=msa_CoordShortAddr;
              
              msa_AssociateReq.coordPanId = pData->beaconNotifyInd.pPanDesc->coordPanId;
              if (msa_IsDirectMsg)
                msa_AssociateReq.capabilityInformation = MAC_CAPABLE_ALLOC_ADDR | MAC_CAPABLE_RX_ON_IDLE;
              else
                msa_AssociateReq.capabilityInformation = MAC_CAPABLE_ALLOC_ADDR;
              msa_AssociateReq.sec.securityLevel = MAC_SEC_LEVEL_NONE;

              /* Retrieve beacon order and superframe order from the beacon */
              msa_BeaconOrder = MAC_SFS_BEACON_ORDER(pData->beaconNotifyInd.pPanDesc->superframeSpec);
              msa_SuperFrameOrder = MAC_SFS_SUPERFRAME_ORDER(pData->beaconNotifyInd.pPanDesc->superframeSpec);
              
              /*va inserito l'else che prevede di salvare in memoria un nodo che ha un buon LQI ma non � il migliore
              ovvero un nodo che non diventa padre in quanto non ha il miglior LQI ma pu� essere utilizzato per procedure di fault tolerance*/
            }
          }

          break;

        case MAC_MLME_START_CNF:
          /* Retrieve the message */
          pData = (macCbackEvent_t *) pMsg;
          /* Set some indicator for the Coordinator */
          if (pData->startCnf.hdr.status == MAC_SUCCESS)
          {
             ENABLE_SENSTH(); //attivo alimentazione dei sensori
           
             #if defined WINETHP
              ENABLE_STEP_UP();
              // metto input P0_2 per leggere il "conversion ready state"
              P0DIR &= ~0x04;
            #endif
             
            osal_start_timerEx(MSA_TaskId, MSA_SLEEP_EVENT, 1000);

          }
          break;

        case MAC_MLME_SCAN_CNF:
          /* Check if there is any Coordinator out there */
          pData = (macCbackEvent_t *) pMsg;

          /* If there is no other on the channel or no other with sampleBeacon start as coordinator */
          if (countPck==0)
          {
             MSA_ScanReq(MAC_SCAN_ACTIVE, 3);
          }
//          else { if ((msa_IsSampleBeacon) && (pData->scanCnf.resultListSize != 0))
  //                {
          else{         /* Start the devive up as beacon enabled or not */
                    MSA_DeviceStartup();

                  /* Call Associate Req */
                  MSA_AssociateReq();
                  
                  countPck=0;
                  }
               /* else {
                countPck=0;
                MSA_ScanReq(MAC_SCAN_ACTIVE, 3);
          
                }
          }*/
          break;

        case MAC_MCPS_DATA_CNF:
          pData = (macCbackEvent_t *) pMsg;
          
          if(orphan) setMessageTipology(0);

          /* If last transmission completed, ready to send the next one */
          if /*(*/(pData->dataCnf.hdr.status == MAC_SUCCESS)/* ||
              (pData->dataCnf.hdr.status == MAC_CHANNEL_ACCESS_FAILURE) ||
              (pData->dataCnf.hdr.status == MAC_NO_ACK))*/
          {
            
            //variabile per consentire la trasmissione in uno istante "galleggiante" della finestra di SEND_STATE 
                time=TRUE;    
            
            switch(msa_State)
            {
            case MSA_SETUP_STATE: //il pacchetto con il nuovo tempo di risveglio non prevede ACK
            //     osal_start_timerEx(MSA_TaskId, MSA_SETUP_EVENT, 100);
              break;
              
            case MSA_SEND_STATE:
              if(txToCoord==TRUE)
              {
             
              /*Ho ricevuto l'ACK di un pacchetto dati*/
                if(orphan) orphan=FALSE; //se ricevo l'ACK non sono pi� orfano
             
                stato=0; //se ricevo l'ack nella macchina a stati per la fault tolerance riparto dallo stato 0

                // se ho ricevuto l'ACK aggiorno la variabile byteEpromTx che mi servir� per cancellare dalla memoria i dati inviati              
                byteEpromTx=byteEpromTx+lenOfSon-lenOfMyPack;
                
                //faccio partire timer da 500ms che mi porter� ad inviare nuovi dati o ad andare in Sleep
                osal_start_timerEx(MSA_TaskId, MSA_SEND_EVENT, 300);
              }
              
                else 
              {
                   //chiamo la funzione calcOsalTimerPeriod() che mi calcola quanto tempo ho ha disposizione prima di inviare i miei dati al padre
                osalTimerValue=calcOsalTimerPeriod();

                //se sono nello stato 2 dopo aver inviato data e orario mi prepara ad andare in MSA_BROAD_EVENT per inviare il pacchetto di broadcast per trovare un nuovo padre                
                if (stato==2) osal_start_timerEx(MSA_TaskId, MSA_BROAD_EVENT, osalTimerValue);
                //in caso mi trovi negli altri stati vado in MSA_SEND_EVENT per inviare i dati salvati su eprom al nodo padre
                else osal_start_timerEx(MSA_TaskId, MSA_SEND_EVENT, osalTimerValue); 
                
              
              }
              
              break;
              case MSA_START_STATE: 
              
                 osal_start_timerEx(MSA_TaskId, MSA_SEND_EVENT, 300);
    
              break;
              case MSA_ASSOC_STATE: break;
              
            }
            
          }
          // non ho ricevuto l'ACK atteso
          else {
             
                switch(msa_State)
                {
                case MSA_SETUP_STATE:  //il pacchetto con il nuovo tempo di risveglio non prevede ACK
                  // nessun ACK al pacchetto di settaggio tempo di risveglio andando in MSA_SETUP_EVENT provo ad inviarlo a qualche altro figlio da cui ho ricevuto i dati nella presente finestra temporale
                 // osal_start_timerEx(MSA_TaskId, MSA_SETUP_EVENT, 100);
                  break;
                  
                case MSA_SEND_STATE:
                  
                   if(txToCoord==TRUE)
                   {// nessun ACK al pacchetto dati inviato
                      //spengo la radio per evitare che la non ricezione dell'ACK sia stato una congestione sul canale e quindi mio padre mi invii comunque il pacchetto con data e ora
                      setMessageTipology(0); //matteo 9 Ottobre
  
                      // se nel pacchetto sul quale non ho ricevuto l'ACK c'erano dati letti in fase di risveglio li vado a salvare in eprom dato che mio padre non li avr� ricevuti
                      if (lenOfMyPack!=0)
                      {
                       dataLen=lenOfMyPack;
              
                        //se non ho ricevuto l'ACK significa che potrei non avere pi� un padre e quindi metto 0000 nel campo father all'interno del pacchetto che salvo in memoria
                        msa_Data1[4]=0x00;
                        msa_Data1[5]=0x00; 
                        
                        rssi=0;
                        //chiamo la funzione per il salvataggio dei dati in EEprom            
                         saveData();
                         
                         /*chiamo la funzione di fault tolerance per valutare quale nuovo nodo padre posso trovare all'interno della rete
                         la chiamo dentro questa selezione perch� deve andare in fault tolerance solo se non riceve l'ack del primo pacchetto inviato*/
                          faultTolerance();
                  
                      }
                  
                    //chiamo la funzione di fault tolerance per valutare quale nuovo nodo padre posso trovare all'interno della rete
                    //faultTolerance();
                  
                    // non avendo ricevuto l'ACK vado in sleep
                    osal_start_timerEx(MSA_TaskId, MSA_SLEEP_EVENT, 100);
                  
                    }
                
                    else 
                    {//caso in cui non ricevo l'ACK al pacchetto con data e ora
                     //calcolo il tempo rimanente nella finestra SEND_STATE dopodich� dovr� inviare i miei dati al padre
                      osalTimerValue=calcOsalTimerPeriod();
                 
                            //se sono nello stato 2 dopo aver inviato data e orario mi prepara ad andare in MSA_BROAD_EVENT per inviare il pacchetto di broadcast per trovare un nuovo padre                
                      if (stato==2) osal_start_timerEx(MSA_TaskId, MSA_BROAD_EVENT, osalTimerValue);
                      //in caso mi trovi negli altri stati vado in MSA_SEND_EVENT per inviare i dati salvati su eprom al nodo padre
                      else osal_start_timerEx(MSA_TaskId, MSA_SEND_EVENT, osalTimerValue); 
                
                      
                    }   
                   
                    break;
                   
              case MSA_START_STATE: 
                
                        dataLen=lenOfMyPack;
              
                        //se non ho ricevuto l'ACK significa che potrei non avere pi� un padre e quindi metto 0000 nel campo father all'interno del pacchetto che salvo in memoria
                        msa_Data1[4]=0x00;
                        msa_Data1[5]=0x00; 
                        
                        rssi=0;
                        //chiamo la funzione per il salvataggio dei dati in EEprom            
                         saveData();
                  
                      
                  
                    //chiamo la funzione di fault tolerance per valutare quale nuovo nodo padre posso trovare all'interno della rete
                    faultTolerance();
                  
                    // non avendo ricevuto l'ACK vado in sleep
                    osal_start_timerEx(MSA_TaskId, MSA_SLEEP_EVENT, 100);
                
                break;
              case MSA_ASSOC_STATE: break;
                  
                }
                
                
             }

          //osal_msg_deallocate((uint8 *) pData->dataCnf.pDataReq);
          mac_msg_deallocate((uint8**)&pData->dataCnf.pDataReq);
          break;

        case MAC_MCPS_DATA_IND:
          pData = (macCbackEvent_t*)pMsg;
          
          //alla ricezione di un pacchetto spengo la radio per evitare che arrivino altri pacchetti e interrompano i vari settaggi
          setMessageTipology(0);
          
          /*ricezione del pacchetto con i nuovi tempi di sleep */
           if (msa_State==MSA_SETUP_STATE)
           {
               if( sinch==FALSE)
               {
                  osal_stop_timerEx(MSA_TaskId, MSA_SLEEP_EVENT);
                  sinch=TRUE;
                    //se sono nello stato di SETUP il pacchetto ricevuto conterr� il nuovo intervallo di sleep che salvo nella variabile minToSleep
                  minToSleep=pData->dataInd.msdu.p[0];
                  // di seguito chiamo l'evento SETUP per inoltrare tale tempo ai miei figli 
                  osal_start_timerEx(MSA_TaskId, MSA_SETUP_EVENT,100);
               }
             }
          else 
          {
           if ((pData->dataInd.msdu.p[0]==0xFF) && (pData->dataInd.msdu.p[1]==0xFF))
           {
            if (stato==2) {
            stato=0;
            orphan=FALSE; //Matteo 30 Ottobre
            fatherAdd=pData->dataInd.mac.srcAddr.addr.shortAddr;
            }
          
            }  
             else 
             {
            
              if (pData->dataInd.mac.srcAddr.addr.shortAddr==fatherAdd) 
              {
                //pacchetto ricevuto dal nodo padre contenente data e ora
                
                // faccio lo stop dei due timer perch� ho ricevuto un pacchetto dal nodo figlio e devo dargli la possibilit� di inviarmi tutti i dati che possiede 
                osal_stop_timerEx(MSA_TaskId, MSA_SEND_EVENT);
                osal_stop_timerEx(MSA_TaskId, MSA_SLEEP_EVENT); //Matteo 18 Ottobre per la fase di associazione
           
                time=TRUE;
                //le dal pacchetto la data e l'ora e le salvo su un buffer che utilizzer� per settare l'astronomico
                for(uint8 i=0; i<8; i++)
                {
                  timeBuffer[i]=pData->dataInd.msdu.p[i];
                }
              
                //nel caso in cui riceva dal padre un orario sballato FF non effettuo alcun settaggio
                if (timeBuffer[0]!=0xFF)
                {
              
                  astroConv(timeBuffer);
                
                  //incremento di un'unit� i secondi per recuperare il tempo che attendo con l'istruzione halWaitms sottostante
                  astroBuffer[1]++;
          
                  //chiamo la funzione che mi calcola la data dovuta all'aumento di un'unit� dei secondi
                  defineDate();
          
                  //introduco un ritardo per allinearmi con i centesimi del nodo padre
                  halWaitms((100-newDecCent)*10);
           
                  /*Una volta ricevuto il pacchetto di avvio rete setto l'astronomico*/
                  ENABLE_PERIPH(); // attivo 
        
                  setAstronomic(START,newSecondi,newMinuti,newOre,
                        newdayWeek,newGiorno,newMese,newAnno);
                }
                /*setto la variabile minWakeUp che indica a quali secondi si deve svegliare il nodo*/
                minWakeUp=HextoBCD(pData->dataInd.msdu.p[8]);

                if (minWakeUp==0x00) minWakeUp=60; 
                minWakeUp=minWakeUp-2; //il nodo figlio si sveglier� 2 secondi in anticipo rispetto al padre
        
                // se mio padre � il coordinatore prelevo dal pacchetto anche il tempo di sleep che a sua volta lui aveva scaricato dal sito web
                if (fatherAdd==MSA_COORD_SHORT_ADDR) 
                {
                  minToSleep=pData->dataInd.msdu.p[9];
                  sinch=TRUE;
                }
                
                //caso in cui mi trovi nella fase di accensione del nodo
                if(minWakeUpOld==0xFF)
                {
                  minWakeUpOld=minWakeUp;
                }
                // se sono nella fase attiva e mio padre mi ha comunicato che il tempo di risveglio � cambiato (risalita di un livello) setto la variabile diffMinWakeUp per indicare che al prossimo risveglio devo attendere i dati dai miei figli nello stesso sloto temporale ma devo inviarli al nodo padre dopo questo ritardo
                else 
                {
                  diffMinWakeUp=minWakeUp-minWakeUpOld;
                }

                
                /* Dopo aver ricevuto l'orario vado in SEND_EVENT per trasmettere nuovi pacchetti oppure andare in sleep
                imposto 100ms nell'osal_start_timer per consentire il dealloc */
                
                osal_start_timerEx(MSA_TaskId, MSA_SEND_EVENT,100);  
          
            
              }
              //ricevo i dati da mio figlio o da un nodo che sta cercando il padre
              else 
              {
                if (!txToCoord)
                {
                  /*Matteo 10 Dicembre if/else che si trovava dentro l'else relativo al nodo figlio*/
                  //se mi trovo nello stato 2 devo bloccare l'invio del pacchetto di broadcast, salvare i dati ricevuti e poi ripristinare tale timer
                    if (stato==2) osal_stop_timerEx(MSA_TaskId, MSA_BROAD_EVENT);
                    //situazione di normale ricezione di pacchetto
                    else osal_stop_timerEx(MSA_TaskId, MSA_SEND_EVENT);
            
                   //se ricevo FFFF significa che il nodo che mi ha trasmesso ha perso il padre
                  if (pData->dataInd.mac.dstAddr.addr.shortAddr==0XFFFF)
                  {
                    //se sta cercando un nuovo padre con un livello e un tempo di risveglio uguali ai miei salvo l'indirizzo del nodo e gli rispondo dicendogli che dalla prossima finestra temporale pu� inviare a me i dati da far giungere al coordinatore
                    if ((pData->dataInd.msdu.p[0]==HI_UINT16(msa_DevShortAddr)) && (pData->dataInd.msdu.p[1]==minWakeUpOld))
                    {
                      broadShortAddress=pData->dataInd.mac.srcAddr.addr.shortAddr;
                      osal_start_timerEx(MSA_TaskId, MSA_RESP_BROAD_EVENT,100);
                    }
                    /*Se il pacchetto di broadcast non era per me riaccendo la radio */
                    else 
                    {
                      //se il nodo da cui ho ricevuto il pacchetto dati � presente nell'array msa_DeviceRecord non invio data ora (perch� a lui un pacchetto � gi� stato inviato) e faccio ripartire timer per invio dati in memoria verso il nodo padre
                        osalTimerValue=calcOsalTimerPeriod();
                      //il timer da far ripartire � riferito al BROAD_EVENT se devo cercare un nodo padre altrimenti invio al pdre consolidato
                        if (stato==2) osal_start_timerEx(MSA_TaskId, MSA_BROAD_EVENT, osalTimerValue);
                        else osal_start_timerEx(MSA_TaskId, MSA_SEND_EVENT, osalTimerValue); 
                        
                      setMessageTipology(1);
                    }
                  }
          
                  else 
                  {
                  // pacchetto ricevuto dal nodo figlio
                  
                    //salvo la lunghezza del pacchetto dati per poter definire lo spazio di memoria richiesto
                    dataLen=pData->dataInd.msdu.len;
  
                    // salvo i dati da scrivere in eeprom
                    for(uint8 j=0;j<dataLen;j++)
                     msa_Data1[j]=pData->dataInd.msdu.p[j];
  
                    //salva il valore di rssi
                    rssi=pData->dataInd.mac.rssi;
  
                  //se il pacchetto contiene come primi dati quelli del nodo figlio da cui ho ricevuto il pacchetto calcolo i sensori che ha collegati in modo fa poter definire con precisione il byte in cui salvare l'RSSI rilevato
                    if (BUILD_UINT16(msa_Data1[1],msa_Data1[0])==pData->dataInd.mac.srcAddr.addr.shortAddr) 
                    {
                      numSensori = countSens(BUILD_UINT16(msa_Data1[12],msa_Data1[13]));
                      msa_Data1[16+(numSensori*2)]=rssi;
                    }
              
                    saveData(); // salvo i dati in EEPROM
               
                    if (msa_State==MSA_SEND_STATE)
                    {//dato che l'orario verso uno stesso nodo voglio inviarlo una sola volta utilizzo la variabile Present per effettuare la valutazione
                      present=FALSE;
                
                      for(int i=0; i<MSA_MAX_DEVICE_NUM; i++)
                      {
                        //salvo all'interno del buffer msa_DeviceRecord gli indirizzi dei nodi da cui ricevo i dati in modo da poterli leggere e confrontarli ogni volta che ricevo un pacchetto da un nodo figlio, tale array viene pulito ad ogni risveglio    
                        if(pData->dataInd.mac.srcAddr.addr.shortAddr==msa_DeviceRecord[i])
                        { 
                          //se trovo l'indirizzo del nodo figlio all'interno dell'array significa che gli ho gi� inviato il pacchetto con ora e data
                          present=TRUE;
                          //trovato l'indirizzo del nodo figlio posso uscire dal ciclo
                          i=MSA_MAX_DEVICE_NUM;
                        
                        }
                      }
                
                      if(!present)
                      {
                      //nodo non presente nell'array msa_DeviceRecord, salvo tale indirizzo e invio pacchetto con ora e data     
                        msa_DeviceRecord[msa_NumOfDevices]=pData->dataInd.mac.srcAddr.addr.shortAddr;
                  
                        //indice per gestire il salvataggio degli indirizzi nell'array msa_DeviceRecord
                        msa_NumOfDevices++;
                  
                        //il vettore ha dimensione massima MSA_MAX_DEVICE_NUM, faccio un controllo sull'indice
                        if(msa_NumOfDevices>=MSA_MAX_DEVICE_NUM) msa_NumOfDevices=0; 
                        assocShortAddress=pData->dataInd.mac.srcAddr.addr.shortAddr;
  
                      // Mettiamo 100ms per consentire il dealloc, invio del pacchetto con ora e data 
                        osal_start_timerEx(MSA_TaskId, MSA_READ_EVENT,100); 
             
                      }
                
                      else  
                      {
                      //se il nodo da cui ho ricevuto il pacchetto dati � presente nell'array msa_DeviceRecord non invio data ora (perch� a lui un pacchetto � gi� stato inviato) e faccio ripartire timer per invio dati in memoria verso il nodo padre
                        osalTimerValue=calcOsalTimerPeriod();
                      //il timer da far ripartire � riferito al BROAD_EVENT se devo cercare un nodo padre altrimenti invio al pdre consolidato
                        if (stato==2) osal_start_timerEx(MSA_TaskId, MSA_BROAD_EVENT, osalTimerValue);
                        else osal_start_timerEx(MSA_TaskId, MSA_SEND_EVENT, osalTimerValue); 
                        
                        //dopo aver correttamente processato il pacchetto dati del nodo figlio riaccendo la radio per consentire eventuali nuove ricezioni dati
                       setMessageTipology(1);
                      }
                  
                    }
                    else
                    {
                      enableAssociation=TRUE;
                      
                      //dopo aver correttamente processato il pacchetto del nodo figlio riaccendo la radio per consentire eventuali nuove associazioni
                       setMessageTipology(1);
                    }
                      // Matteo 18 Ottobre if(timePermitAssociation(1)) enableAssociation=TRUE;
              
                  }
                }
              }
             }
          }
      }
      
      
      /* Deallocate */
      //osal_msg_deallocate((uint8 *) pMsg);
      mac_msg_deallocate((uint8 **)&pMsg);

    }

  return events ^ SYS_EVENT_MSG;
  }

  if (events & MSA_SLEEP_EVENT)
  { 
    uint8 tempMin=0;
    
    //entro in questo stato in cui ogni nodo termina la finestra di associazione
    if(msa_State==MSA_ASSOC_STATE)
    {
      
      ENABLE_SENSTH(); //attivo alimentazione dei sensori
      
      #if defined WINETHP
              ENABLE_STEP_UP();
              // metto input P0_2 per leggere il "conversion ready state"
              P0DIR &= ~0x04;
      #endif
              
      //non acconsento pi� alle associazioni perch� dovr� iniziare il la fase di trasmissione/ricezione dei dati
      MAC_MlmeSetReq(MAC_ASSOCIATION_PERMIT, &msa_MACFalse);
      //setto lo stato di trasmissione/ricezione
      msa_State=MSA_SEND_STATE;
      //calcolo la finestra per la ricezione dei dati dai nodi figlio considerando un periodo fisso (MSA_WAIT_PERIOD), un termine variabile (windowStar) per consentire ai nodi figli di ricevere il pacchetto con ora e data nel caso di multiple associzioni allo stesso nodo padre e un termina diffMinWakeUp che serve nel caso in cui il nodo padre sia salito di livello
      winRx= MSA_WAIT_PERIOD+windowStar+(diffMinWakeUp*1000);
      
      //reset della variabile
      diffMinWakeUp=0;
         
      //leggo l'astronomico per poter valutare l'intervallo di tempo in cui il nodo � rimasto nello stato di SEND_STATE
      readAstronomic(matchBuffer,0x0000,8);

      //se sono nello stato 2 (ricerca di padre) andr� nell'evento per l'invio del pacchetto broadcast altrimenti nell'evento di trasmissione pacchetto dati       
      if (stato==2) osal_start_timerEx(MSA_TaskId, MSA_BROAD_EVENT, winRx);
      else osal_start_timerEx(MSA_TaskId, MSA_SEND_EVENT, winRx);
    

    }
    
     else{  
     //spengo la radio per evitare che pacchetti inviati in ritardo possano compromettere le azioni di sleep
       setMessageTipology(0);
           
       switch(msa_State){
         
       case MSA_SEND_STATE: 
         updateEpromAddres(byteEpromTx);
   
         // converto e salvo i secondi del risveglio
         astroConv(activeBuffer);
         tempMin=astroBuffer[2];
         
         //leggo astronomico
         readAstronomic(timeBuffer,0x0000,0x08); // leggo tutti i dati

         astroConv(timeBuffer); // converto in BCD risultati in astroBuffer ( in 0 ci sono i decimi )
 
         // margine perch� in caso il nodo invii molti pacchetti pu� scadere il timer del broadcast event
         if ( (tempMin==astroBuffer[2]) || ( astroBuffer[1]+2 < SEC_TO_SYNCH) )
         {
           msa_State=MSA_SETUP_STATE; 
           incrementAstro(SLEEP_TIME); 
         }
         else 
         {
         if(minToSleep==0xFF || minToSleep==0x00) incrementAstro(SLEEP_TIME);
           else incrementAstro(minToSleep); 
         }
       break;
      
       case MSA_SETUP_STATE:
         msa_State=MSA_SEND_STATE;
         if(minToSleep==0xFF || minToSleep==0x00) incrementAstro(SLEEP_TIME);
         else incrementAstro(minToSleep);
         break;
         
         case MSA_START_STATE:
         if(minToSleep==0xFF || minToSleep==0x00) incrementAstro(SLEEP_TIME);
         else incrementAstro(minToSleep);
         // modifica EMA era incrementAstro(SLEEP_TIME);
         break;
          }
         
    
    SleepAstro();
    }
    return events ^ MSA_SLEEP_EVENT;
  }

   
  if (events & MSA_READ_EVENT)
  {

       readAstronomic(readBuffer,0x0000,8);

      msa_Data2[0] = readBuffer[0];
      msa_Data2[1] = readBuffer[1];
      msa_Data2[2] = readBuffer[2];
      msa_Data2[3] = readBuffer[3];
      msa_Data2[4] = readBuffer[4];
      msa_Data2[5] = readBuffer[5];
      msa_Data2[6] = readBuffer[6];
      msa_Data2[7] = readBuffer[7];
      msa_Data2[8] = BcdToHex(minWakeUpOld);
   
      //dopo aver ricevuto il pacchetto con i dati dal nodo figlio riaccendo la radio per inviare data e ora e per poter ricevere nuovi dati
       setMessageTipology(1);
      
    MSA_McpsDataReq((uint8*)msa_Data2,
                        0x09,
                        TRUE,
                        assocShortAddress,
                        TRUE);

    return events ^ MSA_READ_EVENT;
  }

  
  /* Evento di trasmissione dati verso nodo padre*/
  if (events & MSA_SEND_EVENT)
  {
      txToCoord=TRUE;
      
      if(lenOfSon==0) {lenOfMyPack=preparePacket();}
      else lenOfMyPack=0;  
      
      /* se il pacchetto del nodo padre � maggiore di 83 non chiamo la funzione readData in quanto non riuscirei a creare un pacchetto cos� grande
      occorre introdurre un controllo nel caso in cui il nodo padre crei un pacchetto maggiore di 104 byte*/
      if(lenOfMyPack<84)
      lenOfSon=readData(lenOfMyPack);
      else  lenOfSon=lenOfMyPack;
      
       if (lenOfSon>0){ 
         
       //riaccendo la radio perch� potrei essere orphan oppure pottei aver ricevuto il dati dai miei figli
       setMessageTipology(1);
         
        MSA_McpsDataReq((uint8*)msa_Data1,
                        lenOfSon,
                        TRUE,
                        fatherAdd,
                        TRUE);
       }
       
      else osal_start_timerEx(MSA_TaskId, MSA_SLEEP_EVENT,1);
    
    return events ^ MSA_SEND_EVENT;
  }

  if (events & MSA_SETUP_EVENT)
  {
   /* Matteo 25 Ottobre invece che l'invio deio pacchetti nella gerarchia padre-figlio faccio tanti broadcast in modo
    tale che possano riceverlo pi� nodi della rete
    if(msa_NumOfDevices > 0){
          
          msa_NumOfDevices--;
      
          MSA_McpsDataReq((uint8*) &minToSleep,
                        0x01,
                        TRUE,
                        msa_DeviceRecord[msa_NumOfDevices],
                        TRUE);
         
        }
     else  osal_start_timerEx(MSA_TaskId, MSA_SLEEP_EVENT, 1);*/
    
    //dopo l'invio vado in sleep
    osal_start_timerEx(MSA_TaskId, MSA_SLEEP_EVENT, 300);
    
    //dopo aver ricevuto il pacchetto broadcast con il nuovo tempo di rilevazione accendo la radio e inoltro in broadcast tale tempo
     setMessageTipology(1);
                
    MSA_McpsDataReq((uint8*) &minToSleep,
                        0x01,
                        TRUE,
                        0xFFFF,
                        FALSE);
    
  return events ^ MSA_SETUP_EVENT;
  }
  
  if (events & MSA_BROAD_EVENT)
  {
    txToCoord=TRUE;
    
    dataLen=preparePacket();
    
    rssi=0;
         
    saveData();
    
    osal_start_timerEx(MSA_TaskId, MSA_FAULT_TOL_EVENT, 1000);
    
    //riaccendo la radio perch� potrei trovarmi nello stato di orphan oppure perch� ho ricevuto i dati dai miei figlio ma non padre
    setMessageTipology(1);
    
    msa_Data1[0]=HI_UINT16(fatherAdd);
    msa_Data1[1]=minWakeUpOld+2;
    
    MSA_McpsDataReq( (uint8*)msa_Data1,
                        0x02,
                        TRUE,
                        0xFFFF,
                        FALSE);
    
    
    return events ^ MSA_BROAD_EVENT;
  }

  if (events & MSA_RESP_BROAD_EVENT)
  {
    msa_Data1[0]=0xFF;
    msa_Data1[1]=0xFF;
    
    //dopo aver ricevuto il pacchetto con la richiesta di un padre se rientro nei requisiti riaccendo la radio e invio la risposta
     setMessageTipology(1);
     
     //se il nodo da cui ho ricevuto il pacchetto dati � presente nell'array msa_DeviceRecord non invio data ora (perch� a lui un pacchetto � gi� stato inviato) e faccio ripartire timer per invio dati in memoria verso il nodo padre
      osalTimerValue=calcOsalTimerPeriod();
      //il timer da far ripartire � riferito al BROAD_EVENT se devo cercare un nodo padre altrimenti invio al pdre consolidato
      if (stato==2) osal_start_timerEx(MSA_TaskId, MSA_BROAD_EVENT, osalTimerValue);
      else osal_start_timerEx(MSA_TaskId, MSA_SEND_EVENT, osalTimerValue); 
   
              MSA_McpsDataReq( (uint8*)msa_Data1,
                                  0x02,
                                  TRUE,
                                  broadShortAddress,
                                  FALSE);
    
              return events ^ MSA_RESP_BROAD_EVENT;
  }
  
   if (events & MSA_FAULT_TOL_EVENT)
  {
    
    if (stato==0) osal_start_timerEx(MSA_TaskId, MSA_SLEEP_EVENT, 100);
    else {
      
                 faultTolerance();
                  
                  osal_start_timerEx(MSA_TaskId, MSA_SLEEP_EVENT, 100);
                  
    }
    
                 return events ^ MSA_FAULT_TOL_EVENT;
  }
    return 0;
}

/**************************************************************************************************
 *
 * @fn          MAC_CbackEvent
 *
 * @brief       This callback function sends MAC events to the application.
 *              The application must implement this function.  A typical
 *              implementation of this function would allocate an OSAL message,
 *              copy the event parameters to the message, and send the message
 *              to the application's OSAL event handler.  This function may be
 *              executed from task or interrupt context and therefore must
 *              be reentrant.
 *
 * @param       pData - Pointer to parameters structure.
 *
 * @return      None.
 *
 **************************************************************************************************/
void MAC_CbackEvent(macCbackEvent_t *pData)
{

  macCbackEvent_t *pMsg = NULL;

  uint8 len = msa_cbackSizeTable[pData->hdr.event];

  switch (pData->hdr.event)
  {
      case MAC_MLME_BEACON_NOTIFY_IND:

      len += sizeof(macPanDesc_t) + pData->beaconNotifyInd.sduLength +
             MAC_PEND_FIELDS_LEN(pData->beaconNotifyInd.pendAddrSpec);
      if ((pMsg = (macCbackEvent_t *) osal_msg_allocate(len)) != NULL)
      {
        /* Copy data over and pass them up */
        osal_memcpy(pMsg, pData, sizeof(macMlmeBeaconNotifyInd_t));
        pMsg->beaconNotifyInd.pPanDesc = (macPanDesc_t *) ((uint8 *) pMsg + sizeof(macMlmeBeaconNotifyInd_t));
        osal_memcpy(pMsg->beaconNotifyInd.pPanDesc, pData->beaconNotifyInd.pPanDesc, sizeof(macPanDesc_t));
        pMsg->beaconNotifyInd.pSdu = (uint8 *) (pMsg->beaconNotifyInd.pPanDesc + 1);
        osal_memcpy(pMsg->beaconNotifyInd.pSdu, pData->beaconNotifyInd.pSdu, pData->beaconNotifyInd.sduLength);
      }
      break;

    case MAC_MCPS_DATA_IND:
      pMsg = pData;
      break;

    default:
      if ((pMsg = (macCbackEvent_t *) osal_msg_allocate(len)) != NULL)
      {
        osal_memcpy(pMsg, pData, len);
      }
      break;
  }

  if (pMsg != NULL)
  {
    osal_msg_send(MSA_TaskId, (uint8 *) pMsg);
  }
}

/**************************************************************************************************
 *
 * @fn      MAC_CbackCheckPending
 *
 * @brief   Returns the number of indirect messages pending in the application
 *
 * @param   None
 *
 * @return  Number of indirect messages in the application
 *
 **************************************************************************************************/
uint8 MAC_CbackCheckPending(void)
{
  return (0);
}

/**************************************************************************************************
 *
 * @fn      MSA_DeviceStartup()
 *
 * @brief   Update the timer per tick
 *
 * @param   beaconEnable: TRUE/FALSE
 *
 * @return  None
 *
 **************************************************************************************************/
void MSA_DeviceStartup()
{
  /* Setup MAC_BEACON_PAYLOAD_LENGTH */
  MAC_MlmeSetReq(MAC_BEACON_PAYLOAD_LENGTH, &msa_BeaconPayloadLen);

  /* Setup MAC_BEACON_PAYLOAD */
  MAC_MlmeSetReq(MAC_BEACON_PAYLOAD, &msa_BeaconPayload);

  /* Setup PAN ID */
  MAC_MlmeSetReq(MAC_PAN_ID, &msa_PanId);

  /* This device is setup for Direct Message */
  if (msa_IsDirectMsg)
    MAC_MlmeSetReq(MAC_RX_ON_WHEN_IDLE, &msa_MACTrue);
  else
    MAC_MlmeSetReq(MAC_RX_ON_WHEN_IDLE, &msa_MACFalse);

  /* Setup Coordinator short address */
  MAC_MlmeSetReq(MAC_COORD_SHORT_ADDRESS, &msa_AssociateReq.coordAddress.addr.shortAddr);

   uint8 maxRetries=0;
  MAC_MlmeSetReq(MAC_MAX_FRAME_RETRIES, &maxRetries);
  
#ifdef MAC_SECURITY
  /* Setup Coordinator short address for security */
  MAC_MlmeSetSecurityReq(MAC_PAN_COORD_SHORT_ADDRESS, &msa_AssociateReq.coordAddress.addr.shortAddr);
#endif /* MAC_SECURITY */

  if (msa_BeaconOrder != 15)
  {
    /* Setup Beacon Order */
    MAC_MlmeSetReq(MAC_BEACON_ORDER, &msa_BeaconOrder);

    /* Setup Super Frame Order */
    MAC_MlmeSetReq(MAC_SUPERFRAME_ORDER, &msa_SuperFrameOrder);

   }

  MAC_MlmeSetReq(MAC_ASSOCIATION_PERMIT, &msa_MACFalse);
  
  /* Power saving */
  MSA_PowerMgr (MSA_PWR_MGMT_ENABLED);

}

/**************************************************************************************************
 *
 * @fn      MSA_AssociateReq()
 *
 * @brief
 *
 * @param    None
 *
 * @return  None
 *
 **************************************************************************************************/
void MSA_AssociateReq(void)
{
  MAC_MlmeAssociateReq(&msa_AssociateReq);
}

/**************************************************************************************************
 *
 * @fn      MSA_AssociateRsp()
 *
 * @brief   This routine is called by Associate_Ind inorder to return the response to the device
 *
 * @param   pMsg - pointer to the structure recieved by MAC_MLME_ASSOCIATE_IND
 *
 * @return  None
 *
 **************************************************************************************************/
void MSA_AssociateRsp(macCbackEvent_t* pMsg)
{
  assocExtAdd[0]=pMsg->associateInd.deviceAddress[0];
  assocExtAdd[1]=pMsg->associateInd.deviceAddress[1];

  
  assocShortAddress = BUILD_UINT16(assocExtAdd[0], HI_UINT16(msa_DevShortAddr)+1);
  
  
#ifdef MAC_SECURITY
  uint16 panID;

  /* Add device to device table for security */
  MAC_MlmeGetReq(MAC_PAN_ID, &panID);
  MSA_SecuredDeviceTableUpdate(panID, assocShortAddress,
                               pMsg->associateInd.deviceAddress,
                               &msa_NumOfSecuredDevices);
#endif /* MAC_SECURITY */

   /* If the number of devices are more than MAX_DEVICE_NUM, turn off the association permit */
  //if (LO_UINT16(assocShortAddress) >= MSA_MAX_DEVICE_NUM){
   // MAC_MlmeSetReq(MAC_ASSOCIATION_PERMIT, &msa_MACFalse);
   // }

  //else{
  /* Fill in association respond message */
  sAddrExtCpy(msa_AssociateRsp.deviceAddress, pMsg->associateInd.deviceAddress);
  msa_AssociateRsp.assocShortAddress = assocShortAddress;
  msa_AssociateRsp.status = MAC_SUCCESS;
  msa_AssociateRsp.sec.securityLevel = MAC_SEC_LEVEL_NONE;

  /* Call Associate Response */
  MAC_MlmeAssociateRsp(&msa_AssociateRsp);
  //}
}

/**************************************************************************************************
 *
 * @fn      MSA_McpsDataReq()
 *
 * @brief   This routine calls the Data Request
 *
 * @param   data       - contains the data that would be sent
 *          dataLength - length of the data that will be sent
 *
 * @return  None
 *
 **************************************************************************************************/
void MSA_McpsDataReq(uint8* data, uint8 dataLength, bool directMsg, uint16 dstShortAddr, bool ack)
{
  macMcpsDataReq_t  *pData;
  static uint8      handle = 0;

  if ((pData = MAC_McpsDataAlloc(dataLength, msa_securityLevel, msa_keyIdMode)) != NULL)
  {
    pData->mac.srcAddrMode = SADDR_MODE_SHORT;
    pData->mac.dstAddr.addrMode = SADDR_MODE_SHORT;
    pData->mac.dstAddr.addr.shortAddr = dstShortAddr;
    pData->mac.dstPanId = msa_PanId;
    pData->mac.msduHandle = handle++;
    
    //matteo 19 Settembre 
// pData->mac.txOptions = /*MAC_TXOPTION_NO_CNF;*/ MAC_TXOPTION_ACK;    
    if (ack) pData->mac.txOptions = MAC_TXOPTION_ACK;
    else pData->mac.txOptions = MAC_TXOPTION_NO_CNF;

    /* MAC security parameters */
    osal_memcpy( pData->sec.keySource, msa_keySource, MAC_KEY_SOURCE_MAX_LEN );
    pData->sec.securityLevel = msa_securityLevel;
    pData->sec.keyIdMode = msa_keyIdMode;
    pData->sec.keyIndex = msa_keyIndex;

     /* Copy data */
    osal_memcpy (pData->msdu.p, data, dataLength);

    /* Send out data request */
    MAC_McpsDataReq(pData);
  }

}

/**************************************************************************************************
 *
 * @fn      MacSampelApp_ScanReq()
 *
 * @brief   Performs active scan on specified channel
 *
 * @param   None
 *
 * @return  None
 *
 **************************************************************************************************/
void MSA_ScanReq(uint8 scanType, uint8 scanDuration)
{
  macMlmeScanReq_t scanReq;

  /* Fill in information for scan request structure */
  scanReq.scanChannels = (uint32) 1 << MSA_MAC_CHANNEL;
  scanReq.scanType = scanType;
  scanReq.scanDuration = scanDuration;
  scanReq.maxResults = MSA_MAC_MAX_RESULTS;
  scanReq.permitJoining = MSA_EBR_PERMITJOINING;
  scanReq.linkQuality = MSA_EBR_LINKQUALITY;
  scanReq.percentFilter = MSA_EBR_PERCENTFILTER;
  scanReq.result.pPanDescriptor = msa_PanDesc;
  osal_memset(&scanReq.sec, 0, sizeof(macSec_t));

  /* Call scan request */
  MAC_MlmeScanReq(&scanReq);
}

/**************************************************************************************************
 *
 * @fn      MSA_BeaconPayLoadCheck()
 *
 * @brief   Check if the beacon comes from MSA but not zigbee
 *
 * @param   pSdu - pointer to the buffer that contains the data
 *
 * @return  TRUE or FALSE
 *
 **************************************************************************************************/
bool MSA_BeaconPayLoadCheck(uint8* pSdu)
{
  uint8 i = 0;
  for (i=0; i<msa_BeaconPayloadLen; i++)
  {
    if (pSdu[i] != msa_BeaconPayload[i])
    {
      return FALSE;
    }
  }

  return TRUE;
}

/**************************************************************************************************
 *
 * @fn      MSA_HandleKeys
 *
 * @brief   Callback service for keys
 *
 * @param   keys  - keys that were pressed
 *          state - shifted
 *
 * @return  void
 *
 **************************************************************************************************/
void MSA_HandleKeys(uint8 keys, uint8 shift)
{
  uint8 clearBuff[1];

#if defined WINETAQ
  if ( keys & HAL_KEY_SW_1 )
  {
    /* wait for 32Mhz stable clock */
    while ((CLKCONSTA & 0x40) != 0);
    {
     asm("NOP");
    }
    
    InputOutputWakeUp();

    ENABLE_PERIPH(); // attivo 

    restoreTime();  // ripristino i registri esterni per leggere ora e data
    readAstronomic(clearBuff,0x000F,0x01); // leggo il flag
    writeAstronomic(0x00,0x0A,1); // setto a zero AFE e ABE per sicurezza
    halWait(10);
    readAstronomic(timeStampBuffer,0x0000,8);
    
    if (msa_State==MSA_SETUP_STATE)
    {
      setMessageTipology(1);

     
      if (fatherAdd==MSA_COORD_SHORT_ADDR && sinch==TRUE) 
      {
        
       osal_start_timerEx(MSA_TaskId, MSA_SETUP_EVENT, 100);
      }
      else  
      {
        
        osal_start_timerEx(MSA_TaskId, MSA_SLEEP_EVENT, 1000);
      }
    }
    
    else
    {
      
      if(!orphan) setMessageTipology(1);

      readAstronomic(matchBuffer,0x0000,8);
      // Emanuele - 15/12/12 copio in activebuffer perch� poi matchbuffer viene manipolato
      osal_memcpy(activeBuffer,matchBuffer,8);
      
      //Teo test Fault Tolerance
   /*  indexChannel++;
      
      if(indexChannel==1) macRadioSetChannel(MAC_CHAN_11);
      
      if(indexChannel==10) macRadioSetChannel(MSA_MAC_CHANNEL);
   */
  
      //Teo prova 3 Dicembre
   // indexChannel=0;
   // macRadioSetChannel(MSA_MAC_CHANNEL);
      //fine prova
      
      for(int i=0; i<MSA_MAX_DEVICE_NUM; i++) msa_DeviceRecord[i] = 0xFFFF;
      msa_NumOfDevices=0;
   
      msa_State=MSA_ASSOC_STATE;
      lenOfSon=0;
      byteEpromTx=0;
      txToCoord=FALSE;
      assocShortAddress=0xFFFF;
      minWakeUpOld=minWakeUp;
      enableAssociation=TRUE;
      sinch=FALSE; //per abilitare la ricezione del pacchetto broadcast con i tempi di lettura trasmessi da pc
    
    
      if(time) time=FALSE;
      else 
      {
        if (windowStar>950) windowStar=0;
        else  windowStar=windowStar+230;
      }
     
      MAC_MlmeSetReq(MAC_ASSOCIATION_PERMIT, &msa_MACTrue); 
      osal_start_timerEx(MSA_TaskId, MSA_SLEEP_EVENT, MSA_ASSOCIATION_PERIOD);    
      }
   
  }
#endif

}

void SleepAstro()
{
    /* set RX off */
    setMessageTipology(0);

#if defined WINETAQ
    DISABLE_SENSTH();  // disattiva le periferiche
    DISABLE_PERIPH();  // disattiva i sensori
    HAL_TURN_OFF_LED1(); // spegni led
    InputOutputSleep(); // porte configurate per basso consumo
#endif

    SLEEPCMD &= ~PMODE; /* clear mode bits */
    SLEEPCMD |= 0x03;   /* set mode bits   */

    while (!(STLOAD & LDRDY));
    {
      PCON = PCON_IDLE;
      asm("NOP");
    }

}


void saveData()
{
   ENABLE_PERIPH();
  
   uint8 lBuffer[2];

   /* leggo quanti dati ho in memoria - lunghezza dei dati */
   readEEProm(lBuffer,MSA_READ_LENGHT,2);
   totLenght1=BUILD_UINT16(lBuffer[0], lBuffer[1]);

   if(totLenght1==0xFFFF) totLenght1=0;
   
   //setto l'indirzzo di partenza per la scrittura
   writeAddress=MSA_READ_ADDRESS+totLenght1;
   
   /*Se i dati salvati superano il limite della EEPROM li riscrivo (tutti quelli in memoria vengono persi)*/
   if((writeAddress+dataLen)>PROM_SIZE-1) writeAddress=MSA_READ_ADDRESS;
   
   
   //salva in EEPROM i dati ricevuti
   retControl=writeEEProm(msa_Data1,writeAddress,dataLen);

   // aggiorna address
   writeAddress=writeAddress+dataLen;

   totLenght1=writeAddress-MSA_READ_ADDRESS;
   
   lBuffer[0]=LO_UINT16(totLenght1);
   lBuffer[1]=HI_UINT16(totLenght1);
   retControl=writeEEProm(lBuffer,MSA_READ_LENGHT,2);  // salvo la lunghezza dei dati

   }

uint8 readData(uint16 len){

    uint8 lBuffer[2],nSensor=0;
    uint16 totLenght=0, idSens, parLen=0;
    bool tooLong=FALSE;


    /*teo20 Luglio
    uint8 tempDataBuffer[500];*/
    uint8 tempDataBuffer[MSA_PACKET_LENGTH];
    
//teo 20 Luglio    uint8 lenghtOfPack[20];
    
    ENABLE_PERIPH();
    
    /* leggo quanti dati ho in memoria - lunghezza dei dati */
   readEEProm(lBuffer,MSA_READ_LENGHT,2);
   totLenght=BUILD_UINT16(lBuffer[0], lBuffer[1])-byteEpromTx;
   
   
   /*se in memoria non ho nulla procedo con l'invio del pacchetto dati*/
   if(totLenght!=0xFFFF && totLenght>0){
      
    /*teo 20 Luglio
     for(uint16 k=0; k<sizeof(tempDataBuffer); k++)
      { tempDataBuffer[k]=0; }
 
       for(uint8 k=0; k<sizeof(lenghtOfPack); k++)
       { lenghtOfPack[k]=0; }
      */     
     
     /* teo 20 Luglio 
     while(memLen<totLenght){
        if((totLenght-memLen)>200){
        readEEProm((uint8*)&tempDataBuffer[memLen],MSA_READ_ADDRESS+memLen,200);
        memLen=memLen+200;
        }
        else {
        readEEProm((uint8*)&tempDataBuffer[memLen],MSA_READ_ADDRESS+memLen,totLenght-memLen);
        memLen=totLenght;
        }
      }*/
      
     if(totLenght+len>100){
     readEEProm(tempDataBuffer,MSA_READ_ADDRESS+byteEpromTx,100-len);
     
     while(!tooLong){
     idSens= BUILD_UINT16(tempDataBuffer[12+parLen],tempDataBuffer[13+parLen]);
      nSensor = countSens(idSens);
      
      if(len+parLen+((nSensor*2)+17)<100){
      parLen=parLen+(nSensor*2)+17;
      }
      else tooLong=TRUE; 
      }
      
     }
     else {
       readEEProm(tempDataBuffer,MSA_READ_ADDRESS+byteEpromTx,totLenght);
       parLen=totLenght;
     }
           
       for(int i=0; i<parLen; i++){
         msa_Data1[i+len]=tempDataBuffer[i];
       }

     /*Teo 21 Luglio troppo lento l'accesso alla Eprom perdita di sincronizzazione
     if(totLenght+len>100){
     
       while(totLenght>parLen){
          
         if((totLenght+len-parLen) >100){
          readEEProm(tempDataBuffer,MSA_READ_ADDRESS+parLen,100);
     
         idSens= BUILD_UINT16(tempDataBuffer[8],tempDataBuffer[9]);
         nSensor = countSens(idSens);
         
         parLen=parLen+(nSensor*2)+13;
         }
         else {
         readEEProm(tempDataBuffer,MSA_READ_ADDRESS+parLen,totLenght-parLen);
         parLen=totLenght-parLen;
         totLenght=parLen;
         }
       
      }
    }
     else readEEProm(tempDataBuffer,MSA_READ_ADDRESS,totLenght);
      
     
     for(uint8 i=0; i<totLenght; i++){
     msa_Data1[i+len]=tempDataBuffer[i];
     }*/
     
     /*se i dati presenti in memoria sono minori della dimensione massima del pacchetto carico i dati e procedo con l'invio*/
     /* teo 20 Luglio
     if((len+totLenght)<MSA_PACKET_LENGTH){
   
            for(uint16 i=0;i<totLenght;i++)
            msa_Data1[len+i]=tempDataBuffer[i];
            
            return LO_UINT16(len+totLenght);
        }*/
        /*altrimenti gestisco la memoria in modalit� LIFO*/
    /* teo 20 Luglio     
    else{
           */
           /*ciclo per salvare nel vettore lenghtOfPack le lunghezze dei pacchetti salvati in memoria*/
        //   while(parLen<totLenght){
           /*leggo il campo che mi indica quanti sensori ho collegati*/
       //    idSens= BUILD_UINT16(tempDataBuffer[8+parLen],tempDataBuffer[9+parLen]);
          /*richiamo la funzione che conta quanti sensori ho collegati*/ 
         //  nSensor = countSens(idSens);
         
           //lenghtOfPack[npack]=(nSensor*2)+13;
         
            //npack++;
         
            // parLen=parLen+(nSensor*2)+13;
          // }
           
          // parLen=0;
          // npack--; 
           /*ciclo per la creazione del pacchetto*/
     
          /* while((len+parLen+lenghtOfPack[npack])<MSA_PACKET_LENGTH)
           {
             for(uint8 k=0; k<lenghtOfPack[npack]; k++)
             {
             msa_Data1[len+parLen+k]=tempDataBuffer[totLenght-lenghtOfPack[npack]+k-parLen];
             }
           
                parLen=parLen + lenghtOfPack[npack];
                npack--;                            
            }
         */
        return LO_UINT16(len+parLen);
        }
   
   
   return len;
   
}

 void updateEpromAddres(uint16 updateLen){
                          
   uint8 lBuffer[2], transitionBuffer[100];
   uint16 totLenght2,writeAddress=0;

   readEEProm(lBuffer,MSA_READ_LENGHT,2);
   totLenght2=BUILD_UINT16(lBuffer[0], lBuffer[1]);
   
   if(totLenght2==0xFFFF) totLenght2=updateLen;
   else totLenght2=totLenght2-updateLen;
   
   lBuffer[0]=LO_UINT16(totLenght2);
   lBuffer[1]=HI_UINT16(totLenght2);
   retControl=writeEEProm(lBuffer,MSA_READ_LENGHT,2); 
   
    if(updateLen>0){
      /*Teo 21 Luglio*/
      while(totLenght2>0){
     
    
      if(totLenght2>=100){
            readEEProm(transitionBuffer,MSA_READ_ADDRESS+updateLen+writeAddress,100);              
            retControl=writeEEProm(transitionBuffer,MSA_READ_ADDRESS+writeAddress,100);
            writeAddress=writeAddress+100;
            totLenght2=totLenght2-100;
       }
       else {
               readEEProm(transitionBuffer,MSA_READ_ADDRESS+writeAddress+updateLen,totLenght2);
               retControl=writeEEProm(transitionBuffer,MSA_READ_ADDRESS+writeAddress,totLenght2);
               totLenght2=0;
        }
    
      }
       
     }                          
}
                                                                                                                                 
uint8 countSens(uint16 id){

  uint8 numSens=0;
  uint16 maskVar=0x0001;
               
        for(uint8 j=0; j<16; j++){
          if ((id & maskVar) == maskVar)
          {  
            numSens++;
          }
        maskVar=maskVar<<1;
        }
        return numSens;

}

void InputOutputSleep()
{
   /* I2C */
    IO_DIR_PORT_PIN( CLK_PORT, CLK_PIN, IO_OUT );
    IO_DIR_PORT_PIN( DATA_PORT, DATA_PIN, IO_OUT );
    SDA = 0x01;
    SCL = 0x01;
 
    P0SEL=0x00; // I/O
    IO_DIR_PORT_PIN( 0, 7, IO_OUT );
    IO_DIR_PORT_PIN( 0, 6, IO_OUT );
    IO_DIR_PORT_PIN( 0, 5, IO_OUT );
    IO_DIR_PORT_PIN( 0, 4, IO_OUT );
    IO_DIR_PORT_PIN( 0, 3, IO_OUT );
    IO_DIR_PORT_PIN( 0, 2, IO_OUT );
    IO_DIR_PORT_PIN( 0, 1, IO_OUT );
    IO_DIR_PORT_PIN( 0, 0, IO_OUT );

   
    P0_7=0x00;
    P0_6=0x00;
    P0_5=0x00;
    P0_4=0x00;
    P0_3=0x00;
    P0_2=0x00;
    P0_1=0x00;
    P0_0=0x00;
}


void InputOutputWakeUp()
{

     /* Init I2C Peripherals */
     initI2C();

    /* ADC */
    IO_DIR_PORT_PIN( 0, 7, IO_IN );
    IO_DIR_PORT_PIN( 0, 6, IO_IN );
    IO_DIR_PORT_PIN( 0, 5, IO_IN );
    IO_DIR_PORT_PIN( 0, 4, IO_IN );
    
    #if defined WINETHP
     DISABLE_STEP_UP();
    #else
     IO_DIR_PORT_PIN( 0, 3, IO_IN );
    #endif
      
    IO_DIR_PORT_PIN( 0, 2, IO_IN );
    IO_DIR_PORT_PIN( 0, 1, IO_IN );
    IO_DIR_PORT_PIN( 0, 0, IO_IN );
 
}

void readIEEEAddressFromNv()
{
  uint8 aExtendedAddress[8];
  
  // Attempt to read the extended address from the designated location in the Info Page.
  osal_memcpy(aExtendedAddress, (uint8 *)(P_INFOPAGE+HAL_INFOP_IEEE_OSET), HAL_FLASH_IEEE_SIZE);

  // write the Extended Address in the NV Memory space - First IEEE
  (void)osal_nv_write(ZCD_NV_EXTADDR, 0, HAL_FLASH_IEEE_SIZE, &aExtendedAddress);

  // leggo dalla memoria il Second IEEE impostato con SMARTRF Programmer
  HalFlashRead(HAL_FLASH_IEEE_PAGE, HAL_FLASH_IEEE_OSET, aSecExtendedAddress, Z_EXTADDR_LEN);

  // Set the MAC extended address - uso il secondary address
  MAC_MlmeSetReq(MAC_EXTENDED_ADDRESS, &aSecExtendedAddress);

}

void incrementAstro(uint8 min)
{
  /*Inserire procedura di controllo come nel coordinatore*/
  uint8 minOld,diffMin;
  
  // minOld=min;
  
  ENABLE_PERIPH(); // attivo 
  
  restoreTime();  // ripristino i registri esterni per leggere ora e data
  
  //la funzione diffTimeStamp calcola e restituisce la differenza di tempo della fase attiva del nodo
  diffMin=diffTimestamp();
  
   minOld=astroBuffer[1]; //Teo 10 Dicembre  
  
    if (msa_State==MSA_SETUP_STATE) {
  
        //nel caso in cui arrivi qui con 59 secondi potrebbe decrementare e poi andare in sleep con 00 e quindi saltare una trasmissione
        if(astroBuffer[1]<=SEC_TO_SYNCH) min--; 

       }
  else {

     if(astroBuffer[1]<HextoBCD(minWakeUpOld)) 
        min--; //caso uguale non considerato perch� passa pi� di un secondo

  }
  
  astroBuffer[2]=astroBuffer[2]+min; /*setta i minuti di risveglio*/
  
  defineDate();

  if (msa_State==MSA_SETUP_STATE) newSecondi=BcdToHex(SEC_TO_SYNCH);

  else newSecondi=BcdToHex(minWakeUpOld);
  
  /*Matteo 10 Dicembre gestisco le situazioni in cui un nodo setta l'astronomico a 59 secondi e poi passa a 00*/
  readAstronomic(timeBuffer,0x0000,0x08); // leggo tutti i dati

  astroConv(timeBuffer); // converto in BCD risultati in astroBuffer ( in 0 ci sono i decimi )
 
  if ( astroBuffer[1] < minOld) newMinuti--;
  /*Fine modifica 10 Dicembre*/
      
  setAstroInterrupt(AI_ENABLE,newSecondi,newMinuti,newOre,newGiorno,newMese);
  
}

void defineDate(){

  newAnno=astroBuffer[7]; //anno
  newMese=astroBuffer[6];  // mese
  newGiorno=astroBuffer[5]; // giorno
  newdayWeek=astroBuffer[4];
  newOre=astroBuffer[3]; // ore
  newMinuti=astroBuffer[2]; // minuti letti + minuti di sleep
  newSecondi=astroBuffer[1]; // secondi
  newDecCent=astroBuffer[0];
  
  
   if (newSecondi==60)
   {
    newSecondi=0;
    newMinuti++;
   }
   else if (newSecondi>60)
   {
    newSecondi=newSecondi-60;
    newMinuti++;
   }

  
  if (newMinuti==60)
   {
    newMinuti=0;
    newOre++;
   }
   else if (newMinuti>60)
   {
    newMinuti=newMinuti-60;
    newOre++;
   }
  
   if (newOre==24) {
   newOre=0;
   checkMeseUp(newMese);
   }
   
  newSecondi=BcdToHex(newSecondi);;
  newMinuti=BcdToHex(newMinuti);
  newOre=BcdToHex(newOre);
  newGiorno=BcdToHex(newGiorno);
  newMese=BcdToHex(newMese);
  newAnno=BcdToHex(newAnno);
   
}


void checkMeseUp(uint8 mese)
{
 switch (newMese)     // switch sul mese
      {
      case 1:
        if ((newGiorno+1)>31)
        {  // giorno data
         newGiorno=1; // giorno 1
         newMese=2;
        }
        else newGiorno++;
      break;

      case 2:
        if ((newAnno%4)==0) // anno  bisestile
        {
         if ((newGiorno+1)>29)
          {
            newGiorno=1; // giorno 1
            newMese=3;
           }
          else newGiorno++;
        }
        else if ((newGiorno+1)>28)
        {
          newGiorno=1; // giorno 1
          newMese=3;
        } // mese Marzo
        else newGiorno++;
        break;

      case 3:
        if ((newGiorno+1)>31)
        {
          newGiorno=1;
          newMese=4;
        }
        else newGiorno++;
        break;

       case 4:
         if ((newGiorno+1)>30)
         {
          newGiorno=1;
          newMese=5;
         }
         else newGiorno++;
        break;

       case 5:
        if ((newGiorno+1)>31)
        {
          newGiorno=1;
          newMese=6;
        }
        else newGiorno++;
        break;

        case 6:
        if ((newGiorno+1)>30)
        {
          newGiorno=1;
          newMese=7;
        }
        else newGiorno++;
        break;

       case 7:
        if ((newGiorno+1)>31)
        {
          newGiorno=1;
          newMese=8;
        }
        else newGiorno++;
        break;

       case 8:
       if ((newGiorno+1)>31)
       {
          newGiorno=1;
          newMese=9;
       }
       else newGiorno++;
       break;

       case 9:
       if ((newGiorno+1)>30)
       {
          newGiorno=1;
          newMese=10;
       }
       else newGiorno++;
       break;

       case 10:
       if ((newGiorno+1)>31)
       {
          newGiorno=1;
          newMese=11;
       }
       else newGiorno++;
       break;

      case 11:
      if ((newGiorno+1)>30)
      {
          newGiorno=1;
          newMese=12;
      }
      else newGiorno++;
      break;

      case 12:
      if ((newGiorno+1)>31)
      {
          newGiorno=1;
          newMese=1;
          newAnno++;
      }
      else newGiorno++;
      break;
    }
}

uint8 preparePacket(void){
  
  uint16 IDSensor, maskVar, readData, readBattery;
  uint8 a=14;
 
  /*Byte 0 e 1 ID Nodo*/
        msa_Data1[0]=HI_UINT16(msa_DevShortAddr);
        msa_Data1[1]=LO_UINT16(msa_DevShortAddr);
        
        // leggo dalla memoria il Second IEEE impostato con SMARTRF Programmer
        HalFlashRead(HAL_FLASH_IEEE_PAGE, HAL_FLASH_IEEE_OSET, aSecExtendedAddress, Z_EXTADDR_LEN);
        
        msa_Data1[2]=aSecExtendedAddress[1];
        msa_Data1[3]=aSecExtendedAddress[0];  
        
        msa_Data1[4]=HI_UINT16(fatherAdd);
        msa_Data1[5]=LO_UINT16(fatherAdd);  
        
        
        /*6 byte relativi alla date-time della lettura*/
        readAstronomic(readBuffer,0x0000,8);
        
        msa_Data1[6]=readBuffer[5];
        msa_Data1[7]=readBuffer[6];
        msa_Data1[8]=readBuffer[7];
        msa_Data1[9]=readBuffer[3];
        msa_Data1[10]=readBuffer[2];
        msa_Data1[11]=readBuffer[1];
        
        /*Recupero 3� e 4� byte dell'indirizzo esteso per definire il tipo di sensori collegati*/
        IDSensor=BUILD_UINT16(aSecExtendedAddress[2],aSecExtendedAddress[3]);
        
        msa_Data1[12]=HI_UINT16(IDSensor);
        msa_Data1[13]=LO_UINT16(IDSensor);
        
        /*Lettura batteria da fare prima di disabilitare le periferiche*/
        readBattery=getBattery();

        DISABLE_PERIPH(); // disattivo le periferiche per leggere correttamente la P0.1
        
        maskVar=0x00001; 
        
        for(uint8 j=0; j<16; j++){
          if ((IDSensor & maskVar) == maskVar) { 
            readData=readSensor(maskVar);
          
         msa_Data1[a]=HI_UINT16(readData);
         msa_Data1[a+1]=LO_UINT16(readData);
        
         a=a+2;
         
         }
         maskVar=maskVar<<1;
        }
 
         msa_Data1[a]=HI_UINT16(readBattery);
         msa_Data1[a+1]=LO_UINT16(readBattery);
        
         msa_Data1[a+2]=0x00; //rssi field
         a=a+3;
        
         //teo Prova 3 Dicembre
         
        /* for(int j=a; j<100; j++)
           msa_Data1[j]=j;
         
         a=100;*/
      
         // fine prova
        return a;
}

void faultTolerance(){
  
  uint8 tempTime;
  
  switch(stato){
  case 0: stato=1; 
        if(orphan) // Matteo 5 Novembre && (fatherAdd==MSA_COORD_SHORT_ADDR))
        {
            fatherAdd=msa_CoordShortAddr; 
            
            if(fatherAdd!=MSA_COORD_SHORT_ADDR) //Matteo 6 Novembre
            {
              minWakeUp=minWakeUp-(HI_UINT16(msa_CoordShortAddr)*2);
              minWakeUpOld=minWakeUp;
            }
        }
          return; 
          break;
  case 1: 
    
     orphan=TRUE;
         
    if(fatherAdd!=MSA_COORD_SHORT_ADDR){
              fatherAdd=BUILD_UINT16(0x00,HI_UINT16(msa_DevShortAddr)-1);
    
              tempTime=minWakeUp-(60-(HI_UINT16(msa_DevShortAddr)*2));
              countLevel=1+(tempTime/2);
            
              //matteo 20 Settembre
           //   numOfReply=0;
              
              //
             
              //matteo 20 Settembre
               //fatherAdd=BUILD_UINT16(numOfReply,HI_UINT16(msa_DevShortAddr)-1);
              
              stato=2; 
              }
          else{
            fatherAdd=MSA_COORD_SHORT_ADDR;
            
            stato=3;
          }
          return;
   break;
          
  case 2: //numOfReply++;
          //fatherAdd=BUILD_UINT16(numOfReply,HI_UINT16(fatherAdd));
          // if(numOfReply<MSA_NUM_OF_NODES_FAULT_TOLERANCE) {stato=2;}
          // else {
               
               countLevel--;
              
               if(countLevel==0){
                 
                 countLevel=HI_UINT16(msa_DevShortAddr)-HI_UINT16(fatherAdd)+1;
                 
                 if (countLevel!=HI_UINT16(msa_DevShortAddr)) 
                   fatherAdd= BUILD_UINT16(0x00,HI_UINT16(msa_DevShortAddr));
                 
                
            //  if((minWakeUp<58) || ((minWakeUp==58) && (HI_UINT16(fatherAdd)==1))){
              diffMinWakeUp=diffMinWakeUp+2;
              if(minWakeUp==58) minWakeUp=0;
              else minWakeUp=minWakeUp+2;
              //}
               
               }
              
              if(HI_UINT16(fatherAdd)>1) { fatherAdd=BUILD_UINT16(0x00,HI_UINT16(fatherAdd)-1);  stato=2;}    
              else {fatherAdd=MSA_COORD_SHORT_ADDR; stato=3;}
          // }
            return;
            break;
            
  case 3: stato=0;
          minToSleep=SLEEP_TIME;
          return;
          break;
  
  default: return;
          
  }
}

uint16 calcOsalTimerPeriod(){
  
    uint16 diff;
    uint8 tempSec, tempDecCent;
    
    readAstronomic(readBuffer,0x0000,8);
    astroConv(readBuffer);
    
    tempSec=astroBuffer[1];
    tempDecCent=astroBuffer[0];
    
    astroConv(matchBuffer);
    
    if(tempSec<astroBuffer[1]) {tempSec=tempSec+60;}
    if(tempDecCent<astroBuffer[0]) {tempDecCent=tempDecCent+100; tempSec--;}
    
    tempSec=tempSec-astroBuffer[1];
    tempDecCent=tempDecCent-astroBuffer[0];
    
    
    
    if(((tempSec*100)+tempDecCent)>=(winRx/10)) return 1000;
    else {
           diff=(winRx/10)-((tempSec*100)+tempDecCent);
            
           if (diff<100) return 1000;
           else return diff*10;
    }
  
  }  

//bool timePermitAssociation(uint8 mode){
  bool timePermitAssociation(){
   uint16 diffAssoc; 
   uint8 tempSec, tempDecCent;
  
    
    readAstronomic(readBuffer,0x0000,8);
    astroConv(readBuffer);
    
    tempSec=astroBuffer[1];
    tempDecCent=astroBuffer[0];
    
    astroConv(matchBuffer); //orologio letto nell'HandleKeys
    
    if(tempSec<astroBuffer[1]) {tempSec=tempSec+60;}
    if(tempDecCent<astroBuffer[0]) {tempDecCent=tempDecCent+100; tempSec--;}
    
    tempSec=tempSec-astroBuffer[1];
    tempDecCent=tempDecCent-astroBuffer[0];
    
        /*    Matteo 18 Ottobre se la modifica diventa definitiva modificare il parametro passato al metodo
    switch(mode){
    case 0:  diffAssoc=(tempSec*100)+tempDecCent;
              if (diffAssoc<40) return TRUE;
              else return FALSE;
              break;
    //associazione nell'ASSOC_STATE
    case 1:   diffAssoc=(MSA_ASSOCIATION_PERIOD/10)-((tempSec*100)+tempDecCent);
              if (diffAssoc<180) return FALSE;
              else return TRUE;
              break;
    }*/
    
    diffAssoc=(MSA_ASSOCIATION_PERIOD/10)-((tempSec*100)+tempDecCent);
              if (diffAssoc<50) return FALSE;
              else return TRUE;
  
  }  

uint8 diffTimestamp(){

uint8 minOld, minNew, hourOld, hourNew, diff;

  astroConv(timeStampBuffer);

  minOld=astroBuffer[2];
  hourOld=astroBuffer[3];
  
  readAstronomic(timeBuffer,0x0000,0x08); // leggo tutti i dati

  astroConv(timeBuffer); // converto in BCD risultati in astroBuffer ( in 0 ci sono i decimi )
 
  minNew=astroBuffer[2];
  hourNew=astroBuffer[3];

  if(hourNew<hourOld) hourNew=hourNew+24;
  if(minNew<minOld) { minNew=minNew+60; hourNew--;}
  
  if (msa_State== MSA_START_STATE) diff=0;
  else diff=((hourNew-hourOld)*60)+(minNew-minOld);
  
  return diff;
}

/*********************************************************************
 * @fn      setMessageTipology(uint8 val)
 *
 * @brief   Funzione fondamentale per lo sleep ed il corretto wake up
 *
 *
 * @param   val
 *          FALSE viene impostato l'indirect message
 *          permettendo al nodo di entrare il PM2 correttamente.
 *          Non riceve per� i dati perch� RX � OFF.
 *          TRUE viene impostato il direct message
 *          permettendo al nodo di ricevere i dati.
 *
 * @return  none
 */

void setMessageTipology(uint8 val)
{
 /* Decide if direct or indirect messaging is used */
  msa_IsDirectMsg = val;

  /* This device is setup for Direct Message */
  if (msa_IsDirectMsg)
  MAC_MlmeSetReq(MAC_RX_ON_WHEN_IDLE, &msa_MACTrue);
  else
  MAC_MlmeSetReq(MAC_RX_ON_WHEN_IDLE, &msa_MACFalse);

}


/*********************************************************************
 * @fn      getBattery
 *
 * @brief   get the value of the battery in milliVolt
 *          and write it into the output buffer
 *
 * @param   none
 *
 * @return  none
 */

uint16 getBattery(void)
{

  int16 tempRead=0;
  uint32 valueRead=0;

  // Enable channel 0x00
  APCFG |= 0x01;

  for(uint8 m=0;m<CAMPIONI;m++)
  {

   /* writing to this register starts the conversion - riferimento P0.7 e risoluzione massima */
    //ADCCON3 = 0x00 | 0x30 | 0x80;
    /* writing to this register starts the conversion - riferimento Vdd e risoluzione massima */
    ADCCON3 = 0x00 | 0x30 | 0x40;

    /* Wait for the conversion to be done */
    while (!(ADCCON1 & 0x80));

     /* Read the result */
     tempRead = (int16) (ADCL);
     tempRead |= (int16) (ADCH << 8);

     tempRead >>= 4; // Shift 4 due to 12 bits resolution true

     valueRead+=tempRead; // salvo e sommo

  }

  APCFG &=~ 0x01;  // ripristino P0.0 come input/output non ADC

  //valueRead = (uint32)(valueRead * 3000/LIVELLI);  // 3000 � la Vstabilizzata

  //valueRead = (uint32)(valueRead * 2500/LIVELLI);   // se riferimento � P0.7
  
  return (valueRead/CAMPIONI);

}
