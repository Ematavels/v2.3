/**************************************************************************************************
  Filename:       msa.c
  Revised:        $Date: 2011-09-16 11:36:37 -0700 (Fri, 16 Sep 2011) $
  Revision:       $Revision: 27599 $

  Description:    This file contains the sample application that can be use to test
                  the functionality of the MAC, HAL and low level.


  Copyright 2006-2011 Texas Instruments Incorporated. All rights reserved.

  IMPORTANT: Your use of this Software is limited to those specific rights
  granted under the terms of a software license agreement between the user
  who downloaded the software, his/her employer (which must be your employer)
  and Texas Instruments Incorporated (the "License").  You may not use this
  Software unless you agree to abide by the terms of the License. The License
  limits your use, and you acknowledge, that the Software may not be modified,
  copied or distributed unless embedded on a Texas Instruments microcontroller
  or used solely and exclusively in conjunction with a Texas Instruments radio
  frequency transceiver, which is integrated into your product.  Other than for
  the foregoing purpose, you may not use, reproduce, copy, prepare derivative
  works of, modify, distribute, perform, display or sell this Software and/or
  its documentation for any purpose.

  YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE
  PROVIDED �AS IS� WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
  INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, TITLE,
  NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL
  TEXAS INSTRUMENTS OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER CONTRACT,
  NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER
  LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
  INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE
  OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT
  OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
  (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.

  Should you have any questions regarding your right to use this Software,
  contact Texas Instruments Incorporated at www.TI.com.
**************************************************************************************************/

/**************************************************************************************************

    Description:

                                KEY UP (or S1)
                              - Non Beacon
                              - First board in the network will be setup as the coordinator
                              - Any board after the first one will be setup as the device
                                 |
          KEY LEFT               |      KEY RIGHT(or S2)
                             ----+----- - Start transmitting
                                 |
                                 |
                                KEY DOWN



**************************************************************************************************/


/**************************************************************************************************
 *                                           Includes
 **************************************************************************************************/

/* Hal Driver includes */
#include "hal_types.h"
#include "hal_key.h"
#include "hal_timer.h"
#include "hal_drivers.h"
#include "hal_led.h"
#include "hal_adc.h"


/* OS includes */
#include "OSAL.h"
#include "OSAL_Tasks.h"
#include "OSAL_PwrMgr.h"

/* Application Includes */
#include "OnBoard.h"

/* For Random numbers */
#include "mac_radio_defs.h"
#include "mac_low_level.h"

/* MAC Application Interface */
#include "mac_api.h"
#include "mac_main.h"
#include "mac_radio.h"

#ifdef MAC_SECURITY
#include "mac_spec.h"

/* MAC Sceurity */
#include "mac_security_pib.h"
#include "mac_security.h"
#endif /* MAC_SECURITY */

/* Application */
#include "RouterFrane.h"

/* WinetAQ */
#include "hal_i2c.h"
#include "astronomic.h"
#include "eeprom.h"

#include "string.h"

#if defined MODEM
#include "modem.h"
#endif

#include "hal_uart.h"

#include "OSAL_Nv.h"
#include "hal_flash.h"
#include "sensors.h"
#include "adc1115.h"
#include "OSAL_Clock.h"

#if defined SPI
 #include "hal_spi.h"
 #include "sdcard.h"
 #include "ff.h"
 #include "ffconf.h"
 #include "integer.h"
 uint8 saveP2DIR;
 bool sdOk=false;
#endif

#if defined DEC5TE_P04
 #include "SdiCore.h"
 #include "5TEParser.h"
#endif

#if defined FLORA_1  
 #include "SdiCore.h"
 #include "FloraParser.h"
#endif 
 
#if defined SENTEK_90 || SENTEK_60 || defined SENTEK_30 || defined SENTEK_10A || defined SENTEK_10B || defined SENTEK_10C || defined SENTEK_10D || defined SENTEK_10E || defined SENTEK_10F
  #include "SdiCore.h"
  #include "SentekParser.h"
#endif 

#if defined DECGS3_P04 
#include "SdiCore.h"
#include "Gs3Parser.h"
#endif

#if defined DECCDT
#include "SdiCore.h"
#include "CDTParser.h"
#endif

#if defined MPS6_P04
 #include "SdiCore.h"
 #include "MPS6Parser.h"
#endif

#if defined (PIOGGIA) || defined (VENTO) || defined (PRESS0)
 #include "attiny85.h"
#endif
 
#if defined RS485
#include "rs485.h"
#endif
 
#include "i2c_multiplexer.h"
 

/**************************************************************************************************
 *                                           Constant
 **************************************************************************************************/

#define MSA_MAC_MAX_RESULTS       5             /* Maximun number of scan result that will be accepted */
#define MSA_MAX_DEVICE_NUM        50            /* Maximun number of devices can associate with the coordinator */

#if defined (HAL_BOARD_CC2420DB)
  #define MSA_HAL_ADC_CHANNEL     HAL_ADC_CHANNEL_0             /* AVR - Channel 0 and Resolution 10 */
  #define MSA_HAL_ADC_RESOLUTION  HAL_ADC_RESOLUTION_10
#elif defined (HAL_BOARD_DZ1611) || defined (HAL_BOARD_DZ1612) || defined (HAL_BOARD_DRFG4618) || defined (HAL_BOARD_F2618)
  #define MSA_HAL_ADC_CHANNEL     HAL_ADC_CHANNEL_0             /* DZ1611 and DZ1612 - Channel 0 and Resolution 12 */
  #define MSA_HAL_ADC_RESOLUTION  HAL_ADC_RESOLUTION_12
#else
  #define MSA_HAL_ADC_CHANNEL     HAL_ADC_CHANNEL_7             /* CC2430 EB & DB - Channel 7 and Resolution 14 */
  #define MSA_HAL_ADC_RESOLUTION  HAL_ADC_RESOLUTION_14
#endif

#define MSA_EBR_PERMITJOINING    TRUE
#define MSA_EBR_LINKQUALITY      1
#define MSA_EBR_PERCENTFILTER    0xFF

/* Size table for MAC structures */
const CODE uint8 msa_cbackSizeTable [] =
{
  0,                                   /* unused */
  sizeof(macMlmeAssociateInd_t),       /* MAC_MLME_ASSOCIATE_IND */
  sizeof(macMlmeAssociateCnf_t),       /* MAC_MLME_ASSOCIATE_CNF */
  sizeof(macMlmeDisassociateInd_t),    /* MAC_MLME_DISASSOCIATE_IND */
  sizeof(macMlmeDisassociateCnf_t),    /* MAC_MLME_DISASSOCIATE_CNF */
  sizeof(macMlmeBeaconNotifyInd_t),    /* MAC_MLME_BEACON_NOTIFY_IND */
  sizeof(macMlmeOrphanInd_t),          /* MAC_MLME_ORPHAN_IND */
  sizeof(macMlmeScanCnf_t),            /* MAC_MLME_SCAN_CNF */
  sizeof(macMlmeStartCnf_t),           /* MAC_MLME_START_CNF */
  sizeof(macMlmeSyncLossInd_t),        /* MAC_MLME_SYNC_LOSS_IND */
  sizeof(macMlmePollCnf_t),            /* MAC_MLME_POLL_CNF */
  sizeof(macMlmeCommStatusInd_t),      /* MAC_MLME_COMM_STATUS_IND */
  sizeof(macMcpsDataCnf_t),            /* MAC_MCPS_DATA_CNF */
  sizeof(macMcpsDataInd_t),            /* MAC_MCPS_DATA_IND */
  sizeof(macMcpsPurgeCnf_t),           /* MAC_MCPS_PURGE_CNF */
  sizeof(macEventHdr_t)                /* MAC_PWR_ON_CNF */
};

/**************************************************************************************************
 *                                        Local Variables
 **************************************************************************************************/

/* Device information */
uint16        msa_PanId = MSA_PAN_ID; //PAN ID: se non corrisponde a quella delle rete non viene effettuata l'associazione
uint16        msa_CoordShortAddr = MSA_COORD_SHORT_ADDR; //Indirizzo del nodo padre al momento dell'associazione
uint16        msa_DevShortAddr   = 0x0000; // indirizzo SHORT del nodo
uint16        fatherAdd; // indirizzo del nodo padre che potrebbe essere diverso da quello definito durante l'associazione nel caso di Fault Tolerance

/*varibili che gesticono la fase di Fault Tolerance: numOfReply serve per cercare un nodo padre tra quelli che possono essere presenti
su differenti livelli, stato gestisce la macchina a stati secondo il meccanismo definito nel caso di Fault Tolerance
ATTENZIONE: la variabile stato deve essere inizializzata a 0*/
uint8         numOfReply=0,stato=0;

/* Indice utilizzato per inviare un solo pacchetto con l'orario al nodo figlio che invia pi� pacchetti dati */
uint8         msa_NumOfDevices = 0;

#ifdef MAC_SECURITY
  /* Current number of devices (including coordinator) communicating to this device */
  uint8       msa_NumOfSecuredDevices = 0;
#endif /* MAC_SECURITY */

/* Vettore utilizzato per salvare i dati in memoria e per inviare il pacchetto dati */
uint8         msa_Data1[MSA_PACKET_LENGTH];

/* Vettore utilizzato per inviare il pacchetto ai figli con l'orario (8byte) e i secondi in cui il nodo si risveglia (1byte) */
uint8         msa_Data2[11];

/* TRUE and FALSE value */
bool msa_MACTrue = TRUE;
bool msa_MACFalse = FALSE;

/* Beacon payload, this is used to determine if the device is not zigbee device */
uint8         msa_BeaconPayload[] = {0x22, 0x33, 0x44};
uint8         msa_BeaconPayloadLen = 3;

/* Contains pan descriptor results from scan */
macPanDesc_t  msa_PanDesc[MSA_MAC_MAX_RESULTS];

/* flags used in the application */
bool          msa_IsSampleBeacon = FALSE;   /* True if the beacon payload match with the predefined */
bool          msa_IsDirectMsg    = TRUE;   /* True if the messages will be sent as direct messages */

/* Variabile che consente di definire gli stati di funzionamento del nodo: MSA_ASSOC_STATE e MSA_SEND_STATE
La variabile viene inizializzata a MSA_START_STATE in modo tale che nello SLEEP_EVENT il nodo vada subito in sleep*/
uint8         msa_State = MSA_START_STATE;

/* Structure that used for association request */
macMlmeAssociateReq_t msa_AssociateReq;

/* Structure that used for association response */
macMlmeAssociateRsp_t msa_AssociateRsp;

/* Vettore che consente di gestire l'invio dell'orario una sola volta al figlio che trasmette pi� pacchetti */
uint16 msa_DeviceRecord[MSA_MAX_DEVICE_NUM];

uint8 msa_SuperFrameOrder;
uint8 msa_BeaconOrder;

/* Task ID */
uint8 MSA_TaskId;

#ifdef MAC_SECURITY
/**************************************************************************************************
 *                                  MAC security related constants
 **************************************************************************************************/
#define MSA_KEY_TABLE_ENTRIES         1
#define MSA_KEY_ID_LOOKUP_ENTRIES     1
#define MSA_KEY_DEVICE_TABLE_ENTRIES  8
#define MSA_KEY_USAGE_TABLE_ENTRIES   1
#define MSA_DEVICE_TABLE_ENTRIES      0 /* can grow up to MAX_DEVICE_TABLE_ENTRIES */
#define MSA_SECURITY_LEVEL_ENTRIES    1
#define MSA_MAC_SEC_LEVEL             MAC_SEC_LEVEL_ENC
#define MSA_MAC_KEY_ID_MODE           MAC_KEY_ID_MODE_1
#define MSA_MAC_KEY_SOURCE            {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}
#define MSA_MAC_DEFAULT_KEY_SOURCE    {0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33}
#define MSA_MAC_KEY_INDEX             3  /* cannot be zero for implicit key identifier */


/* Default MSA Security Parameters for outgoing frames */
bool  macSecurity       = TRUE;
uint8 msa_securityLevel = MSA_MAC_SEC_LEVEL;
uint8 msa_keyIdMode     = MSA_MAC_KEY_ID_MODE;
uint8 msa_keySource[]   = MSA_MAC_KEY_SOURCE;
uint8 msa_keyIndex      = MSA_MAC_KEY_INDEX;

/**************************************************************************************************
 *                                 Security PIBs for outgoing frames
 **************************************************************************************************/
const keyIdLookupDescriptor_t msa_keyIdLookupList[] =
{
  {
    {0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x03}, 0x01 /* index 3, 9 octets */
  }
};

/* Key device list can be modified at run time
 */
keyDeviceDescriptor_t msa_keyDeviceList[] =
{
  {0x00, false, false},
  {0x00, false, false},
  {0x00, false, false},
  {0x00, false, false},
  {0x00, false, false},
  {0x00, false, false},
  {0x00, false, false},
  {0x00, false, false}
};

const keyUsageDescriptor_t msa_keyUsageList[] =
{
  {MAC_FRAME_TYPE_DATA, MAC_DATA_REQ_FRAME}
};

const keyDescriptor_t msa_keyTable[] =
{
  {
    (keyIdLookupDescriptor_t *)msa_keyIdLookupList, MSA_KEY_ID_LOOKUP_ENTRIES,
    (keyDeviceDescriptor_t *)msa_keyDeviceList, MSA_KEY_DEVICE_TABLE_ENTRIES,
    (keyUsageDescriptor_t *)msa_keyUsageList, MSA_KEY_USAGE_TABLE_ENTRIES,
    {0x12, 0x34, 0x56, 0x78, 0x9a, 0xbc, 0xde, 0xf0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}
  }
};

const uint8 msa_keyDefaultSource[] = MSA_MAC_DEFAULT_KEY_SOURCE;


/**************************************************************************************************
 *                                 Security PIBs for incoming frames
 **************************************************************************************************/

/* Device table can be modified at run time. */
deviceDescriptor_t msa_deviceTable[] =
{
  {0, 0xFFFF, 0, 0, FALSE},
  {0, 0xFFFF, 0, 0, FALSE},
  {0, 0xFFFF, 0, 0, FALSE},
  {0, 0xFFFF, 0, 0, FALSE},
  {0, 0xFFFF, 0, 0, FALSE},
  {0, 0xFFFF, 0, 0, FALSE},
  {0, 0xFFFF, 0, 0, FALSE},
  {0, 0xFFFF, 0, 0, FALSE}
};

const securityLevelDescriptor_t msa_securityLevelTable[] =
{
  {MAC_FRAME_TYPE_DATA, MAC_DATA_REQ_FRAME, MAC_SEC_LEVEL_NONE, FALSE}
};
#else /* MAC_SECURITY */
uint8 msa_securityLevel = MAC_SEC_LEVEL_NONE;
uint8 msa_keyIdMode     = MAC_KEY_ID_MODE_NONE;
uint8 msa_keySource[]   = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
uint8 msa_keyIndex      = 0;
#endif /* MAC_SECURITY */

/**************************************************************************************************
 *                                     Local Function Prototypes
 **************************************************************************************************/
/* Setup routines */
void MSA_DeviceStartup(void);

/* MAC related routines */
void MSA_AssociateReq(void);
void MSA_AssociateRsp(macCbackEvent_t* pMsg);
void MSA_McpsDataReq(uint8* data, uint8 dataLength, bool directMsg, uint16 dstShortAddr,bool ack);
void MSA_ScanReq(uint8 scanType, uint8 scanDuration);

/* Support */
bool MSA_BeaconPayLoadCheck(uint8* pSdu);
/* Sleep - funzioni e variabili di supporto */
void incrementAstro(uint16 min); //funzione che consente di settare l'orario di risveglio del nodo
void SleepAstro(void); //funzione che manda in sleep il nodo
void Sleep(uint16 timeout);
void WakeUp(void);
void checkMeseUp(uint8 mese); //funzione che consente di gestire i mesi per il settaggio dell'orario e dell'interrupt di risveglio
void checkMeseDown(uint8 mese);
void defineDate(uint16 min); // funzione che consente di definire la data per il settaggio dell'orario e dell'interrupt di risveglio
uint8 preparePacket(void); //funzione che consente di preparare il pacchetto dati da inviare, restituisce la lunghezza del pacchetto

/*funzione utilizzata per gestire la Fault Tolerance, passando come parametro lo short address del padre che non risponde pi�
fornisce in uscita il numero di tentativi da effettuare per cercare un nuovo padre*/
void faultTolerance(void);

//funzione che calcola una differenza di tempi
uint16 calcOsalTimerPeriod(void);
//funzione che riattiva le porte al risveglio del chip
void InputOutputWakeUp(void);
//funzione che spegne le porte prima della fase di sleep
void InputOutputSleep(void);
//funzione che consente di accendere e spegnare la sezione radio
void setMessageTipology(uint8 val);
//funzione che effettua il salvataggio dei dati ricevuti
void saveData(void);
//funzione che consente la lettura dei dati da eprom
uint8 readData(uint16 len);
uint8 ripeti;
//funzione che effettua la cancellazione da Eprom dei dati inviati correttamente
void updateEpromAddres(uint16 updateLen);
/* ADC */
uint16 getAdcChannel(uint8 channel);
uint16 getBattery(void);
void checkBattery(uint8 forceSleep);
//uint8 countSens(uint32 id);
bool timePermitAssociation(void);
void readSensorBefore(void);

//bool timePermitAssociation(uint8 mode);
uint8 timeBuffer[8]; // buffer dove memorizzo secondi,minuti,ore...
uint8 newGiorno,newMese,newMinuti,newOre,newAnno,newSecondi,newDecCent,newdayWeek; // per settaggio allarme
uint8 tempMin=0;    
uint8 readBuffer[8], matchBuffer[8],activeBuffer[8];
uint8 aSecExtendedAddress[8]; //variabile che consente di salvare l'indirizzo secondario esteso del nodo da cui ricaviamo anche il numero/tipo di sensori collegati
/*Rete funzioni e variabili di supporto*/
uint8 minWakeUp=0; //variabile che indica il valori di secondi nei quali si deve svegliare il nodo
uint8 minWakeUpOld=0xFF; //il settaggio iniziale serve per far partire la sincronizzazione Matteo 3 Agosto
uint8 diffMinWakeUp=0; //Matteo 3 Agosto
uint16 minToSleep=SLEEP_TIME;
uint16 sleepTime=SLEEP_TIME+2;
uint16 assocShortAddress=0xFFFF;
uint16 broadShortAddress=0xFFFF;
uint8 assocExtAdd[2];
bool txToCoord=FALSE;
uint8 countPck=0;
bool enableAssociation=TRUE; //settaggio indifferente, viene messo FALSE al primo risveglio
uint8 rssi;
uint8 dataLen;
uint16 writeAddress=MSA_READ_ADDRESS;
uint8 tempDataBuffer[102]; // massima dimensione lo uso sia per pachet che per read eeprom 
uint16 lenOfMyPack=0,lenOfSon=0;
uint16 byteEpromTx=0;
bool orphan=FALSE;
bool present;
uint16 winRx,osalTimerValue;
uint16 addrFree=0, windowStar=0; //Matteo 11 Ottobre
bool time=FALSE;
bool msa_IsStarted= FALSE;   /* Variabile da mantenere per evitare che se un nodo si resetta altri nodi cambino padre
settando la variabile fatherAdd nell'evento BEACON_NOTIFY_IND*/
bool sinch=FALSE;
bool assocStart=FALSE;
bool firstStart=TRUE;
uint8 countLevel;
uint16 readBattery;
uint8 maxSendRetries=MAX_SEND_RETRIES;
uint8 maxRetriesToReset=MAX_NUM_RETRIES;
uint8 maxFatherRetries=MAX_FATHER_RETRIES;


uint8 watchDogAbeAfe[2]={0x7F,0x00}; // setto a 0x00 AFE e ABE e WD = 124 secondi
uint8 clearBuff[1];
uint8 contaTx=0;
uint8 tmpLevBuffer[2];
uint8 saveAdd[1];
UTCTimeStruct timeRead;
uint32 secOld=0,secStart=0;  
uint8 LQI,txPowerValue=MAC_TX_POWER;

#if defined WD_AQ22
  void setWDTime(uint16 minute);
#endif


#if defined FLORA_1 || DEC5TE_P04 || SENTEK_10A || defined SENTEK_10B || defined SENTEK_10C || defined SENTEK_10D || defined SENTEK_10E || defined SENTEK_10F || defined SENTEK_60 || defined SENTEK_90 || defined SENTEK_30 || defined SENTEK_30_B || defined MPS6_P04 || defined DECGS3_P04 || defined DECCDT
 
 uint8 sdiBuffer[40]={
 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
 
 uint8 sdiBuffer2[40]={
 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
 
 uint8 sdiBuffer3[40]={
 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
 
 uint8 sdiBuffer4[40]={
 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

#if defined SENTEK_90 || SENTEK_60 || SENTEK_30 
int16 resSentekHum[6];    //1-6
int16 resSentekHumB[6];   //7-12
int16 resSentekSal[6];    //1-6
int16 resSentekSalB[6];   //7-12
int16 resSentekTemp[6];   //1-6
int16 resSentekTempB[6];  //7-12
#endif

void clearSdiBuffers(void);
uint8 sdiRetries; 

#if defined FLORA_1
  int16 floraResBuffer[3];
#endif

#if defined SENTEK_10A
int16 resSentek10A[3];
#endif

#if defined SENTEK_10B
int16 resSentek10B[3];
#endif

#if defined SENTEK_10C
int16 resSentek10C[3];
#endif

#if defined SENTEK_10D
int16 resSentek10D[3];
#endif

#if defined SENTEK_10E
int16 resSentek10E[3];
#endif

#if defined SENTEK_10F
int16 resSentek10F[3];
#endif

#if defined DEC5TE_P04 
  int16 resBuffer[3];
#endif

#if defined MPS6_P04  
  // le altre variabili sono gia definite perch� ho per forza 5TE su P0_4 se occupo P0_5 
  int16 resBuffer2[3];
  uint8 sdiDevice2=0x06; //Indirizzo Sensore MPS6
#endif    
  
#endif
  
#if defined SPI
uint16 sdcount=0;  
UINT bytes_count;
FATFS Fatfs;		/* File system object */
FIL Fil;			/* File object */
FRESULT rc;/* FatFs return value */

char tempBufferSD[200];
void BytesToCharSD(uint8* buffer,uint16 lenghtBytes,uint8 len);
bool WriteToSDCARD(uint8* buffer,uint8 lengthOfData);
void resetTempBufferSD(void);
#endif

  
#ifdef MAC_SECURITY
/**************************************************************************************************
 *
 * @fn          MSA_SecurityInit
 *
 * @brief       Initialize the security part of the application
 *
 * @param       none
 *
 * @return      none
 *
 **************************************************************************************************/
static void MSA_SecurityInit(void)
{
  uint8   keyTableEntries      = MSA_KEY_TABLE_ENTRIES;
  uint8   autoRequestSecLevel  = MAC_SEC_LEVEL_NONE;
  uint8   securityLevelEntries = MSA_SECURITY_LEVEL_ENTRIES;

  /* Write key table PIBs */
  MAC_MlmeSetSecurityReq(MAC_KEY_TABLE, (void *)msa_keyTable);
  MAC_MlmeSetSecurityReq(MAC_KEY_TABLE_ENTRIES, &keyTableEntries );

  /* Write default key source to PIB */
  MAC_MlmeSetSecurityReq(MAC_DEFAULT_KEY_SOURCE, (void *)msa_keyDefaultSource);

  /* Write security level table to PIB */
  MAC_MlmeSetSecurityReq(MAC_SECURITY_LEVEL_TABLE, (void *)msa_securityLevelTable);
  MAC_MlmeSetSecurityReq(MAC_SECURITY_LEVEL_TABLE_ENTRIES, &securityLevelEntries);

  /* TBD: MAC_AUTO_REQUEST is true on by default
   * need to set auto request security PIBs.
   * dieable auto request security for now
   */
  MAC_MlmeSetSecurityReq(MAC_AUTO_REQUEST_SECURITY_LEVEL, &autoRequestSecLevel);

  /* Turn on MAC security */
  MAC_MlmeSetReq(MAC_SECURITY_ENABLED, &macSecurity);
}


/**************************************************************************************************
 *
 * @fn          MSA_SecuredDeviceTableUpdate
 *
 * @brief       Update secured device table and key device descriptor handle
 *
 * @param       panID - Secured network PAN ID
 * @param       shortAddr - Other device's short address
 * @param       extAddr - Other device's extended address
 * @param       pNumOfSecuredDevices - pointer to number of secured devices
 *
 * @return      none
 *
 **************************************************************************************************/
static void MSA_SecuredDeviceTableUpdate(uint16 panID,
                                         uint16 shortAddr,
                                         sAddrExt_t extAddr,
                                         uint8 *pNumOfSecuredDevices)
{
  /* Update key device list */
  msa_keyDeviceList[*pNumOfSecuredDevices].deviceDescriptorHandle = *pNumOfSecuredDevices;

  /* Update device table */
  msa_deviceTable[*pNumOfSecuredDevices].panID = panID;
  msa_deviceTable[*pNumOfSecuredDevices].shortAddress = shortAddr;
  sAddrExtCpy(msa_deviceTable[*pNumOfSecuredDevices].extAddress, extAddr);
  MAC_MlmeSetSecurityReq(MAC_DEVICE_TABLE, msa_deviceTable);

  /* Increase the number of secured devices */
  (*pNumOfSecuredDevices)++;
  MAC_MlmeSetSecurityReq(MAC_DEVICE_TABLE_ENTRIES, pNumOfSecuredDevices);
}
#endif /* MAC_SECURITY */


/**************************************************************************************************
 *
 * @fn          MSA_Init
 *
 * @brief       Initialize the application
 *
 * @param       taskId - taskId of the task after it was added in the OSAL task queue
 *
 * @return      none
 *
 **************************************************************************************************/
void MSA_Init(uint8 taskId)
{

  /* Initialize the task id */
  MSA_TaskId = taskId;

  /* initialize MAC features */
  MAC_InitDevice();
  MAC_InitCoord();

  /* Initialize MAC beacon */
  MAC_InitBeaconDevice();
  MAC_InitBeaconCoord();

  /* Reset the MAC */
  MAC_MlmeResetReq(TRUE);

  TXPOWER=MAC_TX_POWER;

  /* Imposta l'indirizzo IEEE Secondario */
  readIEEEAddressFromNv();

  #ifdef MAC_SECURITY
   /* Initialize the security part of the application */
   MSA_SecurityInit();
  #endif /* MAC_SECURITY */

  for(int i=0; i<MSA_PACKET_LENGTH; i++)
    msa_Data1[i]=0;

  msa_BeaconOrder = MSA_MAC_BEACON_ORDER;
  msa_SuperFrameOrder = MSA_MAC_SUPERFRAME_ORDER;
  
  // leggo la batteria e salvo lo stato carica
  ENABLE_SENSTH(); halWait(100); 
  readBattery=getBattery();
  DISABLE_SENSTH();
  
  //check se sono in condizioni buone per fare lo SCAN ( se non lo sono sleep e wd reset )
  checkBattery(0);
  
  #if defined RS485
    ENABLE_SENSTH(); 
    halWaitSec(6); 
    reqSent=0x01; // 1� convertitore
    osal_start_timerEx ( RS485_TaskID, NEXT_CMD_RS485, RS485_IDLE);
  #else
    
   setMessageTipology(0);
   
   // MUX_TCA9584 
   #if defined SHT3X
    checkMux=multiplexerChannel(NO_CHANNEL);
    if (checkMux) 
    {
     checkSensorConnected();
    }
    else countSht3x[0]=0x01; // se non ho il mux collegato setto che ho un sensore TH diretto
   #endif   
  
   readSensorBefore(); // leggo i sensori in modo da avere i dati al primo preparePacket
   
   setMessageTipology(1);
  
   HalLedSet (HAL_LED_1, HAL_LED_MODE_ON);  // led ON - device acceso
   
   // timer da max_beacon_time per consentire associazione
   osal_start_timerEx(MSA_TaskId,MSA_READ_EVENT,MAX_BEACON_TIME);
   
   MSA_ScanReq(MAC_SCAN_ACTIVE, 2);
  #endif
}

/**************************************************************************************************
 *
 * @fn          MSA_ProcessEvent
 *
 * @brief       This routine handles events
 *
 * @param       taskId - ID of the application task when it registered with the OSAL
 *              events - Events for this task
 *
 * @return      16bit - Unprocessed events
 *
 **************************************************************************************************/
uint16 MSA_ProcessEvent(uint8 taskId, uint16 events)
{
  uint8* pMsg;
  macCbackEvent_t* pData;
  uint8 setClear[1]={0x00};

#ifdef MAC_SECURITY
  uint16       panID;
  uint16       panCoordShort;
  sAddrExt_t   panCoordExtAddr;
#endif /* MAC_SECURITY */

  if (events & SYS_EVENT_MSG)
  {
    while ((pMsg = osal_msg_receive(MSA_TaskId)) != NULL)
    {
      switch ( *pMsg )
      {
        case MAC_MLME_ASSOCIATE_IND:

          if (enableAssociation){

            enableAssociation=FALSE;
            
            if (timePermitAssociation())
            { 
              setMessageTipology(1);
              MSA_AssociateRsp((macCbackEvent_t*)pMsg);
            }
          }

          break;

        case MAC_MLME_ASSOCIATE_CNF:
          /* Retrieve the message */
          pData = (macCbackEvent_t *) pMsg;
        
          if ((!msa_IsStarted) && (pData->associateCnf.hdr.status == MAC_SUCCESS))
          {

            msa_IsStarted = TRUE;
             /* Retrieve MAC_SHORT_ADDRESS */

            msa_DevShortAddr = pData->associateCnf.assocShortAddress;

            /* Setup MAC_SHORT_ADDRESS - obtained from Association */
            MAC_MlmeSetReq(MAC_SHORT_ADDRESS, &msa_DevShortAddr);
           
          #ifdef MAC_SECURITY
            /* Add the coordinator to device table and key device table for security */
            MAC_MlmeGetReq(MAC_PAN_ID, &panID);
            MAC_MlmeGetSecurityReq(MAC_PAN_COORD_SHORT_ADDRESS, &panCoordShort);
            MAC_MlmeGetSecurityReq(MAC_PAN_COORD_EXTENDED_ADDRESS, &panCoordExtAddr);
            MSA_SecuredDeviceTableUpdate(panID, panCoordShort,
                                         panCoordExtAddr, &msa_NumOfSecuredDevices);
          #endif /* MAC_SECURITY */

          macMlmeStartReq_t   startReq;
          startReq.startTime = 0;
          startReq.panId = msa_PanId;
          startReq.logicalChannel = MSA_MAC_CHANNEL;
          startReq.beaconOrder = msa_BeaconOrder;
          startReq.superframeOrder = msa_SuperFrameOrder;
          startReq.panCoordinator = TRUE;
          startReq.batteryLifeExt = FALSE;
          startReq.coordRealignment = FALSE;
          startReq.realignSec.securityLevel = FALSE;
          startReq.beaconSec.securityLevel = FALSE;

          /* Call start request to start the device as a coordinator */
          MAC_MlmeStartReq(&startReq);
          }
          else
          {
           resetBoard();
          }
          break;

        case MAC_MLME_COMM_STATUS_IND:
          setMessageTipology(0);
          osal_start_timerEx(MSA_TaskId, MSA_READ_EVENT, 60);
          break;
          
          
      case MAC_MCPS_PURGE_CNF:
 
        break;

        case MAC_MLME_BEACON_NOTIFY_IND:
          /* Retrieve the message */
          pData = (macCbackEvent_t *) pMsg;

          uint16 readPermit;
          uint16 BeaconPanID;
          uint8 receiveLevel;
	
          LQI=pData->beaconNotifyInd.pPanDesc->linkQuality;
          readPermit=pData->beaconNotifyInd.pPanDesc->superframeSpec;
          BeaconPanID=pData->beaconNotifyInd.pPanDesc->coordPanId;

          /* Check for correct beacon payload */
          if (LQI>MIN_LQI_VALUE)
          {
              if ( ((readPermit & 0x8000)==0x8000) && (BeaconPanID==msa_PanId) && (!msa_IsStarted) && (!assocStart) )
              {
               receiveLevel=HI_UINT16(pData->beaconNotifyInd.pPanDesc->coordAddress.addr.shortAddr);
            
               // se coordinatore salvo come level 0x00
               if (receiveLevel==0xAA) tmpLevBuffer[1]=0x00;
               else tmpLevBuffer[1]=receiveLevel;
                  
               if ( ((tmpLevBuffer[0]!=0xFF) && (tmpLevBuffer[1]<tmpLevBuffer[0]) ) || ((tmpLevBuffer[0]==0xFF)) )
               {
                countPck++;
                /*Effettuo un controllo sul pacchetto di beacon ricevuto che deve contenere un payload definito*/
                msa_IsSampleBeacon = MSA_BeaconPayLoadCheck(pData->beaconNotifyInd.pSdu);
    
                /* If it's the correct beacon payload, retrieve the data for association req */
                if (msa_IsSampleBeacon)
                {
                  msa_AssociateReq.logicalChannel = MSA_MAC_CHANNEL;
                  msa_AssociateReq.coordAddress.addrMode = SADDR_MODE_SHORT;
                  msa_AssociateReq.coordAddress.addr.shortAddr = pData->beaconNotifyInd.pPanDesc->coordAddress.addr.shortAddr;
    
                  msa_CoordShortAddr=pData->beaconNotifyInd.pPanDesc->coordAddress.addr.shortAddr;
                  
                    fatherAdd=msa_CoordShortAddr;
      
                    msa_AssociateReq.coordPanId = pData->beaconNotifyInd.pPanDesc->coordPanId;
                    if (msa_IsDirectMsg)
                      msa_AssociateReq.capabilityInformation = MAC_CAPABLE_ALLOC_ADDR | MAC_CAPABLE_RX_ON_IDLE;
                    else
                      msa_AssociateReq.capabilityInformation = MAC_CAPABLE_ALLOC_ADDR;
                    msa_AssociateReq.sec.securityLevel = MAC_SEC_LEVEL_NONE;
      
                    /* Retrieve beacon order and superframe order from the beacon */
                    msa_BeaconOrder = MAC_SFS_BEACON_ORDER(pData->beaconNotifyInd.pPanDesc->superframeSpec);
                    msa_SuperFrameOrder = MAC_SFS_SUPERFRAME_ORDER(pData->beaconNotifyInd.pPanDesc->superframeSpec);
                    
                    /*va inserito l'else che prevede di salvare in memoria un nodo che ha un buon LQI ma non � il migliore
                    ovvero un nodo che non diventa padre in quanto non ha il miglior LQI ma pu� essere utilizzato per procedure di fault tolerance*/
                 }     
                }
              }
            }
            else // aumento di uno step il txPower fino al raggiungimento del massimo 
            {
             if (txPowerValue<=0xE5) txPowerValue=txPowerValue+0x10;
             else TXPOWER=txPowerValue;
            }   
            break;

        case MAC_MLME_START_CNF:
        /* Retrieve the message */
          pData = (macCbackEvent_t *) pMsg;
     
           /* Set some indicator for the Coordinator */
          if (pData->startCnf.hdr.status == MAC_SUCCESS)
          {
            // ok fermo il read event scatenato dal risparmio batteria
            osal_stop_timerEx(MSA_TaskId, MSA_READ_EVENT);
            
            /* associazione Ok - led spento */
            HalLedSet (HAL_LED_1, HAL_LED_MODE_OFF);
        
            osal_start_timerEx(MSA_TaskId, MSA_SLEEP_EVENT, 1000);
          }
          else
          {
           resetBoard();
          }
        break;

        case MAC_MLME_SCAN_CNF:
          /* Check if there is any Coordinator out there */
          pData = (macCbackEvent_t *) pMsg;
          
          /* If there is no other on the channel or no other with sampleBeacon start as coordinator */
          if (countPck==0)
          {
             HalLedSet (HAL_LED_1, HAL_LED_MODE_OFF);
             halWait(250);
             HalLedSet (HAL_LED_1, HAL_LED_MODE_ON);
            
            MSA_ScanReq(MAC_SCAN_ACTIVE, 2);
          }   
          else
          { 
            setMessageTipology(0);
           
            #if defined RS485
             ENABLE_PERIPH(); // attivo
            #endif
             
            #if defined BOOST_FORO  // ho necessit� di 4-5 secondi per avere stabilit�
             ENABLE_SENSTH();
            #endif 
            
            assocStart=TRUE; // fase di associazione avviata 
            countPck=0;
             
            // Set Watch-dog procedure
            readAstronomic(clearBuff,0x0F,1); // pulisco eventuali flag pendenti
            writeAstronomic(watchDogAbeAfe,0x09,2);
                        
            setMessageTipology(1);
            
             /* Start the devive up as beacon enabled or not */
            MSA_DeviceStartup();

            /* Call Associate Req */
            MSA_AssociateReq();
           }
          break;

        case MAC_MCPS_DATA_CNF:
          pData = (macCbackEvent_t *) pMsg;

          setMessageTipology(0); //Matteo 29/08/2014
           
          /* If last transmission completed, ready to send the next one */
          if (pData->dataCnf.hdr.status == MAC_SUCCESS)
          {

            switch(msa_State)
            {
            case MSA_SETUP_STATE: //il pacchetto con il nuovo tempo di risveglio non prevede ACK
            break;
            
            case MSA_FAULT_STATE:
              setMessageTipology(1);
            break;


            case MSA_SEND_STATE:
              if(txToCoord==TRUE)
              {
                time=TRUE;
                contaTx=0;
                
                maxRetriesToReset=MAX_NUM_RETRIES; //nel caso il coordinatore venga riagganciato ripristino 
                maxFatherRetries=MAX_FATHER_RETRIES; // ripristino

                /*Ho ricevuto l'ACK di un pacchetto dati*/
                if(orphan)
		orphan=FALSE; //se ricevo l'ACK non sono pi� orfano
             	
                stato=0; //se ricevo l'ack nella macchina a stati per la fault tolerance riparto dallo stato 0

                // se ho ricevuto l'ACK aggiorno la variabile byteEpromTx che mi servir� per cancellare dalla memoria i dati inviati
                byteEpromTx=byteEpromTx+lenOfSon-lenOfMyPack;
 
                setMessageTipology(1);		
                
                //faccio partire timer da 350ms che mi porter� ad inviare nuovi dati o ad andare in Sleep
                // era 300ms ma a volte il coord ci mette 310ms per salvare un pacchetto grande
                osal_start_timerEx(MSA_TaskId, MSA_SEND_EVENT, SEND_DELAY);  
              }
              else
              {
                //chiamo la funzione calcOsalTimerPeriod() che mi calcola quanto tempo ho ha disposizione prima di inviare i miei dati al padre
                osalTimerValue=calcOsalTimerPeriod();
                
                osal_start_timerEx(MSA_TaskId, MSA_SEND_EVENT, osalTimerValue);

                setMessageTipology(1);
              }

              break;
              
               case MSA_START_STATE:               
                osal_start_timerEx(MSA_TaskId, MSA_SEND_EVENT, 100);  
              break;

              case MSA_ASSOC_STATE:
                 setMessageTipology(1);
                break;

            }

          }
          // non ho ricevuto l'ACK atteso o altro evento radio failure
          else 
          {
                switch(msa_State)
                {
                case MSA_SETUP_STATE:  //il pacchetto con il nuovo tempo di risveglio non prevede ACK
                break;
                
                case MSA_FAULT_STATE:
                setMessageTipology(1);
                break;


                case MSA_SEND_STATE:

                  if(txToCoord==TRUE)
                  {  
                     // nessun ACK al pacchetto dati inviato o altra failure
                     
                      // Ema 24/06/2019 faccio in contTx dentro 
                      if (contaTx<maxSendRetries) // incremento i retries fino a maxSendRetries
                      { 
                       contaTx++;

                       osal_start_timerEx(MSA_TaskId, MSA_REPEAT_DATA_EVENT, SEND_DELAY);
                       
                       setMessageTipology(1);                     
                      }
                      else
                      {
                      contaTx=0;
                      
                      if (minToSleep==SLEEP_TIME) maxRetriesToReset--; // se arrivo ad 1 minuto ad ogni giro decremento i tentativi
                      
                      if (maxRetriesToReset==0) resetBoard();  // se arrivo a oltre 2 ore di retries si resetta

                      // se nel pacchetto sul quale non ho ricevuto l'ACK c'erano dati letti in fase di risveglio li vado a salvare in eprom dato che mio padre non li avr� ricevuti
                      if (lenOfMyPack!=0)
                      {
                       dataLen=lenOfMyPack;

                        //se non ho ricevuto l'ACK significa che potrei non avere pi� un padre e quindi metto 0000 nel campo father all'interno del pacchetto che salvo in memoria
                        msa_Data1[4]=0x00;
                        msa_Data1[5]=0x00;

                        rssi=0;
                        
                        //chiamo la funzione per il salvataggio dei dati in EEprom
                        saveData();

                        /*chiamo la funzione di fault tolerance per valutare quale nuovo nodo padre posso trovare all'interno della rete
                        la chiamo dentro questa selezione perch� deve andare in fault tolerance solo se non riceve l'ack del primo pacchetto inviato*/
                        faultTolerance();
                      }
                    
                       // non avendo ricevuto l'ACK vado in sleep
                       osal_start_timerEx(MSA_TaskId, MSA_SLEEP_EVENT, 100);
                    }
                   }

                    else
                    {
                       //caso in cui non ricevo l'ACK al pacchetto con data e ora
                       //calcolo il tempo rimanente nella finestra SEND_STATE dopodich� dovr� inviare i miei dati al padre
                      osalTimerValue=calcOsalTimerPeriod();

                      osal_start_timerEx(MSA_TaskId, MSA_SEND_EVENT, osalTimerValue);
                      
                      setMessageTipology(1);
                    }

                    break;

              case MSA_START_STATE:

                    dataLen=lenOfMyPack;

                    //se non ho ricevuto l'ACK significa che potrei non avere pi� un padre e quindi metto 0000 nel campo father all'interno del pacchetto che salvo in memoria
                    msa_Data1[4]=0x00;
                    msa_Data1[5]=0x00;

                    rssi=0;
                    //chiamo la funzione per il salvataggio dei dati in EEprom
                    saveData();

                    //chiamo la funzione di fault tolerance per valutare quale nuovo nodo padre posso trovare all'interno della rete
                    faultTolerance();

                    // non avendo ricevuto l'ACK vado in sleep
                    osal_start_timerEx(MSA_TaskId, MSA_SLEEP_EVENT, 100);

                break;
                
              case MSA_ASSOC_STATE: 
                 setMessageTipology(1);
                break;

                }
             }
        
          mac_msg_deallocate((uint8**)&pData->dataCnf.pDataReq);
          break;

        case MAC_MCPS_DATA_IND:
          pData = (macCbackEvent_t*)pMsg;

          uint16 DataPanID;
          uint8 numSensori;
         
          DataPanID=pData->dataInd.mac.srcPanId;

          if (DataPanID==msa_PanId)
          {

            //alla ricezione di un pacchetto spengo la radio per evitare che arrivino altri pacchetti e interrompano i vari settaggi
            setMessageTipology(0);

             /*ricezione del pacchetto con i nuovi tempi di sleep */
             if (msa_State==MSA_SETUP_STATE)
             {
                 if( sinch==FALSE) //Matteo 06 Maggio 2014 if( sinch==FALSE)
                 {
                    osal_stop_timerEx(MSA_TaskId, MSA_SLEEP_EVENT);
                    
                    sinch=TRUE;
                    //se sono nello stato di SETUP il pacchetto ricevuto conterr� il nuovo intervallo di sleep che salvo nella variabile minToSleep
                    minToSleep=BUILD_UINT16(pData->dataInd.msdu.p[0],pData->dataInd.msdu.p[1]);
                    // di seguito chiamo l'evento SETUP per inoltrare tale tempo ai miei figli
                    osal_start_timerEx(MSA_TaskId, MSA_SETUP_EVENT,( (HI_UINT16(msa_DevShortAddr)*50) + MAC_RandomByte()/10) ); 
                 }
              }
            else
            {
             if ((pData->dataInd.msdu.p[0]==0xFF) && (pData->dataInd.msdu.p[1]==0xFF) && (msa_State==MSA_FAULT_STATE))
             {
              if (stato==2)
              {
              stato=0;
              orphan=FALSE; //Matteo 30 Ottobre tolto in data 19/12/2012 perch� � la volta successiva che so se padre � ok
              fatherAdd=pData->dataInd.mac.srcAddr.addr.shortAddr;
              minToSleep=BUILD_UINT16(pData->dataInd.msdu.p[2],pData->dataInd.msdu.p[3]);    // aggiunto da EMA 1/1/2015           
              }
             }
             else
             {
                // se pacchetto � del padre e il pacchetto � quello dell'orario ( indirizzato a me )
                if ( (pData->dataInd.mac.srcAddr.addr.shortAddr==fatherAdd) && (pData->dataInd.mac.dstAddr.addr.shortAddr==msa_DevShortAddr) )
                {
                  //pacchetto ricevuto dal nodo padre contenente data e ora
                  // faccio lo stop dei due timer perch� ho ricevuto un pacchetto dal nodo figlio e devo dargli la possibilit� di inviarmi tutti i dati che possiede
                  osal_stop_timerEx(MSA_TaskId, MSA_SEND_EVENT);
                  osal_stop_timerEx(MSA_TaskId, MSA_REPEAT_DATA_EVENT);
                  osal_stop_timerEx(MSA_TaskId, MSA_SLEEP_EVENT); //Matteo 18 Ottobre per la fase di associazione

                  time=TRUE;
                  
                  //le dal pacchetto la data e l'ora e le salvo su un buffer che utilizzer� per settare l'astronomico
                  for(uint8 i=0; i<8; i++)
                  {
                    timeBuffer[i]=pData->dataInd.msdu.p[i];
                  }

                  //nel caso in cui riceva dal padre un orario sballato FF non effettuo alcun settaggio
                  if (timeBuffer[0]!=0xFF)
                  {
                    astroConv(timeBuffer);

                    //incremento di un'unit� i secondi per recuperare il tempo che attendo con l'istruzione halWaitms sottostante
                    astroBuffer[1]++;

                    //chiamo la funzione che mi calcola la data dovuta all'aumento di un'unit� dei secondi
                    defineDate(astroBuffer[2]);

                    //introduco un ritardo per allinearmi con i centesimi del nodo padre
                    halWaitms((100-newDecCent)*10);

                    /*Una volta ricevuto il pacchetto di avvio rete setto l'astronomico*/
                    setAstronomic(START,newSecondi,newMinuti,newOre,newdayWeek,newGiorno,newMese,newAnno);
                    
                    writeAstronomic(setClear,0x0F,1); // setto a zero OF 
 
                  }

                  /*setto la variabile minWakeUp che indica a quali secondi si deve svegliare il nodo*/
                  minWakeUp=HextoBCD(pData->dataInd.msdu.p[8]);

                  if (minWakeUp==0x00) minWakeUp=60;
                  minWakeUp=minWakeUp-MSA_WAKE_PERIOD; //il nodo figlio si sveglier� MSA_WAKE_PERIOD secondi in anticipo rispetto al padre

                  //matteo 17/04/2014 aggiunta ricezione tempi di sleep accensione
                  minToSleep=BUILD_UINT16(pData->dataInd.msdu.p[10],pData->dataInd.msdu.p[9]);

                  // se mio padre � il coordinatore prelevo dal pacchetto anche il tempo di sleep che a sua volta lui aveva scaricato dal sito web
                  if (fatherAdd==MSA_COORD_SHORT_ADDR)
                  {
                    sinch=TRUE;
                    orphan=FALSE; // ok non sono orphan
                  }
                  
                  //caso in cui mi trovi nella fase di accensione del nodo
                  if(minWakeUpOld==0xFF)
                  {
                    minWakeUpOld=minWakeUp;
                  }
                  // se sono nella fase attiva e mio padre mi ha comunicato che il tempo di risveglio � cambiato (risalita di un livello) setto la variabile diffMinWakeUp per indicare che al prossimo risveglio devo attendere i dati dai miei figli nello stesso sloto temporale ma devo inviarli al nodo padre dopo questo ritardo
                  else
                  {
                    diffMinWakeUp=minWakeUp-minWakeUpOld;
                  }
                  
                  /* Dopo aver ricevuto l'orario vado in SEND_EVENT per trasmettere nuovi pacchetti oppure andare in sleep
                  imposto 100ms nell'osal_start_timer per consentire il dealloc */
                  osal_start_timerEx(MSA_TaskId, MSA_SEND_EVENT,100);
                }
                //ricevo i dati da mio figlio o da un nodo che sta cercando il padre
                else
                {
                  //Matteo 06 Maggio 2014 if (!txToCoord)
                  if ( (!txToCoord) && (!orphan) ) // se sono ancora orphan non devo rispondere
                  {
                   
                    osal_stop_timerEx(MSA_TaskId, MSA_SEND_EVENT);

                     //se ricevo FFFF significa che il nodo che mi ha trasmesso ha perso il padre
                    if (pData->dataInd.mac.dstAddr.addr.shortAddr==0XFFFF)
                    {
                      //se sta cercando un nuovo padre con un livello e un tempo di risveglio uguali ai miei salvo l'indirizzo del nodo e gli rispondo dicendogli che dalla prossima finestra temporale pu� inviare a me i dati da far giungere al coordinatore
                      if ((pData->dataInd.msdu.p[0]==HI_UINT16(msa_DevShortAddr)) && (pData->dataInd.msdu.p[1]==minWakeUpOld))
                      {
                        broadShortAddress=pData->dataInd.mac.srcAddr.addr.shortAddr;
                        osal_start_timerEx(MSA_TaskId, MSA_RESP_BROAD_EVENT,100);
                      }
                      /*Se il pacchetto di broadcast non era per me riaccendo la radio */
                      else
                      {
                        //se il nodo da cui ho ricevuto il pacchetto dati � presente nell'array msa_DeviceRecord non invio data ora (perch� a lui un pacchetto � gi� stato inviato) e faccio ripartire timer per invio dati in memoria verso il nodo padre
                        osalTimerValue=calcOsalTimerPeriod();
                          
                        osal_start_timerEx(MSA_TaskId, MSA_SEND_EVENT, osalTimerValue);                        

                        setMessageTipology(1);

                      }
                    }

                    else
                    {
                    // pacchetto ricevuto dal nodo figlio

                      //salvo la lunghezza del pacchetto dati per poter definire lo spazio di memoria richiesto
                      dataLen=pData->dataInd.msdu.len;

                      // salvo i dati da scrivere in eeprom
                      for(uint8 j=0;j<dataLen;j++)
                       msa_Data1[j]=pData->dataInd.msdu.p[j];

                      //salva il valore di rssi
                      rssi=pData->dataInd.mac.rssi;

                      //se il pacchetto contiene come primi dati quelli del nodo figlio da cui ho ricevuto il pacchetto calcolo i sensori che ha collegati in modo fa poter definire con precisione il byte in cui salvare l'RSSI rilevato
                      if ( (BUILD_UINT16(msa_Data1[1],msa_Data1[0])) == pData->dataInd.mac.srcAddr.addr.shortAddr)
                      {
                        //numSensori = countSens(BUILD_UINT32(msa_Data1[12],msa_Data1[13],msa_Data1[14],msa_Data1[15]));
                        numSensori = msa_Data1[16];
                        msa_Data1[(MSA_PACKET_HEADER-1)+(numSensori*2)]=rssi;
                      }

                      saveData(); // salvo i dati in EEPROM

                      if (msa_State==MSA_SEND_STATE)
                      {//dato che l'orario verso uno stesso nodo voglio inviarlo una sola volta utilizzo la variabile Present per effettuare la valutazione
                        present=FALSE;

                        for(uint8 i=0; i<MSA_MAX_DEVICE_NUM; i++)
                        {
                          //salvo all'interno del buffer msa_DeviceRecord gli indirizzi dei nodi da cui ricevo i dati in modo da poterli leggere e confrontarli ogni volta che ricevo un pacchetto da un nodo figlio, tale array viene pulito ad ogni risveglio
                          if(pData->dataInd.mac.srcAddr.addr.shortAddr==msa_DeviceRecord[i])
                          {
                            //se trovo l'indirizzo del nodo figlio all'interno dell'array significa che gli ho gi� inviato il pacchetto con ora e data
                            present=TRUE;
                            //trovato l'indirizzo del nodo figlio posso uscire dal ciclo
                            i=MSA_MAX_DEVICE_NUM;

                          }
                        }

                        if(!present)
                        {
                        //nodo non presente nell'array msa_DeviceRecord, salvo tale indirizzo e invio pacchetto con ora e data
                          msa_DeviceRecord[msa_NumOfDevices]=pData->dataInd.mac.srcAddr.addr.shortAddr;

                          //indice per gestire il salvataggio degli indirizzi nell'array msa_DeviceRecord
                          msa_NumOfDevices++;

                          //il vettore ha dimensione massima MSA_MAX_DEVICE_NUM, faccio un controllo sull'indice
                          if(msa_NumOfDevices>=MSA_MAX_DEVICE_NUM) msa_NumOfDevices=0;
                          assocShortAddress=pData->dataInd.mac.srcAddr.addr.shortAddr;

                          // Mettiamo 60ms per consentire il dealloc, invio del pacchetto con ora e data
                          if (msa_State!= MSA_ASSOC_STATE) 
                            osal_start_timerEx(MSA_TaskId, MSA_READ_EVENT,60);

                        }

                        else
                        {
                        //se il nodo da cui ho ricevuto il pacchetto dati � presente nell'array msa_DeviceRecord non invio data ora (perch� a lui un pacchetto � gi� stato inviato) e faccio ripartire timer per invio dati in memoria verso il nodo padre
                        osalTimerValue=calcOsalTimerPeriod();
                          
                        osal_start_timerEx(MSA_TaskId, MSA_SEND_EVENT, osalTimerValue);

                        //dopo aver correttamente processato il pacchetto dati del nodo figlio riaccendo la radio per consentire eventuali nuove ricezioni dati
                        setMessageTipology(1);
                         }

                      }
                      else
                      {
                        enableAssociation=TRUE;

                        //dopo aver correttamente processato il pacchetto del nodo figlio riaccendo la radio per consentire eventuali nuove associazioni
                        setMessageTipology(1);                        
                      }
                      // Matteo 18 Ottobre if(timePermitAssociation(1)) enableAssociation=TRUE;

                    }
                  }
                }
               }
            }
          }
      }

      /* Deallocate */
      mac_msg_deallocate((uint8 **)&pMsg);

    }

  return events ^ SYS_EVENT_MSG;
  }

  if (events & MSA_SLEEP_EVENT)
  {
    uint16 minW;
    setMessageTipology(0);
 
    //entro in questo stato in cui ogni nodo termina la finestra di associazione
    if(msa_State==MSA_ASSOC_STATE)
    {
      
      //non acconsento pi� alle associazioni perch� dovr� iniziare il la fase di trasmissione/ricezione dei dati
      MAC_MlmeSetReq(MAC_ASSOCIATION_PERMIT, &msa_MACFalse);
      //setto lo stato di trasmissione/ricezione
      msa_State=MSA_SEND_STATE;
      //calcolo la finestra per la ricezione dei dati dai nodi figlio considerando un periodo fisso (MSA_WAIT_PERIOD), 
      //un termine variabile (windowStar) per consentire ai nodi figli di ricevere il pacchetto con ora e data nel caso 
      //di multiple associzioni allo stesso nodo padre e un termina diffMinWakeUp che serve nel caso in cui il nodo padre sia salito di livello
      winRx = MSA_WAIT_PERIOD+windowStar+(diffMinWakeUp*1000);
     
      //reset della variabile
      diffMinWakeUp=0;

      checkOsc();
      //leggo l'astronomico per poter valutare l'intervallo di tempo in cui il nodo � rimasto nello stato di SEND_STATE
      readAstronomic(matchBuffer,0x0000,8);
               
      //se sono nello stato 2 (ricerca di padre) andr� nell'evento per l'invio del pacchetto broadcast altrimenti nell'evento di trasmissione pacchetto dati
      if (stato==2) osal_start_timerEx(MSA_TaskId, MSA_BROAD_EVENT, winRx);
      else osal_start_timerEx(MSA_TaskId, MSA_SEND_EVENT, winRx);
      
      if(!orphan) setMessageTipology(1);
    }

     else{
    
       switch(msa_State){

       case MSA_SEND_STATE:
         updateEpromAddres(byteEpromTx);

         // converto e salvo i secondi del risveglio
         astroConv(activeBuffer);
         tempMin=astroBuffer[2];
           
         /**** - CHECK - ****/
         // Salvo il tempo di risveglio in secondi
         timeRead.seconds=astroBuffer[1];
         timeRead.minutes=astroBuffer[2];
         timeRead.hour=astroBuffer[3];
         timeRead.day=astroBuffer[5]-1;
         timeRead.month=astroBuffer[6]-1;
         timeRead.year=2000+astroBuffer[7];
      
         secOld=osal_ConvertUTCSecs(&timeRead); // memorizzo i secondi di partenza       
        /**** - - - - ******/
          
         checkOsc();
         //leggo astronomico
         readAstronomic(timeBuffer,0x0000,0x08); // leggo tutti i dati

         astroConv(timeBuffer); // converto in BCD risultati in astroBuffer ( in 0 ci sono i decimi )
         
          /**** - CHECK - ****/
          // Salvo il tempo letto in secondi
          timeRead.seconds=astroBuffer[1];
          timeRead.minutes=astroBuffer[2];
          timeRead.hour=astroBuffer[3];
          timeRead.day=astroBuffer[5]-1;
          timeRead.month=astroBuffer[6]-1;
          timeRead.year=2000+astroBuffer[7];
        
          secStart=osal_ConvertUTCSecs(&timeRead); // memorizzo i secondi letti
          
           if (secStart<secOld) // errore grave di lettura
             resetBoard();
          /**** - - - - ******/

         // margine perch� in caso il nodo invii molti pacchetti pu� scadere il timer del broadcast event
         // devo verificare: sono gia nel minuto successivo e il tempo � inferiore a SEC_TO_SYNCH
         // devo per� controllare di non essere oltre al minuto successivo
         // io mi risveglio sempre nel minuto precedente quindi tempMin=astroBuffer vuol dire che sono ancora nel minuto precedente
         // non contemplato di essere oltre il minuto precedente 
         if ( (tempMin==astroBuffer[2]) || (astroBuffer[1]+SEC_TO_SYNCH_MARGIN < SEC_TO_SYNCH) )
         {
           msa_State=MSA_SETUP_STATE;
           incrementAstro(SLEEP_TIME);
         }
         else
         {
          msa_State=MSA_BEFORE_STATE;
           
          if(minToSleep==0xFFFF || minToSleep==0x0000) incrementAstro(SLEEP_TIME);
           else incrementAstro(minToSleep);
         }
       break;

       case MSA_SETUP_STATE:
         msa_State=MSA_BEFORE_STATE;
             
         if(minToSleep==0xFFFF || minToSleep==0x0000) incrementAstro(SLEEP_TIME);
         else incrementAstro(minToSleep);
        break;
         
        case MSA_BEFORE_STATE:
         msa_State=MSA_SEND_STATE;
         
         astroConv(activeBuffer);
         tempMin=astroBuffer[2]; // analizzo il minuto a cui mi sono svegliato
         
           /**** - CHECK - ****/
         // Salvo il tempo di risveglio in secondi
         timeRead.seconds=astroBuffer[1];
         timeRead.minutes=astroBuffer[2];
         timeRead.hour=astroBuffer[3];
         timeRead.day=astroBuffer[5]-1;
         timeRead.month=astroBuffer[6]-1;
         timeRead.year=2000+astroBuffer[7];
      
         secOld=osal_ConvertUTCSecs(&timeRead); // memorizzo i secondi di partenza       
        /**** - - - - ******/
         
         checkOsc(); 
         readAstronomic(readBuffer,0x0000,8);
    
         astroConv(readBuffer); 
         
          /**** - CHECK - ****/
          // Salvo il tempo letto in secondi
          timeRead.seconds=astroBuffer[1];
          timeRead.minutes=astroBuffer[2];
          timeRead.hour=astroBuffer[3];
          timeRead.day=astroBuffer[5]-1;
          timeRead.month=astroBuffer[6]-1;
          timeRead.year=2000+astroBuffer[7];
        
          secStart=osal_ConvertUTCSecs(&timeRead); // memorizzo i secondi letti
          
           if (secStart<secOld) // errore grave di lettura
             resetBoard();
          /**** - - - - ******/
         
        if (astroBuffer[2]==tempMin) // sono nello stesso minuto quindi devo incrementar di 1 minuto
         {
           minW=astroBuffer[2]+1;
         } 
         else minW=astroBuffer[2];
         
         defineDate(minW);

         newSecondi=BcdToHex(minWakeUpOld);

         setAstroInterrupt(newSecondi,newMinuti,newOre,newGiorno,newMese);
   
        break;
        
        case MSA_START_STATE:
         msa_State=MSA_SETUP_STATE;
            
         /* Salvo in EEPROM il livello solo la prima volta*/
         // voglio evitare che se si associa ad un nodo di livello inferiore 
         // sia poi un problema in caso di fault di un nodo intermedio
         // es. nodo livello2 che passa al coord e nel mezzo ho un nodo di livello1 scarico
          
         if (tmpLevBuffer[0]==0xFF)
         {
          // salvo il livello 
          saveAdd[0]=HI_UINT16(msa_DevShortAddr); 
         
          writeEEProm(saveAdd,MSA_SAVE_LEVEL,1);
         }
        
         incrementAstro(SLEEP_TIME);
         
         break;
        }

      SleepAstro();
    
    }
    return events ^ MSA_SLEEP_EVENT;
  }


  if (events & MSA_READ_EVENT)
  {
     if (!msa_IsStarted)
     {
       
         #if defined RS485
          ENABLE_PERIPH(); // attivo
          restoreTime();
         #endif
          
         // leggo la batteria e salvo lo stato carica
         ENABLE_SENSTH(); halWait(100); 
         readBattery=getBattery();
         DISABLE_SENSTH();
          
        //check se sono in condizioni buone per fare lo SCAN ( se non lo sono sleep e reset )
        checkBattery(1);
     }
     else {
      checkOsc();
      readAstronomic(readBuffer,0x0000,8);
                        
      msa_Data2[0] = readBuffer[0];
      msa_Data2[1] = readBuffer[1];
      msa_Data2[2] = readBuffer[2];
      msa_Data2[3] = readBuffer[3];
      msa_Data2[4] = readBuffer[4];
      msa_Data2[5] = readBuffer[5];
      msa_Data2[6] = readBuffer[6];
      msa_Data2[7] = readBuffer[7];
      msa_Data2[8] = BcdToHex(minWakeUpOld);
      msa_Data2[9] = HI_UINT16(minToSleep);
      msa_Data2[10] = LO_UINT16(minToSleep);

      setMessageTipology(1);
      
      MSA_McpsDataReq((uint8*)msa_Data2,
                        11,
                        TRUE,
                        assocShortAddress,
                        TRUE);
     }
    
    return events ^ MSA_READ_EVENT;
  }


  /* Evento di trasmissione dati verso nodo padre*/
  if (events & MSA_SEND_EVENT)
  {
     setMessageTipology(0);
    
     txToCoord=TRUE;
      
      if(lenOfSon==0) { lenOfMyPack=preparePacket(); } //per fare prove senza sensor lenOfMyPack=falsePreparePacket();}
      else lenOfMyPack=0;

      /* se il pacchetto del nodo padre � maggiore di 83 non chiamo la funzione readData in quanto non riuscirei a creare un pacchetto cos� grande
      occorre introdurre un controllo nel caso in cui il nodo padre crei un pacchetto maggiore di 104 byte*/
      if ( (lenOfMyPack<61) && (msa_State!=MSA_START_STATE) ) 
      {
       lenOfSon=readData(lenOfMyPack);
      }
      else lenOfSon=lenOfMyPack;  
      
      if (lenOfSon>0){

       setMessageTipology(1);
            
       MSA_McpsDataReq((uint8*)msa_Data1,
                        lenOfSon,
                        TRUE,
                        fatherAdd,
                        TRUE);  
      }
      else  
      {
        if (msa_State==MSA_START_STATE) // se sono all'avvio segnalo anche il livello del segnale
        {  
           // LQI - Led rosso mi indica lo stato del link ( 100ms - 5000ms ) 
           HalLedSet (HAL_LED_2, HAL_LED_MODE_ON);
           if (LQI<=20) halWaitms(100);
           else if (LQI>20 && LQI<=80) halWaitms(300);
           else if (LQI>80 && LQI<=160) halWaitms(600);
           else if (LQI>160)  halWaitms(1000);
           HalLedSet (HAL_LED_2, HAL_LED_MODE_OFF);
        }
        osal_start_timerEx(MSA_TaskId, MSA_SLEEP_EVENT,10);
      }
      
  return events ^ MSA_SEND_EVENT;
  }

  if (events & MSA_SETUP_EVENT)
  {
      
    //dopo l'invio vado in sleep
    osal_start_timerEx(MSA_TaskId, MSA_SLEEP_EVENT, 200);
                          
    msa_Data2[0] = LO_UINT16(minToSleep);
    msa_Data2[1] = HI_UINT16(minToSleep);
    
    setMessageTipology(1);
        
    MSA_McpsDataReq((uint8*) &msa_Data2,
                        0x02,
                        TRUE,
                        0xFFFF,
                        FALSE);
   
  return events ^ MSA_SETUP_EVENT;
  }

  if (events & MSA_BROAD_EVENT)
  {
    setMessageTipology(0);
  
    msa_State=MSA_FAULT_STATE; // fault tolerance state

    osal_start_timerEx(MSA_TaskId, MSA_FAULT_TOL_EVENT, SEND_DELAY);
    
    msa_Data1[0]=HI_UINT16(fatherAdd);
    msa_Data1[1]=minWakeUpOld+MSA_WAKE_PERIOD;
   
    setMessageTipology(1);
    
    MSA_McpsDataReq( (uint8*)msa_Data1,
                        0x02,
                        TRUE,
                        0xFFFF,
                        TRUE);
  
    return events ^ MSA_BROAD_EVENT;
  }


  if (events & MSA_RESP_BROAD_EVENT)
  {
      msa_Data1[0]=0xFF;
      msa_Data1[1]=0xFF;
      msa_Data1[2] = LO_UINT16(minToSleep);  // aggiunto da EMA 1/1/2015
      msa_Data1[3] = HI_UINT16(minToSleep);  // aggiunto da EMA 1/1/2015 e lunghezza 4

     //se il nodo da cui ho ricevuto il pacchetto dati � presente nell'array msa_DeviceRecord non invio data ora (perch� a lui un pacchetto � gi� stato inviato) e faccio ripartire timer per invio dati in memoria verso il nodo padre
      osalTimerValue=calcOsalTimerPeriod();
      
      osal_start_timerEx(MSA_TaskId, MSA_SEND_EVENT, osalTimerValue);

      //dopo aver ricevuto il pacchetto con la richiesta di un padre se rientro nei requisiti riaccendo la radio e invio la risposta
     setMessageTipology(1);

      MSA_McpsDataReq( (uint8*)msa_Data1,
                          0x04,
                          TRUE,
                          broadShortAddress,
                          FALSE);

      return events ^ MSA_RESP_BROAD_EVENT;
  }

  if (events & MSA_FAULT_TOL_EVENT)
  {
    setMessageTipology(0);
    
    // Spostato dall'evento MSA_BROAD_EVENT che chiamo prima del ricerca padere 
    txToCoord=TRUE;

    dataLen=preparePacket();

    rssi=0;
    
    saveData();
    
    msa_State=MSA_SEND_STATE;

    if (stato==0) osal_start_timerEx(MSA_TaskId, MSA_SLEEP_EVENT, 10);
    else 
    {
     faultTolerance();

     osal_start_timerEx(MSA_TaskId, MSA_SLEEP_EVENT, 100);
    }
    return events ^ MSA_FAULT_TOL_EVENT;
  }
  
#if defined RS485
  if (events & MSA_RS485_READ_EVENT)
  {  
    if (msa_State==MSA_BEFORE_STATE)
    {
     setMessageTipology(0); 
      
     readSensorBefore();  
    }
    else 
    {
     DISABLE_SENSTH(); // ora posso disattivare i sensori ( nella fase modem non mi servono on )
     halWait(10);
      
     setMessageTipology(1);
  
     HalLedSet (HAL_LED_1, HAL_LED_MODE_ON);  // led ON - device acceso
     
     // timer da max_beacon_time per consentire associazione
     osal_start_timerEx(MSA_TaskId,MSA_READ_EVENT,MAX_BEACON_TIME);
     
     MSA_ScanReq(MAC_SCAN_ACTIVE, 2);
    }
    
    return events ^ MSA_RS485_READ_EVENT;
  }
#endif  

  //da valutare se possibile farlo utilizzando evento SEND_EVENT
  //Matteo 15/04/2014
  if (events & MSA_REPEAT_DATA_EVENT)
  {
    if (lenOfSon>0)
    { 
      MSA_McpsDataReq((uint8*)msa_Data1,
                        lenOfSon,
                        TRUE,
                        fatherAdd,
                        TRUE);
    }
    else  osal_start_timerEx(MSA_TaskId, MSA_SLEEP_EVENT,100);
    
   return events ^ MSA_REPEAT_DATA_EVENT;
  }
  
   return 0;
}



/**************************************************************************************************
 *
 * @fn          MAC_CbackEvent
 *
 * @brief       This callback function sends MAC events to the application.
 *              The application must implement this function.  A typical
 *              implementation of this function would allocate an OSAL message,
 *              copy the event parameters to the message, and send the message
 *              to the application's OSAL event handler.  This function may be
 *              executed from task or interrupt context and therefore must
 *              be reentrant.
 *
 * @param       pData - Pointer to parameters structure.
 *
 * @return      None.
 *
 **************************************************************************************************/
void MAC_CbackEvent(macCbackEvent_t *pData)
{

  macCbackEvent_t *pMsg = NULL;

  uint8 len = msa_cbackSizeTable[pData->hdr.event];

  switch (pData->hdr.event)
  {
      case MAC_MLME_BEACON_NOTIFY_IND:

      len += sizeof(macPanDesc_t) + pData->beaconNotifyInd.sduLength +
             MAC_PEND_FIELDS_LEN(pData->beaconNotifyInd.pendAddrSpec);
      if ((pMsg = (macCbackEvent_t *) osal_msg_allocate(len)) != NULL)
      {
        /* Copy data over and pass them up */
        osal_memcpy(pMsg, pData, sizeof(macMlmeBeaconNotifyInd_t));
        pMsg->beaconNotifyInd.pPanDesc = (macPanDesc_t *) ((uint8 *) pMsg + sizeof(macMlmeBeaconNotifyInd_t));
        osal_memcpy(pMsg->beaconNotifyInd.pPanDesc, pData->beaconNotifyInd.pPanDesc, sizeof(macPanDesc_t));
        pMsg->beaconNotifyInd.pSdu = (uint8 *) (pMsg->beaconNotifyInd.pPanDesc + 1);
        osal_memcpy(pMsg->beaconNotifyInd.pSdu, pData->beaconNotifyInd.pSdu, pData->beaconNotifyInd.sduLength);
      }
      break;

    case MAC_MCPS_DATA_IND:
      pMsg = pData;
      break;

    default:
      if ((pMsg = (macCbackEvent_t *) osal_msg_allocate(len)) != NULL)
      {
        osal_memcpy(pMsg, pData, len);
      }
      break;
  }

  if (pMsg != NULL)
  {
    osal_msg_send(MSA_TaskId, (uint8 *) pMsg);
  }
}

/**************************************************************************************************
 *
 * @fn      MAC_CbackCheckPending
 *
 * @brief   Returns the number of indirect messages pending in the application
 *
 * @param   None
 *
 * @return  Number of indirect messages in the application
 *
 **************************************************************************************************/
uint8 MAC_CbackCheckPending(void)
{
  return (0);
}

/**************************************************************************************************
 *
 * @fn      MSA_DeviceStartup()
 *
 * @brief   Update the timer per tick
 *
 * @param   beaconEnable: TRUE/FALSE
 *
 * @return  None
 *
 **************************************************************************************************/
void MSA_DeviceStartup()
{
  /* Setup MAC_BEACON_PAYLOAD_LENGTH */
  MAC_MlmeSetReq(MAC_BEACON_PAYLOAD_LENGTH, &msa_BeaconPayloadLen);

  /* Setup MAC_BEACON_PAYLOAD */
  MAC_MlmeSetReq(MAC_BEACON_PAYLOAD, &msa_BeaconPayload);

  /* Setup PAN ID */
  MAC_MlmeSetReq(MAC_PAN_ID, &msa_PanId);

  /* This device is setup for Direct Message */
  if (msa_IsDirectMsg)
    MAC_MlmeSetReq(MAC_RX_ON_WHEN_IDLE, &msa_MACTrue);
  else
    MAC_MlmeSetReq(MAC_RX_ON_WHEN_IDLE, &msa_MACFalse);

  /* Setup Coordinator short address */
  MAC_MlmeSetReq(MAC_COORD_SHORT_ADDRESS, &msa_AssociateReq.coordAddress.addr.shortAddr);

  uint8 maxRetries=0;
  MAC_MlmeSetReq(MAC_MAX_FRAME_RETRIES, &maxRetries);

#ifdef MAC_SECURITY
  /* Setup Coordinator short address for security */
  MAC_MlmeSetSecurityReq(MAC_PAN_COORD_SHORT_ADDRESS, &msa_AssociateReq.coordAddress.addr.shortAddr);
#endif /* MAC_SECURITY */

  if (msa_BeaconOrder != 15)
  {
    /* Setup Beacon Order */
    MAC_MlmeSetReq(MAC_BEACON_ORDER, &msa_BeaconOrder);

    /* Setup Super Frame Order */
    MAC_MlmeSetReq(MAC_SUPERFRAME_ORDER, &msa_SuperFrameOrder);

   }

  MAC_MlmeSetReq(MAC_ASSOCIATION_PERMIT, &msa_MACFalse);

  /* Power saving */
  MSA_PowerMgr (MSA_PWR_MGMT_ENABLED);

}

/**************************************************************************************************
 *
 * @fn      MSA_AssociateReq()
 *
 * @brief
 *
 * @param    None
 *
 * @return  None
 *
 **************************************************************************************************/
void MSA_AssociateReq(void)
{
  
  MAC_MlmeAssociateReq(&msa_AssociateReq);

}

/**************************************************************************************************
 *
 * @fn      MSA_AssociateRsp()
 *
 * @brief   This routine is called by Associate_Ind inorder to return the response to the device
 *
 * @param   pMsg - pointer to the structure recieved by MAC_MLME_ASSOCIATE_IND
 *
 * @return  None
 *
 **************************************************************************************************/
void MSA_AssociateRsp(macCbackEvent_t* pMsg)
{
  assocExtAdd[0]=pMsg->associateInd.deviceAddress[0];
  assocExtAdd[1]=pMsg->associateInd.deviceAddress[1];

  assocShortAddress = BUILD_UINT16(assocExtAdd[0], HI_UINT16(msa_DevShortAddr)+1);

#ifdef MAC_SECURITY
  uint16 panID;

  /* Add device to device table for security */
  MAC_MlmeGetReq(MAC_PAN_ID, &panID);
  MSA_SecuredDeviceTableUpdate(panID, assocShortAddress,
                               pMsg->associateInd.deviceAddress,
                               &msa_NumOfSecuredDevices);
#endif /* MAC_SECURITY */

  /* Fill in association respond message */
  sAddrExtCpy(msa_AssociateRsp.deviceAddress, pMsg->associateInd.deviceAddress);
  msa_AssociateRsp.assocShortAddress = assocShortAddress;
  msa_AssociateRsp.status = MAC_SUCCESS;
  msa_AssociateRsp.sec.securityLevel = MAC_SEC_LEVEL_NONE;
  setMessageTipology(1);
  /* Call Associate Response */
  MAC_MlmeAssociateRsp(&msa_AssociateRsp);
  
}

/**************************************************************************************************
 *
 * @fn      MSA_McpsDataReq()
 *
 * @brief   This routine calls the Data Request
 *
 * @param   data       - contains the data that would be sent
 *          dataLength - length of the data that will be sent
 *
 * @return  None
 *
 **************************************************************************************************/
void MSA_McpsDataReq(uint8* data, uint8 dataLength, bool directMsg, uint16 dstShortAddr, bool ack)
{
  macMcpsDataReq_t  *pData;
  static uint8 handle = 0;
 
  MAC_McpsPurgeReq(handle);
  
  if ((pData = MAC_McpsDataAlloc(dataLength, msa_securityLevel, msa_keyIdMode)) != NULL)
  {
    
    pData->mac.srcAddrMode = SADDR_MODE_SHORT;
    pData->mac.dstAddr.addrMode = SADDR_MODE_SHORT;
    pData->mac.dstAddr.addr.shortAddr = dstShortAddr;
    pData->mac.dstPanId = msa_PanId;
    pData->mac.msduHandle = handle++;
   
    //matteo 19 Settembre
    // pData->mac.txOptions = /*MAC_TXOPTION_NO_CNF;*/ MAC_TXOPTION_ACK;
    if (ack) pData->mac.txOptions = MAC_TXOPTION_ACK;
    else pData->mac.txOptions = MAC_TXOPTION_NO_CNF;

    /* MAC security parameters */
    osal_memcpy( pData->sec.keySource, msa_keySource, MAC_KEY_SOURCE_MAX_LEN );
    pData->sec.securityLevel = msa_securityLevel;
    pData->sec.keyIdMode = msa_keyIdMode;
    pData->sec.keyIndex = msa_keyIndex;

     /* Copy data */
    osal_memcpy (pData->msdu.p, data, dataLength);
   
    /* Send out data request */
    MAC_McpsDataReq(pData);
    
  }

}

/**************************************************************************************************
 *
 * @fn      MacSampelApp_ScanReq()
 *
 * @brief   Performs active scan on specified channel
 *
 * @param   None
 *
 * @return  None
 *
 **************************************************************************************************/
void MSA_ScanReq(uint8 scanType, uint8 scanDuration)
{
  macMlmeScanReq_t scanReq;

  /* Fill in information for scan request structure */
  scanReq.scanChannels = (uint32) 1 << MSA_MAC_CHANNEL;
  scanReq.scanType = scanType;
  scanReq.scanDuration = scanDuration;
  scanReq.maxResults = MSA_MAC_MAX_RESULTS;
  scanReq.permitJoining = MSA_EBR_PERMITJOINING;
  scanReq.linkQuality = MSA_EBR_LINKQUALITY;
  scanReq.percentFilter = MSA_EBR_PERCENTFILTER;
  scanReq.result.pPanDescriptor = msa_PanDesc;
  osal_memset(&scanReq.sec, 0, sizeof(macSec_t));

  /* Call scan request */
  MAC_MlmeScanReq(&scanReq);
}

/**************************************************************************************************
 *
 * @fn      MSA_BeaconPayLoadCheck()
 *
 * @brief   Check if the beacon comes from MSA but not zigbee
 *
 * @param   pSdu - pointer to the buffer that contains the data
 *
 * @return  TRUE or FALSE
 *
 **************************************************************************************************/
bool MSA_BeaconPayLoadCheck(uint8* pSdu)
{
  uint8 i = 0;
  for (i=0; i<msa_BeaconPayloadLen; i++)
  {
    if (pSdu[i] != msa_BeaconPayload[i])
    {
      return FALSE;
    }
  }

  return TRUE;
}
/**************************************************************************************************
 *
 * @fn      MSA_HandleKeys
 *
 * @brief   Callback service for keys
 *
 * @param   keys  - keys that were pressed
 *          state - shifted
 *
 * @return  void
 *
 **************************************************************************************************/
void MSA_HandleKeys(uint8 keys, uint8 shift)
{
  uint8 WDBuff[1];
  uint8 stopBit[1];
  
#if defined WINETAQ
  if ( keys & HAL_KEY_SW_6 )
  {
    /* wait for 32Mhz stable clock */
    while ((CLKCONSTA & 0x40) != 0);
    {
     asm("NOP");
    }
    /* CC2530 Workaround */
     macMcuTimer2OverflowWorkaround();

     InputOutputWakeUp();
         
     ENABLE_PERIPH(); // attivo
     halWait(25); // tps stabile 
     
        //**** CHECK WD E OF ****//
    readAstronomic(WDBuff,0x0F,0x01); // leggo il flag 
    halWait(5);
    writeAstronomic(0x00,0x0F,1);  // scrivo a zero il registro
   
    if ( (WDBuff[0] & 0x80) || (WDBuff[0] & 0x20) || (WDBuff[0] & 0x04) ) // valuto se � scattato il WD o TF o OF
    {
      if (WDBuff[0] & 0x04) // procedura per OF - riabilito oscillatore
      {  
        // impostare lo STOP a 1 e poi a 0
        stopBit[0]=0x80;
        writeAstronomic(stopBit,0x01,0x01);
        halWait(5);
        stopBit[0]=0x00;
        writeAstronomic(stopBit,0x01,0x01);
        halWait(5);
        writeAstronomic(stopBit,0x0F,1); // setto a zero OF 
      }
      
      err=8; 
      resetBoard();   // resetto il device
    }
    // **************//
        
     restoreTime();  // ripristino i registri esterni per leggere ora e data

     checkOsc();
     readAstronomic(clearBuff,0x000F,0x01); // leggo il flag

     // setto a zero AFE e ABE per sicurezza e imposto il WD
     writeAstronomic(watchDogAbeAfe,0x09,2);
     
    if (msa_State==MSA_BEFORE_STATE)
    {
      readAstronomic(matchBuffer,0x0000,8);
   
      osal_memcpy(activeBuffer,matchBuffer,8);
     
      #if defined RS485
        ENABLE_SENSTH(); 
        halWaitSec(6); 
        reqSent=0x01; // 1� convertitore
        osal_start_timerEx ( RS485_TaskID, NEXT_CMD_RS485, RS485_IDLE);
      #else
        
        setMessageTipology(0);
    
        readSensorBefore();   
     
      #endif
    }
    else if (msa_State==MSA_SETUP_STATE)
    { 
      // sono di livello 1 e ho ricevuto orario dal padre     
      if (fatherAdd==MSA_COORD_SHORT_ADDR && sinch==TRUE)
      {
       osal_start_timerEx(MSA_TaskId, MSA_SETUP_EVENT, 200 + MAC_RandomByte() ); 
      } // sono di livello 1 e nno ho ricevuto orario dal padre
      else if (fatherAdd==MSA_COORD_SHORT_ADDR )
      {
       osal_start_timerEx(MSA_TaskId, MSA_SLEEP_EVENT, 10); 
      }
      else // non sono di livello 1
      { // ogni volta allargo di 1000 
        osal_start_timerEx(MSA_TaskId, MSA_SLEEP_EVENT, 1000);
      }
      //accendo la radio per poter ricevere il pacchetto con il nuovo tempo di risveglio
      setMessageTipology(1);  
    }
    else
    {
      checkOsc();
      readAstronomic(matchBuffer,0x0000,8);
   
      // Emanuele - 15/12/12 copio in activebuffer perch� poi matchbuffer viene manipolato
      osal_memcpy(activeBuffer,matchBuffer,8);
    
      for(int i=0; i<MSA_MAX_DEVICE_NUM; i++) msa_DeviceRecord[i] = 0xFFFF;
      msa_NumOfDevices=0;

      msa_State=MSA_ASSOC_STATE;
      lenOfSon=0;
      byteEpromTx=0;
      txToCoord=FALSE;
      assocShortAddress=0xFFFF;
      minWakeUpOld=minWakeUp;
      enableAssociation=TRUE;
      sinch=FALSE;
        
      /*Attenzione la windows start viene aggiornata ogni volta che un padre non riceve 
      il pacchetto dal figlio o non va a buon fine la trasmissione verso il proprio padre */
      if (time) time=FALSE;
      else
      {
        //fare delle misure pi� precise e legare i numeri alla rete 
        if (windowStar>MSA_WINDOW_STAR) windowStar=0;
        else windowStar=windowStar+MSA_WS_INC; 
      }

      #if defined BOOST_FORO  // ho necessit� di 4-5 secondi per avere stabilit�
        ENABLE_SENSTH();
      #endif
       
       if (saveAdd[0]<=MAX_DEPTH)
       MAC_MlmeSetReq(MAC_ASSOCIATION_PERMIT, &msa_MACTrue);
       
       osal_start_timerEx(MSA_TaskId, MSA_SLEEP_EVENT, MSA_ASSOCIATION_PERIOD);

       if(!orphan) setMessageTipology(1);       
    }

  }
#endif

}

void readSensorBefore(void)
{
    #if defined FLORA_1 || SENTEK_90 || SENTEK_60 || defined SENTEK_30 || defined SENTEK_30_B || defined SENTEK_10A || defined SENTEK_10B || defined SENTEK_10C || defined SENTEK_10D || defined SENTEK_10E || defined SENTEK_10F || defined DEC5TE_P04 
     uint8 saveP0Inp=P0INP;
    #endif
     
       #if defined PIOGGIA
     if (longReadAttiny85(ATTINY85_RAIN_R,pioggia,0x00,0x02)==0) // lettura errata
      { 
        pioggia[0]=0xFF;
        pioggia[1]=0xFF; 
      }
      halWait(20); // margine 
     #endif
      
    #if defined VENTO
      if (longReadAttiny85(ATTINY85_WIND_R,vento,0x00,0x0A)==0) // lettura errata
      {
       vento[0]=0xFF;
       vento[1]=0xFF;
       vento[2]=0x7F; // valore non possibile
      }
       halWait(20); // margine
    #endif
      
     #if defined PRESS0
      if (longReadAttiny85(ATTINY85_PRESS0_R,press0,0x00,0x05)==0) // lettura errata
      {
       //1 byte : Stato linea (DD -> flusso non rilevato , CC-> flusso rilevato) 
       //2-3-4 byte : tempo trascorso ( fine - start) in millisecondi
       press0[0]=0xFF;
      }
       halWait(20); // margine
    #endif
     
    #if defined PRESS1
      if (longReadAttiny85(ATTINY85_PRESS1_R,press1,0x00,0x05)==0) // lettura errata
      {
       //1 byte : Stato linea (DD -> flusso non rilevato , CC-> flusso rilevato) 
       //2-3-4 byte : tempo trascorso ( fine - start) in millisecondi
       press1[0]=0xFF;
      }
       halWait(20); // margine
    #endif
      
     #if defined PRESS2
      if (longReadAttiny85(ATTINY85_PRESS2_R,press2,0x00,0x05)==0) // lettura errata
      {
       //1 byte : Stato linea (DD -> flusso non rilevato , CC-> flusso rilevato) 
       //2-3-4 byte : tempo trascorso ( fine - start) in millisecondi
       press2[0]=0xFF;
      }
       halWait(20); // margine
     #endif
      
     #if defined PRESS3
      if (longReadAttiny85(ATTINY85_PRESS3_R,press3,0x00,0x05)==0) // lettura errata
      {
       //1 byte : Stato linea (DD -> flusso non rilevato , CC-> flusso rilevato) 
       //2-3-4 byte : tempo trascorso ( fine - start) in millisecondi
       press3[0]=0xFF;
      }
       halWait(20); // margine
    #endif
    
    #if defined FLOW0
      if (longReadAttiny85(ATTINY85_FLOW0_R,flow0,0x00,0x08)==0) // lettura errata
      {
       //1 byte : Stato linea (DD -> flusso non rilevato , CC-> flusso rilevato) 
       //2-3-4 byte : tempo trascorso ( fine - start) in millisecondi
       flow0[0]=0xFF;
      }
       halWait(20); // margine
    #endif
      
    #if defined FLOW1
      if (longReadAttiny85(ATTINY85_FLOW1_R,flow1,0x00,0x08)==0) // lettura errata
      {
       //1 byte : Stato linea (DD -> flusso non rilevato , CC-> flusso rilevato) 
       //2-3-4 byte : tempo trascorso ( fine - start) in millisecondi
       flow1[0]=0xFF;
      }
       halWait(20); // margine
    #endif
   
    ENABLE_SENSTH(); //attivo alimentazione dei sensori
    halWaitms(SENSOR_ON_TIME); // attendo SENSOR_ON_TIME per essere stabile 100ms per SM100
    
    DISABLE_PERIPH();
    halWait(10);
            
    #if defined FLORA_1 || SENTEK_60 || defined SENTEK_30 || defined SENTEK_30_B || defined SENTEK_10A || defined SENTEK_10B || defined SENTEK_10C || defined SENTEK_10D || defined SENTEK_10E || defined SENTEK_10F || defined DEC5TE_P04 
      saveP0Inp=P0INP;  
    #endif
      
    #if defined FLORA_1
     IO_DIR_PORT_PIN( 0, 4, IO_OUT );
     P0_4=0;
     P0INP=0x10;   
     halWaitms(20);   
     clearSdiBuffers();
     sdiRetries=askSdi(0x00,sdiBuffer,SDI_DEFAULT_TRIM);
     if (sdiRetries!=0x00)
     {
       FloraParser(sdiBuffer,floraResBuffer);    
       asm("NOP");
     }
     else memset(floraResBuffer,0x00,sizeof(floraResBuffer));
    #endif    
   
    
    #if defined DEC5TE_P04
     IO_DIR_PORT_PIN( 0, 4, IO_OUT );
     P0_4=0;
     P0INP=0x10;   
     halWaitms(10);   
     resetSdiBuffer(sdiBuffer);
     sdiRetries=askSdi(sdiDevice,sdiBuffer,SDI_DEFAULT_TRIM);
  
      if (sdiRetries!=0x00)
      {
        HalLedSet (HAL_LED_1, HAL_LED_MODE_OFF);
        HalLedSet (HAL_LED_2, HAL_LED_MODE_OFF);
        Te5Parser(sdiBuffer,resBuffer);
      }
      else{
      HalLedSet (HAL_LED_1, HAL_LED_MODE_ON);
      HalLedSet (HAL_LED_2, HAL_LED_MODE_ON);
      resBuffer[0]=0x00;
      resBuffer[1]=0x00;
      resBuffer[2]=0x00;
      }
     #endif
      
      
     #if defined SENTEK_90
     IO_DIR_PORT_PIN( 0, 4, IO_OUT );
     P0_4=0;
     P0INP=0x10;   
     halWaitms(10);   
     clearSdiBuffers();
     
      sdiRetries=askSentek(0x1E,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x09,SOIL);
      asm("NOP");
      if (sdiRetries!=0x00) 
      {
        ParserSentek(sdiBuffer,resSentekHum,sizeof(sdiBuffer),0x01,SOIL);
        asm("NOP");
        ParserSentek(sdiBuffer2,resSentekHum,sizeof(sdiBuffer2),0x02,SOIL);
        asm("NOP");
        ParserSentek(sdiBuffer3,resSentekHumB,sizeof(sdiBuffer3),0x01,SOIL);
        asm("NOP");
      }
       
 #ifdef SENTEK_90_SAL
      halWaitms(200);
      sdiRetries=0x03;
      clearSdiBuffers();
           
      sdiRetries=askSentek(0x1E,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x09,SAL);
      asm("NOP");
      if (sdiRetries!=0x00) 
      {
        ParserSentek(sdiBuffer,resSentekSal,sizeof(sdiBuffer),0x01,SAL);
        asm("NOP");
        ParserSentek(sdiBuffer2,resSentekSal,sizeof(sdiBuffer2),0x02,SAL);
        asm("NOP");
        ParserSentek(sdiBuffer3,resSentekSalB,sizeof(sdiBuffer3),0x01,SAL);
        asm("NOP");
      }
#endif
      halWaitms(200);
      sdiRetries=0x03;
      clearSdiBuffers();
      
      sdiRetries=askSentek(0x1E,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x09,TEMP);
      if (sdiRetries!=0x00) 
      {
        ParserSentek(sdiBuffer,resSentekTemp,sizeof(sdiBuffer),0x01,TEMP);
        asm("NOP");
        ParserSentek(sdiBuffer2,resSentekTemp,sizeof(sdiBuffer2),0x02,TEMP);
        asm("NOP");
        ParserSentek(sdiBuffer3,resSentekTempB,sizeof(sdiBuffer3),0x01,TEMP);
        asm("NOP");    
      }
      asm("NOP");
    #endif 

    
    #if defined SENTEK_60
     IO_DIR_PORT_PIN( 0, 4, IO_OUT );
     P0_4=0;
     P0INP=0x10;   
     halWaitms(10);
     clearSdiBuffers();
     
      sdiRetries=askSentek(0x31,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x06,SOIL);
      asm("NOP");
      if (sdiRetries!=0x00) 
      {
        ParserSentek(sdiBuffer,resSentekHum,sizeof(sdiBuffer),0x01,SOIL);
        asm("NOP");
        ParserSentek(sdiBuffer2,resSentekHum,sizeof(sdiBuffer2),0x02,SOIL);
        asm("NOP");
      }
      else{
        memset(resSentekHum,0x00,sizeof(resSentekHum));
      }
       
  #ifdef SENTEK_60_SAL
      halWaitms(200);
      sdiRetries=0x03;
      clearSdiBuffers();
      
      sdiRetries=askSentek(0x31,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x06,SAL);
      if (sdiRetries!=0x00) 
      {
        ParserSentek(sdiBuffer,resSentekSal,sizeof(sdiBuffer),0x01,SAL);
        ParserSentek(sdiBuffer2,resSentekSal,sizeof(sdiBuffer2),0x02,SAL);
      }
       else{
        memset(resSentekSal,0x00,sizeof(resSentekSal));
      }
     
     #endif
      halWaitms(200);
      sdiRetries=0x03;
      clearSdiBuffers();
     
      sdiRetries=askSentek(0x31,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x06,TEMP);
      if (sdiRetries!=0x00) 
      {
        ParserSentek(sdiBuffer,resSentekTemp,sizeof(sdiBuffer),0x01,TEMP);
        ParserSentek(sdiBuffer2,resSentekTemp,sizeof(sdiBuffer2),0x02,TEMP);
      }
       else{
        memset(resSentekTemp,0x00,sizeof(resSentekTemp));
      }
     
    #endif 
      
      #if defined SENTEK_60_B  //INDIRIZZO 0x41 'q'
     halWaitms(200);
     IO_DIR_PORT_PIN( 0, 4, IO_OUT );
     P0_4=0;
     P0INP=0x10;   
     halWaitms(10);   
     clearSdiBuffers();
     
      sdiRetries=askSentek(0x41,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x06,SOIL);
      if (sdiRetries!=0x00) 
      {
        // sfrutto il resSentekHumB dedicato alle profondit� 7-12 che nella 60 non ci sono in modo da non creare un altro buffer
        ParserSentek(sdiBuffer,resSentekHumB,sizeof(sdiBuffer),0x01,SOIL);
        ParserSentek(sdiBuffer2,resSentekHumB,sizeof(sdiBuffer2),0x02,SOIL);
      }
      else{
        memset(resSentekHumB,0x00,sizeof(resSentekHumB));
      }
      
    #ifdef SENTEK_60_SAL_B
      halWaitms(200);
      sdiRetries=0x03;
      clearSdiBuffers();
      
      sdiRetries=askSentek(0x41,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x06,SAL);
      if (sdiRetries!=0x00) 
      {
        ParserSentek(sdiBuffer,resSentekSalB,sizeof(sdiBuffer),0x01,SAL);
        ParserSentek(sdiBuffer2,resSentekSalB,sizeof(sdiBuffer2),0x02,SAL);
      }
      else memset(resSentekSal,0x00,sizeof(resSentekSal));
    #endif
      
      halWaitms(200);
      sdiRetries=0x03;
      clearSdiBuffers();
      
      sdiRetries=askSentek(0x41,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x06,TEMP);
      if (sdiRetries!=0x00) 
      {
        ParserSentek(sdiBuffer,resSentekTempB,sizeof(sdiBuffer),0x01,TEMP);
        ParserSentek(sdiBuffer2,resSentekTempB,sizeof(sdiBuffer2),0x02,TEMP);
      }
       else{
        memset(resSentekTempB,0x00,sizeof(resSentekTempB));
      }
    #endif  
      
  #if defined SENTEK_30  //INDIRIZZO 0x32 'b'
     IO_DIR_PORT_PIN( 0, 4, IO_OUT );
     P0_4=0;
     P0INP=0x10;   
     halWaitms(20);   
     sdiRetries=0x03;
     clearSdiBuffers();
     
      sdiRetries=askSentek(0x32,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x03,SOIL);
      if (sdiRetries!=0x00) 
      {
        ParserSentek(sdiBuffer,resSentekHum,sizeof(sdiBuffer),0x01,SOIL);
      } 
      else{
        memset(resSentekHum,0x00,sizeof(resSentekHum));
      }
      
      #ifdef SENTEK_30_SAL
      halWaitms(200);
      sdiRetries=0x03;
      clearSdiBuffers();
      
      sdiRetries=askSentek(0x32,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x03,SAL);
      if (sdiRetries!=0x00) 
      {
        ParserSentek(sdiBuffer,resSentekSal,sizeof(sdiBuffer),0x01,SAL);
      }
      else memset(resSentekSal,0x00,sizeof(resSentekSal));
     #endif
     
      halWaitms(200);
      sdiRetries=0x03;
      clearSdiBuffers();
      
      sdiRetries=askSentek(0x32,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x03,TEMP);
      if (sdiRetries!=0x00) 
      {
        ParserSentek(sdiBuffer,resSentekTemp,sizeof(sdiBuffer),0x01,TEMP);
      }
       else{
        memset(resSentekTemp,0x00,sizeof(resSentekTemp));
      }
    #endif
              
   
  #if defined SENTEK_30_B  //INDIRIZZO 0x12 'B'
      IO_DIR_PORT_PIN( 0, 4, IO_OUT );
     P0_4=0;
     P0INP=0x10;   
     halWaitms(10);   
     clearSdiBuffers();
     
      sdiRetries=askSentek(0x12,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x03,SOIL);
      asm("NOP");
      if (sdiRetries!=0x00) 
      {
        ParserSentek(sdiBuffer,resSentekHumB,sizeof(sdiBuffer),0x01,SOIL);
      }
      else memset(resSentekHumB,0x00,sizeof(resSentekHumB));
      
     #ifdef SENTEK_30_B_SAL
      halWaitms(200);
      sdiRetries=0x03;
      clearSdiBuffers();
      
      sdiRetries=askSentek(0x32,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x03,SAL);
      if (sdiRetries!=0x00) 
      {
        ParserSentek(sdiBuffer,resSentekSalB,sizeof(sdiBuffer),0x01,SAL);
      }
      else memset(resSentekSalB,0x00,sizeof(resSentekSalB));
     #endif
      
      halWaitms(200);
      sdiRetries=0x03;
      clearSdiBuffers();
      
      sdiRetries=askSentek(0x12,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x03,TEMP);
      if (sdiRetries!=0x00) 
      {
        ParserSentek(sdiBuffer,resSentekTempB,sizeof(sdiBuffer),0x01,TEMP);
      }
      else memset(resSentekTempB,0x00,sizeof(resSentekTempB));
      halWaitSec(1);
    #endif  
      
      
#if defined SENTEK_10A  //INDIRIZZO 0x33 'c'  
    IO_DIR_PORT_PIN( 0, 4, IO_OUT );
    P0_4=0;
    P0INP=0x10;   
    halWaitms(10);    
    clearSdiBuffers();
     
    sdiRetries=askSentek(0x33,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x01,SOIL);
    asm("NOP");
    if (sdiRetries!=0x00) 
    {
      SenParser10(sdiBuffer,resSentek10A,0x01);
      asm("NOP"); 
    }
    else{
      memset(resSentek10A,0x00,sizeof(resSentek10A));
    }
    
    halWaitms(200);
    sdiRetries=0x03;
    clearSdiBuffers();
     
    sdiRetries=askSentek(0x33,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x01,TEMP);
    if (sdiRetries!=0x00) 
    {
      SenParser10(sdiBuffer,resSentek10A,0x03);
      asm("NOP");
    }
    else{
      memset(resSentek10A,0x00,sizeof(resSentek10A));
    }

    asm("NOP");
  #endif
    
#if defined SENTEK_10B  //INDIRIZZO 0x34 'd'  
    IO_DIR_PORT_PIN( 0, 4, IO_OUT );
     P0_4=0;
     P0INP=0x10;   
     halWaitms(10);     
     clearSdiBuffers();
     
    sdiRetries=askSentek(0x34,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x01,SOIL);
    asm("NOP");
    if (sdiRetries!=0x00) 
    {
      SenParser10(sdiBuffer,resSentek10B,0x01);
      asm("NOP"); 
    }
    else{
      memset(resSentek10B,0x00,sizeof(resSentek10B));
    }

    
    halWaitms(200);
    sdiRetries=0x03;
    clearSdiBuffers(); 
    
    sdiRetries=askSentek(0x34,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x01,TEMP);
    if (sdiRetries!=0x00) 
    {
      SenParser10(sdiBuffer,resSentek10B,0x03);
      asm("NOP");
    }
    else{
      memset(resSentek10B,0x00,sizeof(resSentek10B));
    }

    asm("NOP");
  #endif
    
    #if defined SENTEK_10C  //INDIRIZZO 0x35 'e'  
     IO_DIR_PORT_PIN( 0, 4, IO_OUT );
     P0_4=0;
     P0INP=0x10;   
     halWaitms(10);      
    clearSdiBuffers();
     
    sdiRetries=askSentek(0x35,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x01,SOIL);
    asm("NOP");
    if (sdiRetries!=0x00) 
    {
      SenParser10(sdiBuffer,resSentek10C,0x01);
      asm("NOP"); 
    }
    else{
      memset(resSentek10C,0x00,sizeof(resSentek10C));
    }

    
    halWaitms(200);
    sdiRetries=0x03;
    clearSdiBuffers();
     
    sdiRetries=askSentek(0x35,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x01,TEMP);
    if (sdiRetries!=0x00) 
    {
      SenParser10(sdiBuffer,resSentek10C,0x03);
      asm("NOP");
    }
    else{
      memset(resSentek10C,0x00,sizeof(resSentek10C));
    }

    asm("NOP");
  #endif
  
    #if defined SENTEK_10D  //INDIRIZZO 0x36 'f'  
    IO_DIR_PORT_PIN( 0, 4, IO_OUT );
     P0_4=0;
     P0INP=0x10;   
     halWaitms(10);      
    clearSdiBuffers();
     
    sdiRetries=askSentek(0x36,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x01,SOIL);
    asm("NOP");
    if (sdiRetries!=0x00) 
    {
      SenParser10(sdiBuffer,resSentek10D,0x01);
      asm("NOP"); 
    }
    else{
      memset(resSentek10D,0x00,sizeof(resSentek10D));
    }

    
    halWaitms(200);
    sdiRetries=0x03;
    clearSdiBuffers();
     
    sdiRetries=askSentek(0x36,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x01,TEMP);
    if (sdiRetries!=0x00) 
    {
      SenParser10(sdiBuffer,resSentek10D,0x03);
      asm("NOP");
    }
    else{
      memset(resSentek10D,0x00,sizeof(resSentek10D));
    }

    asm("NOP");
  #endif

    #if defined SENTEK_10E  //INDIRIZZO 0x37 'g'  
    IO_DIR_PORT_PIN( 0, 4, IO_OUT );
     P0_4=0;
     P0INP=0x10;   
     halWaitms(10);     
    clearSdiBuffers();
     
    sdiRetries=askSentek(0x37,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x01,SOIL);
    asm("NOP");
    if (sdiRetries!=0x00) 
    {
      SenParser10(sdiBuffer,resSentek10E,0x01);
      asm("NOP"); 
    }
    else{
      memset(resSentek10E,0x00,sizeof(resSentek10E));
    }

    
    halWaitms(200);
    sdiRetries=0x03;
    clearSdiBuffers();
     
    sdiRetries=askSentek(0x37,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x01,TEMP);
    if (sdiRetries!=0x00) 
    {
      SenParser10(sdiBuffer,resSentek10E,0x03);
      asm("NOP");
    }
    else{
      memset(resSentek10E,0x00,sizeof(resSentek10E));
    }

    asm("NOP");
  #endif

    #if defined SENTEK_10F  //INDIRIZZO 0x38 'h'  
   IO_DIR_PORT_PIN( 0, 4, IO_OUT );
     P0_4=0;
     P0INP=0x10;   
     halWaitms(10);    
    clearSdiBuffers();
     
    sdiRetries=askSentek(0x38,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x01,SOIL);
    asm("NOP");
    if (sdiRetries!=0x00) 
    {
      SenParser10(sdiBuffer,resSentek10F,0x01);
      asm("NOP"); 
    }
    else{
      memset(resSentek10F,0x00,sizeof(resSentek10F));
    }
    
    halWaitms(200);
    sdiRetries=0x03;
    clearSdiBuffers();
     
    sdiRetries=askSentek(0x38,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x01,TEMP);
    if (sdiRetries!=0x00) 
    {
      SenParser10(sdiBuffer,resSentek10F,0x03);
      asm("NOP");
    }
    else{
      memset(resSentek10F,0x00,sizeof(resSentek10F));
    }

    asm("NOP");
  #endif

      
      
    #if defined MPS6_P04
     IO_DIR_PORT_PIN( 0, 4, IO_OUT );
     P0_4=0;
     P0INP=0x10;   
     halWaitms(10);
      resetSdiBuffer(sdiBuffer);
      sdiRetries=askSdi(sdiDevice2,sdiBuffer,SDI_DEFAULT_TRIM);
      if (sdiRetries!=0x00)
      {
        Mps6Parser(sdiBuffer,resBuffer2);
      }
      else{
      resBuffer2[0]=0x00;
      resBuffer2[1]=0x00;
      resBuffer2[2]=0x00;
      }
    #endif
      
     #if defined DECGS3_P04
    IO_DIR_PORT_PIN( 0, 4, IO_OUT );
     P0_4=0;
     P0INP=0x10;   
     halWaitms(10);   
      
      resetSdiBuffer(sdiBuffer);
      sdiRetries=askSdi(sdiDevice3,sdiBuffer,SDI_DEFAULT_TRIM);
      if (sdiRetries!=0x00)
      {
        HalLedSet (HAL_LED_2, HAL_LED_MODE_OFF);  
        Gs3Parser(sdiBuffer,resBuffer3);
      }
      else{
      HalLedSet (HAL_LED_2, HAL_LED_MODE_ON);
      resBuffer3[0]=0x00;
      resBuffer3[1]=0x00;
      resBuffer3[2]=0x00;
      }
    #endif
   
    #if defined ACC_TDT
   IO_DIR_PORT_PIN( 0, 4, IO_OUT );
     P0_4=0;
     P0INP=0x10;   
     halWaitms(10);      
      resetSdiBuffer(sdiBuffer);
      sdiRetries=askSdi(sdiDevice4,sdiBuffer,SDI_DEFAULT_TRIM);
      if (sdiRetries!=0x00)
      {
        AcctParser(sdiBuffer,accResBuffer1);
      }
    #endif  
      
    #if defined ACC_TDTP
     IO_DIR_PORT_PIN( 0, 4, IO_OUT );
     P0_4=0;
     P0INP=0x10;   
     halWaitms(10);     
      resetSdiBuffer(sdiBuffer);
      sdiRetries=askSdi(sdiDevice5,sdiBuffer,SDI_DEFAULT_TRIM);
      if (sdiRetries!=0x00)
      {
        AcctParser(sdiBuffer,accResBuffer2);        
      }
    #endif  
      
    #if defined DECCDT
     IO_DIR_PORT_PIN( 0, 4, IO_OUT );
     P0_4=0;
     P0INP=0x10;   
     halWaitms(10);          
      resetSdiBuffer(sdiBuffer);
      sdiRetries=askSdi(sdiDevice6,sdiBuffer,SDI_DEFAULT_TRIM);
      if (sdiRetries!=0x00)
      {
        HalLedSet (HAL_LED_1, HAL_LED_MODE_OFF);
        CDTParser(sdiBuffer,resBuffer4);        
      }
      else{
      HalLedSet (HAL_LED_1, HAL_LED_MODE_ON);
      resBuffer4[0]=0;
      resBuffer4[1]=0;
      resBuffer4[2]=0;
      }
    #endif 
        
    #if defined DEC5TE_P04 || defined SENTEK_10A || defined SENTEK_10B || defined SENTEK_10C || defined SENTEK_10D || defined SENTEK_10E || defined SENTEK_10F || defined SENTEK_60 || defined SENTEK_90 || defined SENTEK_30 || defined SENTEK_30_B || defined MPS6_P04 || defined DECGS3_P04 || defined ACC_TDT || defined ACC_TDTP || defined DECCDT
     P0INP=saveP0Inp;
     IO_DIR_PORT_PIN( 0, 4, IO_IN);
    #endif
    
     ENABLE_PERIPH();
     halWait(100);
     
     DISABLE_SENSTH();
     halWait(100);
     
     restoreTime();
   
   if (!firstStart)
   {  
    osal_start_timerEx(MSA_TaskId, MSA_SLEEP_EVENT, 100); 
   }
  
}


void SleepAstro()
{

    /* set RX off */
    setMessageTipology(0);
    
    #if defined WINETTX
     DISABLE_MODEM();  // disattivo modem
     halWait(100);
    #endif 

   #if defined WINETAQ
       DISABLE_SENSTH();  // disattiva sensori
       halWait(100);
       DISABLE_PERIPH();  // disattiva le periferiche
       halWait(100);
       HAL_TURN_OFF_LED1(); // spegni led  
       halWait(100);
       #if !defined DEBUG
        HAL_TURN_OFF_LED2(); // spegni led 
        halWait(100);
      #endif
       InputOutputSleep(); // porte configurate per basso consumo
       P0IFG=0x00;
     #endif
       
     sleepTime=(minToSleep+ST_TIME_RESET);// ST_TIME_RESET minuti oltre il risveglio 
     Sleep(sleepTime);  // sleep timer per risveglio di sicurezza  e reset
}



void Sleep(uint16 timeout)
{
    /* Sleep in modalit� PM2 */
    halSleepSetTimer(SLEEP_MS_TO_320US(SLEEP_TIME_RPT));

    /* Interrupt Sleep Timer Enable and Interrupt on */
    SLEEP_TIMER_ENABLE_INT();
    
    SLEEPCMD &= ~PMODE; /* clear mode bits */
    SLEEPCMD |= 0x02;   /* set mode bits   */
    
    while (!(STLOAD & LDRDY));
    {
      PCON = PCON_IDLE;
      asm("NOP");
    }  

  if (IEN0 & 0x20) // se lo sleep timer � attivo ovvero sono entrato qui non da risveglio astronomico
  {  
    if(sleepTime==0) WakeUp();
        else {  sleepTime--;   Sleep(sleepTime);  }  // ricorsivo finch� time=0 e quindi Wake up
  }
}

void WakeUp()
{
  // sicuramente astronomico � partito quindi resetto in modo che ripeschi l'orario corretto
  // non serve impostare fake orario perch� tanto il router lo ripesca dal padre/coordinatore 
  resetBoard(); 

}


void saveData()
{
  uint8 lBuffer[2];
  uint16 totLenght1=0;
  uint8 retControl;
   
   /* leggo quanti dati ho in memoria - lunghezza dei dati */
   retControl=readEEProm(lBuffer,MSA_READ_LENGHT,2);
   
   if (retControl>0)
   {  
    totLenght1=BUILD_UINT16(lBuffer[0], lBuffer[1]);
    
    if(totLenght1==0xFFFF) totLenght1=0;

    //setto l'indirzzo di partenza per la scrittura
    writeAddress=MSA_READ_ADDRESS+totLenght1;

    /*Se i dati salvati superano il limite della EEPROM li riscrivo (tutti quelli in memoria vengono persi)*/
    if( (writeAddress+dataLen) > (PROM_SIZE-4) )  // -4: SIZE-1byte level-2byte lenght
      writeAddress=MSA_READ_ADDRESS;

    //salva in EEPROM i dati ricevuti
    retControl=writeEEProm(msa_Data1,writeAddress,dataLen);
    
    if (retControl>0)
    {
      // aggiorna address se retControl>0
      writeAddress=writeAddress+dataLen;

      totLenght1=writeAddress-MSA_READ_ADDRESS;

      lBuffer[0]=LO_UINT16(totLenght1);
      lBuffer[1]=HI_UINT16(totLenght1);
      retControl=writeEEProm(lBuffer,MSA_READ_LENGHT,2);  // salvo la lunghezza dei dati
    }
   }
 }

uint8 readData(uint16 len){

    uint8 lBuffer[2];
    uint8 nSensor=0;
    uint16 parLen=0;
    //uint32 idSens=0;
    uint16 totLenght=0;
    uint8 retValue;
     
   retValue=readEEProm(lBuffer,MSA_READ_LENGHT,2);
   
   totLenght=BUILD_UINT16(lBuffer[0], lBuffer[1])-byteEpromTx;
   
   //se in memoria non ho nulla o ho letto male procedo con l'invio del pacchetto dati
   if(totLenght!=0xFFFF && totLenght>0 && retValue>0){

    if(totLenght+len>MSA_PACKET_LENGTH) 
    {
       
     readEEProm(tempDataBuffer,MSA_READ_ADDRESS+byteEpromTx,MSA_PACKET_LENGTH-len);
     
     while (parLen<(MSA_PACKET_LENGTH-MSA_PACKET_HEADER-len)) 
     {
 
         //idSens=BUILD_UINT32(tempDataBuffer[12+parLen],tempDataBuffer[13+parLen],tempDataBuffer[14+parLen],tempDataBuffer[15+parLen]);
       
         nSensor=tempDataBuffer[16+parLen];
         //nSensor = countSens(idSens);
  
         //variabile da verificare il valore 61 � corretto? vecchio valore=100
         if ( (parLen+((nSensor*2)+MSA_PACKET_HEADER)+len) <= (MSA_PACKET_LENGTH) )
         {
          parLen=parLen+(nSensor*2)+MSA_PACKET_HEADER;        
         }
         else break;
       }
     }
     else 
     {
       readEEProm(tempDataBuffer,MSA_READ_ADDRESS+byteEpromTx,totLenght);
       
       parLen=totLenght;
     }

       for(uint8 i=0; i<parLen; i++){
         msa_Data1[i+len]=tempDataBuffer[i]; 
       }
     
        return (uint8)(len+parLen);
    }
   
   return (uint8)len; 
  
   
   /*if(len>0)
    ripeti=0;
    
    if (ripeti<3)
    {
    for(uint8 i=0; i<50; i++){
         msa_Data1[i+len]=i*1; 
       }
    ripeti++;
    if (len>0) len=len+50;
    else len=50;
    } 
    
  return (uint8)(len);  */
    
}

void updateEpromAddres(uint16 updateLen){

   uint8 lBuffer[2];
   uint16 totLenght2=0;
   uint16 wrAddress=0;
   uint8 retControl;
  
   readEEProm(lBuffer,MSA_READ_LENGHT,2);
   totLenght2=BUILD_UINT16(lBuffer[0], lBuffer[1]);
   
   if(totLenght2==0xFFFF) totLenght2=updateLen;
   else totLenght2=totLenght2-updateLen;

   lBuffer[0]=LO_UINT16(totLenght2);
   lBuffer[1]=HI_UINT16(totLenght2);
   retControl=writeEEProm(lBuffer,MSA_READ_LENGHT,2);
  
   if (retControl>0)
   {
    if(updateLen>0){
     
      while(totLenght2>0){

      if(totLenght2>=100){
            readEEProm(tempDataBuffer,MSA_READ_ADDRESS+updateLen+wrAddress,100);
            writeEEProm(tempDataBuffer,MSA_READ_ADDRESS+wrAddress,100);
            wrAddress=wrAddress+100;
            totLenght2=totLenght2-100;
            }
       else {
           readEEProm(tempDataBuffer,MSA_READ_ADDRESS+wrAddress+updateLen,totLenght2);
           writeEEProm(tempDataBuffer,MSA_READ_ADDRESS+wrAddress,totLenght2);
           totLenght2=0;
           }
        }
      }
    }
   
}

/*uint8 countSens(uint32 id){

  uint8 numSens=0;
  uint32 maskVar=0x00000001;

        for(uint8 j=0; j<32; j++){
          if ((id & maskVar) == maskVar)
          {
            numSens++;
          }
        maskVar=maskVar<<1;
        }
        return numSens;

}*/

void InputOutputSleep()
{
   #if defined HAL_PA_LNA
    P2_0=0x00;
   #endif
  
   /* I2C */
    IO_DIR_PORT_PIN( CLK_PORT, CLK_PIN, IO_OUT );
    IO_DIR_PORT_PIN( DATA_PORT, DATA_PIN, IO_OUT );
    SDA = 0x01;
    SCL = 0x01;

    #if defined RS485
    /* Porta 0 seriale->basso consumo */
    // Configure Port P0 pins for I/O function:
    P0SEL &= ~0x0C;
    #endif
      
    P0SEL=0x00;
    IO_DIR_PORT_PIN( 0, 7, IO_OUT );
    IO_DIR_PORT_PIN( 0, 5, IO_OUT );
    IO_DIR_PORT_PIN( 0, 4, IO_OUT ); 
    #if defined RS485
    IO_DIR_PORT_PIN( 0, 3, IO_IN );
    IO_DIR_PORT_PIN( 0, 2, IO_IN );
    #else
    IO_DIR_PORT_PIN( 0, 3, IO_OUT );
    IO_DIR_PORT_PIN( 0, 2, IO_OUT); 
    #endif
    
    // P0.1 � interrupt astronomico
    IO_DIR_PORT_PIN( 0, 0, IO_OUT );

    P0_7=0x00;
    P0_5=0x00;
    P0_4=0x00;
    #if !defined RS485
     P0_3=0x00;
     P0_2=0x00;
    #endif
    P0_0=0x00;

   #if defined CREA
    // SE CREA INSERIRE QUESTO
    DISABLE_MODEM();  // disattivo modem - CREA
    halWait(5);
   #else
    P0_6=0x00; 
   #endif
   
}


void InputOutputWakeUp()
{

      /* ADC */
    IO_DIR_PORT_PIN( 0, 7, IO_IN );
    IO_DIR_PORT_PIN( 0, 6, IO_IN );
    IO_DIR_PORT_PIN( 0, 5, IO_IN );
    IO_DIR_PORT_PIN( 0, 4, IO_IN );
    IO_DIR_PORT_PIN( 0, 3, IO_IN );
    IO_DIR_PORT_PIN( 0, 2, IO_IN );
    IO_DIR_PORT_PIN( 0, 1, IO_IN );
    IO_DIR_PORT_PIN( 0, 0, IO_IN );
 
    #if defined HAL_PA_LNA
     P2_0=0x01;
    #endif
     
      #if defined RS485
        // Configure UART1 for Alternative 2 => Port P0 (PERCFG.U0CFG = 0)
        PERCFG |=0x02;
        // pull-up su tutta la porta per semplicit�
        P0INP &= ~0xFF;
        // Configure relevant Port P0 pins for peripheral function:
        P0SEL |= 0x0C;
    #endif
     

}

void readIEEEAddressFromNv()
{
  uint8 aExtendedAddress[8];

  // Attempt to read the extended address from the designated location in the Info Page.
  osal_memcpy(aExtendedAddress, (uint8 *)(P_INFOPAGE+HAL_INFOP_IEEE_OSET), HAL_FLASH_IEEE_SIZE);

  // write the Extended Address in the NV Memory space - First IEEE
  (void)osal_nv_write(ZCD_NV_EXTADDR, 0, HAL_FLASH_IEEE_SIZE, &aExtendedAddress);

  // leggo dalla memoria il Second IEEE impostato con SMARTRF Programmer
  HalFlashRead(HAL_FLASH_IEEE_PAGE, HAL_FLASH_IEEE_OSET, aSecExtendedAddress, Z_EXTADDR_LEN);

  // Set the MAC extended address - uso il secondary address
  MAC_MlmeSetReq(MAC_EXTENDED_ADDRESS, &aSecExtendedAddress);
  
  // check indirizzo nodo
  //if ( (aSecExtendedAddress[0]!=0x29) && (aSecExtendedAddress[1]!=0x1E) && (aSecExtendedAddress[2]!=0x04) && (aSecExtendedAddress[3]!=0x6A) )
  //  resetBoard();
  

}

void incrementAstro(uint16 min)
{
   if ( (msa_State==MSA_SETUP_STATE) || (msa_State==MSA_BEFORE_STATE) )
   {
         if(min>1) // devo decrementare di 1 minuto perch� mi devo svegliare 1 minuto prima
         {
            min--;
         }
      
         //Matteo 20/04/2014 pesare se mettere un controllo per evitare che essendo molto vicino a SEC_To_SYNCH setti un tempo che poi risulter� passato
         //nel caso in cui arrivi qui con 59 secondi potrebbe decrementare e poi andare in sleep con 00 e quindi saltare una trasmissione
         if ( (astroBuffer[1]<SEC_TO_SYNCH) && (min>0) ) min--;
    }
    else
    {
      if(min>1) // devo decrementare di 1 minuto perch� mi devo svegliare 1 minuto prima
      {
        min--;
      }
      
     // se < vuol dire che ho sforato il minuto successivo al risveglio
     if ( (astroBuffer[1]<minWakeUpOld) && (min>0) )
        min--; //caso uguale non considerato perch� passa pi� di un secondo

    }

  // ulteriore check di sicurezza - non pu� svegliarsi il minuto stesso rischio che setti BEFORE_STATE con 00 
  // sul minuto gia passato
  if ( (msa_State==MSA_BEFORE_STATE) && (min==0) ) min=SLEEP_TIME;
  
  min=astroBuffer[2]+min;
  
  // setto WD con 10 minuti di margine dal risveglio
  #if defined WD_AQ22
  if (msa_State==MSA_SEND_STATE) // lo devo settare solo nel SEND_STATE 
  {
    setWDTime(min+WD_TIME_RESET);
  }
  #endif

  defineDate(min);

  if (msa_State==MSA_SETUP_STATE) newSecondi=BcdToHex(SEC_TO_SYNCH);
  else newSecondi=BcdToHex(minWakeUpOld);

  setAstroInterrupt(newSecondi,newMinuti,newOre,newGiorno,newMese);
  
}

#if defined WD_AQ22
void setWDTime(uint16 minute)
{
  newSecondi=BcdToHex(0x00);
  
  defineDate(minute);
    
  setWDInterrupt(newSecondi,newMinuti,newOre,newGiorno,newMese);

}
#endif

void defineDate(uint16 min){

  newAnno=astroBuffer[7]; //anno
  newMese=astroBuffer[6];  // mese
  newGiorno=astroBuffer[5]; // giorno
  newdayWeek=astroBuffer[4];
  newOre=astroBuffer[3]; // ore
 // newMinuti=astroBuffer[2]; // minuti letti + minuti di sleep
  newSecondi=astroBuffer[1]; // secondi
  newDecCent=astroBuffer[0];


  while (newSecondi>=60)
     {
      newSecondi=newSecondi-60;
      min++;
     }

   while (min>=60)
     {
      min=min-60;
      newOre++;
     }
    newMinuti=LO_UINT16(min);

   while (newOre>=24)
     {
      newOre=newOre-24;
      newGiorno++;
     }

   checkMeseUp(newMese);
   
  //check errori critici su settaggio data/ora
  if ( (newSecondi>59) || (newMinuti>59) || (newOre>23) || (newGiorno<1) || (newGiorno>31) || (newMese<1) || (newMese>12) )
  {
   resetBoard();  
  } 

  newSecondi=BcdToHex(newSecondi);;
  newMinuti=BcdToHex(newMinuti);
  newOre=BcdToHex(newOre);
  newGiorno=BcdToHex(newGiorno);
  newMese=BcdToHex(newMese);
  newAnno=BcdToHex(newAnno);
  
        
}


void checkMeseUp(uint8 mese)
{
 switch (newMese)     // switch sul mese
      {
      case 1:
        if (newGiorno>31)
        {  // giorno data
         newGiorno=newGiorno-31;
         newMese=2;
        }
       break;

      case 2:
        if ((newAnno%4)==0) // anno  bisestile
        {
         if (newGiorno>29)
          {
            newGiorno=newGiorno-29;
            newMese=3;
           }
        }
        else if (newGiorno>28)
        {
          newGiorno=newGiorno-28;
          newMese=3;
        }
        break;

      case 3:
        if (newGiorno>31)
        {
          newGiorno=newGiorno-31;
          newMese=4;
        }
        break;

       case 4:
         if (newGiorno>30)
         {
          newGiorno=newGiorno-30;
          newMese=5;
         }
        break;

       case 5:
        if (newGiorno>31)
        {
          newGiorno=newGiorno-31;
          newMese=6;
        }
        break;

        case 6:
        if (newGiorno>30)
        {
          newGiorno=newGiorno-30;
          newMese=7;
        }
        break;

       case 7:
        if (newGiorno>31)
        {
          newGiorno=newGiorno-31;
          newMese=8;
        }
        break;

       case 8:
       if (newGiorno>31)
       {
          newGiorno=newGiorno-31;
          newMese=9;
       }
       break;

       case 9:
       if (newGiorno>30)
       {
          newGiorno=newGiorno-30;
          newMese=10;
       }
       break;

       case 10:
       if (newGiorno>31)
       {
          newGiorno=newGiorno-31;
          newMese=11;
       }
       break;

      case 11:
      if (newGiorno>30)
      {
          newGiorno=newGiorno-30;
          newMese=12;
      }
      break;

      case 12:
      if (newGiorno>31)
      {
          newGiorno=newGiorno-31;
          newMese=1;
          newAnno++;
      }
      break;
    }
}

void checkMeseDown(uint8 mese)
{
 switch (newMese)     // switch sul mese
      {
      case 1:
        if (newGiorno==1)
        {  // giorno data
         newGiorno=31;
         newMese=12;
	 newAnno--;
        }
        else newGiorno--;
      break;

      case 2:
        if (newGiorno==1)
        {
          newGiorno=31;
          newMese=1;
        }
        else newGiorno--;
        break;


      case 3:
        if ((newAnno%4)==0) // anno  bisestile
        {
         if (newGiorno==1)
          {
            newGiorno=29; // giorno 1
            newMese=2;
           }
          else newGiorno--;
        }
        else if (newGiorno==1)
        {
          newGiorno=28; // giorno 1
          newMese=2;
        }
        else newGiorno--;
        break;

       case 4:
         if (newGiorno==1)
         {
          newGiorno=31;
          newMese=3;
         }
         else newGiorno--;
        break;

       case 5:
        if (newGiorno==1)
        {
          newGiorno=30;
          newMese=4;
        }
        else newGiorno--;
        break;

        case 6:
        if (newGiorno==1)
        {
          newGiorno=31;
          newMese=5;
        }
        else newGiorno--;
        break;

       case 7:
        if (newGiorno==1)
        {
          newGiorno=30;
          newMese=6;
        }
        else newGiorno--;
        break;

       case 8:
       if (newGiorno==1)
       {
          newGiorno=31;
          newMese=7;
       }
       else newGiorno--;
       break;

       case 9:
       if (newGiorno==1)
       {
          newGiorno=31;
          newMese=8;
       }
       else newGiorno--;
       break;

       case 10:
       if (newGiorno==1)
       {
          newGiorno=30;
          newMese=9;
       }
       else newGiorno--;
       break;

      case 11:
      if (newGiorno==1)
      {
          newGiorno=31;
          newMese=10;
      }
      else newGiorno--;
      break;

      case 12:
      if (newGiorno==1)
      {
          newGiorno=30;
          newMese=11;
      }
      else newGiorno--;
      break;
    }
}

uint8 preparePacket(void){

  uint32 IDSensor, maskVar;
  uint8 lenMeas;
  uint8 a=17;
  
  /*Byte 0 e 1 ID Nodo*/
        msa_Data1[0]=HI_UINT16(msa_DevShortAddr);
        msa_Data1[1]=LO_UINT16(msa_DevShortAddr);

        // leggo dalla memoria il Second IEEE impostato con SMARTRF Programmer
        HalFlashRead(HAL_FLASH_IEEE_PAGE, HAL_FLASH_IEEE_OSET, aSecExtendedAddress, Z_EXTADDR_LEN);

        msa_Data1[2]=aSecExtendedAddress[1];
        msa_Data1[3]=aSecExtendedAddress[0];

        msa_Data1[4]=HI_UINT16(fatherAdd);
        msa_Data1[5]=LO_UINT16(fatherAdd);

         // check oscillatore 
        checkOsc();
         
        /*6 byte relativi alla date-time della lettura*/
        readAstronomic(readBuffer,0x0000,8);
    
        msa_Data1[6]=readBuffer[5];
        msa_Data1[7]=readBuffer[6];
        msa_Data1[8]=readBuffer[7];
        msa_Data1[9]=readBuffer[3];
        msa_Data1[10]=readBuffer[2];
        msa_Data1[11]=readBuffer[1]; 

        /*Recupero 3� e 4� byte dell'indirizzo esteso per definire il tipo di sensori collegati*/
        IDSensor=BUILD_UINT32(aSecExtendedAddress[2],aSecExtendedAddress[3],aSecExtendedAddress[4],aSecExtendedAddress[5]);
        
        msa_Data1[12]=BREAK_UINT32(IDSensor,3);
        msa_Data1[13]=BREAK_UINT32(IDSensor,2);
        msa_Data1[14]=BREAK_UINT32(IDSensor,1);
        msa_Data1[15]=BREAK_UINT32(IDSensor,0);
       
     #if !defined BOOST_FORO
      ENABLE_SENSTH(); // attivo i sensori
      halWaitms(SENSOR_ON_TIME); // attendo SENSOR_ON_TIME per essere stabile 300ms per SM100
     #endif  
     
      /*Lettura batteria da fare prima di disabilitare le periferiche*/
     readBattery=getBattery();
    
     DISABLE_PERIPH(); // disattivo le periferiche per leggere correttamente le porte sensori
     halWait(10);  
        
     maskVar=0x00000001;

     for(uint8 j=0; j<32; j++)
     {
       if ((IDSensor & maskVar) == maskVar)
       {
        asm("NOP");
        
        lenMeas=readSensor(maskVar);
        
        for (uint8 ind=0;ind<lenMeas;ind++)
        {
         msa_Data1[a]=HI_UINT16(readSensorM[ind]);
         msa_Data1[a+1]=LO_UINT16(readSensorM[ind]);
        
         a=a+2;
        }
       
       }

        maskVar=maskVar<<1;
        
        if (maskVar>0x00010000) // se ho superato la P0_5 attivo il WD 
         if (P1_0!=0x01) // verifico di non averlo gia attivato
         {
           ENABLE_PERIPH();
           halWait(20);
           // Set Watch-dog procedure
           readAstronomic(clearBuff,0x0F,1); // pulisco eventuali flag pendenti
           writeAstronomic(watchDogAbeAfe,0x09,2);
          }
        }
     
         msa_Data1[16]=numSensTot; //salvo il numero dei sensori (letto solo in firstStart)
         firstStart=FALSE;
         
         DISABLE_SENSTH(); // ora posso disattivare i sensori
         halWait(10);
        
         msa_Data1[a]=HI_UINT16(readBattery);
         msa_Data1[a+1]=LO_UINT16(readBattery);

         msa_Data1[a+2]=0x00; //rssi field
         a=a+3;
         
         #if defined SPI
          HalLedSet (HAL_LED_1, HAL_LED_MODE_ON);  // led verde ON che indica scrittura
          SpiInit();
          sdOk=WriteToSDCARD(msa_Data1,a); // li scrivo sulla SD card
          resetTempBufferSD();// converto in char 
          HalLedSet (HAL_LED_1, HAL_LED_MODE_OFF);  // led rosso OFF - fine scrittura 
         #endif

        return a;
}

#if defined SPI
 void resetTempBufferSD()
 {
  for (uint8 i=0;i<200;i++) {
  tempBufferSD[i]=0x00;
  }
}

uint8 errSD=0;
uint8 lenSD=0;
bool WriteToSDCARD(uint8* buffer,uint8 len){
   
    lenSD=(len*2);
    saveP2DIR=P2DIR;
    P2DIR = 0x07; // imposto la por
     
    BytesToCharSD(buffer,lenSD,0x0000);
    errSD=0;
    
    //Faccio Mount della SDQui hocard
    rc = f_mount(&Fatfs,"",0);
    
    // Se ok apro il file
    if(rc==FR_OK){
    rc = f_open(&Fil, "log.txt",FA_OPEN_APPEND | FA_WRITE);
    errSD=1;
    }
    
    // Se ok scrivo nel file 
    if(rc==FR_OK){
    rc = f_write(&Fil,tempBufferSD,lenSD,&bytes_count);
    errSD=3;
    }
    
    // Se ok chiudo il file
    if(rc==FR_OK){
    rc = f_close(&Fil);
    errSD=4;
    }
    
    // Se ok smonto la sd
    if(rc==FR_OK){
    rc = f_mount(0, "", 0);  
    errSD=5;
    }
    
    //Se ok tutto � andato a buon fine
    if(rc==FR_OK){
    errSD=6;
    }
    
    //Riconfiguro le porte come erano in orgine 
    P2DIR=saveP2DIR;
    
    if(rc!=FR_OK){
        HalLedSet (HAL_LED_2, HAL_LED_MODE_ON);
        sdOk=false;
    }
    else{
       sdcount++;
       HalLedSet (HAL_LED_2, HAL_LED_MODE_OFF);
       sdOk=true;
    }
  return sdOk;
}
  
void BytesToCharSD(uint8* buffer,uint16 lenghtBytes,uint8 startAdd)
{
  uint8 k=0;
  char tempSupportSD[3]={0x00,0x00,0x00}; // init + terminatore
  uint8 supportBufferSD[MSA_PACKET_LENGTH*2]; // qui non mi serve SIZE perch� da 80bytes faccio 160char

  for(uint8 i=startAdd;i<(startAdd+lenghtBytes);i++)
  {
   tempSupportSD[0]=HI_UINT8(buffer[i]);
   tempSupportSD[1]=LO_UINT8(buffer[i]);

   if ((tempSupportSD[0]!=0x0A) && (tempSupportSD[0]!=0x0B) && (tempSupportSD[0]!=0x0C)
   && (tempSupportSD[0]!=0x0D) && (tempSupportSD[0]!=0x0E) && (tempSupportSD[0]!=0x0F))
   supportBufferSD[k++]=tempSupportSD[0]+48;  // per avere il codice ascii del valore memorizzato
   else supportBufferSD[k++]=tempSupportSD[0]+55;
  
   if ((tempSupportSD[1]!=0x0A) && (tempSupportSD[1]!=0x0B) && (tempSupportSD[1]!=0x0C)
   && (tempSupportSD[1]!=0x0D) && (tempSupportSD[1]!=0x0E) && (tempSupportSD[1]!=0x0F))
   supportBufferSD[k++]=tempSupportSD[1]+48;  // per avere il codice ascii del valore memorizzato
   else supportBufferSD[k++]=tempSupportSD[1]+55;
  
  }
  
  osal_memcpy(&tempBufferSD[startAdd],supportBufferSD,k);
  asm("NOP");
}
#endif

void faultTolerance(){

  uint8 tempTime=0;

  switch(stato){
  
  case 0: 
  
   if (maxFatherRetries>0)  
   {
     maxFatherRetries--;
     orphan=TRUE;
   }
   else stato=1;
   return;
  break;
 
  case 1:

     orphan=TRUE;
     
     if(fatherAdd!=MSA_COORD_SHORT_ADDR)
     {
              fatherAdd=BUILD_UINT16(0x00,HI_UINT16(msa_DevShortAddr)-1);

              tempTime=minWakeUp-(60-(HI_UINT16(msa_DevShortAddr)*MSA_WAKE_PERIOD));
              countLevel=1+(tempTime/MSA_WAKE_PERIOD); //Matteo 27/08/2014
            
              stato=2;
      }
      else{
            fatherAdd=MSA_COORD_SHORT_ADDR;

            stato=3;
          }
     return;
   break;

  case 2:
        countLevel--;
      
       if(countLevel==0)
       {

         countLevel=HI_UINT16(msa_DevShortAddr)-HI_UINT16(fatherAdd)+1;

         if (countLevel!=HI_UINT16(msa_DevShortAddr))
           fatherAdd=BUILD_UINT16(0x00,HI_UINT16(msa_DevShortAddr));

          diffMinWakeUp=diffMinWakeUp+MSA_WAKE_PERIOD;
          
          minWakeUp=minWakeUp+MSA_WAKE_PERIOD;
         
       }

      if(HI_UINT16(fatherAdd)>1) { 
        fatherAdd=BUILD_UINT16(0x00,HI_UINT16(fatherAdd)-1);  
        stato=2;
      }
      else {
        fatherAdd=MSA_COORD_SHORT_ADDR; 
        stato=3;
      }
      return;
   break;

  case 3: 
  stato=0;
  maxFatherRetries=MAX_FATHER_RETRIES;

  if (minToSleep <= SLEEP_TIME) { minToSleep=SLEEP_TIME; }
   else minToSleep=(minToSleep/2);
  
  //ripristino la ricerca sul padre originale (coincide con il coord se sono di livello1 )
  fatherAdd=msa_CoordShortAddr;
  
  if(fatherAdd!=MSA_COORD_SHORT_ADDR) //Matteo 6 Novembre
  {
     minWakeUp=minWakeUp-(HI_UINT16(msa_CoordShortAddr)*MSA_WAKE_PERIOD);
     minWakeUpOld=minWakeUp;
  }
  return;
 break;

 default: 
    break;

  }
}

uint16 calcOsalTimerPeriod(){
  
   uint16 diff;
   uint8 tempSec, tempDecCent;
   
    checkOsc();
    readAstronomic(readBuffer,0x0000,8);
    astroConv(readBuffer);
    
    tempSec=astroBuffer[1];
    tempDecCent=astroBuffer[0];
   
    astroConv(matchBuffer);
    
    if(tempSec<astroBuffer[1]) {tempSec=tempSec+60;}
    if(tempDecCent<astroBuffer[0]) {tempDecCent=tempDecCent+100; tempSec--;}

    tempSec=tempSec-astroBuffer[1];
    tempDecCent=tempDecCent-astroBuffer[0];

    if(((tempSec*100)+tempDecCent)>=(winRx/10)) return WIN_GUARD_RX; //Matteo 28/08/2014 vecchio valore 1000
    else {
           diff=(winRx/10)-((tempSec*100)+tempDecCent);
           
           if (diff<(WIN_GUARD_RX/10)) return WIN_GUARD_RX;
           else return diff*10;
    }

  }

  bool timePermitAssociation(){
    
   uint16 diffAssoc;
   uint8 tempSec, tempDecCent;
   bool r;

    setMessageTipology(0);

    checkOsc();
    readAstronomic(readBuffer,0x0000,8);
    astroConv(readBuffer);

    tempSec=astroBuffer[1];
    tempDecCent=astroBuffer[0];

    astroConv(matchBuffer); //orologio letto nell'HandleKeys

    if(tempSec<astroBuffer[1]) {tempSec=tempSec+60;}
    if(tempDecCent<astroBuffer[0]) {tempDecCent=tempDecCent+100; tempSec--;}

    tempSec=tempSec-astroBuffer[1];
    tempDecCent=tempDecCent-astroBuffer[0];

    diffAssoc=(MSA_ASSOCIATION_PERIOD/10)-((tempSec*100)+tempDecCent);
              if (diffAssoc<50) r=FALSE;
              else r=TRUE;
                             
     return r;

  }


/*********************************************************************
 * @fn      setMessageTipology(uint8 val)
 *
 * @brief   Funzione fondamentale per lo sleep ed il corretto wake up
 *
 *
 * @param   val
 *          FALSE viene impostato l'indirect message
 *          permettendo al nodo di entrare il PM2 correttamente.
 *          Non riceve per� i dati perch� RX � OFF.
 *          TRUE viene impostato il direct message
 *          permettendo al nodo di ricevere i dati.
 *
 * @return  none
 */

void setMessageTipology(uint8 val)
{ 
  /* This device is setup for Direct Message */
  if (val)
  { 
   #if defined HAL_PA_LNA
     P1_2=0x00;
     P1_3=0x00;
     macRadioTurnOnPower();
     do { P2DIR|=0x01; P2_0 = 0x01;  } while (0);
     halWait(4);
   #endif  
   MAC_MlmeSetReq(MAC_RX_ON_WHEN_IDLE, &msa_MACTrue);
   halWait(4);
  }
  else
  {
   #if defined HAL_PA_LNA
    P1_2=0x00;
    P1_3=0x00;
    OBSSEL2 &=~0xFF;   
    OBSSEL3 &=~0xFF;
    do { P2DIR|=0x01; P2_0 = 0x00;  } while (0);
    halWait(4); 
   #endif
   MAC_MlmeSetReq(MAC_RX_ON_WHEN_IDLE, &msa_MACFalse);
   halWait(4);
  } 
}

/*********************************************************************
 * @fn      getBattery
 *
 * @brief   get the value of the battery in milliVolt
 *          and write it into the output buffer
 *
 * @param   none
 *
 * @return  none
 */

uint16 getBattery(void)
{

  int16 tempRead=0;
  uint32 valueRead=0;

  // Enable channel 0x00
  APCFG |= 0x01;

  for(uint8 m=0;m<CAMPIONI;m++)
  {

   /* writing to this register starts the conversion - riferimento P0.7 e risoluzione massima */
    //ADCCON3 = 0x00 | 0x30 | 0x80;
    /* writing to this register starts the conversion - riferimento Vdd e risoluzione massima */
    ADCCON3 = 0x00 | 0x30 | 0x40;

    /* Wait for the conversion to be done */
    while (!(ADCCON1 & 0x80));

     /* Read the result */
     tempRead = (int16) (ADCL);
     tempRead |= (int16) (ADCH << 8);

     tempRead >>= 4; // Shift 4 due to 12 bits resolution true

     valueRead+=tempRead; // salvo e sommo

  }

  APCFG &=~ 0x01;  // ripristino P0.0 come input/output non ADC

  //valueRead = (uint32)(valueRead * 3000/LIVELLI);  // 3000 � la Vstabilizzata

  //valueRead = (uint32)(valueRead * 2500/LIVELLI);   // se riferimento � P0.7

  return (valueRead/CAMPIONI);

}

void checkBattery(uint8 forceSleep)
{
   uint8 abeEnable[1]={0x20};
 
   if ( (readBattery<=LOW_LEVEL_BATTERY) || (forceSleep) ) // segnalo che la batteria � scarica
   {
     setMessageTipology(0);
     
     if (readBattery<=LOW_LEVEL_BATTERY)
     {
      for(uint8 rs=0;rs<5;rs++)
      { 
          HalLedSet (HAL_LED_2, HAL_LED_MODE_ON);  // led rosso ON 
          halWaitSec(1);
          HalLedSet (HAL_LED_2, HAL_LED_MODE_OFF);  // led rosso OFF  
          halWaitSec(1);
      }
     }
     
     // se scade in timer dopo BEACON_TIME vuol dire che non ho trovato padre 
     // setto un orario affidabile e da li setto il WD 
     setAstronomic(START,SECONDI,MINUTI,ORE,GIORNO,DATA,MESE,ANNO);
     readAstronomic(readBuffer,0x0000,8);
     astroConv(readBuffer);
     writeAstronomic(abeEnable,0x0A,1); // abilito ABE ( non � attivo al primissimo avvio )
     minToSleep=TIME_CHECK_BATTERY;
     setWDTime(TIME_CHECK_BATTERY); // setto il risveglio per il prossimo check/beacon
     SleepAstro();  
   }
}

#if defined FLORA_1 || DEC5TE_P04 || defined SENTEK_10A || defined SENTEK_10B || defined SENTEK_10C || defined SENTEK_10D || defined SENTEK_10E || defined SENTEK_10F || defined SENTEK_60 || defined SENTEK_90 || defined SENTEK_30 || defined SENTEK_30_B || defined MPS6_P04 || defined DECGS3_P04 || defined DECCDT

void clearSdiBuffers()
{
     memset(sdiBuffer,0x00,sizeof(sdiBuffer));
     memset(sdiBuffer2,0x00,sizeof(sdiBuffer2));
     memset(sdiBuffer3,0x00,sizeof(sdiBuffer3));
     memset(sdiBuffer4,0x00,sizeof(sdiBuffer4));
}
#endif

