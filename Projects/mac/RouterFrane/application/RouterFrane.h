/**************************************************************************************************
  Filename:       msa.h
  Revised:        $Date: 2008-06-10 11:29:36 -0700 (Tue, 10 Jun 2008) $
  Revision:       $Revision: 17192 $

  Description:    This file contains the the Mac Sample Application protypes and definitions


  Copyright 2006-2007 Texas Instruments Incorporated. All rights reserved.

  IMPORTANT: Your use of this Software is limited to those specific rights
  granted under the terms of a software license agreement between the user
  who downloaded the software, his/her employer (which must be your employer)
  and Texas Instruments Incorporated (the "License").  You may not use this
  Software unless you agree to abide by the terms of the License. The License
  limits your use, and you acknowledge, that the Software may not be modified,
  copied or distributed unless embedded on a Texas Instruments microcontroller
  or used solely and exclusively in conjunction with a Texas Instruments radio
  frequency transceiver, which is integrated into your product.  Other than for
  the foregoing purpose, you may not use, reproduce, copy, prepare derivative
  works of, modify, distribute, perform, display or sell this Software and/or
  its documentation for any purpose.

  YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE
  PROVIDED �AS IS� WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
  INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, TITLE,
  NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL
  TEXAS INSTRUMENTS OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER CONTRACT,
  NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER
  LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
  INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE
  OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT
  OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
  (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.

  Should you have any questions regarding your right to use this Software,
  contact Texas Instruments Incorporated at www.TI.com.
**************************************************************************************************/

#ifndef MACSAMPLEAPP_H
#define MACSAMPLEAPP_H

#ifdef __cplusplus
extern "C"
{
#endif

/**************************************************************************************************
 * INCLUDES
 **************************************************************************************************/
#include "hal_types.h"

/**************************************************************************************************
 *                                        User's  Defines
 **************************************************************************************************/

#define MSA_MAC_CHANNEL           MAC_CHAN_21 /* Default channel - change it to desired channel */
                                  // alac D5 e 4100 4
                                  // giuliani d5 4100 4 e ch 15
                                  // ricasoli 2100 2 300
                                  // marconi 4100 acclima ecc..
                                  // fabbroni ch15 e 7100

#define MSA_PAN_ID                0x0367  /* PAN ID default 0x11CC */
#define MSA_COORD_SHORT_ADDR      0xAABB   /* Coordinator short address */
                              
#define MSA_WAIT_PERIOD           3100       /* Time between each packet - Lower = faster sending rate */
#define MSA_WAKE_PERIOD           (MSA_WAIT_PERIOD-100)/1000  /* Variabile che im\posta risveglio/livello nodo - se MSA_WAIT_PERIOD=2100 deve essere 2 */
#define SEND_DELAY                460        /* era 300ms ma il coordinatore ci pu� mettere anche 350 a salvare un pacchetto grande */
#define MAX_DEPTH                 8          // massimo livello raggiungibile in profondit�
#define MSA_WINDOW_STAR           950        /* Wait Period - finestra da 500ms di guardia prima e dopo ( 500+500 ) + piccolo margine */
                                             /* con 2100 mettere 950 - con 3100 mettere 1700 */
  
#define MSA_WS_INC                230          
#define WIN_GUARD_RX              1500
#define MIN_LQI_VALUE             1            /* 10 = circa -83dbm */
#define ST_TIME_RESET             5            // minuti di wake da sleep time cosa che non dovrebbe accadere mai
#define WD_TIME_RESET             10           // minuti di attivit� nella fase ON prima del reset hardware

#define SENSOR_ON_TIME            500          /* Tempo di attivazione dei sensori */  
  
#define MSA_ASSOCIATION_PERIOD    1500
                                               
#if defined HAL_PA_LNA
 #define MAC_TX_POWER              0xE5         /* Useful value 20 dBm 0xE5 166mA, 19 dBm 0xD5 149mA, 18 dBm 0xC5 138mA, 
                                                17 dBm 0xB5 127mA, 16 dBm 0xA5 115mA, 14.5 dBm 0x95 100mA,
                                                13 dBm 0x85 94mA, 11.5 dBm 0x75 86mA, 10 dBm 0x65 79mA*/  
#else
 #define MAC_TX_POWER              0xF5          /* Useful value 0xF5=4.5dBm 0xE5=2.5 dbm 0xD5=1dBm 0xB5=-1.5dBm 0x95=-4.0dBm 0x05=-22dBm*/
#endif
  

/* parametri per lo sleep */
#define SLEEP_TIME                1       /* Sleep time - valore espresso in minuti */
#define SLEEP_TIME_RPT            60000
#define SEC_TO_SYNCH              25      /* Secondi nei quali i nodi si devono risvegliare per ricevere i minuti di sleep*/
#define SEC_TO_SYNCH_MARGIN       3
  
#define MAX_SEND_RETRIES          2        // max send retries radio
#define MAX_FATHER_RETRIES        2        // tentativi sul padre prima di andare a fare la ricerca  ( fa 1 tx + n_retries+1 )
#define MAX_NUM_RETRIES           15       // considero reti con tempo medio 20 minuti dopo 30 min di tentativi da 1 minuto mi resetto
#define LOW_LEVEL_BATTERY         940      // ADC mi indica 5.9V
 
#define MAX_SLEEP_TIME            500000
#define MAX_BEACON_TIME           8400000 //60000 // 140 minuti
#define TIME_CHECK_BATTERY        120     //10 // 120 minuti prima di rifare la beacon request 

#define MSA_DIRECT_MSG_ENABLED    TRUE          /* True if direct messaging is used, False if polling is used */

#define MSA_MAC_BEACON_ORDER      15            /* Setting beacon order to 15 will disable the beacon */
#define MSA_MAC_SUPERFRAME_ORDER  15            /* Setting superframe order to 15 will disable the superframe */

#define MSA_PACKET_HEADER         20             /* Header del pacchetto address,mac,data,ora.... */

#if defined MAC_SECURITY
  #define MSA_PACKET_LENGTH       81             /* Min = 4, Max = 102 */
#else
  #define MSA_PACKET_LENGTH       100            /* Min = 4, Max = 102 */
#endif
  
#define MSA_PWR_MGMT_ENABLED      FALSE         /* Enable or Disable power saving */

#define MSA_KEY_INT_ENABLED       TRUE           /*
                                                 * FALSE = Key Polling
                                                 * TRUE  = Key interrupt
                                                 *
                                                 * Notes: Key interrupt will not work well with 2430 EB because
                                                 *        all the operations using up/down/left/right switch will
                                                 *        no longer work. Normally S1 + up/down/left/right is used
                                                 *        to invoke the switches but on the 2430 EB board,  the
                                                 *        GPIO for S1 is used by the LCD.
                                                 */

#if (MSA_MAC_SUPERFRAME_ORDER > MSA_MAC_BEACON_ORDER)
#error "ERROR! Superframe order cannot be greater than beacon order."
#endif

#if ((MSA_MAC_SUPERFRAME_ORDER != 15) || (MSA_MAC_BEACON_ORDER != 15)) && (MSA_DIRECT_MSG_ENABLED == FALSE)
#error "ERROR! Cannot run beacon enabled on a polling device"
#endif

#if (MSA_PACKET_LENGTH < 4) || (MSA_PACKET_LENGTH > 102)
#error "ERROR! Packet length has to be between 4 and 102"
#endif

/**************************************************************************************************
 * CONSTANTS
 **************************************************************************************************/

/* Set Astronomic Parameters */
#define START 0x00
#define STOP 0x80
#define SECONDI 0x00
#define MINUTI  0x00
#define ORE 0x14
#define GIORNO 0x04
#define DATA 0x21
#define MESE 0x06
#define ANNO 0x07

 /* Set Astronomic Alarm */
#define AI_ENABLE 0x80
#define I_MESE 0x12
#define I_DATA 0x30
#define I_SECONDI 0x10
#define I_MINUTI  0x34
#define I_ORE 0x14

/* Reference voltage: Internal 1.15 V,
Resolution: 12 bits,
ADC input: VDD/3 (VDD is the battery voltage) */
#define SAMPLE_BATTERY_VOLTAGE(v) \
do { \
ADCCON2 = 0x3F; \
ADCCON1 = 0x73; \
while(!(ADCCON1 & 0x80)); \
v = ADCL; \
v |= (((unsigned int)ADCH) << 8); \
} while(0)
// Max ADC input voltage = reference voltage =>
// (VDD/3) max = 1.15 V => max VDD = 3.45 V
// 12 bits resolution means that max ADC value = 0x07FF = 2047 (dec)
// (the ADC value is 2�s complement)
// Battery voltage, VDD = adc value * (3.45 / 2047)
// To avoid using a float, the below function will return the battery voltage * 1000 -> mVolt
#define KBATT 1.68539 // (3.45 / 2047) * 1000

  /* sleep timer interrupt control */
#define SLEEP_TIMER_ENABLE_INT()        st(IEN0 |= BV(5);)     /* enable sleep timer interrupt */

/* convert msec to 320 usec units with round */
#define SLEEP_MS_TO_320US(ms)           (((((uint32) (ms)) * 100) + 31) / 32)

/* Event IDs */
#define MSA_SEND_EVENT   0x0001
#define MSA_READ_EVENT   0x0002
#define MSA_SLEEP_EVENT  0x0004
#define MSA_SETUP_EVENT  0x0008
#define MSA_BROAD_EVENT  0x0010
#define MSA_RESP_BROAD_EVENT  0x0020
#define MSA_FAULT_TOL_EVENT  0x0040
#define MSA_REPEAT_DATA_EVENT  0x0080
#if defined RS485
 #define MSA_RS485_READ_EVENT  0x0100
#endif


/* Application State */
#define MSA_ASSOC_STATE     0x00
#define MSA_SEND_STATE      0x01
#define MSA_SETUP_STATE     0x02
#define MSA_START_STATE     0x04
#define MSA_BEFORE_STATE    0x08
#define MSA_FAULT_STATE     0x10
  
#if defined (SMEC300) 
 extern uint16 readSensorSM[3];
#endif
 #if defined DEC5TE_P04
extern int16 resBuffer[3];
// VWC - TEMP - EC
#endif

#if defined SENTEK_60 || defined SENTEK_30 || defined SENTEK_90
extern int16 resSentekHum[6];
extern int16 resSentekHumB[6];
extern int16 resSentekSal[6];
extern int16 resSentekSalB[6];
extern int16 resSentekTemp[6];
extern int16 resSentekTempB[6];
// VWC - TEMP - EC
#endif

extern bool firstStart;

#if defined FLORA_1
extern int16 floraResBuffer[3];
#endif

#ifdef SENTEK_10A
extern int16 resSentek10A[3];
#endif

#ifdef SENTEK_10B
extern int16 resSentek10B[3];
#endif


#ifdef SENTEK_10C
extern int16 resSentek10C[3];
#endif


#ifdef SENTEK_10D
extern int16 resSentek10D[3];
#endif


#ifdef SENTEK_10E
extern int16 resSentek10E[3];
#endif


#ifdef SENTEK_10F
extern int16 resSentek10F[3];
#endif


#if defined MPS6_P04
extern int16 resBuffer2[3];
// TENS - TEMP - N.C.
#endif

/**************************************************************************************************
 * GLOBALS
 **************************************************************************************************/
extern uint8 MSA_TaskId;

/*********************************************************************
 * FUNCTIONS
 */

/*
 * Task Initialization for the Mac Sample Application
 */
extern void MSA_Init( uint8 task_id );

/*
 * Task Event Processor for the Mac Sample Application
 */
extern uint16 MSA_ProcessEvent( uint8 task_id, uint16 events );

/*
 * Handle keys
 */
extern void MSA_HandleKeys( uint8 keys, uint8 shift );

/*
 * Handle power saving
 */
extern void MSA_PowerMgr (uint8 mode);

extern void readIEEEAddressFromNv(void);

extern uint8 tmpLevBuffer[2];

/*********************************************************************
*********************************************************************/

#ifdef __cplusplus
}
#endif

#endif /* MACSAMPLEAPP_H */
