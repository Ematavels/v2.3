/**************************************************************************************************
  Filename:      msa.c
  Revised:        $Date: 2011-09-16 11:36:37 -0700 (Fri, 16 Sep 2011) $
  Revision:       $Revision: 27599 $

  Firmware Version: 2.2.1

  Description:    This file contains the sample application that can be use to test
                  the functionality of the MAC, HAL and low level.


  Copyright 2006-2011 Texas Instruments Incorporated. All rights reserved.

  IMPORTANT: Your use of this Software is limited to those specific rights
  granted under the terms of a software license agreement between the user
  who downloaded the software, his/her employer (which must be your employer)
  and Texas Instruments Incorporated (the "License").  You may not use this
  Software unless you agree to abide by the terms of the License. The License
  limits your use, and you acknowledge, that the Software may not be modified,
  copied or distributed unless embedded on a Texas Instruments microcontroller
  or used solely and exclusively in conjunction with a Texas Instruments radio
  frequency transceiver, which is integrated into your product.  Other than for
  the foregoing purpose, you may not use, reproduce, copy, prepare derivative
  works of, modify, distribute, perform, display or sell this Software and/or
  its documentation for any purpose.

  YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE
  PROVIDED �AS IS� WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
  INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, TITLE,
  NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL
  TEXAS INSTRUMENTS OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER CONTRACT,
  NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER
  LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
  INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE
  OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT
  OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
  (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.

  Should you have any questions regarding your right to use this Software,
  contact Texas Instruments Incorporated at www.TI.com.
**************************************************************************************************/

/**************************************************************************************************

    Description:

                                KEY UP (or S1)
                              - Non Beacon
                              - First board in the network will be setup as the coordinator
                              - Any board after the first one will be setup as the device
                                 |
          KEY LEFT               |      KEY RIGHT(or S2)
                             ----+----- - Start transmitting
                                 |
                                 |
                                KEY DOWN



**************************************************************************************************/


/**************************************************************************************************
 *                                           Includes
 **************************************************************************************************/
/* Hal Driver includes */
#include "hal_types.h"
#include "hal_key.h"
#include "hal_timer.h"
#include "hal_drivers.h"
#include "hal_led.h"
#include "hal_adc.h"
#include "hal_lcd.h"

/* OS includes */
#include "OSAL.h"
#include "OSAL_Tasks.h"
#include "OSAL_PwrMgr.h"

/* Application Includes */
#include "OnBoard.h"

/* For Random numbers */
#include "mac_radio_defs.h"

/* MAC Application Interface */
#include "mac_api.h"
#include "mac_main.h"

#ifdef MAC_SECURITY
#include "mac_spec.h"

/* MAC Sceurity */
#include "mac_security_pib.h"
#include "mac_security.h"
#endif /* MAC_SECURITY */

/* Application */
#include "CoordFrane.h"
#include "SHT11_Davis.h"


/* WinetAQ */
#include "hal_i2c.h"
#include "astronomic.h"
#include "eeprom.h"
#include "adc1115.h"

#if defined WINETTX
#include "modem.h"
#endif

#include "string.h"

#if defined SPI
#include "hal_spi.h"
#include "sdcard.h"
#include "ff.h"
#include "ffconf.h"
#include "integer.h"
uint8 saveP2DIR;
bool sdOk=false;
uint16 readValueSD;
uint8 dataOraBuff[8]; //max + terminatore  
char charBuffer[18]; //max + terminatore   
//byte dataToWrite[18]; // max + terminatore
bool funcToSDCARD(uint8 operation,uint8* buffer,uint8 lengthOfData,char* fileName);
#endif

#include "stdio.h"
#include "hal_uart.h"

#include "OSAL_Nv.h"
#include "hal_flash.h"
#include "sensors.h"
#include "OSAL_Clock.h"
#include "SHT21_Winet.h"

#if defined (PIOGGIA) || defined (VENTO) || defined (PRESS0) || defined (FLOW0) || defined (FLOW1)
 #include "attiny85.h"
#endif

#if defined ACC_TDTP || defined ACC_TDR
#include "SdiCore.h"
#include "ACCTParser.h"
#endif

#if defined DEC5TE_P04  
 #include "SdiCore.h"
 #include "5TEParser.h"
#endif

#if defined FLORA_1  
 #include "SdiCore.h"
 #include "FloraParser.h"
#endif

#if defined SENTEK_90 || defined SENTEK_60 || defined SENTEK_90 || defined SENTEK_120 || defined SENTEK_30 || defined SENTEK_10A || defined SENTEK_10B || defined SENTEK_10C || defined SENTEK_10D || defined SENTEK_10E || defined SENTEK_10F || defined SENTEK_30_B
  #include "SdiCore.h"
  #include "SentekParser.h"
#endif

#if defined MPS6_P04
 #include "SdiCore.h"
 #include "MPS6Parser.h"
#endif
#if defined DECGS3_P04
 #include "SdiCore.h"
 #include "GS3Parser.h"
#endif

#if defined DECCDT
 #include "SdiCore.h"
 #include "CDTParser.h"
#endif

#include "i2c_multiplexer.h"

/**************************************************************************************************
 *                                           Constant
 **************************************************************************************************/

#define MSA_DEV_SHORT_ADDR        0x0000        /* Device initial short address - This will change after association */

#define MSA_HEADER_LENGTH         4             /* Header includes DataLength + DeviceShortAddr + Sequence */
#define MSA_ECHO_LENGTH           8             /* Echo packet */

#define MSA_MAC_MAX_RESULTS       5             /* Maximun number of scan result that will be accepted */
#define MSA_MAX_DEVICE_NUM        50           /* Maximun number of devices can associate with the coordinator */

#if defined (HAL_BOARD_CC2420DB)
  #define MSA_HAL_ADC_CHANNEL     HAL_ADC_CHANNEL_0             /* AVR - Channel 0 and Resolution 10 */
  #define MSA_HAL_ADC_RESOLUTION  HAL_ADC_RESOLUTION_10
#elif defined (HAL_BOARD_DZ1611) || defined (HAL_BOARD_DZ1612) || defined (HAL_BOARD_DRFG4618) || defined (HAL_BOARD_F2618)
  #define MSA_HAL_ADC_CHANNEL     HAL_ADC_CHANNEL_0             /* DZ1611 and DZ1612 - Channel 0 and Resolution 12 */
  #define MSA_HAL_ADC_RESOLUTION  HAL_ADC_RESOLUTION_12
#else
  #define MSA_HAL_ADC_CHANNEL     HAL_ADC_CHANNEL_7             /* CC2430 EB & DB - Channel 7 and Resolution 14 */
  #define MSA_HAL_ADC_RESOLUTION  HAL_ADC_RESOLUTION_14
#endif

#define MSA_EBR_PERMITJOINING    TRUE
#define MSA_EBR_LINKQUALITY      1
#define MSA_EBR_PERCENTFILTER    0xFF

/* Size table for MAC structures */
const CODE uint8 msa_cbackSizeTable [] =
{
  0,                                   /* unused */
  sizeof(macMlmeAssociateInd_t),       /* MAC_MLME_ASSOCIATE_IND */
  sizeof(macMlmeAssociateCnf_t),       /* MAC_MLME_ASSOCIATE_CNF */
  sizeof(macMlmeDisassociateInd_t),    /* MAC_MLME_DISASSOCIATE_IND */
  sizeof(macMlmeDisassociateCnf_t),    /* MAC_MLME_DISASSOCIATE_CNF */
  sizeof(macMlmeBeaconNotifyInd_t),    /* MAC_MLME_BEACON_NOTIFY_IND */
  sizeof(macMlmeOrphanInd_t),          /* MAC_MLME_ORPHAN_IND */
  sizeof(macMlmeScanCnf_t),            /* MAC_MLME_SCAN_CNF */
  sizeof(macMlmeStartCnf_t),           /* MAC_MLME_START_CNF */
  sizeof(macMlmeSyncLossInd_t),        /* MAC_MLME_SYNC_LOSS_IND */
  sizeof(macMlmePollCnf_t),            /* MAC_MLME_POLL_CNF */
  sizeof(macMlmeCommStatusInd_t),      /* MAC_MLME_COMM_STATUS_IND */
  sizeof(macMcpsDataCnf_t),            /* MAC_MCPS_DATA_CNF */
  sizeof(macMcpsDataInd_t),            /* MAC_MCPS_DATA_IND */
  sizeof(macMcpsPurgeCnf_t),           /* MAC_MCPS_PURGE_CNF */
  sizeof(macEventHdr_t)                /* MAC_PWR_ON_CNF */
};

/**************************************************************************************************
 *                                        Local Variables
 **************************************************************************************************/

/*
  Extended address of the device, for coordinator, it will be msa_ExtAddr1
  For any device, it will be msa_ExtAddr2 with the last 2 bytes from a ADC read
*/
sAddrExt_t    msa_ExtAddr;
sAddrExt_t    msa_ExtAddr1 = {0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77, 0x88};
sAddrExt_t    msa_ExtAddr2 = {0xA0, 0xB0, 0xC0, 0xD0, 0xE0, 0xF0, 0x00, 0x00};

/* Coordinator and Device information */
uint16        msa_PanId = MSA_PAN_ID;
uint16        msa_CoordShortAddr = MSA_COORD_SHORT_ADDR;
uint16        msa_DevShortAddr   = MSA_DEV_SHORT_ADDR;

uint16 terzo;
uint8        msa_DevShortAddrLow=0x00;
/* Current number of devices associated to the coordinator */
uint8         msa_NumOfDevices = 0;

#ifdef MAC_SECURITY
  /* Current number of devices (including coordinator) communicating to this device */
uint8  msa_NumOfSecuredDevices = 0;
#endif /* MAC_SECURITY */

/* Predefined packet that will be sent out by the coordinator */
uint8 msa_Data1[MSA_PACKET_LENGTH];

/* Predefined packet that will be sent back to the coordinator by the device */
uint8 msa_Data2[11];

/* TRUE and FALSE value */
bool          msa_MACTrue = TRUE;
bool          msa_MACFalse = FALSE;

/* Beacon payload, this is used to determine if the device is not zigbee device */
uint8         msa_BeaconPayload[] = {0x22, 0x33, 0x44};
uint8         msa_BeaconPayloadLen = 3;

/* Contains pan descriptor results from scan */
macPanDesc_t  msa_PanDesc[MSA_MAC_MAX_RESULTS];

/* flags used in the application */
bool          msa_IsCoordinator  = FALSE;   /* True if the device is started as a Pan Coordinate */
bool          msa_IsStarted      = FALSE;   /* True if the device started, either as Pan Coordinator or device */
bool          msa_IsSampleBeacon = FALSE;   /* True if the beacon payload match with the predefined */
bool          msa_IsDirectMsg    = FALSE;   /* True if the messages will be sent as direct messages */
uint8         msa_State = MSA_START_STATE;   /* Either IDLE state or SEND state */

/* Structure that used for association request */
macMlmeAssociateReq_t msa_AssociateReq;

/* Structure that used for association response */
macMlmeAssociateRsp_t msa_AssociateRsp;

/* Structure that contains information about the device that associates with the coordinator */
typedef struct
{
  uint16 devShortAddr;
  uint8  isDirectMsg;
}msa_DeviceInfo_t;

uint16 msa_DeviceRecord[MSA_MAX_DEVICE_NUM];

uint8 msa_SuperFrameOrder;
uint8 msa_BeaconOrder;

/* Task ID */
uint8 MSA_TaskId;

uint16 assocShortAddress=0xFFFF;
uint8 watchDogAbeAfe[2]={0xFF,0x00}; // setto a 0x00 AFE e ABE e WD = 124 secondi
uint8 assocExtAdd[2];
uint8 minWakeUp=0;
uint16 minToSleep=SLEEP_TIME;
uint16 time=SLEEP_TIME+ST_TIME_RESET;
uint16 nextMinToSleep;
bool coordinatorMode=1;
uint8 countTxGprs=0;
bool firstStart=TRUE;
UTCTimeStruct timeRead;
uint32 secOld=0,secStart=0;  
bool lowBattery=FALSE;


#ifdef MAC_SECURITY
/**************************************************************************************************
 *                                  MAC security related constants
 **************************************************************************************************/
#define MSA_KEY_TABLE_ENTRIES         1
#define MSA_KEY_ID_LOOKUP_ENTRIES     1
#define MSA_KEY_DEVICE_TABLE_ENTRIES  8
#define MSA_KEY_USAGE_TABLE_ENTRIES   1
#define MSA_DEVICE_TABLE_ENTRIES      0 /* can grow up to MAX_DEVICE_TABLE_ENTRIES */
#define MSA_SECURITY_LEVEL_ENTRIES    1
#define MSA_MAC_SEC_LEVEL             MAC_SEC_LEVEL_ENC
#define MSA_MAC_KEY_ID_MODE           MAC_KEY_ID_MODE_1
#define MSA_MAC_KEY_SOURCE            {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}
#define MSA_MAC_DEFAULT_KEY_SOURCE    {0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33}
#define MSA_MAC_KEY_INDEX             3  /* cannot be zero for implicit key identifier */


/* Default MSA Security Parameters for outgoing frames */
bool  macSecurity       = TRUE;
uint8 msa_securityLevel = MSA_MAC_SEC_LEVEL;
uint8 msa_keyIdMode     = MSA_MAC_KEY_ID_MODE;
uint8 msa_keySource[]   = MSA_MAC_KEY_SOURCE;
uint8 msa_keyIndex      = MSA_MAC_KEY_INDEX;

/**************************************************************************************************
 *                                 Security PIBs for outgoing frames
 **************************************************************************************************/
const keyIdLookupDescriptor_t msa_keyIdLookupList[] =
{
  {
    {0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x33, 0x03}, 0x01 /* index 3, 9 octets */
  }
};

/* Key device list can be modified at run time
 */
keyDeviceDescriptor_t msa_keyDeviceList[] =
{
  {0x00, false, false},
  {0x00, false, false},
  {0x00, false, false},
  {0x00, false, false},
  {0x00, false, false},
  {0x00, false, false},
  {0x00, false, false},
  {0x00, false, false}
};

const keyUsageDescriptor_t msa_keyUsageList[] =
{
  {MAC_FRAME_TYPE_DATA, MAC_DATA_REQ_FRAME}
};

const keyDescriptor_t msa_keyTable[] =
{
  {
    (keyIdLookupDescriptor_t *)msa_keyIdLookupList, MSA_KEY_ID_LOOKUP_ENTRIES,
    (keyDeviceDescriptor_t *)msa_keyDeviceList, MSA_KEY_DEVICE_TABLE_ENTRIES,
    (keyUsageDescriptor_t *)msa_keyUsageList, MSA_KEY_USAGE_TABLE_ENTRIES,
    {0x12, 0x34, 0x56, 0x78, 0x9a, 0xbc, 0xde, 0xf0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}
  }
};

const uint8 msa_keyDefaultSource[] = MSA_MAC_DEFAULT_KEY_SOURCE;


/**************************************************************************************************
 *                                 Security PIBs for incoming frames
 **************************************************************************************************/

/* Device table can be modified at run time. */
deviceDescriptor_t msa_deviceTable[] =
{
  {0, 0xFFFF, 0, 0, FALSE},
  {0, 0xFFFF, 0, 0, FALSE},
  {0, 0xFFFF, 0, 0, FALSE},
  {0, 0xFFFF, 0, 0, FALSE},
  {0, 0xFFFF, 0, 0, FALSE},
  {0, 0xFFFF, 0, 0, FALSE},
  {0, 0xFFFF, 0, 0, FALSE},
  {0, 0xFFFF, 0, 0, FALSE}
};

const securityLevelDescriptor_t msa_securityLevelTable[] =
{
  {MAC_FRAME_TYPE_DATA, MAC_DATA_REQ_FRAME, MAC_SEC_LEVEL_NONE, FALSE}
};
#else /* MAC_SECURITY */
uint8 msa_securityLevel = MAC_SEC_LEVEL_NONE;
uint8 msa_keyIdMode     = MAC_KEY_ID_MODE_NONE;
uint8 msa_keySource[]   = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
uint8 msa_keyIndex      = 0;
#endif /* MAC_SECURITY */


/**************************************************************************************************
 *                                     Local Function Prototypes
 **************************************************************************************************/
/* Setup routines */
void MSA_CoordinatorStartup(void);
void MSA_DeviceStartup(void);

/* MAC related routines */
void MSA_AssociateReq(void);
void MSA_AssociateRsp(macCbackEvent_t* pMsg);
void MSA_McpsDataReq(uint8* data, uint8 dataLength, bool directMsg, uint16 dstShortAddr);
void MSA_McpsPollReq(void);
void MSA_ScanReq(uint8 scanType, uint8 scanDuration);
void MSA_SyncReq(void);

/* Support */
bool MSA_BeaconPayLoadCheck(uint8* pSdu);
bool MSA_DataCheck(uint8* data, uint8 dataLength);
uint8 aSecExtendedAddress[8];
uint8 readBuffer[8],matchBuffer[8];

/* Sleep - funzioni e variabili di supporto */
void incrementAstro(uint16 min);
void SleepAstro(void);
void Sleep(uint16 timeout);
void defineDate(uint16 min); // funzione che consente di definire la data per il settaggio dell'orario e dell'interrupt di risveglio
void checkMeseUp(uint8 mese);
void WakeUp(void);
void InputOutputWakeUp(void);
void InputOutputSleep(void);
void setMessageTipology(uint8 val);
bool saveData(uint8 lengthOfData);
void preparePacket(void);
uint8 diffTimestamp(void);
bool timePermitAssociation(void);
bool checkBattery(void);
uint8 lowBattRead=0x00;
#if defined WD_AQ22
  void setWDTime(uint16 minute);
#endif
#if defined DEBUG
 void testBoard(void);
  uint16 secondiA;
    uint16 minutiA;
    uint16 ore;
    uint16 giornoA;
 uint8 retVal;
 uint8 test[100];
 uint16 readBattery;
 uint16 read,read2;
  uint16 read16bit0,read16bit1,read16bit2,read16bit3,read16bit4,read16bit5,read16bit6,read16bit7;
#endif
  uint16 read;
 
#if defined SMEC300
uint16 readSensorSM[3];
#endif

#if defined FLORA_1 || SENTEK_10A || defined SENTEK_10B || defined SENTEK_10C || defined SENTEK_10D || defined SENTEK_10E || defined SENTEK_10F || defined SENTEK_60 || defined SENTEK_120 || defined SENTEK_90 || defined SENTEK_30 || defined SENTEK_30_B || defined MPS6_P04 || defined DECGS3_P04 || defined DECCDT || defined ACC_TDTP || defined ACC_TDR

 uint8 sdiBuffer[40]={
 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
 
 uint8 sdiBuffer2[40]={
 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};

 uint8 sdiBuffer3[40]={
 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
 
 uint8 sdiBuffer4[40]={
 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
  
#if defined SENTEK_120 || SENTEK_90 || SENTEK_60 || SENTEK_30 
int16 resSentekHum[6];    //1-6
int16 resSentekHumB[6];   //7-12
int16 resSentekSal[6];    //1-6
int16 resSentekSalB[6];   //7-12
int16 resSentekTemp[6];   //1-6
int16 resSentekTempB[6];  //7-12
#endif

void clearSdiBuffers(void);
uint8 sdiRetries; 

#if defined SENTEK_30_C
int16 resSentekHumC[6];   //7-12
int16 resSentekTempC[6];  //7-12
#endif

#if defined SENTEK_10A
int16 resSentek10A[3];
#endif

#if defined SENTEK_10B
int16 resSentek10B[3];
#endif

#if defined SENTEK_10C
int16 resSentek10C[3];
#endif

#if defined SENTEK_10D
int16 resSentek10D[3];
#endif

#if defined SENTEK_10E
int16 resSentek10E[3];
#endif

#if defined SENTEK_10F
int16 resSentek10F[3];
#endif


#if defined DEC5TE_P04 
  int16 resBuffer[3];
  uint8 sdiDevice=0x05; //Indirizzo Sensore 5TE P04
#endif

  
#if defined MPS6_P04  
  // le altre variabili sono gia definite perch� ho per forza 5TE su P0_4 se occupo P0_5 
   int16 resBuffer2[3];
  uint8 sdiDevice2=0x06; //Indirizzo Sensore MPS6
#endif  
#if defined DECGS3_P04
  int16 resBuffer3[3];
  uint8 sdiDevice3=0x03; //Indirizzo Sensore GS3 P04
#endif
  
#if defined FLORA_1
  int16 floraResBuffer[3];
#endif

#if defined ACC_TDTP
  uint8 sdiDevice5=0x08;
#endif
  
#if defined ACC_TDR
  // address 0x07
  int16 accResBuffer[6];
#endif

#if defined ACC_TDR_2
  int16 accResBuffer2[6];
#endif  
  
#if defined ACC_TDR_3
  int16 accResBuffer3[6];
#endif  
    
#if defined ACC_TDR_4
  int16 accResBuffer4[6];
#endif   
  
#if defined ACC_TDR_5
  int16 accResBuffer5[6];
#endif  

#if defined ACC_TDR_6
  int16 accResBuffer6[6];
#endif  
  
#if defined DECCDT
 int16 resBuffer4[3];
#endif
 
#endif 

#if defined WEBCAM
bool doPhotoNow=FALSE;
uint8 attivaWebCam=0x00;
#endif

  
#if defined SPI
UINT bytes_count;
FATFS Fatfs;		/* File system object */
FIL Fil;		/* File object */
FRESULT rc;/* FatFs return value */
char fileName[20];
#endif
  
//uint8 countSens(uint32 id);
uint8 timeBuffer[8]; // buffer dove memorizzo secondi,minuti,ore...
uint8 newGiorno,newMese,newMinuti,newOre,newAnno,newSecondi,newDecCent,newdayWeek; // per settaggio allarme

/* ADC */
uint16 getBattery(void);

/* IEEE Support  */
void readIEEEAddressFromNv(void);

bool enableAssociation=TRUE;
bool present;
bool permitAssoc=TRUE;
uint8 dataLen,rssi,numSensori;
uint16 writeAddress=MSA_READ_ADDRESS;
uint8 clearBuf[1];
uint8 diffMin,divMin,minOld;    
uint16 readBattery;
       
#ifdef MAC_SECURITY
/**************************************************************************************************
 *
 * @fn          MSA_SecurityInit
 *
 * @brief       Initialize the security part of the application
 *
 * @param       none
 *
 * @return      none
 *
 **************************************************************************************************/
static void MSA_SecurityInit(void)
{
  uint8   keyTableEntries      = MSA_KEY_TABLE_ENTRIES;
  uint8   autoRequestSecLevel  = MAC_SEC_LEVEL_NONE;
  uint8   securityLevelEntries = MSA_SECURITY_LEVEL_ENTRIES;

  /* Write key table PIBs */
  MAC_MlmeSetSecurityReq(MAC_KEY_TABLE, (void *)msa_keyTable);
  MAC_MlmeSetSecurityReq(MAC_KEY_TABLE_ENTRIES, &keyTableEntries );

  /* Write default key source to PIB */
  MAC_MlmeSetSecurityReq(MAC_DEFAULT_KEY_SOURCE, (void *)msa_keyDefaultSource);

  /* Write security level table to PIB */
  MAC_MlmeSetSecurityReq(MAC_SECURITY_LEVEL_TABLE, (void *)msa_securityLevelTable);
  MAC_MlmeSetSecurityReq(MAC_SECURITY_LEVEL_TABLE_ENTRIES, &securityLevelEntries);

  /* TBD: MAC_AUTO_REQUEST is true on by default
   * need to set auto request security PIBs.
   * dieable auto request security for now
   */
  MAC_MlmeSetSecurityReq(MAC_AUTO_REQUEST_SECURITY_LEVEL, &autoRequestSecLevel);

  /* Turn on MAC security */
  MAC_MlmeSetReq(MAC_SECURITY_ENABLED, &macSecurity);
}


/**************************************************************************************************
 *
 * @fn          MSA_SecuredDeviceTableUpdate
 *
 * @brief       Update secured device table and key device descriptor 
 *
 * @param       panID - Secured network PAN ID
 * @param       shortAddr - Other device's short address
 * @param       extAddr - Other device's extended address
 * @param       pNumOfSecuredDevices - pointer to number of secured devices
 *
 * @return      none
 *
 **************************************************************************************************/
static void MSA_SecuredDeviceTableUpdate(uint16 panID,
                                         uint16 shortAddr,
                                         sAddrExt_t extAddr,
                                         uint8 *pNumOfSecuredDevices)
{
  /* Update key device list */
  msa_keyDeviceList[*pNumOfSecuredDevices].deviceDescriptorHandle = *pNumOfSecuredDevices;

  /* Update device table */
  msa_deviceTable[*pNumOfSecuredDevices].panID = panID;
  msa_deviceTable[*pNumOfSecuredDevices].shortAddress = shortAddr;
  sAddrExtCpy(msa_deviceTable[*pNumOfSecuredDevices].extAddress, extAddr);
  MAC_MlmeSetSecurityReq(MAC_DEVICE_TABLE, msa_deviceTable);

  /* Increase the number of secured devices */
  (*pNumOfSecuredDevices)++;
  MAC_MlmeSetSecurityReq(MAC_DEVICE_TABLE_ENTRIES, pNumOfSecuredDevices);
}
#endif /* MAC_SECURITY */


/**************************************************************************************************
 *
 * @fn          MSA_Init
 *
 * @brief       Initialize the application
 *
 * @param       taskId - taskId of the task after it was added in the OSAL task queue
 *
 * @return      none
 *
 **************************************************************************************************/
void MSA_Init(uint8 taskId)
{
  //ENABLE_PERIPH();halWait(5);
  //setAstronomic(START,SECONDI,MINUTI,ORE,GIORNO,DATA,MESE,ANNO);
  /*Lettura batteria da fare prima di disabilitare le periferiche*/
  //setAstronomic(START,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF);

  /* Initialize the task id */
  MSA_TaskId = taskId;

  /* initialize MAC features */
  MAC_InitCoord();

  /* Initialize MAC beacon */
  MAC_InitBeaconCoord();

  /* Reset the MAC */
  MAC_MlmeResetReq(TRUE);

  TXPOWER=MAC_TX_POWER;
  
  setMessageTipology(0);

  /* Imposta l'indirizzo IEEE Secondario */
  readIEEEAddressFromNv();

  #ifdef MAC_SECURITY
   /* Initialize the security part of the application */
   MSA_SecurityInit();
  #endif /* MAC_SECURITY */

  minToSleep=SLEEP_TIME;
  nextMinToSleep=SLEEP_TIME;

 for(int i=0; i<MSA_MAX_DEVICE_NUM; i++) msa_DeviceRecord[i] = 0xFFFF;

  msa_BeaconOrder = MSA_MAC_BEACON_ORDER;
  msa_SuperFrameOrder = MSA_MAC_SUPERFRAME_ORDER;

  MSA_CoordinatorStartup();

}

/**************************************************************************************************
 *
 * @fn          MSA_ProcessEvent
 *
 * @brief       This routine handles events
 *
 * @param       taskId - ID of the application task when it registered with the OSAL
 *              events - Events for this task
 *
 * @return      16bit - Unprocessed events
 *
 **************************************************************************************************/
uint16 MSA_ProcessEvent(uint8 taskId, uint16 events)
{
  uint8* pMsg;
  macCbackEvent_t* pData;
 
 
#ifdef MAC_SECURITY
  uint16       panID;
  uint16       panCoordShort;
  sAddrExt_t   panCoordExtAddr;
#endif /* MAC_SECURITY */

  if (events & SYS_EVENT_MSG)
  {
    while ((pMsg = osal_msg_receive(MSA_TaskId)) != NULL)
    {
      switch ( *pMsg )
      {
        case MAC_MLME_ASSOCIATE_IND:
    
         if (enableAssociation)
         {
            enableAssociation=FALSE;

            if (timePermitAssociation())
            { 
              setMessageTipology(1);
              MSA_AssociateRsp((macCbackEvent_t*)pMsg);
            }
          }
          break;

        case MAC_MLME_ASSOCIATE_CNF:
          /* Retrieve the message */
          pData = (macCbackEvent_t *) pMsg;

          if ((!msa_IsStarted) && (pData->associateCnf.hdr.status == MAC_SUCCESS))
          {
            msa_IsStarted = TRUE;

            /* Retrieve MAC_SHORT_ADDRESS */

            msa_DevShortAddr = pData->associateCnf.assocShortAddress;

            /* Setup MAC_SHORT_ADDRESS - obtained from Association */
            MAC_MlmeSetReq(MAC_SHORT_ADDRESS, &msa_DevShortAddr);

         #ifdef MAC_SECURITY
            /* Add the coordinator to device table and key device table for security */
            MAC_MlmeGetReq(MAC_PAN_ID, &panID);
            MAC_MlmeGetSecurityReq(MAC_PAN_COORD_SHORT_ADDRESS, &panCoordShort);
            MAC_MlmeGetSecurityReq(MAC_PAN_COORD_EXTENDED_ADDRESS, &panCoordExtAddr);
            MSA_SecuredDeviceTableUpdate(panID, panCoordShort,
                                         panCoordExtAddr, &msa_NumOfSecuredDevices);
         #endif /* MAC_SECURITY */


          }
          break;

        case MAC_MLME_COMM_STATUS_IND:
          setMessageTipology(0);
          osal_start_timerEx(MSA_TaskId, MSA_READ_EVENT, 100);
          break;

        case MAC_MLME_BEACON_NOTIFY_IND:
          /* Retrieve the message */
          pData = (macCbackEvent_t *) pMsg;

          /* Check for correct beacon payload */
          if (!msa_IsStarted)
          {
            msa_IsSampleBeacon = MSA_BeaconPayLoadCheck(pData->beaconNotifyInd.pSdu);

            /* If it's the correct beacon payload, retrieve the data for association req */
            if (msa_IsSampleBeacon)
            {
              msa_AssociateReq.logicalChannel = MSA_MAC_CHANNEL;
              msa_AssociateReq.coordAddress.addrMode = SADDR_MODE_SHORT;
              msa_AssociateReq.coordAddress.addr.shortAddr = pData->beaconNotifyInd.pPanDesc->coordAddress.addr.shortAddr;
              msa_AssociateReq.coordPanId = pData->beaconNotifyInd.pPanDesc->coordPanId;
              if (msa_IsDirectMsg)
                msa_AssociateReq.capabilityInformation = MAC_CAPABLE_ALLOC_ADDR | MAC_CAPABLE_RX_ON_IDLE;
              else
                msa_AssociateReq.capabilityInformation = MAC_CAPABLE_ALLOC_ADDR;
                msa_AssociateReq.sec.securityLevel = MAC_SEC_LEVEL_NONE;

              /* Retrieve beacon order and superframe order from the beacon */
              msa_BeaconOrder = MAC_SFS_BEACON_ORDER(pData->beaconNotifyInd.pPanDesc->superframeSpec);
              msa_SuperFrameOrder = MAC_SFS_SUPERFRAME_ORDER(pData->beaconNotifyInd.pPanDesc->superframeSpec);
            }
          }

          break;

        case MAC_MLME_START_CNF:
          /* Retrieve the message */
          pData = (macCbackEvent_t *) pMsg;
          /* Set some indicator for the Coordinator */
          
          if ((!msa_IsStarted) && (pData->startCnf.hdr.status == MAC_SUCCESS))
          {
            setMessageTipology(0);
            
            HalLedSet (HAL_LED_2, HAL_LED_MODE_OFF);
            
            for (uint8 i=0;i<2;i++)
            {
             HalLedSet (HAL_LED_1, HAL_LED_MODE_ON);
             halWait(250); halWait(250); halWait(250);
             HalLedSet (HAL_LED_1, HAL_LED_MODE_OFF);
             halWait(250); halWait(250); halWait(250);
            }
             
            
             // DISTOMETRI
            #if defined DISTOMETRI
             ENABLE_SENSTH();  // utilizzo la P0_5 per comandare lo stepup
             halWaitSec(180);
            #endif
            
            // Set Watch-dog procedure
            readAstronomic(clearBuf,0x0F,1); // pulisco eventuali flag pendenti
            writeAstronomic(watchDogAbeAfe,0x09,1);  
                 
            msa_IsStarted = TRUE;
            msa_IsCoordinator = TRUE;
            
            // MUX_TCA9584
            #if defined SHT3X
            checkMux=multiplexerChannel(NO_CHANNEL);
            if (checkMux) 
            {
              checkSensorConnected();
              asm("NOP");
            }
            else countSht3x[0]=0x01; // se non ho il mux collegato setto che ho un sensore TH SHT35 diretto*/
            asm("NOP");
            #endif

           #if defined DEBUG
              testBoard();
           #else
           
            #if defined WATERMARK
             P0DIR |= BV(5);    // Set P0.5 in uscita     
             P0_5 = 0x00;   // Alimento il primo sensore
             halWaitSec(1); 
            #endif
             
           #if defined SPI
            HalLedSet (HAL_LED_1, HAL_LED_MODE_ON);  // led verde ON che indica scrittura
             SpiInit();
             funcToSDCARD(2,tempBuffer,10,"time.txt"); // li leggo sulla SD card
             memset(tempBuffer,0x00,sizeof(tempBuffer)); 
             // read auto or manual 
             SpiInit();
             funcToSDCARD(2,tempBuffer,10,"mode.txt"); // li leggo sulla SD card
             memset(tempBuffer,0x00,sizeof(tempBuffer)); 
             HalLedSet (HAL_LED_1, HAL_LED_MODE_OFF);  // led verde OFF - fine scrittura 
           #endif
            
           
           osal_start_timerEx(MSA_TaskId, MSA_MODEM_EVENT, 10); // Ok chiamo il modem event
           
          #endif
            
          }
          break;

        case MAC_MLME_SCAN_CNF:
          /* Check if there is any Coordinator out there */
          pData = (macCbackEvent_t *) pMsg;

          /* If there is no other on the channel or no other with sampleBeacon start as coordinator */
          if (((pData->scanCnf.resultListSize == 0) && (pData->scanCnf.hdr.status == MAC_NO_BEACON)) || (!msa_IsSampleBeacon))
          {
            MSA_CoordinatorStartup();
          }
          else if ((msa_IsSampleBeacon) && pData->scanCnf.hdr.status == MAC_SUCCESS)
          {
            /* Start the devive up as beacon enabled or not */
            MSA_DeviceStartup();

            /* Call Associate Req */
            MSA_AssociateReq();
          }
          break;

        case MAC_MCPS_DATA_CNF:
          pData = (macCbackEvent_t *) pMsg;

          /* If last transmission completed, ready to send the next one */
          if ((pData->dataCnf.hdr.status == MAC_SUCCESS) ||
              (pData->dataCnf.hdr.status == MAC_CHANNEL_ACCESS_FAILURE) ||
              (pData->dataCnf.hdr.status == MAC_NO_ACK))
          {

            if (msa_State == MSA_SEND_STATE) 
              osal_start_timerEx(MSA_TaskId, MSA_MODEM_EVENT, MSA_MODEM_PERIOD);
          
          }
           

          mac_msg_deallocate((uint8**)&pData->dataCnf.pDataReq);
          break;

        case MAC_MCPS_DATA_IND:
          pData = (macCbackEvent_t*)pMsg;
          uint16 DataPanID;

          DataPanID=pData->dataInd.mac.srcPanId;

          if (DataPanID==msa_PanId)
          {

            //ogni volta che ricevo un pacchetto spengo la radio per aver modo di processarlo senza essere disturbato da altri nodi che mi inviano i propri pacchetti
            setMessageTipology(0);
            /*l'aggiornamento dell'orologio dell'astronomico viene fatto prendendo l'orario dal modem*/

            if (pData->dataInd.mac.dstAddr.addr.shortAddr!=0XFFFF)
            {
               if (msa_State == MSA_SEND_STATE) osal_stop_timerEx(MSA_TaskId, MSA_MODEM_EVENT);

                dataLen=pData->dataInd.msdu.len;

                // salvo i dati da scrivere in eeprom
                for(uint8 j=0;j<dataLen;j++)
                  msa_Data1[j]=pData->dataInd.msdu.p[j];

                //salva il valore di rssi
                rssi=pData->dataInd.mac.rssi;

                if (BUILD_UINT16(msa_Data1[1],msa_Data1[0])==pData->dataInd.mac.srcAddr.addr.shortAddr)
                {
                  //numSensori = countSens(BUILD_UINT32(msa_Data1[12],msa_Data1[13],msa_Data1[14],msa_Data1[15]));
                  numSensori=msa_Data1[16];
                  
                  msa_Data1[19+(numSensori*2)]=rssi;
                }
                
                saveData(dataLen); // salvo i dati
                
                if (msa_State == MSA_SEND_STATE)
                {
                   present=FALSE;

                  for(int i=0; i<MSA_MAX_DEVICE_NUM; i++)
                  {

                      if(pData->dataInd.mac.srcAddr.addr.shortAddr==msa_DeviceRecord[i])
                      {
                        present=TRUE;
                        i=MSA_MAX_DEVICE_NUM;

                      }
                  }

                  if(!present)
                  {

                    msa_DeviceRecord[msa_NumOfDevices]=pData->dataInd.mac.srcAddr.addr.shortAddr;

                    msa_NumOfDevices++;

                    if(msa_NumOfDevices>=MSA_MAX_DEVICE_NUM) msa_NumOfDevices=0;
                    assocShortAddress=pData->dataInd.mac.srcAddr.addr.shortAddr;
                    osal_start_timerEx(MSA_TaskId, MSA_READ_EVENT,60);
                  }

                  else
                  {
                    osal_start_timerEx(MSA_TaskId, MSA_MODEM_EVENT, MSA_MODEM_PERIOD);
                    
                    //terminato il processamento del pacchetto dati riattivo la radio consentire la ricezioni di ulteriori pacchetti
                    setMessageTipology(1);
                  }
                }
                else
                {
                 enableAssociation=TRUE;

                 //terminato il processamento del primo pacchetto dati ricevuto dal nodo associato riattivo la radio per consentire eventuali ulteriori associazioni
                 setMessageTipology(1);
                }
              } 
          }
        }

      /* Deallocate */
      mac_msg_deallocate((uint8 **)&pMsg);
    }

    return events ^ SYS_EVENT_MSG;
  }


  if (events & MSA_SLEEP_EVENT)
  {
    setMessageTipology(0);
    if(msa_State == MSA_ASSOC_STATE)
    {
      
      MAC_MlmeSetReq(MAC_ASSOCIATION_PERMIT, &msa_MACFalse);
      msa_State = MSA_SEND_STATE;
      
      #if defined WATERMARK
       P0DIR |= BV(5);    // Set P0.5 in uscita                                           
       P0_5 = 0x00;   // Alimento il primo sensore ( il WAIT PERIOD � 1500ms quindi sono sicuramente ok)
      #endif
            
      osal_start_timerEx(MSA_TaskId, MSA_MODEM_EVENT, MSA_WAIT_PERIOD);
    
      setMessageTipology(1); 
    }
    else
    {
      
      #if defined WINETTX
       DISABLE_MODEM();  // disattivo modem
       halWait(5);
      #endif

      // se la batteria � scarica imposto un tempo di risveglio maggiore per sperare nella ricarica
       if (lowBattery)  
       {
        if (nextMinToSleep<=TIME_CHECK_BATTERY) // max sleep time = 240 e time check default 120 
        { 
         if ( (TIME_CHECK_BATTERY % nextMinToSleep)!=0 ) 
         nextMinToSleep=( nextMinToSleep + (nextMinToSleep*(TIME_CHECK_BATTERY/nextMinToSleep))); 
         else nextMinToSleep=((nextMinToSleep*(TIME_CHECK_BATTERY/nextMinToSleep))); 
       
         countTxGprs=sendModem; // per spedire immediatamente al prossimo giro
         
         // max sleep retry time 120 standard - calcolo per mantenere la sync
        }
       }
       
         
      #if defined SPI
      memset(charBuffer,0x00,sizeof(charBuffer));
       BytesToChar(readBuffer,0x08); // salva in tempBuffer
       charBuffer[0]=tempBuffer[10];
       charBuffer[1]=tempBuffer[11];
       charBuffer[2]='-';
       charBuffer[3]=tempBuffer[12];
       charBuffer[4]=tempBuffer[13];
       charBuffer[5]='-';
       charBuffer[6]=tempBuffer[14];
       charBuffer[7]=tempBuffer[15];
       charBuffer[8]=' ';
       charBuffer[9]=tempBuffer[6];
       charBuffer[10]=tempBuffer[7];
       charBuffer[11]=':';
       charBuffer[12]=tempBuffer[4];
       charBuffer[13]=tempBuffer[5];
       charBuffer[14]=':';
       charBuffer[15]=tempBuffer[2];
       charBuffer[16]=tempBuffer[3];
       asm("NOP");
       HalLedSet (HAL_LED_1, HAL_LED_MODE_ON);  // led verde ON che indica scrittura
       SpiInit();
       sdOk=funcToSDCARD(0,charBuffer,0x17,"dataora.txt"); // li scrivo sulla SD card
       HalLedSet (HAL_LED_1, HAL_LED_MODE_OFF);  // led verde OFF - fine scrittura  
     #endif
       
       // fw 2.2.1 - sostituisce il vecchio define CENTRALINA perch� lo gestisco dal modem
       if (coordinatorMode)
       minToSleep=nextMinToSleep;
       
       #if defined WEBCAM
        if (doPhotoNow)
        {
         ENABLE_SENSTH(); 
         attivaWebCam=0;
        }
        else //attivo la webcam ogni 3 cicli se non faccio doPhotoNow
        if (attivaWebCam>=N_CYCLE_SHOOT) 
        {
          ENABLE_SENSTH();  
           // devo stare on per 160secondi
          writeAstronomic(watchDogAbeAfe,0x09,2);  // riattivo wd per altri 120 secondi
          halWaitSec(90);
          writeAstronomic(watchDogAbeAfe,0x09,2);  // riattivo wd per altri 120 secondi
          halWaitSec(90);
          DISABLE_SENSTH();
          attivaWebCam=0;
        }
        else DISABLE_SENSTH();
       #endif

    
      if(minToSleep==0xFFFF) incrementAstro(SLEEP_TIME);
      else incrementAstro(minToSleep);
    

      SleepAstro();
      
    }
    return events ^ MSA_SLEEP_EVENT;
  }

   if (events & MSA_READ_EVENT)
  {
      // se la batteria � scarica imposto un tempo di risveglio maggiore per sperare nella ricarica
      if (lowBattery)  
      {
        if (nextMinToSleep<=TIME_CHECK_BATTERY) // max sleep time = 240 e time check default 120 
        { 
         if ( (TIME_CHECK_BATTERY % nextMinToSleep)!=0 ) 
         nextMinToSleep=( nextMinToSleep + (nextMinToSleep*(TIME_CHECK_BATTERY/nextMinToSleep))); 
         else nextMinToSleep=((nextMinToSleep*(TIME_CHECK_BATTERY/nextMinToSleep))); 
       
         countTxGprs=sendModem; // per spedire immediatamente al prossimo giro
         
         // max sleep retry time 120 standard - calcolo per mantenere la sync
        } 
       }
    
      minToSleep=nextMinToSleep;  
      
      // check oscillatore 
      checkOsc();
     
      readAstronomic(readBuffer,0x0000,8);
      
      msa_Data2[0] = readBuffer[0];
      msa_Data2[1] = readBuffer[1];
      msa_Data2[2] = readBuffer[2];
      msa_Data2[3] = readBuffer[3];
      msa_Data2[4] = readBuffer[4];
      msa_Data2[5] = readBuffer[5];
      msa_Data2[6] = readBuffer[6];
      msa_Data2[7] = readBuffer[7];
      msa_Data2[8] = BcdToHex(minWakeUp);
      msa_Data2[9] = HI_UINT16(minToSleep);
      msa_Data2[10] = LO_UINT16(minToSleep);

       /* Accendo la radio */
      setMessageTipology(1);
      
     MSA_McpsDataReq((uint8*)msa_Data2,
                        11,
                        TRUE,
                        assocShortAddress);  
                   
    return events ^ MSA_READ_EVENT;
  }
  
  
 if (events & MSA_MODEM_EVENT)
  {
    uint8 lBuffer[2]={0xFF,0xFF};
    lenghtData=0; // init variabile

    //prima di avviare la fase di invio dati GPRS spengo la radio per evitare di ricevere pacchetti mentre sto facendo operazioni con il modem
    setMessageTipology(0);
    
    preparePacket();
    
      /* leggo quanti dati ho in memoria - lunghezza dei dati */
    if ((readEEProm(lBuffer,MSA_READ_LENGHT,2))>0) // se leggo correttamente
      lenghtData=BUILD_UINT16(lBuffer[0], lBuffer[1]);

    if ( (countTxGprs>=sendModem) || (firstOn) ) // � il momento di trasmettere o � il primo avvio
    {
      countTxGprs=0; // init della variabile
        
      if ( (lenghtData!=0xFFFF) || (firstOn) ) // se ho letto male vado in sleep e al primo avvio vado sempre nel modem 
      {
      
         #if defined WINETTX
            // configure UART pin
            P1DIR|=0x20;
            // Configure UART1 for Alternative 2 => Port P0 (PERCFG.U0CFG = 0)
            PERCFG |=0x02;
            // pull-up su tutta la porta per semplicit�
            P1INP &= ~0xFF;
            // Configure relevant Port P1 pins for peripheral function:
            P1SEL |= 0xD0;
            
            // NUOVO
            // Disable flow control
            U1UCR  &= ~0x40;
            // Flush del buffer seriale
            U1UCR |= 0x80;
            // Enable flow control
            U1UCR |= 0x40;
           
            cmdSent=0x02;
            osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, 10);  // lasciare 10ms per evitare problemi di cambio task
      
          #endif
         }
         else osal_start_timerEx(MSA_TaskId, MSA_SLEEP_EVENT, 10);  
    }
    else osal_start_timerEx(MSA_TaskId, MSA_SLEEP_EVENT, 10); 

    return events ^ MSA_MODEM_EVENT;
  }
  
  
  return 0;
}


/**************************************************************************************************
 *
 * @fn          MAC_CbackEvent
 *
 * @brief       This callback function sends MAC events to the application.
 *              The application must implement this function.  A typical
 *              implementation of this function would allocate an OSAL message,
 *              copy the event parameters to the message, and send the message
 *              to the application's OSAL event handler.  This function may be
 *              executed from task or interrupt context and therefore must
 *              be reentrant.
 *
 * @param       pData - Pointer to parameters structure.
 *
 * @return      None.
 *
 **************************************************************************************************/
void MAC_CbackEvent(macCbackEvent_t *pData)
{

  macCbackEvent_t *pMsg = NULL;

  uint8 len = msa_cbackSizeTable[pData->hdr.event];

  switch (pData->hdr.event)
  {
      case MAC_MLME_BEACON_NOTIFY_IND:

      len += sizeof(macPanDesc_t) + pData->beaconNotifyInd.sduLength +
             MAC_PEND_FIELDS_LEN(pData->beaconNotifyInd.pendAddrSpec);
      if ((pMsg = (macCbackEvent_t *) osal_msg_allocate(len)) != NULL)
      {
        /* Copy data over and pass them up */
        osal_memcpy(pMsg, pData, sizeof(macMlmeBeaconNotifyInd_t));
        pMsg->beaconNotifyInd.pPanDesc = (macPanDesc_t *) ((uint8 *) pMsg + sizeof(macMlmeBeaconNotifyInd_t));
        osal_memcpy(pMsg->beaconNotifyInd.pPanDesc, pData->beaconNotifyInd.pPanDesc, sizeof(macPanDesc_t));
        pMsg->beaconNotifyInd.pSdu = (uint8 *) (pMsg->beaconNotifyInd.pPanDesc + 1);
        osal_memcpy(pMsg->beaconNotifyInd.pSdu, pData->beaconNotifyInd.pSdu, pData->beaconNotifyInd.sduLength);
      }
      break;

    case MAC_MCPS_DATA_IND:
      pMsg = pData;
      break;

    default:
      if ((pMsg = (macCbackEvent_t *) osal_msg_allocate(len)) != NULL)
      {
        osal_memcpy(pMsg, pData, len);
      }
      break;
  }

  if (pMsg != NULL)
  {
    osal_msg_send(MSA_TaskId, (uint8 *) pMsg);
  }
}

/**************************************************************************************************
 *
 * @fn      MAC_CbackCheckPending
 *
 * @brief   Returns the number of indirect messages pending in the application
 *
 * @param   None
 *
 * @return  Number of indirect messages in the application
 *
 **************************************************************************************************/
uint8 MAC_CbackCheckPending(void)
{
  return (0);
}

/**************************************************************************************************
 *
 * @fn      MSA_CoordinatorStartup()
 *
 * @brief   Update the timer per tick
 *
 * @param   n/a
 *
 * @return  None
 *
 **************************************************************************************************/
void MSA_CoordinatorStartup()
{
  macMlmeStartReq_t   startReq;

  /* Setup MAC_EXTENDED_ADDRESS */
  sAddrExtCpy(msa_ExtAddr, msa_ExtAddr1);
  MAC_MlmeSetReq(MAC_EXTENDED_ADDRESS, &msa_ExtAddr);

  /* Setup MAC_SHORT_ADDRESS */
  MAC_MlmeSetReq(MAC_SHORT_ADDRESS, &msa_CoordShortAddr);

  /* Setup MAC_BEACON_PAYLOAD_LENGTH */
  MAC_MlmeSetReq(MAC_BEACON_PAYLOAD_LENGTH, &msa_BeaconPayloadLen);

  /* Setup MAC_BEACON_PAYLOAD */
  MAC_MlmeSetReq(MAC_BEACON_PAYLOAD, &msa_BeaconPayload);

  /* Enable RX */
  MAC_MlmeSetReq(MAC_RX_ON_WHEN_IDLE, &msa_MACTrue);

  /* Setup MAC_ASSOCIATION_PERMIT */
  MAC_MlmeSetReq(MAC_ASSOCIATION_PERMIT, &msa_MACFalse);

   uint8 maxRetries=0;
  MAC_MlmeSetReq(MAC_MAX_FRAME_RETRIES, &maxRetries);

  /* Fill in the information for the start request structure */
  startReq.startTime = 0;
  startReq.panId = msa_PanId;
  startReq.logicalChannel = MSA_MAC_CHANNEL;
  startReq.beaconOrder = msa_BeaconOrder;
  startReq.superframeOrder = msa_SuperFrameOrder;
  startReq.panCoordinator = TRUE;
  startReq.batteryLifeExt = FALSE;
  startReq.coordRealignment = FALSE;
  startReq.realignSec.securityLevel = FALSE;
  startReq.beaconSec.securityLevel = FALSE;

  /* Call start request to start the device as a coordinator */
  MAC_MlmeStartReq(&startReq);

  /* Allow Beacon mode coordinator to sleep */
  if (msa_BeaconOrder != 15)
  {
    /* Power saving */
    MSA_PowerMgr (MSA_PWR_MGMT_ENABLED);
  }

}

/**************************************************************************************************
 *
 * @fn      MSA_DeviceStartup()
 *
 * @brief   Update the timer per tick
 *
 * @param   beaconEnable: TRUE/FALSE
 *
 * @return  None
 *
 **************************************************************************************************/
void MSA_DeviceStartup()
{

  /* Setup MAC_BEACON_PAYLOAD_LENGTH */
  MAC_MlmeSetReq(MAC_BEACON_PAYLOAD_LENGTH, &msa_BeaconPayloadLen);

  /* Setup MAC_BEACON_PAYLOAD */
  MAC_MlmeSetReq(MAC_BEACON_PAYLOAD, &msa_BeaconPayload);

  /* Setup PAN ID */
  MAC_MlmeSetReq(MAC_PAN_ID, &msa_PanId);

  /* This device is setup for Direct Message */
  if (msa_IsDirectMsg)
    MAC_MlmeSetReq(MAC_RX_ON_WHEN_IDLE, &msa_MACTrue);
  else
    MAC_MlmeSetReq(MAC_RX_ON_WHEN_IDLE, &msa_MACFalse);

  /* Setup Coordinator short address */
  MAC_MlmeSetReq(MAC_COORD_SHORT_ADDRESS, &msa_AssociateReq.coordAddress.addr.shortAddr);

#ifdef MAC_SECURITY
  /* Setup Coordinator short address for security */
  MAC_MlmeSetSecurityReq(MAC_PAN_COORD_SHORT_ADDRESS, &msa_AssociateReq.coordAddress.addr.shortAddr);
#endif /* MAC_SECURITY */

  if (msa_BeaconOrder != 15)
  {
    /* Setup Beacon Order */
    MAC_MlmeSetReq(MAC_BEACON_ORDER, &msa_BeaconOrder);

    /* Setup Super Frame Order */
    MAC_MlmeSetReq(MAC_SUPERFRAME_ORDER, &msa_SuperFrameOrder);

    /* Sync request */
    MSA_SyncReq();
  }

  /* Power saving */
  MSA_PowerMgr (MSA_PWR_MGMT_ENABLED);

}

/**************************************************************************************************
 *
 * @fn      MSA_AssociateReq()
 *
 * @brief
 *
 * @param    None
 *
 * @return  None
 *
 **************************************************************************************************/
void MSA_AssociateReq(void)
{
  MAC_MlmeAssociateReq(&msa_AssociateReq);
}

/**************************************************************************************************
 *
 * @fn      MSA_AssociateRsp()
 *
 * @brief   This routine is called by Associate_Ind inorder to return the response to the device
 *
 * @param   pMsg - pointer to the structure recieved by MAC_MLME_ASSOCIATE_IND
 *
 * @return  None
 *
 **************************************************************************************************/
void MSA_AssociateRsp(macCbackEvent_t* pMsg)
{

  assocExtAdd[0]=pMsg->associateInd.deviceAddress[0];
  assocExtAdd[1]=pMsg->associateInd.deviceAddress[1];

  assocShortAddress = BUILD_UINT16(assocExtAdd[0], 0x01);


#ifdef MAC_SECURITY
  uint16 panID;

  /* Add device to device table for security */
  MAC_MlmeGetReq(MAC_PAN_ID, &panID);
  MSA_SecuredDeviceTableUpdate(panID, assocShortAddress,
                               pMsg->associateInd.deviceAddress,
                               &msa_NumOfSecuredDevices);
#endif /* MAC_SECURITY */

  /* Fill in association respond message */
  sAddrExtCpy(msa_AssociateRsp.deviceAddress, pMsg->associateInd.deviceAddress);
  msa_AssociateRsp.assocShortAddress = assocShortAddress;
  msa_AssociateRsp.status = MAC_SUCCESS;
  msa_AssociateRsp.sec.securityLevel = MAC_SEC_LEVEL_NONE;
  
  /* Call Associate Response */
 MAC_MlmeAssociateRsp(&msa_AssociateRsp);

}

/**************************************************************************************************
 *
 * @fn      MSA_McpsDataReq()
 *
 * @brief   This routine calls the Data Request
 *
 * @param   data       - contains the data that would be sent
 *          dataLength - length of the data that will be sent
 *
 * @return  None
 *
 **************************************************************************************************/
void MSA_McpsDataReq(uint8* data, uint8 dataLength, bool directMsg, uint16 dstShortAddr)
{
  macMcpsDataReq_t  *pData;
  static uint8      handle = 0;

  if ((pData = MAC_McpsDataAlloc(dataLength, msa_securityLevel, msa_keyIdMode)) != NULL)
  {
    pData->mac.srcAddrMode = SADDR_MODE_SHORT;
    pData->mac.dstAddr.addrMode = SADDR_MODE_SHORT;
    pData->mac.dstAddr.addr.shortAddr = dstShortAddr;
    pData->mac.dstPanId = msa_PanId;
    pData->mac.msduHandle = handle++;
    pData->mac.txOptions = MAC_TXOPTION_ACK;

    /* MAC security parameters */
    osal_memcpy( pData->sec.keySource, msa_keySource, MAC_KEY_SOURCE_MAX_LEN );
    pData->sec.securityLevel = msa_securityLevel;
    pData->sec.keyIdMode = msa_keyIdMode;
    pData->sec.keyIndex = msa_keyIndex;

    /* If it's the coordinator and the device is in-direct message */
    if (msa_IsCoordinator)
    {
      if (!directMsg)
      {
        pData->mac.txOptions |= MAC_TXOPTION_INDIRECT;
      }
    }

    /* Copy data */
    osal_memcpy (pData->msdu.p, data, dataLength);

    /* Send out data request */
    MAC_McpsDataReq(pData);
  }

}

/**************************************************************************************************
 *
 * @fn      MSA_McpsPollReq()
 *
 * @brief   Performs a poll request on the coordinator
 *
 * @param   None
 *
 * @return  None
 *
 **************************************************************************************************/
void MSA_McpsPollReq(void)
{
  macMlmePollReq_t  pollReq;

  /* Fill in information for poll request */
  pollReq.coordAddress.addrMode = SADDR_MODE_SHORT;
  pollReq.coordAddress.addr.shortAddr = msa_CoordShortAddr;
  pollReq.coordPanId = msa_PanId;
  pollReq.sec.securityLevel = MAC_SEC_LEVEL_NONE;

  /* Call poll reuqest */
  MAC_MlmePollReq(&pollReq);
}

/**************************************************************************************************
 *
 * @fn      MacSampelApp_ScanReq()
 *
 * @brief   Performs active scan on specified channel
 *
 * @param   None
 *
 * @return  None
 *
 **************************************************************************************************/
void MSA_ScanReq(uint8 scanType, uint8 scanDuration)
{
  macMlmeScanReq_t scanReq;

  /* Fill in information for scan request structure */
  scanReq.scanChannels = (uint32) 1 << MSA_MAC_CHANNEL;
  scanReq.scanType = scanType;
  scanReq.scanDuration = scanDuration;
  scanReq.maxResults = MSA_MAC_MAX_RESULTS;
  scanReq.permitJoining = MSA_EBR_PERMITJOINING;
  scanReq.linkQuality = MSA_EBR_LINKQUALITY;
  scanReq.percentFilter = MSA_EBR_PERCENTFILTER;
  scanReq.result.pPanDescriptor = msa_PanDesc;
  osal_memset(&scanReq.sec, 0, sizeof(macSec_t));

  /* Call scan request */
  MAC_MlmeScanReq(&scanReq);
}

/**************************************************************************************************
 *
 * @fn      MSA_SyncReq()
 *
 * @brief   Sync Request
 *
 * @param   None
 *
 * @return  None
 *
 **************************************************************************************************/
void MSA_SyncReq(void)
{
  macMlmeSyncReq_t syncReq;

  /* Fill in information for sync request structure */
  syncReq.logicalChannel = MSA_MAC_CHANNEL;
  syncReq.channelPage    = MAC_CHANNEL_PAGE_0;
  syncReq.trackBeacon    = TRUE;

  /* Call sync request */
  MAC_MlmeSyncReq(&syncReq);
}

/**************************************************************************************************
 *
 * @fn      MSA_BeaconPayLoadCheck()
 *
 * @brief   Check if the beacon comes from MSA but not zigbee
 *
 * @param   pSdu - pointer to the buffer that contains the data
 *
 * @return  TRUE or FALSE
 *
 **************************************************************************************************/
bool MSA_BeaconPayLoadCheck(uint8* pSdu)
{
  uint8 i = 0;
  for (i=0; i<msa_BeaconPayloadLen; i++)
  {
    if (pSdu[i] != msa_BeaconPayload[i])
    {
      return FALSE;
    }
  }

  return TRUE;
}

/**************************************************************************************************
 *
 * @fn      MSA_DataCheck()
 *
 * @brief   Check if the data match with the predefined data
 *
 * @param    data - pointer to the buffer where the data will be checked against the predefined data
 *           dataLength - length of the data
 *
 * @return  TRUE if the data matched else it's the response / echo packet
 *
 **************************************************************************************************/
bool MSA_DataCheck(uint8* data, uint8 dataLength)
{
  uint8 i = 0;

  if (data[0] == dataLength)
  {
    for (i=MSA_HEADER_LENGTH; i<(data[0] - 1); i++)
    {
       if (data[i] != msa_Data1[i])
         return FALSE;
    }
  }
  else
  {
    return FALSE;
  }
  return TRUE;
}

/**************************************************************************************************
 *
 * @fn      MSA_HandleKeys
 *
 * @brief   Callback service for keys
 *
 * @param   keys  - keys that were pressed
 *          state - shifted
 *
 * @return  void
 *
 **************************************************************************************************/
void MSA_HandleKeys(uint8 keys, uint8 shift)
{
  uint8 WDBuff[1];
  uint8 stopBit[1];

  if ( keys & HAL_KEY_SW_6 )
  {
  #if defined WINETAQ

   /* wait for 32Mhz stable clock */
   while ((CLKCONSTA & 0x40) != 0);
   {
     asm("NOP");
   }
   
   /* CC2530 Workaround */
   macMcuTimer2OverflowWorkaround();
   
   #if defined WEBCAM
    if (attivaWebCam<0xFF) attivaWebCam++; // incremento per avere i cicli di ON al controllo
    else attivaWebCam=0;
   #endif
   
   if (countTxGprs<sendModem) countTxGprs++; // incremento solo se � andato a buon fine download tempi
   
    InputOutputWakeUp();
    
    #if defined DEBUG
      testBoard();
    #endif

    ENABLE_PERIPH(); // attivo
    halWait(20); // tps stabile
    
    //**** CHECK WD E OF ****//
    readAstronomic(WDBuff,0x0F,0x01); // leggo il flag 
    halWait(5);
    writeAstronomic(0x00,0x0F,1);  // scrivo a zero il registro
   
    if ( (WDBuff[0] & 0x80) || (WDBuff[0] & 0x20) || (WDBuff[0] & 0x04) ) // valuto se � scattato il WD o TF o OF
    {
      if (WDBuff[0] & 0x04) // procedura per OF - riabilito oscillatore
      {  
        // impostare lo STOP a 1 e poi a 0
        stopBit[0]=0x80;
        writeAstronomic(stopBit,0x01,0x01);
        halWait(5);
        stopBit[0]=0x00;
        writeAstronomic(stopBit,0x01,0x01);
        halWait(5);
        writeAstronomic(stopBit,0x0F,1); // setto a zero OF 
      }
      
      err=8; 
      resetBoard();   // resetto il device
    }
    // **************//
    
    restoreTime();  // ripristino i registri esterni per leggere ora e data

    // check oscillatore 
    checkOsc();
    
    // DISTOMETRI
    #if defined DISTOMETRI
     ENABLE_SENSTH();  // utilizzo la P0_5 per comandare lo stepup
     halWaitSec(180);
    #endif
  
    readAstronomic(matchBuffer,0x0000,8);
    // setto a zero AFE e ABE per sicurezza e imposto il WD
    writeAstronomic(watchDogAbeAfe,0x09,1);
    
    for(uint8 i=0; i<MSA_MAX_DEVICE_NUM; i++) msa_DeviceRecord[i] = 0xFFFF;
    msa_NumOfDevices=0;

    msa_State = MSA_ASSOC_STATE;
    assocShortAddress=0xFFFF;
    enableAssociation=TRUE;
    permitAssoc=TRUE;

    MAC_MlmeSetReq(MAC_ASSOCIATION_PERMIT, &msa_MACTrue);
    osal_start_timerEx(MSA_TaskId, MSA_SLEEP_EVENT, MSA_ASSOCIATION_PERIOD);

    // Receiver ON
    setMessageTipology(1);
  }

#endif

}



void readIEEEAddressFromNv()
{
  uint8 aExtendedAddress[8];

  // Attempt to read the extended address from the designated location in the Info Page.
  osal_memcpy(aExtendedAddress, (uint8 *)(P_INFOPAGE+HAL_INFOP_IEEE_OSET), HAL_FLASH_IEEE_SIZE);

  // write the Extended Address in the NV Memory space - First IEEE
  (void)osal_nv_write(ZCD_NV_EXTADDR, 0, HAL_FLASH_IEEE_SIZE, &aExtendedAddress);

  // leggo dalla memoria il Second IEEE impostato con SMARTRF Programmer
  HalFlashRead(HAL_FLASH_IEEE_PAGE, HAL_FLASH_IEEE_OSET, aSecExtendedAddress, Z_EXTADDR_LEN);

  // Set the MAC extended address - uso il secondary address
  MAC_MlmeSetReq(MAC_EXTENDED_ADDRESS, &aSecExtendedAddress);
  
   // check indirizzo nodo
  //if ( (aSecExtendedAddress[0]!=0x78) && (aSecExtendedAddress[1]!=0x1C) && (aSecExtendedAddress[2]!=0x00) && (aSecExtendedAddress[3]!=0x06) )
  //resetBoard();
 

}

void incrementAstro(uint16 min)
{
   restoreTime();
   
   diffMin=diffTimestamp();
   
   if (diffMin<min) min=min-diffMin;
   else
   {
     divMin=diffMin/min;
     min=((divMin+1)*min)-diffMin;
   }
   
    // controllo che faccio solamente se il tempo � diverso da 1 minuto altrimenti
    // altrimenti sforando si sveglier� al minuto successivo
   
   if (min!=1)
   {
      // se sono vicino al minuto aspetto 
      if (astroBuffer[1]>=57) 
      {
        halWaitSec(10); // aspetto 10 secondi in modo da passare sicuramente al minuto successivo
        min--;
        // check oscillatore 
        checkOsc();
        readAstronomic(timeBuffer,0x0000,0x08); // leggo orario attuale
        astroConv(timeBuffer); 
      }
    }    // se sono a 57 rischio di settare un tempo gia passato
    else if (astroBuffer[1]>=57) min++;
    
    // setto WD con 10 minuti di margine dal risveglio
    #if defined WD_AQ22
     setWDTime(min+WD_TIME_RESET);
    #endif
   
    min=astroBuffer[2]+min;
    
    defineDate(min);
  
    newSecondi=BcdToHex(minWakeUp);
    
   setAstroInterrupt(newSecondi,newMinuti,newOre,newGiorno,newMese);
  
}

void defineDate(uint16 min){

  newAnno=astroBuffer[7]; //anno
  newMese=astroBuffer[6];  // mese
  newGiorno=astroBuffer[5]; // giorno
  newdayWeek=astroBuffer[4];
  newOre=astroBuffer[3]; // ore
 // newMinuti=astroBuffer[2]; // minuti letti + minuti di sleep
  newSecondi=astroBuffer[1]; // secondi
  newDecCent=astroBuffer[0];

  while (newSecondi>=60)
     {
      newSecondi=newSecondi-60;
      min++;
     }

   while (min>=60)
     {
      min=min-60;
      newOre++;
     }
  
    newMinuti=LO_UINT16(min);

   while (newOre>=24)
     {
      newOre=newOre-24;
      newGiorno++;
     }

   checkMeseUp(newMese);
   
   // ultimo check time
  timeRead.seconds=newSecondi;
  timeRead.minutes=newMinuti;
  timeRead.hour=newOre;
  timeRead.day=newGiorno-1;
  timeRead.month=newMese-1;
  timeRead.year=2000+newAnno;

  secStart=osal_ConvertUTCSecs(&timeRead); // memorizzo i secondi letti
  
  if (secStart<=secOld)  // errore grave di lettura
  {
    err=11;
    asm("NOP");
    resetBoard();
  }
  /******** Fine Check *******/
  
   //check errori critici su settaggio data/ora
  if ( (newSecondi>59) || (newMinuti>59) || (newOre>23) || (newGiorno<1) || (newGiorno>31) || (newMese<1) || (newMese>12) )
  {
   resetBoard();  
  } 
  
  newSecondi=BcdToHex(newSecondi);;
  newMinuti=BcdToHex(newMinuti);
  newOre=BcdToHex(newOre);
  newGiorno=BcdToHex(newGiorno);
  newMese=BcdToHex(newMese);
  newAnno=BcdToHex(newAnno);
  
}

void checkMeseUp(uint8 mese)
{
 switch (newMese)     // switch sul mese
      {
      case 1:
        if (newGiorno>31)
        {  // giorno data
         newGiorno=newGiorno-31;
         newMese=2;
        }
       break;

      case 2:
        if ((newAnno%4)==0) // anno  bisestile
        {
         if (newGiorno>29)
          {
            newGiorno=newGiorno-29;
            newMese=3;
           }
        }
        else if (newGiorno>28)
        {
          newGiorno=newGiorno-28;
          newMese=3;
        }
        break;

      case 3:
        if (newGiorno>31)
        {
          newGiorno=newGiorno-31;
          newMese=4;
        }
        break;

       case 4:
         if (newGiorno>30)
         {
          newGiorno=newGiorno-30;
          newMese=5;
         }
        break;

       case 5:
        if (newGiorno>31)
        {
          newGiorno=newGiorno-31;
          newMese=6;
        }
        break;

        case 6:
        if (newGiorno>30)
        {
          newGiorno=newGiorno-30;
          newMese=7;
        }
        break;

       case 7:
        if (newGiorno>31)
        {
          newGiorno=newGiorno-31;
          newMese=8;
        }
        break;

       case 8:
       if (newGiorno>31)
       {
          newGiorno=newGiorno-31;
          newMese=9;
       }
       break;

       case 9:
       if (newGiorno>30)
       {
          newGiorno=newGiorno-30;
          newMese=10;
       }
       break;

       case 10:
       if (newGiorno>31)
       {
          newGiorno=newGiorno-31;
          newMese=11;
       }
       break;

      case 11:
      if (newGiorno>30)
      {
          newGiorno=newGiorno-30;
          newMese=12;
      }
      break;

      case 12:
      if (newGiorno>31)
      {
          newGiorno=newGiorno-31;
          newMese=1;
          newAnno++;
      }
      break;
    }
}

#if defined WD_AQ22
void setWDTime(uint16 minute)
{
  uint16 minToSet=minute;
  
  // leggo astronomico per avere il tempo aggiornato
  readAstronomic(readBuffer,0x0000,8);
  astroConv(readBuffer);
  
  minToSet=astroBuffer[2]+minToSet;
  
  defineDate(minToSet);
    
  setWDInterrupt(newSecondi,newMinuti,newOre,newGiorno,newMese);
}
#endif

void SleepAstro()
{

    /* set RX off */
    setMessageTipology(0);
    
    #if defined WINETTX
     DISABLE_MODEM();  // disattivo modem
     halWait(100);
    #endif 

   #if defined WINETAQ
       DISABLE_SENSTH();  // disattiva sensori
       halWait(100);
       DISABLE_PERIPH();  // disattiva le periferiche
       halWait(100);
       HAL_TURN_OFF_LED1(); // spegni led  
       halWait(100);
       #if !defined DEBUG
        HAL_TURN_OFF_LED2(); // spegni led 
        halWait(100);
      #endif
       InputOutputSleep(); // porte configurate per basso consumo
       P0IFG=0x00;
     #endif
       
     time=(minToSleep+ST_TIME_RESET);// 4 minuti oltre il risveglio 
     Sleep(time);  // sleep timer per risveglio di sicurezza  e reset

}



void Sleep(uint16 timeout)
{
    /* Sleep in modalit� PM2 */
    halSleepSetTimer(SLEEP_MS_TO_320US(SLEEP_TIME_RPT));

    /* Interrupt Sleep Timer Enable and Interrupt on */
    SLEEP_TIMER_ENABLE_INT();
    
    SLEEPCMD &= ~PMODE; /* clear mode bits */
    SLEEPCMD |= 0x02;   /* set mode bits   */
    
    while (!(STLOAD & LDRDY));
    {
      PCON = PCON_IDLE;
      asm("NOP");
    }  

  if (IEN0 & 0x20) // se lo sleep timer � attivo ovvero sono entrato qui non da risveglio astronomico
  {  
    if(time==0) WakeUp();
        else {  time--;   Sleep(time);  }  // ricorsivo finch� time=0 e quindi Wake up
  }
}

void WakeUp()
{
  uint8 writeBuffer[2]={0x88,0x00};
 
  // sicuramente astronomico � partito quindi resetto in modo che ripeschi l'orario corretto
  ENABLE_PERIPH();
  halWait(50);
 
  writeAstronomic(writeBuffer,0x03,0x01);
  
  resetBoard(); 
}


/*uint8 countSens(uint32 id){

  uint8 numSens=0;
  uint32 maskVar=0x00000001;

        for(uint8 j=0; j<32; j++){
          if ((id & maskVar) == maskVar)
          {
            numSens++;
          }
        maskVar=maskVar<<1;
        }
        return numSens;
}*/

bool saveData(uint8 lengthOfData)
{
  uint8 lBuffer[2];
  uint16 totLenght1=0;
  uint8 retControl;
  
   /* leggo quanti dati ho in memoria - lunghezza dei dati */
   if ((readEEProm(lBuffer,MSA_READ_LENGHT,2))>0) // se leggo correttamente
   {   
    totLenght1=BUILD_UINT16(lBuffer[0], lBuffer[1]);

    if(totLenght1==0xFFFF) totLenght1=0;

    //setto l'indirzzo di partenza per la scrittura
    writeAddress=MSA_READ_ADDRESS+totLenght1;

    /*Se i dati salvati superano il limite della EEPROM li riscrivo (tutti quelli in memoria vengono persi)*/
    if((writeAddress+lengthOfData)>PROM_SIZE-2)   // -2: SIZE-2byte lenght
      writeAddress=MSA_READ_ADDRESS;

    //salva in EEPROM i dati ricevuti
    retControl=writeEEProm(msa_Data1,writeAddress,lengthOfData);

    // aggiorna address se retControl>0
    if (retControl>0) 
    {
      writeAddress=writeAddress+lengthOfData;

      totLenght1=writeAddress-MSA_READ_ADDRESS;

      lBuffer[0]=LO_UINT16(totLenght1);
      lBuffer[1]=HI_UINT16(totLenght1);
    
      retControl=writeEEProm(lBuffer,MSA_READ_LENGHT,2);  // salvo la lunghezza dei dati
      
      return 1; // ok scrittura
    }
   } 
   else { 
     asm("NOP"); }
   return 0; // problema di scrittura
}

void preparePacket(){

  uint32 maskVar; 
  uint8 lenMeas;
  uint32 IDSensor;
  #if defined FLORA_1 || DEC5TE_P04 || defined SENTEK_10A || defined SENTEK_10B || defined SENTEK_10C || defined SENTEK_10D || defined SENTEK_10E || defined SENTEK_10F || defined SENTEK_90 || defined SENTEK_120  || defined SENTEK_60 || defined SENTEK_30 || defined SENTEK_30_B || defined MPS6_P04 || defined DECGS3_P04  || defined ACC_TDR || defined ACC_TDTP || defined DECCDT
  uint8 saveP0Inp;
  #endif
  uint16 a=15;

   /*Byte 0 e 1 ID Nodo*/
    msa_Data1[0]=HI_UINT16(msa_CoordShortAddr);
    msa_Data1[1]=LO_UINT16(msa_CoordShortAddr);

     // leggo dalla memoria il Second IEEE impostato con SMARTRF Programmer
    HalFlashRead(HAL_FLASH_IEEE_PAGE, HAL_FLASH_IEEE_OSET, aSecExtendedAddress, Z_EXTADDR_LEN);

    msa_Data1[2]=aSecExtendedAddress[1];
    msa_Data1[3]=aSecExtendedAddress[0];

    // check oscillatore 
    checkOsc();
     
    /*6 byte relativi alla date-time della lettura*/
    readAstronomic(readBuffer,0x0000,8);
     
    msa_Data1[4]=readBuffer[5];
    msa_Data1[5]=readBuffer[6];
    msa_Data1[6]=readBuffer[7];
    msa_Data1[7]=readBuffer[3];
    msa_Data1[8]=readBuffer[2];
    msa_Data1[9]=readBuffer[1];
   
    /*Recupero 3� e 4� byte dell'indirizzo esteso per definire il tipo di sensori collegati*/
    IDSensor=BUILD_UINT32(aSecExtendedAddress[2],aSecExtendedAddress[3],aSecExtendedAddress[4],aSecExtendedAddress[5]);

    msa_Data1[10]=BREAK_UINT32(IDSensor,3);
    msa_Data1[11]=BREAK_UINT32(IDSensor,2);
    msa_Data1[12]=BREAK_UINT32(IDSensor,1);
    msa_Data1[13]=BREAK_UINT32(IDSensor,0);
    
   
     #if defined PIOGGIA
     if (longReadAttiny85(ATTINY85_RAIN_R,pioggia,0x00,0x02)==0) // lettura errata
      { 
        pioggia[0]=0xFF;
        pioggia[1]=0xFF;
      }
      halWait(20); // margine 
     #endif
      
    #if defined VENTO
      if (longReadAttiny85(ATTINY85_WIND_R,vento,0x00,0x0A)==0) // lettura errata
      {
       vento[0]=0xFF;
       vento[1]=0xFF;
       vento[2]=0x7F; // valore non possibile  
      } 
       halWait(20); // margine
    #endif
      
     #if defined PRESS0
      if (longReadAttiny85(ATTINY85_PRESS0_R,press0,0x00,0x05)==0) // lettura errata
      {
       //1 byte : Stato linea (DD -> flusso non rilevato , CC-> flusso rilevato) 
       //2-3-4 byte : tempo trascorso ( fine - start) in millisecondi
       press0[0]=0xFF;
      }
       halWait(20); // margine
    #endif
     
    #if defined PRESS1
      if (longReadAttiny85(ATTINY85_PRESS1_R,press1,0x00,0x05)==0) // lettura errata
      {
       //1 byte : Stato linea (DD -> flusso non rilevato , CC-> flusso rilevato) 
       //2-3-4 byte : tempo trascorso ( fine - start) in millisecondi
       press1[0]=0xFF;
      }
      halWait(20); // margine
    #endif
    
    #if defined PRESS2
      if (longReadAttiny85(ATTINY85_PRESS2_R,press2,0x00,0x05)==0) // lettura errata
      {
       //1 byte : Stato linea (DD -> flusso non rilevato , CC-> flusso rilevato) 
       //2-3-4 byte : tempo trascorso ( fine - start) in millisecondi
       press2[0]=0xFF;
      }
        halWait(20); // margine
    #endif
      
    #if defined PRESS3
      if (longReadAttiny85(ATTINY85_PRESS3_R,press3,0x00,0x05)==0) // lettura errata
      {
       //1 byte : Stato linea (DD -> flusso non rilevato , CC-> flusso rilevato) 
       //2-3-4 byte : tempo trascorso ( fine - start) in millisecondi
       press3[0]=0xFF;
      }
       halWait(20); // margine
    #endif
      
    #if defined SMEC300
       // disattivo le periferiche per leggere correttamente il sensore
      DISABLE_PERIPH(); 
      halWait(5);
      //attivo alimentazione dei sensori 
      ENABLE_SENSTH();
      halWaitUs(6100);
      readSensorSM[0]=getAdcChannel(0x04);  //HUMIDITY
      halWaitUs(4000);
      readSensorSM[1]=getAdcChannel(0x04);  //TEMP
      halWaitUs(3000);
      readSensorSM[2]=getAdcChannel(0x04);   //EC attivi i sensori da 1300ms
      DISABLE_SENSTH();
    #endif
   

    ENABLE_PERIPH(); // attivo per leggere correttamente la batteria  
    halWait(20); // tps stabile 
   
    ENABLE_SENSTH(); // attivo i sensori
    halWaitms(SENSOR_ON_TIME); // attendo SENSOR_ON_TIME per essere stabile 100ms per SM100
      
     // leggo la batteria e salvo lo stato carica
    lowBattery=checkBattery(); 
   
    DISABLE_PERIPH(); // disattivo le periferiche per leggere correttamente le porte sensori
    halWait(20); // wait stabile
      
    #if defined FLORA_1 || DEC5TE_P04 || defined SENTEK_10A || defined SENTEK_10B || defined SENTEK_10C || defined SENTEK_10D || defined SENTEK_10E || defined SENTEK_10F || defined SENTEK_90 || defined SENTEK_120 || defined SENTEK_60 || defined SENTEK_30 || defined SENTEK_30_B || defined MPS6_P04 || defined DECGS3_P04 || defined ACC_TDR || defined ACC_TDTP || defined DECCDT
    saveP0Inp=P0INP;  
    #endif
    
    #if defined DEC5TE_P04
     IO_DIR_PORT_PIN( 0, 4, IO_OUT );
     P0_4=0;
     P0INP=0x10;   
     halWaitms(20);   
     clearSdiBuffers();
     sdiRetries=askSdi(sdiDevice,sdiBuffer,SDI_DEFAULT_TRIM);
  
      if (sdiRetries!=0x00)
      {
        HalLedSet (HAL_LED_1, HAL_LED_MODE_OFF);
        HalLedSet (HAL_LED_2, HAL_LED_MODE_OFF);
        Te5Parser(sdiBuffer,resBuffer);
      }
      else{
      HalLedSet (HAL_LED_1, HAL_LED_MODE_ON);
      HalLedSet (HAL_LED_2, HAL_LED_MODE_ON);
      resBuffer[0]=0x00;
      resBuffer[1]=0x00;
      resBuffer[2]=0x00;
      }
     #endif
      
    #if defined SENTEK_120
     IO_DIR_PORT_PIN( 0, 4, IO_OUT );
     P0_4=0;
     P0INP=0x10;   
     halWaitms(20);   
     clearSdiBuffers();
     
      sdiRetries=askSentek(0x46,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x0C,SOIL);
      if (sdiRetries!=0x00) 
      {
        ParserSentek(sdiBuffer,resSentekHum,sizeof(sdiBuffer),0x01,SOIL);
        ParserSentek(sdiBuffer2,resSentekHum,sizeof(sdiBuffer2),0x02,SOIL);
        ParserSentek(sdiBuffer3,resSentekHumB,sizeof(sdiBuffer3),0x01,SOIL);
        ParserSentek(sdiBuffer4,resSentekHumB,sizeof(sdiBuffer4),0x02,SOIL);
      }
      
     #ifdef SENTEK_120_SAL
      halWaitms(200);
      sdiRetries=0x03;
      clearSdiBuffers();
           
      sdiRetries=askSentek(0x46,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x0C,SAL);
      if (sdiRetries!=0x00) 
      {
        ParserSentek(sdiBuffer,resSentekSal,sizeof(sdiBuffer),0x01,SAL);
        ParserSentek(sdiBuffer2,resSentekSal,sizeof(sdiBuffer2),0x02,SAL);
        ParserSentek(sdiBuffer3,resSentekSalB,sizeof(sdiBuffer3),0x01,SAL);
        ParserSentek(sdiBuffer4,resSentekSalB,sizeof(sdiBuffer4),0x02,SAL);
      }
     #endif
     
      halWaitms(200);
      sdiRetries=0x03;
      clearSdiBuffers();
      
      sdiRetries=askSentek(0x46,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x0C,TEMP);
      if (sdiRetries!=0x00) 
      {
        ParserSentek(sdiBuffer,resSentekTemp,sizeof(sdiBuffer),0x01,TEMP);
        ParserSentek(sdiBuffer2,resSentekTemp,sizeof(sdiBuffer2),0x02,TEMP);
        ParserSentek(sdiBuffer3,resSentekTempB,sizeof(sdiBuffer3),0x01,TEMP);
        ParserSentek(sdiBuffer4,resSentekTempB,sizeof(sdiBuffer4),0x02,TEMP);
      }
      asm("NOP");
    #endif 

      
   #if defined SENTEK_90
     IO_DIR_PORT_PIN( 0, 4, IO_OUT );
     P0_4=0;
     P0INP=0x10;   
     halWaitms(20);   
     clearSdiBuffers();
     
      sdiRetries=askSentek(0x1E,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x09,SOIL);
      asm("NOP");
      if (sdiRetries!=0x00) 
      {
        ParserSentek(sdiBuffer,resSentekHum,sizeof(sdiBuffer),0x01,SOIL);
        asm("NOP");
        ParserSentek(sdiBuffer2,resSentekHum,sizeof(sdiBuffer2),0x02,SOIL);
        asm("NOP");
        ParserSentek(sdiBuffer3,resSentekHumB,sizeof(sdiBuffer3),0x01,SOIL);
        asm("NOP");
      }
       
     #ifdef SENTEK_90_SAL
      halWaitms(200);
      sdiRetries=0x03;
      clearSdiBuffers();
           
      sdiRetries=askSentek(0x1E,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x09,SAL);
      if (sdiRetries!=0x00) 
      {
        ParserSentek(sdiBuffer,resSentekSal,sizeof(sdiBuffer),0x01,SAL);
        ParserSentek(sdiBuffer2,resSentekSal,sizeof(sdiBuffer2),0x02,SAL);
        ParserSentek(sdiBuffer3,resSentekSalB,sizeof(sdiBuffer3),0x01,SAL);
      }
     #endif
      halWaitms(200);
      sdiRetries=0x03;
      clearSdiBuffers();
      
      sdiRetries=askSentek(0x1E,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x09,TEMP);
      asm("NOP");
      if (sdiRetries!=0x00) 
      {
        ParserSentek(sdiBuffer,resSentekTemp,sizeof(sdiBuffer),0x01,TEMP);
        asm("NOP");
        ParserSentek(sdiBuffer2,resSentekTemp,sizeof(sdiBuffer2),0x02,TEMP);
        asm("NOP");
        ParserSentek(sdiBuffer3,resSentekTempB,sizeof(sdiBuffer3),0x01,TEMP);
        asm("NOP");    
      }
      asm("NOP");
    #endif 

    
    #if defined SENTEK_60
     IO_DIR_PORT_PIN( 0, 4, IO_OUT );
     P0_4=0;
     P0INP=0x10;   
     halWaitms(20);
     clearSdiBuffers();
     
      sdiRetries=askSentek(0x31,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x06,SOIL);
      if (sdiRetries!=0x00) 
      {
        ParserSentek(sdiBuffer,resSentekHum,sizeof(sdiBuffer),0x01,SOIL);
        ParserSentek(sdiBuffer2,resSentekHum,sizeof(sdiBuffer2),0x02,SOIL);
      }
      else memset(resSentekHum,0x00,sizeof(resSentekHum));
       
     #ifdef SENTEK_60_SAL
      halWaitms(200);
      sdiRetries=0x03;
      clearSdiBuffers();
      
      sdiRetries=askSentek(0x31,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x06,SAL);
      if (sdiRetries!=0x00) 
      {
        ParserSentek(sdiBuffer,resSentekSal,sizeof(sdiBuffer),0x01,SAL);
        ParserSentek(sdiBuffer2,resSentekSal,sizeof(sdiBuffer2),0x02,SAL);
      }
      else memset(resSentekSal,0x00,sizeof(resSentekSal));
     #endif
     
      halWaitms(200);
      sdiRetries=0x03;
      clearSdiBuffers();
     
      sdiRetries=askSentek(0x31,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x06,TEMP);
     
      if (sdiRetries!=0x00) 
      {
        ParserSentek(sdiBuffer,resSentekTemp,sizeof(sdiBuffer),0x01,TEMP);
        ParserSentek(sdiBuffer2,resSentekTemp,sizeof(sdiBuffer2),0x02,TEMP);
      }
       else memset(resSentekTemp,0x00,sizeof(resSentekTemp));
    #endif 
      
     #if defined SENTEK_60_B  //INDIRIZZO 0x41 'q'
     halWaitms(200);
     IO_DIR_PORT_PIN( 0, 4, IO_OUT );
     P0_4=0;
     P0INP=0x10;   
     halWaitms(10);   
     clearSdiBuffers();
     
      sdiRetries=askSentek(0x41,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x06,SOIL);
      if (sdiRetries!=0x00) 
      {
        // sfrutto il resSentekHumB dedicato alle profondit� 7-12 che nella 60 non ci sono in modo da non creare un altro buffer
        ParserSentek(sdiBuffer,resSentekHumB,sizeof(sdiBuffer),0x01,SOIL);
        ParserSentek(sdiBuffer2,resSentekHumB,sizeof(sdiBuffer2),0x02,SOIL);
      }
      else{
        memset(resSentekHumB,0x00,sizeof(resSentekHumB));
      }
      
    #ifdef SENTEK_60_SAL_B
      halWaitms(200);
      sdiRetries=0x03;
      clearSdiBuffers();
      
      sdiRetries=askSentek(0x41,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x06,SAL);
      if (sdiRetries!=0x00) 
      {
        ParserSentek(sdiBuffer,resSentekSalB,sizeof(sdiBuffer),0x01,SAL);
        ParserSentek(sdiBuffer2,resSentekSalB,sizeof(sdiBuffer2),0x02,SAL);
      }
      else memset(resSentekSal,0x00,sizeof(resSentekSal));
    #endif
      
      halWaitms(200);
      sdiRetries=0x03;
      clearSdiBuffers();
      
      sdiRetries=askSentek(0x41,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x06,TEMP);
      if (sdiRetries!=0x00) 
      {
        ParserSentek(sdiBuffer,resSentekTempB,sizeof(sdiBuffer),0x01,TEMP);
        ParserSentek(sdiBuffer2,resSentekTempB,sizeof(sdiBuffer2),0x02,TEMP);
      }
       else{
        memset(resSentekTempB,0x00,sizeof(resSentekTempB));
      }

    #endif  
     
            
    #if defined SENTEK_30  //INDIRIZZO 0x32 'b'
     IO_DIR_PORT_PIN( 0, 4, IO_OUT );
     P0_4=0;
     P0INP=0x10;   
     halWaitms(20);   
     sdiRetries=0x03;
     clearSdiBuffers();
     
      sdiRetries=askSentek(0x32,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x03,SOIL);
      if (sdiRetries!=0x00) 
      {
        ParserSentek(sdiBuffer,resSentekHum,sizeof(sdiBuffer),0x01,SOIL);
      } 
      else{
        memset(resSentekHum,0x00,sizeof(resSentekHum));
      }
      
      #ifdef SENTEK_30_SAL
      halWaitms(200);
      sdiRetries=0x03;
      clearSdiBuffers();
      
      sdiRetries=askSentek(0x32,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x03,SAL);
      if (sdiRetries!=0x00) 
      {
        ParserSentek(sdiBuffer,resSentekSal,sizeof(sdiBuffer),0x01,SAL);
      }
      else memset(resSentekSal,0x00,sizeof(resSentekSal));
     #endif
     
      halWaitms(200);
      sdiRetries=0x03;
      clearSdiBuffers();
      
      sdiRetries=askSentek(0x32,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x03,TEMP);
      if (sdiRetries!=0x00) 
      {
        ParserSentek(sdiBuffer,resSentekTemp,sizeof(sdiBuffer),0x01,TEMP);
      }
       else{
        memset(resSentekTemp,0x00,sizeof(resSentekTemp));
      }
    #endif
    
    #if defined SENTEK_30_B  //INDIRIZZO 0x12 'B'
     halWaitms(200);
     IO_DIR_PORT_PIN( 0, 4, IO_OUT );
     P0_4=0;
     P0INP=0x10;   
     halWaitms(10);   
     clearSdiBuffers();
     
      sdiRetries=askSentek(0x12,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x03,SOIL);
      
      if (sdiRetries!=0x00) 
      {
        ParserSentek(sdiBuffer,resSentekHumB,sizeof(sdiBuffer),0x01,SOIL);
        asm("NOP");
      }
      else{
        memset(resSentekHumB,0x00,sizeof(resSentekHumB));
      }
      
     #ifdef SENTEK_30_B_SAL
      halWaitms(200);
      sdiRetries=0x03;
      clearSdiBuffers();
      
      sdiRetries=askSentek(0x12,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x03,SAL);
      if (sdiRetries!=0x00) 
      {
        ParserSentek(sdiBuffer,resSentekSalB,sizeof(sdiBuffer),0x01,SAL);
      }
      else memset(resSentekSalB,0x00,sizeof(resSentekSalB));
     #endif
     
      halWaitms(200);
      sdiRetries=0x03;
      clearSdiBuffers();
      
      sdiRetries=askSentek(0x12,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x03,TEMP);
      if (sdiRetries!=0x00) 
      {
        ParserSentek(sdiBuffer,resSentekTempB,sizeof(sdiBuffer),0x01,TEMP);
        asm("NOP");
      }
      else memset(resSentekTempB,0x00,sizeof(resSentekTempB));
    #endif  
      
    #if defined SENTEK_30_C  //INDIRIZZO 0x13 'C'
     halWaitms(200);
     IO_DIR_PORT_PIN( 0, 4, IO_OUT );
     P0_4=0;
     P0INP=0x10;   
     halWaitms(10);   
     clearSdiBuffers();
     
      sdiRetries=askSentek(0x13,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x03,SOIL);
      
      if (sdiRetries!=0x00) 
      {
        ParserSentek(sdiBuffer,resSentekHumC,sizeof(sdiBuffer),0x01,SOIL);
        asm("NOP");
      }
      else{
        memset(resSentekHumC,0x00,sizeof(resSentekHumC));
      }
      
      halWaitms(200);
      sdiRetries=0x03;
      clearSdiBuffers();
      
      sdiRetries=askSentek(0x13,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x03,TEMP);
      if (sdiRetries!=0x00) 
      {
        ParserSentek(sdiBuffer,resSentekTempC,sizeof(sdiBuffer),0x01,TEMP);
        asm("NOP");
      }
      else memset(resSentekTempC,0x00,sizeof(resSentekTempC));
    #endif  
   
      
      
#if defined SENTEK_10A  //INDIRIZZO 0x33 'c'  
    IO_DIR_PORT_PIN( 0, 4, IO_OUT );
    P0_4=0;
    P0INP=0x10;   
    halWaitms(20);    
    clearSdiBuffers();
     
    sdiRetries=askSentek(0x33,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x01,SOIL);
   
    if (sdiRetries!=0x00) 
    {
      SenParser10(sdiBuffer,resSentek10A,0x01);
    }
    else{
      memset(resSentek10A,0x00,sizeof(resSentek10A));
    }
    
    halWaitms(200);
    sdiRetries=0x03;
    clearSdiBuffers();
     
    sdiRetries=askSentek(0x33,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x01,TEMP);
    if (sdiRetries!=0x00) 
    {
      SenParser10(sdiBuffer,resSentek10A,0x02);
    }
    else{
      memset(resSentek10A,0x00,sizeof(resSentek10A));
    }
#endif
    
#if defined SENTEK_10B  //INDIRIZZO 0x34 'd'  
    IO_DIR_PORT_PIN( 0, 4, IO_OUT );
     P0_4=0;
     P0INP=0x10;   
     halWaitms(20);     
     clearSdiBuffers();
     
    sdiRetries=askSentek(0x34,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x01,SOIL);
    asm("NOP");
    if (sdiRetries!=0x00) 
    {
      SenParser10(sdiBuffer,resSentek10B,0x01);
      asm("NOP"); 
    }
    else{
      memset(resSentek10B,0x00,sizeof(resSentek10B));
    }

    
    halWaitms(200);
    sdiRetries=0x03;
    clearSdiBuffers(); 
    
    sdiRetries=askSentek(0x34,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x01,TEMP);
    if (sdiRetries!=0x00) 
    {
      SenParser10(sdiBuffer,resSentek10B,0x03);
      asm("NOP");
    }
    else{
      memset(resSentek10B,0x00,sizeof(resSentek10B));
    }

    asm("NOP");
  #endif
    
    #if defined SENTEK_10C  //INDIRIZZO 0x35 'e'  
     IO_DIR_PORT_PIN( 0, 4, IO_OUT );
     P0_4=0;
     P0INP=0x10;   
     halWaitms(20);      
    clearSdiBuffers();
     
    sdiRetries=askSentek(0x35,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x01,SOIL);
    asm("NOP");
    if (sdiRetries!=0x00) 
    {
      SenParser10(sdiBuffer,resSentek10C,0x01);
      asm("NOP"); 
    }
    else{
      memset(resSentek10C,0x00,sizeof(resSentek10C));
    }

    
    halWaitms(200);
    sdiRetries=0x03;
    clearSdiBuffers();
     
    sdiRetries=askSentek(0x35,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x01,TEMP);
    if (sdiRetries!=0x00) 
    {
      SenParser10(sdiBuffer,resSentek10C,0x03);
      asm("NOP");
    }
    else{
      memset(resSentek10C,0x00,sizeof(resSentek10C));
    }

    asm("NOP");
  #endif
  
    #if defined SENTEK_10D  //INDIRIZZO 0x36 'f'  
    IO_DIR_PORT_PIN( 0, 4, IO_OUT );
     P0_4=0;
     P0INP=0x10;   
     halWaitms(20);      
    clearSdiBuffers();
     
    sdiRetries=askSentek(0x36,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x01,SOIL);
    asm("NOP");
    if (sdiRetries!=0x00) 
    {
      SenParser10(sdiBuffer,resSentek10D,0x01);
      asm("NOP"); 
    }
    else{
      memset(resSentek10D,0x00,sizeof(resSentek10D));
    }

    
    halWaitms(200);
    sdiRetries=0x03;
    clearSdiBuffers();
     
    sdiRetries=askSentek(0x36,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x01,TEMP);
    if (sdiRetries!=0x00) 
    {
      SenParser10(sdiBuffer,resSentek10D,0x03);
      asm("NOP");
    }
    else{
      memset(resSentek10D,0x00,sizeof(resSentek10D));
    }

    asm("NOP");
  #endif

    #if defined SENTEK_10E  //INDIRIZZO 0x37 'g'  
    IO_DIR_PORT_PIN( 0, 4, IO_OUT );
     P0_4=0;
     P0INP=0x10;   
     halWaitms(20);     
    clearSdiBuffers();
     
    sdiRetries=askSentek(0x37,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x01,SOIL);
    asm("NOP");
    if (sdiRetries!=0x00) 
    {
      SenParser10(sdiBuffer,resSentek10E,0x01);
      asm("NOP"); 
    }
    else{
      memset(resSentek10E,0x00,sizeof(resSentek10E));
    }

    halWaitms(200);
    sdiRetries=0x03;
    clearSdiBuffers();
     
    sdiRetries=askSentek(0x37,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x01,TEMP);
    if (sdiRetries!=0x00) 
    {
      SenParser10(sdiBuffer,resSentek10E,0x03);
      asm("NOP");
    }
    else{
      memset(resSentek10E,0x00,sizeof(resSentek10E));
    }

    asm("NOP");
  #endif

    #if defined SENTEK_10F  //INDIRIZZO 0x38 'h'  
   IO_DIR_PORT_PIN( 0, 4, IO_OUT );
     P0_4=0;
     P0INP=0x10;   
     halWaitms(20);    
    clearSdiBuffers();
     
    sdiRetries=askSentek(0x38,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x01,SOIL);
    asm("NOP");
    if (sdiRetries!=0x00) 
    {
      SenParser10(sdiBuffer,resSentek10F,0x01);
      asm("NOP"); 
    }
    else{
      memset(resSentek10F,0x00,sizeof(resSentek10F));
    }
    
    halWaitms(200);
    sdiRetries=0x03;
    clearSdiBuffers();
     
    sdiRetries=askSentek(0x38,sdiBuffer,sdiBuffer2,sdiBuffer3,sdiBuffer4,SDI_DEFAULT_TRIM,0x01,TEMP);
    if (sdiRetries!=0x00) 
    {
      SenParser10(sdiBuffer,resSentek10F,0x03);
      asm("NOP");
    }
    else{
      memset(resSentek10F,0x00,sizeof(resSentek10F));
    }

    asm("NOP");
  #endif

    #if defined MPS6_P04
     IO_DIR_PORT_PIN( 0, 4, IO_OUT );
     P0_4=0;
     P0INP=0x10;   
     halWaitms(20);
      clearSdiBuffers();
      sdiRetries=askSdi(sdiDevice2,sdiBuffer,SDI_DEFAULT_TRIM);
      if (sdiRetries!=0x00)
      {
        Mps6Parser(sdiBuffer,resBuffer2);
      }
      else{
      resBuffer2[0]=0x00;
      resBuffer2[1]=0x00;
      resBuffer2[2]=0x00;
      }
    #endif
      
     #if defined DECGS3_P04
    IO_DIR_PORT_PIN( 0, 4, IO_OUT );
     P0_4=0;
     P0INP=0x10;   
     halWaitms(20);   
     clearSdiBuffers();
      sdiRetries=askSdi(sdiDevice3,sdiBuffer,SDI_DEFAULT_TRIM);
      if (sdiRetries!=0x00)
      {
        HalLedSet (HAL_LED_2, HAL_LED_MODE_OFF);  
        Gs3Parser(sdiBuffer,resBuffer3);
      }
      else{
      HalLedSet (HAL_LED_2, HAL_LED_MODE_ON);
      resBuffer3[0]=0x00;
      resBuffer3[1]=0x00;
      resBuffer3[2]=0x00;
      }
    #endif
      
    #if defined FLORA_1
     IO_DIR_PORT_PIN( 0, 4, IO_OUT );
     P0_4=0;
     P0INP=0x10;   
     halWaitms(20);   
     clearSdiBuffers();
     sdiRetries=askSdi(0x00,sdiBuffer,SDI_DEFAULT_TRIM);
     if (sdiRetries!=0x00)
     {
       FloraParser(sdiBuffer,floraResBuffer);    
       asm("NOP");
     }
     else memset(floraResBuffer,0x00,sizeof(floraResBuffer));
    #endif    
   
    #if defined ACC_TDR
     IO_DIR_PORT_PIN( 0, 4, IO_OUT );
     P0_4=0;
     P0INP=0x10;   
     halWaitms(20);   
     clearSdiBuffers();
     sdiRetries=askSdi(0x07,sdiBuffer,SDI_DEFAULT_TRIM);
     if (sdiRetries!=0x00)
     {
       AcctParser(sdiBuffer,accResBuffer); 
      
     }
     else memset(accResBuffer,0x00,sizeof(accResBuffer));
    #endif  
     
     #if defined ACC_TDR_2
     IO_DIR_PORT_PIN( 0, 4, IO_OUT );
     P0_4=0;
     P0INP=0x10;   
     halWaitms(20);   
     clearSdiBuffers();
     sdiRetries=askSdi(0x17,sdiBuffer,SDI_DEFAULT_TRIM);
     if (sdiRetries!=0x00)
     {
       AcctParser(sdiBuffer,accResBuffer2);         
     }
     else memset(accResBuffer2,0x00,sizeof(accResBuffer2));
    #endif  
     
    #if defined ACC_TDR_3
     IO_DIR_PORT_PIN( 0, 4, IO_OUT );
     P0_4=0;
     P0INP=0x10;   
     halWaitms(20);   
     clearSdiBuffers();
     sdiRetries=askSdi(0x27,sdiBuffer,SDI_DEFAULT_TRIM);
     if (sdiRetries!=0x00)
     {
       AcctParser(sdiBuffer,accResBuffer3);        
     }
     else memset(accResBuffer3,0x00,sizeof(accResBuffer3));
    #endif  
     
    #if defined ACC_TDR_4
     IO_DIR_PORT_PIN( 0, 4, IO_OUT );
     P0_4=0;
     P0INP=0x10;   
     halWaitms(20);   
     clearSdiBuffers();
     sdiRetries=askSdi(0x39,sdiBuffer,SDI_DEFAULT_TRIM);
     if (sdiRetries!=0x00)
     {
       AcctParser(sdiBuffer,accResBuffer4);        
     }
     else memset(accResBuffer4,0x00,sizeof(accResBuffer4));
    #endif  
     
      #if defined ACC_TDR_5
     IO_DIR_PORT_PIN( 0, 4, IO_OUT );
     P0_4=0;
     P0INP=0x10;   
     halWaitms(20);   
     clearSdiBuffers();
     sdiRetries=askSdi(0x3A,sdiBuffer,SDI_DEFAULT_TRIM);
     asm("NOP");
     if (sdiRetries!=0x00)
     {
       AcctParser(sdiBuffer,accResBuffer5);        
     }
     else memset(accResBuffer5,0x00,sizeof(accResBuffer5));
    #endif  
     
    #if defined ACC_TDR_6
     IO_DIR_PORT_PIN( 0, 4, IO_OUT );
     P0_4=0;
     P0INP=0x10;   
     halWaitms(20);   
     clearSdiBuffers();
     sdiRetries=askSdi(0x3B,sdiBuffer,SDI_DEFAULT_TRIM);
     if (sdiRetries!=0x00)
     {
       AcctParser(sdiBuffer,accResBuffer6);        
     }
     else memset(accResBuffer6,0x00,sizeof(accResBuffer6));
    #endif  
      
    #if defined ACC_TDTP
     IO_DIR_PORT_PIN( 0, 4, IO_OUT );
     P0_4=0;
     P0INP=0x10;   
     halWaitms(20);     
      clearSdiBuffers();
      sdiRetries=askSdi(sdiDevice5,sdiBuffer,SDI_DEFAULT_TRIM);
      if (sdiRetries!=0x00)
      {
        AcctParser(sdiBuffer,accResBuffer2);        
      }
    #endif  
      
    #if defined DECCDT
     IO_DIR_PORT_PIN( 0, 4, IO_OUT );
     P0_4=0x00;
     P0INP=0x10;   
     halWaitms(20);          
     clearSdiBuffers();
      sdiRetries=askSdi(0x09,sdiBuffer,SDI_DEFAULT_TRIM);
      if (sdiRetries!=0x00)
      {
        HalLedSet (HAL_LED_1, HAL_LED_MODE_OFF);
        CDTParser(sdiBuffer,resBuffer4);    
        asm("NOP");
      }
      else{
      HalLedSet (HAL_LED_1, HAL_LED_MODE_ON);
      resBuffer4[0]=0;
      resBuffer4[1]=0;
      resBuffer4[2]=0;
      }
    #endif 
        
    #if defined FLORA_1 || DEC5TE_P04 || defined SENTEK_10A || defined SENTEK_10B || defined SENTEK_10C || defined SENTEK_10D || defined SENTEK_10E || defined SENTEK_10F || defined SENTEK_60 || defined SENTEK_120 || defined SENTEK_90 || defined SENTEK_30 || defined SENTEK_30_B || defined MPS6_P04 || defined DECGS3_P04 || defined ACC_TDTP || defined ACC_TDR || defined DECCDT
     P0INP=saveP0Inp;
     IO_DIR_PORT_PIN( 0, 4, IO_IN);
     halWaitms(20);
    #endif
           
     maskVar=0x00000001;

     for(uint8 j=0; j<32; j++)
     {
      if ((IDSensor & maskVar) == maskVar)
      {
       
        lenMeas=readSensor(maskVar);
        
        for (uint8 ind=0;ind<lenMeas;ind++)
        {
         msa_Data1[a]=HI_UINT16(readSensorM[ind]);
         msa_Data1[a+1]=LO_UINT16(readSensorM[ind]);
        
         a=a+2;
        }
        
       }

       maskVar=maskVar<<1;
       if (maskVar>0x00010000) // se ho superato la P0_5 attivo il WD 
        if (P1_0!=0x01) // verifico di non averlo gia attivato
        {
           ENABLE_PERIPH();
           halWait(20);
           // Set Watch-dog procedure
           readAstronomic(clearBuf,0x0F,1); // pulisco eventuali flag pendenti
           writeAstronomic(watchDogAbeAfe,0x09,1);
        }
     } 
     
      msa_Data1[14]=numSensTot; //salvo il numero dei sensori (letto solo in firstStart)
      firstStart=FALSE;
      
       // normale ciclo spengo 
      #if defined WEBCAM
      if (!doPhotoNow ) 
        DISABLE_SENSTH();
      #else
        DISABLE_SENSTH(); // ora posso disattivare i sensori ( nella fase modem non mi servono on )
        halWait(10);
      #endif    

      msa_Data1[a]=HI_UINT16(readBattery);
      msa_Data1[a+1]=LO_UINT16(readBattery);
      msa_Data1[a+2]=0x00; //rssi field
      a=a+3;
    
      saveData(a); // salvo i dati   
      
     #if defined SPI
      SpiInit();
      memset(tempBuffer,'\0',sizeof(tempBuffer)); 
      BytesToChar(&readBuffer[0x05],0x03); // salva in tempBuffer
      sprintf((char*)&fileName,(char const*)"w%s.txt",tempBuffer);
      sdOk=funcToSDCARD(1,msa_Data1,a,fileName); // li scrivo sulla SD card
     #endif
   
}

#if defined SPI
uint8 errSD=0;
uint8 lenSD=0;
bool funcToSDCARD(uint8 operation,uint8* buffer,uint8 len,char* fileName){
  
    lenSD=(len*2);
    saveP2DIR=P2DIR;
    P2DIR = 0x07; // imposto la por
    errSD=0;
    
    //Faccio Mount della SDQui hocard
    rc = f_mount(&Fatfs,"",0);
    
    // Se ok apro il file
    if(rc==FR_OK)
    switch (operation)
    {
     case 0x00: // write
       rc = f_open(&Fil,fileName, FA_CREATE_ALWAYS | FA_WRITE);
     break;
    
     case 0x01: // append
      rc = f_open(&Fil,fileName, FA_OPEN_ALWAYS | FA_WRITE );
      if (rc == FR_OK) {
        /* Seek to end of the file to append data */
        f_lseek(&Fil, f_size(&Fil));
      }
     break;
      
     case 0x02: // read
      rc = f_open(&Fil,fileName, FA_READ);
     break;
    }
    
    // Se ok scrivo nel file 
    if(rc==FR_OK)
     switch (operation)
    {
     case 0x00: // write
     case 0x01: // append
       if (fileName!="dataora.txt")
       {
        BytesToChar(buffer,len);
        rc = f_write(&Fil,tempBuffer,lenSD,&bytes_count);
       }
       else {
         rc = f_write(&Fil,buffer,len,&bytes_count);
       }
     break;
    
    case 0x02: // read
     memset(charBuffer,0x00,sizeof(charBuffer)); 
     f_gets(charBuffer, 0x03,&Fil);
     sscanf(charBuffer,"%d",&readValueSD);
     
     // check value - max time da datalogger 60 minuti
     if (fileName=="time.txt")
      if (0<readValueSD<=60)  {
        nextMinToSleep=readValueSD; // memorizzo il tempo di campionamento ( risveglio )
        time=(readValueSD+ST_TIME_RESET);// 4 minuti oltre il risveglio 
        sendModem=1; // calcolo quanti cicli devo fare prima di spedire
        coordinatorMode=1;
      }
     
      if (fileName=="mode.txt") {  // se mode = 1 manuale ovvero setto dataora da file
        if (readValueSD==1) {  // devo settare la data e ora astronomico
          rc = f_open(&Fil,"dataora.txt", FA_READ);
          memset(charBuffer,0x00,sizeof(charBuffer)); 
          f_gets(charBuffer,18,&Fil);
          uint8 k=0;
          for (uint8 i=0;i<18;i++) {
           dataOraBuff[k]=(int)((charBuffer[i])-48);
           k++;
           i++;
           dataOraBuff[k]=(int)((charBuffer[i])-48); // converto da char a hex la parte che mi interessa
           dataOraBuff[k-1]=BUILD_UINT8(dataOraBuff[k-1],dataOraBuff[k]);  // salvo come un bytes hex
           i++;
          }
          
          setAstronomic(START,dataOraBuff[5],(uint8)dataOraBuff[4],(uint8)dataOraBuff[3],
                      GIORNO,(uint8)dataOraBuff[0],(uint8)dataOraBuff[1],(uint8)dataOraBuff[2]);
        
        }
     }
    break;
    } 

    // Se ok chiudo il file
    if(rc==FR_OK){
    rc = f_close(&Fil);
    errSD=4;
    }
    
    // Se ok smonto la sd
    if(rc==FR_OK){
    rc = f_mount(0, "", 0);  
    errSD=5;
    }
    
    //Se ok tutto � andato a buon fine
    if(rc==FR_OK){
    errSD=6;
    }
    
    //Riconfiguro le porte come erano in orgine 
    P2DIR=saveP2DIR;
    // Riconfiguro anche la P0 per letture da sensore 
    P0SEL = 0x00;
    U0CSR = 0x40;
    U0GCR = 0x2D;
     
    //reset del buffer di supporto globale
    memset(tempBuffer,0x00,sizeof(tempBuffer)); 
    
    if(rc!=FR_OK){
      asm("NOP");
        HalLedSet (HAL_LED_2, HAL_LED_MODE_ON);
        sdOk=false;
    }
    else{
       HalLedSet (HAL_LED_2, HAL_LED_MODE_OFF);
       sdOk=true;
    }
  return sdOk;
}
 
#endif

bool timePermitAssociation(){

    uint16 diffAssoc;
    uint8 tempSec, tempDecCent;
    bool r;
    
    setMessageTipology(0);
   
    // check oscillatore 
    checkOsc();
    readAstronomic(readBuffer,0x0000,8);
    astroConv(readBuffer);

    tempSec=astroBuffer[1];
    tempDecCent=astroBuffer[0];

    astroConv(matchBuffer);

    if(tempSec<astroBuffer[1]) {tempSec=tempSec+60;}
    if(tempDecCent<astroBuffer[0]) {tempDecCent=tempDecCent+100; tempSec--;}

    tempSec=tempSec-astroBuffer[1];
    tempDecCent=tempDecCent-astroBuffer[0];

    diffAssoc=(MSA_ASSOCIATION_PERIOD/10)-((tempSec*100)+tempDecCent);
              if (diffAssoc<50) r=FALSE;
              else r=TRUE;
    
    return r;

  }


uint8 diffTimestamp(){

uint8 minOld, minNew, hourOld, hourNew, diff;

  astroConv(matchBuffer);

  // Salvo il tempo di risveglio in secondi
  timeRead.seconds=astroBuffer[1];
  timeRead.minutes=astroBuffer[2];
  timeRead.hour=astroBuffer[3];
  timeRead.day=astroBuffer[5]-1;
  timeRead.month=astroBuffer[6]-1;
  timeRead.year=2000+astroBuffer[7];

  secOld=osal_ConvertUTCSecs(&timeRead); // memorizzo i secondi di partenza   

  minOld=astroBuffer[2];
  hourOld=astroBuffer[3];
    
  // check oscillatore 
  checkOsc();
  readAstronomic(timeBuffer,0x0000,0x08); // leggo tutti i dati
  astroConv(timeBuffer); // converto in BCD risultati in astroBuffer ( in 0 ci sono i decimi )
  
  // Salvo il tempo letto in secondi
  timeRead.seconds=astroBuffer[1];
  timeRead.minutes=astroBuffer[2];
  timeRead.hour=astroBuffer[3];
  timeRead.day=astroBuffer[5]-1;
  timeRead.month=astroBuffer[6]-1;
  timeRead.year=2000+astroBuffer[7];

  secStart=osal_ConvertUTCSecs(&timeRead); // memorizzo i secondi letti
  
  if (secStart<=secOld) // errore grave di lettura
  {
    err=11;
    resetBoard();
  }
    
  minNew=astroBuffer[2];
  hourNew=astroBuffer[3];

  if(hourNew<hourOld) hourNew=hourNew+24;
  if(minNew<minOld) { minNew=minNew+60; hourNew--;}

   if (msa_State== MSA_START_STATE) diff=0;
   else diff=((hourNew-hourOld)*60)+(minNew-minOld);

  return diff;
}

void InputOutputSleep()
{
  
   #if defined HAL_PA_LNA
    P2_0=0x00;
   #endif
  
     /* I2C */
    IO_DIR_PORT_PIN( CLK_PORT, CLK_PIN, IO_OUT );
    IO_DIR_PORT_PIN( DATA_PORT, DATA_PIN, IO_OUT );
    SDA = 0x01;
    SCL = 0x01;

    /* Porta 1 seriale->basso consumo */
    // pull-up su tutta la uart
    P1INP &= 0x0F;
    // Configure Port P1 pins for I/O function:
    P1SEL &= ~0xD0;
    
    // imposto output a 0x00 in modo da abbattere i consumi
    IO_DIR_PORT_PIN( 1, 7, IO_OUT );
    IO_DIR_PORT_PIN( 1, 6, IO_OUT );
    IO_DIR_PORT_PIN( 1, 5, IO_OUT ); 
    IO_DIR_PORT_PIN( 1, 4, IO_OUT );
    P1_7=0x00;
    P1_6=0x00;
    P1_5=0x00;
    P1_4=0x00;
      
    P0SEL=0x00;
    IO_DIR_PORT_PIN( 0, 7, IO_OUT );
    IO_DIR_PORT_PIN( 0, 5, IO_OUT );
    IO_DIR_PORT_PIN( 0, 4, IO_OUT );
    IO_DIR_PORT_PIN( 0, 3, IO_OUT );
    IO_DIR_PORT_PIN( 0, 2, IO_OUT );
    // P0.1 � interrupt astronomico
    IO_DIR_PORT_PIN( 0, 0, IO_OUT );

    P0_7=0x00;
    P0_5=0x00;
    P0_4=0x00;
    P0_3=0x00;
    P0_2=0x00;
    P0_0=0x00;

}


void InputOutputWakeUp()
{
     /* Init I2C Peripherals */
     initI2C();
   
    /* ADC */
    IO_DIR_PORT_PIN( 0, 7, IO_IN );
    IO_DIR_PORT_PIN( 0, 6, IO_IN );
    IO_DIR_PORT_PIN( 0, 5, IO_IN );
    IO_DIR_PORT_PIN( 0, 4, IO_IN );
    IO_DIR_PORT_PIN( 0, 3, IO_IN );
    IO_DIR_PORT_PIN( 0, 2, IO_IN );
    IO_DIR_PORT_PIN( 0, 1, IO_IN );
    IO_DIR_PORT_PIN( 0, 0, IO_IN );

    #if defined HAL_PA_LNA
     P2_0=0x01;
    #endif
     
}


/*********************************************************************
 * @fn      setMessageTipology(uint8 val)
 *
 * @brief   Funzione fondamentale per lo sleep ed il corretto wake up
 *
 *
 * @param   val
 *          FALSE viene impostato l'indirect message
 *          permettendo al nodo di entrare il PM2 correttamente.
 *          Non riceve per� i dati perch� RX � OFF.
 *          TRUE viene impostato il direct message
 *          permettendo al nodo di ricevere i dati.
 *
 * @return  none
 */

void setMessageTipology(uint8 val)
{
  /* This device is setup for Direct Message */
  if (val)
  { 
   #if defined HAL_PA_LNA
    P1_2=0x00;
    P1_3=0x00;
    macRadioTurnOnPower();
    P2DIR|=0x01; 
    P2_0 = 0x01;
    halWait(3);
   #endif  
   MAC_MlmeSetReq(MAC_RX_ON_WHEN_IDLE, &msa_MACTrue);
   halWait(3); 
  }
  else
  {
   #if defined HAL_PA_LNA
    P1_2=0x00;
    P1_3=0x00;
    OBSSEL2 &=~0xFF;   
    OBSSEL3 &=~0xFF;
    P2DIR|=0x01; 
    P2_0 = 0x00;  
    halWait(3);   
   #endif
   MAC_MlmeSetReq(MAC_RX_ON_WHEN_IDLE, &msa_MACFalse);
   halWait(3); 
  }   
}


/*********************************************************************
 * @fn      getBattery
 *
 * @brief   get the value of the battery in milliVolt
 *          and write it into the output buffer
 *
 * @param   none
 *
 * @return  none
 */

uint16 getBattery(void)
{

  int16 tempRead=0;
  uint32 valueRead=0;

  // Enable channel 0x00
  APCFG |= 0x01;

  for(uint8 m=0;m<CAMPIONI;m++)
  {
     /* writing to this register starts the conversion - riferimento P0.7 e risoluzione massima */
    //ADCCON3 = 0x00 | 0x30 | 0x80;
    /* writing to this register starts the conversion - riferimento Vdd e risoluzione massima */
    ADCCON3 = 0x00 | 0x30 | 0x40;

    /* Wait for the conversion to be done */
    while (!(ADCCON1 & 0x80));

     /* Read the result */
     tempRead = (int16) (ADCL);
     tempRead |= (int16) (ADCH << 8);

     tempRead >>= 4; // Shift 4 due to 12 bits resolution true

     valueRead+=tempRead; // salvo e sommo

  }

  APCFG &=~ 0x01;  // ripristino P0.0 come input/output non ADC

 //valueRead = (uint32)(valueRead * 3000/LIVELLI);  // 3000 � la Vstabilizzata

 //valueRead = (uint32)(valueRead * 2500/LIVELLI);   // se riferimento � P0.7

  return (valueRead/CAMPIONI);

}


bool checkBattery()
{
  bool retBattValue=0;
  
    /*Lettura batteria da fare prima di disabilitare le periferiche*/
     readBattery=getBattery();
   
     if (readBattery<=LOW_LEVEL_BATTERY) // segnalo che la batteria � scarica
     {
        for(uint8 rs=0;rs<5;rs++)
        { 
          HalLedSet (HAL_LED_2, HAL_LED_MODE_ON);  // led rosso ON 
          halWaitSec(1);
          HalLedSet (HAL_LED_2, HAL_LED_MODE_OFF);  // led rosso OFF  
          halWaitSec(1);
        }
         
        lowBattRead++;
        
        if (lowBattRead >= LOW_BATT_TIMES ) 
        {
          lowBattRead=0x00;
          retBattValue=1; // batteria scarica
        }
     }
     else lowBattRead=0x00;
     
     return retBattValue;
}


#if defined FLORA_1 || DEC5TE_P04 || defined SENTEK_10A || defined SENTEK_10B || defined SENTEK_10C || defined SENTEK_10D || defined SENTEK_10E || defined SENTEK_10F || defined SENTEK_60 || defined SENTEK_120 || defined SENTEK_90 || defined SENTEK_30 || defined SENTEK_30_B || defined MPS6_P04 || defined DECGS3_P04 || defined DECCDT || defined ACC_TDR 

void clearSdiBuffers()
{
     memset(sdiBuffer,0x00,sizeof(sdiBuffer));
     memset(sdiBuffer2,0x00,sizeof(sdiBuffer2));
     memset(sdiBuffer3,0x00,sizeof(sdiBuffer3));
     memset(sdiBuffer4,0x00,sizeof(sdiBuffer4));
}
#endif


#if defined DEBUG
void testBoard()
{
 uint8 writeBuffer[5];
 
 setMessageTipology(0);
 
 // test della board
 ENABLE_PERIPH(); 
 restoreTime();
 //setAstronomic(START,SECONDI,MINUTI,ORE,GIORNO,DATA,MESE,ANNO);

 readAstronomic(readBuffer,0x0000,8);
 
 HalLedSet (HAL_LED_1, HAL_LED_MODE_ON); 

  halWait(250); halWait(250); halWait(250);
  halWait(250); halWait(250); halWait(250);
  halWait(250); halWait(250); halWait(250);
 
  HalLedSet (HAL_LED_1, HAL_LED_MODE_OFF); 
 
 incrementAstro(1);
    
  SleepAstro();
        
 // test Astronomico
 //retVal=writeAstronomic(0x00,0x0F,1); // setto a zero OF che di default al primo avvio � 1 e non mi abilita INT
 //if (retVal>0) readAstronomic(test,0x0F,1); // pulisco eventuali flag pendenti
 
 //if (retVal>0) 
   // HalLedSet (HAL_LED_1, HAL_LED_MODE_ON); 

 /* halWait(250); halWait(250); halWait(250);
  halWait(250); halWait(250); halWait(250);
  halWait(250); halWait(250); halWait(250);

 
 // test EEprom
  retVal=writeEEProm(test,0x0000,100);
  asm("NOP");
  if (retVal>0) 
    HalLedSet (HAL_LED_2, HAL_LED_MODE_ON); 
 
  halWait(250); halWait(250); halWait(250);
  halWait(250); halWait(250); halWait(250);
  halWait(250); halWait(250); halWait(250);

ENABLE_PERIPH(); 
ENABLE_SENSTH(); // attivo i sensori -> attivo anche il riferimeto a 3V per input sensori
halWait(250); halWait(250); halWait(250);

// leggo batteria ( devo avere le periferiche abilitate per leggere la batteria )
readBattery=getBattery();
asm("NOP");


DISABLE_PERIPH(); // disattivo le periferiche -> selezione input sensori al posto di SD CARD

do{
// leggo sensore su P0.2
read=readSensor(0x00000004);
// leggo sensore su P0.3
read2=readSensor(0x00000008);
  
//read16bit=readSensorsAdc1115(0x01,0xE4E0);
      
//read16bit2=readSensorsAdc1115(0x01,0xF4E0);
asm("NOP");
} while(1);
DISABLE_SENSTH(); // disattivo le periferiche -> selezione input sensori al posto di SD CARD

   // configure UART pin
   P1DIR=0x28;
   // Configure relevant Port P0 pins for peripheral function:
   // P0SEL.SELP0_2/3/4/5 = 1 => RX = P0_2, TX = P0_3, CT = P0_4, RT = P0_5
   P1SEL |= 0xF0;

ENABLE_PERIPH();       
incrementAstro(1);

SleepAstro(); */


}
#endif