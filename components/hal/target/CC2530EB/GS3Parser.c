/******************************************************************************
*                                                                             *
*        **********                                                           *
*       ************                                                          *
*      ***        ***                                                         *
*     ***    ++    ***                                                        *
*     ***   +  +   ***                      CHIPCON                           *
*     ***   +                                                                 *
*     ***   +  +   ***                                                        *
*     ***    ++    ***                                                        *
*      ***        ***                                                         *
*       ************                                                          *
*        **********                                                           *
*                                                                             *
*******************************************************************************

Filename:     GS3Parser.c
Target:       cc2430
Author:       EFU
Revised:      1/3-2007
Revision:     1.1
******************************************************************************/

#if defined (DECGS3_P04)

#include "SdiCore.h"
#include "GS3Parser.h"
#include "ioCC2530.h"
#include "hal_types.h"
#include "hal_sleep.h"
#include "OSAL_Timers.h"
#include "mac_api.h"
#include "astronomic.h"

void Gs3Parser(byte *buffer2,int16 *resbuf);

void hexGS3Multiplier (byte *databuf,uint16 dotpos,uint8 TYPE,int16 *resbuf);

uint8 ipb_GS3;    //indice di buffGS3Pars
//uint8 aft,bef;  //contatori caratteri prima e dopo il punto
uint8 split_GS3[2]; //vettore contenten prima e dopo da passare a hexmultiplier
uint16 somma_GS3;
bool negtemp_GS3=false;   //tiene traccia del segno "-" nella temperatura


uint16 d1_GS3;    //risultato posizione del punto nel primo dato (VWC per GS3), unisce "bef" e "aft"
uint16 d2_GS3;    //risultato posizione del punto nel secondo dato (TEMP per GS3)  unisce "bef" e "aft"
uint16 d3_GS3;    //risultato posizione del punto nel terzo dato (EC per GS3) unisce "bef" e "aft"


//uint8 pi;

uint8 buffGS3Pars[4]={0xFF,0xFF,0xFF,0xFF}; // vettore degli indici posizioni "+" e "-"
uint8 dotn_GS3=0;
uint8 idn_GS3;    // indice per copiare valori nei vari data1_GS3,data2_GS3,data3_GS3

uint8 data1_GS3[5]={0xFF,0xFF,0xFF,0xFF,0xFF};       // VWC  (1-80) � [0,01]
uint8 data2_GS3[4]={0xFF,0xFF,0xFF,0xFF};            // TEMP (-40 , +50) �C [0,1]
uint8 data3_GS3[5]={0xFF,0xFF,0xFF,0xFF,0xFF };      // EC  (0 - 25000) ms/cm

uint16 test1_GS3;   //Variabili di appoggio per BUILD risultato finale misura
uint16 test2_GS3;
uint16 test3_GS3;


void Gs3Parser(byte *buffer2,int16 *resbuf)
{
  //pi=0; // indice del for e if
  uint8 u=0;
  ipb_GS3=0;
 
  
  //Inserire reset delle misure
  
  negtemp_GS3=false;
  //Ricavo i pivot dei dati ( le posizioni dei segni "+" e "-" della stringa
  for(u=0;u<SDI_BUFFER_LENGTH;u++)
  {
    
    //Se uguale a "+", "-" , o "CR" quando la posizione � diversa da 0 ( comincia sempre per CR)
    if(buffer2[u]==0x2B || buffer2[u]==0x2D || (buffer2[u]==0x0D && u!=0))
    {
      buffGS3Pars[ipb_GS3]=u;     //Salvo le posizioni in un buffer per il parsing  
      ipb_GS3++;
      if(buffer2[u]==0x2D)negtemp_GS3=true;
    }
  }
  
  for(u=0;u<sizeof(data1_GS3);u++)
  {
    data1_GS3[u]=0xFF;
  }
  
  for(u=0;u<sizeof(data2_GS3);u++)
  {
    data2_GS3[u]=0xFF;
  }
  
  for(u=0;u<sizeof(data3_GS3);u++)
  {
    data3_GS3[u]=0xFF;
  }
  
  //Separo i 3 diversi valori in 2 buffer separati
  //data1_GS3= VWC
  idn_GS3=0;
  for(u=buffGS3Pars[0]+1;u<buffGS3Pars[1];u++){
    data1_GS3[idn_GS3]=buffer2[u];
    idn_GS3++;
  }
  
  idn_GS3=0;
  //data2_GS3=TEMPERATURA
  for(u=buffGS3Pars[1]+1;u<buffGS3Pars[2];u++){
    data2_GS3[idn_GS3]=buffer2[u];
    idn_GS3++;
  }
  
  
  idn_GS3=0;
  //data3_GS3= EC
  for(u=buffGS3Pars[2]+1;u<buffGS3Pars[3];u++){
    data3_GS3[idn_GS3]=buffer2[u];
    idn_GS3++;
  }
  
  
  //Falsifico i risultati per test
  asm("NOP");
  
  // Trovo il punto  per i 3 buffer dati
  d1_GS3=finddot(data1_GS3,0x05);
  // Lo converto in un valore esadecimale buono per essere spedito via radio
  hexGS3Multiplier(data1_GS3,d1_GS3,VWC,resbuf);
  
  d2_GS3=finddot(data2_GS3,0x04);
  hexGS3Multiplier(data2_GS3,d2_GS3,TEMP,resbuf);
  asm("NOP");
  
  d3_GS3=finddot(data3_GS3,0x05);
  hexGS3Multiplier(data3_GS3,d3_GS3,EC,resbuf);
  
}





  

void hexGS3Multiplier (byte *databuf,uint16 dotpos, uint8 TYPE,int16 *resbuf)
{
  
  //Spezzo il valore uint16 del contatore caratteri in prima e dopo.
  split_GS3[0] = (dotpos >> 8) & 0xff;  //Parte alta
  split_GS3[1] = dotpos & 0xff;         //Parte Bassa
  
  
  // 2:2  
  if(split_GS3[0]==0x02 && split_GS3 [1]==0x02){
    test1_GS3=BUILD_UINT8(databuf[0],databuf[1]);
    test1_GS3=HextoBCD(test1_GS3);
    test2_GS3=BUILD_UINT8(databuf[3],databuf[4]);
    test2_GS3=HextoBCD(test2_GS3);
    test1_GS3=test1_GS3*0x64;
    somma_GS3=test1_GS3 + test2_GS3;
    asm("NOP");
  }
  
  //2:1
  else if (split_GS3[0]==0x02 && split_GS3 [1]==0x01)
  {
    test1_GS3=BUILD_UINT8(databuf[0],databuf[1]);
    test1_GS3=HextoBCD(test1_GS3);
    test2_GS3=BUILD_UINT8(databuf[3],0x00);
    test2_GS3=HextoBCD(test2_GS3);
    test1_GS3=test1_GS3*0x64;
    somma_GS3=test1_GS3 + test2_GS3;
    asm("NOP");
  }
  
  //2:0
  else if (split_GS3[0]==0x02 && split_GS3 [1]==0x00)
  {
    if(TYPE==VWC || TYPE==TEMP)
    {
    test1_GS3=BUILD_UINT8(databuf[0],databuf[1]);
    test1_GS3=HextoBCD(test1_GS3);
    test1_GS3=test1_GS3*0x64;
    test2_GS3=BUILD_UINT16(0x00,0x00);
    somma_GS3=test1_GS3+test2_GS3;
   
    //asm("NOP");
    }
    else
    {
    test1_GS3=BUILD_UINT8(databuf[0],databuf[1]);
    test1_GS3=HextoBCD(test1_GS3);
    somma_GS3=test1_GS3;
    //asm("NOP");
    }
  
  }
  
  //1:2
  else if(split_GS3[0]==0x01 && split_GS3 [1]==0x02)
  {
    test1_GS3=BUILD_UINT8(0x00,databuf[0]);
    test1_GS3=HextoBCD(test1_GS3);
    test2_GS3=BUILD_UINT8(databuf[2],databuf[3]);
    test2_GS3=HextoBCD(test2_GS3);
    test1_GS3=test1_GS3*0x64;
    somma_GS3=test1_GS3 + test2_GS3;
  }
  
  
  //1:1
  else if(split_GS3[0]==0x01 && split_GS3 [1]==0x01)
  {
    /*
      test1_GS3=BUILD_UINT8( 0x00,databuf[0]);
      test1_GS3=HextoBCD(test1_GS3);
      test2_GS3=BUILD_UINT8(0x00,databuf[2]);
      test2_GS3=HextoBCD(test2_GS3);
      test1_GS3=test1_GS3*0x64;
      somma_GS3=test1_GS3 + test2_GS3;
    */
      test1_GS3=BUILD_UINT8( 0x00,databuf[0]);
      test1_GS3=HextoBCD(test1_GS3);
      test2_GS3=BUILD_UINT8(databuf[2],0x00);
      test2_GS3=HextoBCD(test2_GS3);
      test1_GS3=test1_GS3*0x64;
      somma_GS3=test1_GS3+test2_GS3;
  }
  
  //1:0
  else if(split_GS3[0]==0x01 && split_GS3 [1]==0x00)
  {
    
    if(TYPE==VWC || TYPE==TEMP)
    {
      test1_GS3=BUILD_UINT8(0x00,databuf[0]);
      test1_GS3=HextoBCD(test1_GS3);
      test1_GS3=test1_GS3*0x64;
      somma_GS3=test1_GS3;
    }
    else
    {
      test1_GS3=BUILD_UINT8(0x00,databuf[0]);
      test1_GS3=HextoBCD(test1_GS3);
      somma_GS3=test1_GS3;
      //asm("NOP");
    }
  }
  
  //FUNZIONANO SOLO PER EC
  //3:0
  else if (split_GS3[0]==0x03 && split_GS3[1]==0x00)
  {
    test1_GS3=BUILD_UINT8(0x00,databuf[0]);
    test1_GS3=HextoBCD(test1_GS3);
    test2_GS3=BUILD_UINT8(databuf[1],databuf[2]);
    test2_GS3=HextoBCD(test2_GS3);
    test1_GS3=test1_GS3*0x64;
    somma_GS3=test1_GS3 + test2_GS3 + test3_GS3;
  }
  
  //4:0
  else if (split_GS3[0]==0x04 && split_GS3[1]==0x00)
  {
    test1_GS3=BUILD_UINT8(databuf[0],databuf[1]);
    test1_GS3=HextoBCD(test1_GS3);
    test2_GS3=BUILD_UINT8(databuf[2],databuf[3]);
    test2_GS3=HextoBCD(test2_GS3);
    test1_GS3=test1_GS3*0x64;
    somma_GS3=test1_GS3 + test2_GS3;
  }
  //5:0
  else if(split_GS3[0]==0x05)
  {
    test1_GS3=BUILD_UINT8(databuf[1],databuf[2]);
    test1_GS3=HextoBCD(test1_GS3);
    test2_GS3=BUILD_UINT8(databuf[3],databuf[4]);
    test2_GS3=HextoBCD(test2_GS3);
    test1_GS3=test1_GS3*0x64;
    test3_GS3=BUILD_UINT8(0x00,databuf[0]);
    test3_GS3=HextoBCD(test3_GS3);
    test3_GS3=test3_GS3*0x2710;
    
    somma_GS3=test1_GS3 + test2_GS3 + test3_GS3;
  }
  
  
 if(TYPE==VWC) resbuf[0]=somma_GS3;
 else if (TYPE==TEMP)
 { 
   if(negtemp_GS3==false)
   resbuf[1]=somma_GS3;
   else
   {
    somma_GS3=-somma_GS3;
    resbuf[1]=somma_GS3;
   }
 }
 else if (TYPE==EC) resbuf[2]=somma_GS3;
 
}
#endif