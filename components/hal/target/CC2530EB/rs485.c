/******************************************************************************
*                                                                             *
*        **********                                                           *
*       ************                                                          *
*      ***        ***                                                         *
*     ***    ++    ***                                                        *
*     ***   +  +   ***                      CHIPCON                           *
*     ***   +                                                                 *
*     ***   +  +   ***                                                        *
*     ***    ++    ***                                                        *
*      ***        ***                                                         *
*       ************                                                          *
*        **********                                                           *
*                                                                             *
*******************************************************************************

Filename:     MODEM G30
Target:       cc2430
Author:       EFU
Revised:      1/3-2007
Revision:     1.1


******************************************************************************/
#if defined RS485



#include "rs485.h"
#include "ioCC2530.h"
#include "hal_i2c.h"
#include "hal_timer.h"
#include "ZComDef.h"
#include "hal_uart.h"
#include "hal_drivers.h"
#include "hal_led.h"
#include "OSAL_Memory.h"
#include "OSAL_Timers.h"
#include "OSAL.h"
#include "hal_mcu.h"
#include "hal_defs.h"
#include "hal_types.h"
#include "eeprom.h"
#include "astronomic.h"
#include "hal_i2c.h"
#if defined ROUTER
#include "RouterFrane.h"
#else
#include "CoordFrane.h"
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

uint8 RS485_TaskID; // TaskID 

uint16 rspLen=0,rspLenTemp=0; 
uint8 rxBufRS485[RX_CNT_BUFFER];
uint8 reqSent=0xFF;
uint8 rsRetries=RS_RETRIES;

bool doRS485Read=true;

uint16 saveMeasure1=0xFFFF,saveTemperature1=0xFFFF; // Interfaccia1
uint16 saveMeasure2=0xFFFF,saveTemperature2=0xFFFF; // Interfaccia2
char RS485RM[]={0x01,0x03,0x00,0x23,0x00,0x07,0xF5,0xC2}; // default address = 0x01 per il convertitore misura freq. 
//char RS485CONT1[]={0x01,0x06,0x00,0x05,0x00,0x01,0x58,0x0B};
//char RS485CONT2[]={0x02,0x06,0x00,0x05,0x00,0x01,0x58,0x38};
//char RS485SET1[]={0x78,0x06,0x00,0x00,0x00,0x01,0x43,0xA3};
//char RS485SET2[]={0x78,0x06,0x00,0x00,0x00,0x02,0x03,0xA2};

// local functions
void RS485Response(void);
void initBuffer(void);
void setDevice(uint8 idDevice);

void RS485_Init( uint8 task_id )
{
  /* Register task ID */
  RS485_TaskID = task_id;

  /*Setting the connection on port1*/
  halUARTCfg_t uartConfig;

  uartConfig.configured           = TRUE;         // 2430 don't care.
  uartConfig.baudRate             = RS485_BAUD;
  uartConfig.flowControl          = FALSE;
  uartConfig.flowControlThreshold = RS485_THRESH;
  uartConfig.rx.maxBufSize        = RS485_RX_MAX;
  uartConfig.tx.maxBufSize        = RS485_TX_MAX;
  uartConfig.idleTimeout          = RS485_IDLE; // 2430 don't care.
  uartConfig.intEnable            = TRUE;      // 2430 don't care.

  #if defined RS485
  uartConfig.callBackFunc         = rxRS485;    // callback for serial rx event
  #else
  uartConfig.callBackFunc         = NULL;      // callback for serial rx event
  #endif

  // open UART
  HalUARTOpen(HAL_UART_PORT_0,&uartConfig);  
  
  U0UCR |= 0xA8;  // parity even 
  U0CSR |= 0x40;
   
  asm("NOP");
  
  initBuffer(); 
  
}

void setDevice(uint8 idDevice)
{

  switch(idDevice)
  {
    
   case 0x01:
     RS485RM[0]=0x01; // inserisco il dstAddress del device
     RS485RM[7]=0xC2;   // CRC variato
   break;
   
  case 0x02:
     RS485RM[0]=0x02; // inserisco il dstAddress del device
     RS485RM[7]=0xF1;   // CRC variato
  break;

   default:
    break; 
  }
  
}


void initBuffer()
{
 rspLen=0;
 rspLenTemp=0;
 
  osal_stop_timerEx ( RS485_TaskID, NO_RSP_EVENT );

  for (uint8 i=0;i<RX_CNT_BUFFER;i++)
   rxBufRS485[i]='\0';
}


uint16 RS485_ProcessEvent( uint8 task_id, uint16 events )
{
  
  if(events & DATA_RX_RS485 )
  {
   
    RS485Response();
  
   return events ^ DATA_RX_RS485;
  }
  
  if(events &  NEXT_CMD_RS485 )
  {
   
     DISABLE_PERIPH(); // disabilito per leggere le porte P0..
     halWait(10);
     osal_start_timerEx ( RS485_TaskID, NO_RSP_EVENT, NO_RSP_TIME);
   
     switch(reqSent)
     {
         
     case 0x01: // misura1
       setDevice(reqSent);
       HalUARTWrite( HAL_UART_PORT_0, (uint8*)RS485RM, 8);
       //HalUARTWrite( HAL_UART_PORT_0, (uint8*)RS485SET2, 8); 
      break;
      
     case 0x02: // misura2
       setDevice(reqSent);
       HalUARTWrite( HAL_UART_PORT_0, (uint8*)RS485RM, 8); 
      break;
      
      default:
       break; 
     }
    
    return events ^ NEXT_CMD_RS485; 
  }
  
  if (events & NO_RSP_EVENT)
  {
    
    asm("NOP");
    initBuffer();  
    
    if (rsRetries>0)
    {
      rsRetries--;
      osal_start_timerEx ( RS485_TaskID, NEXT_CMD_RS485, RS485_IDLE);
    }
    else if (reqSent>=MAX_CONVERTER)  
    {
    doRS485Read=false; // ho terminato
    reqSent=0xFF; // per evitare falsi processamenti
    #if defined ROUTER
    osal_start_timerEx(MSA_TaskId, MSA_RS485_READ_EVENT, 100);
    #else
    osal_start_timerEx(MSA_TaskId, MSA_MODEM_EVENT, 100); // Ok chiamo il modem event  
    #endif
    }
    else {
     rsRetries=RS_RETRIES;  
     reqSent++;
     osal_start_timerEx ( RS485_TaskID, NEXT_CMD_RS485, RS485_IDLE);
    }
      
    return events ^ NO_RSP_EVENT; 
  }
  
  return 0;
}


void rxRS485( uint8 port, uint8 event )
{
  if (( rspLen = HalUARTRead( HAL_UART_PORT_0, &rxBufRS485[rspLenTemp], RX_CNT_BUFFER-rspLenTemp ) ) != 0 ) // ho ricevuto qualcosa
  {
   if (rspLenTemp==0) osal_start_timerEx(RS485_TaskID,DATA_RX_RS485,120);  // parte il timer seriale di 120ms sto in ascolto e salvo i dati
   rspLenTemp=rspLen+rspLenTemp; // salvo dove sono arrivato
  }
}

void RS485Response()
{

 switch(reqSent)
 {

   case 0x01:  // misura sensore interfaccia1
    osal_stop_timerEx ( RS485_TaskID, NO_RSP_EVENT );
    // salvo la misura letta
    saveMeasure1=BUILD_UINT16(rxBufRS485[4],rxBufRS485[3]);
    saveTemperature1=BUILD_UINT16(rxBufRS485[16],rxBufRS485[15]);
    // interrogo per leggere la temperatura
    reqSent=0x02;
    osal_start_timerEx ( RS485_TaskID, NEXT_CMD_RS485, RS485_IDLE);
   break;
   
   case 0x02: // misura sensore temperatura2
    osal_stop_timerEx ( RS485_TaskID, NO_RSP_EVENT );
    // salvo la misura letta
    saveMeasure2=BUILD_UINT16(rxBufRS485[4],rxBufRS485[3]);
    saveTemperature2=BUILD_UINT16(rxBufRS485[16],rxBufRS485[15]);
    doRS485Read=false; // ho terminato
    reqSent=0xFF; // per evitare falsi processamenti
    rsRetries=RS_RETRIES;
    #if defined ROUTER
     osal_start_timerEx(MSA_TaskId, MSA_RS485_READ_EVENT, 100);
    #else
     osal_start_timerEx(MSA_TaskId, MSA_MODEM_EVENT, 100); // Ok chiamo il modem event  
    #endif
    
   break;
   
   default:
    break; 
 }
 
  initBuffer(); 
   
}


#endif //modem