/******************************************************************************
*                                                                             *
*        **********                                                           *
*       ************                                                          *
*      ***        ***                                                         *
*     ***    ++    ***                                                        *
*     ***   +  +   ***                      CHIPCON                           *
*     ***   +                                                                 *
*     ***   +  +   ***                                                        *
*     ***    ++    ***                                                        *
*      ***        ***                                                         *
*       ************                                                          *
*        **********                                                           *
*                                                                             *
*******************************************************************************

Filename:     adc1115.c
Target:       cc2430
Author:       EFU
Revised:      1/3-2007
Revision:     1.1

Description:
Function implementations for common adc1115 functions for use with the CC2430DB.

******************************************************************************/
#if (defined WINETHP)

#include "adc1115.h"
#include "ioCC2530.h"
#include "hal_timer.h"
#include "ZComDef.h"
#include "hal_i2c.h"
#include "hal_adc.h"
#include "hal_led.h"
#include "OSAL_Timers.h"


bool readAdc1115(byte* buffer, byte address, uint8 length);
byte writeAdc1115(byte* buffer, byte address, int8 length);
void selectAdc(uint8 numbers);
uint16 max(uint16 a[], uint8 dim);
uint16 min(uint16 a[], uint8 dim);

uint8 retAdc1115;
uint16 Adc1115WriteAddress,Adc1115ReadAddress;

void selectAdc(uint8 numbers)
{
  switch (numbers)
  {
  case 0x01:
    Adc1115WriteAddress=ADC1115_WRITEADDRESS_1;
    Adc1115ReadAddress=ADC1115_READADDRESS_1;
  break;

  case 0x02:
    Adc1115WriteAddress=ADC1115_WRITEADDRESS_2;
    Adc1115ReadAddress=ADC1115_READADDRESS_2;
  break;
 }
}

uint16 readSensorsAdc1115(uint8 selAdc, uint16 configAdc )
{
  uint16 readMeasure[CAMPIONI];
  uint8 tempConf[2],tempResult[2];
  uint8 numCampioni=0;
  uint16 maxValue,minValue;
  uint32 tempCalc=0;
  uint16 readVal=0;
  bool singleEnded=false;
  
    // configurazione SE - 2048 e 4096
  if ((configAdc==0xC4E0)||(configAdc==0xD4E0)||(configAdc==0xE4E0)||(configAdc==0xF4E0)
      ||(configAdc==0xC2E0)||(configAdc==0xD2E0)||(configAdc==0xE2E0)||(configAdc==0xF2E0) ) 
    singleEnded=true;
  
  tempConf[0]=HI_UINT16(configAdc);
  tempConf[1]=LO_UINT16(configAdc);

  selectAdc(selAdc);

  // scrivo la configurazione
  if (writeAdc1115(tempConf,CONFIG_REG,0x02)>0)
  {
   for(uint8 i=0;i<CAMPIONI;i++)
   {
    if ( (configAdc==0xE680) || (configAdc==0xF680) )  // 1024 PT1000 128SPS
    {
     halWait(10);
    } 
    else 
    {
     //halWait(1);
     //halWaitUs(400);
     // attendo 1.4ms ( 1.2ms tempo massimo lettura ADC )
     #if defined DELAY_TENS
      halWait(20); // 4ms tra ogni lettura
     #else
      halWait(4); // 4ms tra ogni lettura
     #endif
    }
    
    if (readAdc1115(tempResult,CONV_REG,0x02)==0) return 0xFFFF; // lettura errata ritorna 0xFFFF
    readVal=(BUILD_UINT16(tempResult[1], tempResult[0]));
 
    if ( (singleEnded==true)&&(readVal>32767)&&(i2cRetries>0)) readVal=0; // se ho letto negativo metto 0  
    readMeasure[i]=readVal; // salvo nell'array la lettura
   }
   
    // vado a eliminare il valore massimo e il valore minimo e a fare la media degli altri
    maxValue=max(readMeasure,CAMPIONI);
    minValue=min(readMeasure,CAMPIONI);
    
    // salvo e faccio la media dei valori intermedi
    for(uint8 i=0;i<CAMPIONI;i++)
    {
     if ( (readMeasure[i]>=minValue) && (readMeasure[i]<=maxValue) )
       tempCalc=tempCalc+readVal;
       numCampioni++;
    }
    
   return (uint16)(tempCalc/numCampioni);
  }
  else return 0xFFFF; // lettura errata ritorna 0xFFFF
 }


/******************************************************************************
* See astronomic.h for a description of this function.
******************************************************************************/
bool readAdc1115(byte* buffer, byte address, uint8 length)
{
   byte i;
   byte transmitBuffer[2];
   
   // timer error avviato
   osal_start_timerEx(Peripheral_TaskID, I2C_ERROR_EVENT, I2C_ERROR_TIME);

   // init the retries
   i2cRetries=I2C_RETRIES;

   transmitBuffer[0] = Adc1115WriteAddress;
   transmitBuffer[1] = (byte) (address);

   smbStart();

   for(i = 0; i < 2; i++)
   {
     i2cAck=0; // init della variabile
     while ( (i2cAck==0) && (i2cRetries>0)) //Keep sending until ACK received
     smbSendbyte(transmitBuffer[i]);
   }

   smbClock(0);
   wait();
   DATA_HIGH();
   smbClock(1);

   retAdc1115=smbReceive(Adc1115ReadAddress, length, buffer); // se >0 Ok
 
   // stop timer error
   osal_stop_timerEx(Peripheral_TaskID,I2C_ERROR_EVENT);
   
   return retAdc1115;
}

/******************************************************************************
* See astronomic.h for a description of this function.
* length must be less or equal 64
******************************************************************************/
byte writeAdc1115(byte* buffer, byte address, int8 length)
{
   uint8 i = 0;
   uint8 j;
   byte transmitBuffer[ADC1115_BUFFER_SIZE];

    // timer error avviato
   osal_start_timerEx(Peripheral_TaskID, I2C_ERROR_EVENT, I2C_ERROR_TIME);

   // init the retries
   i2cRetries=I2C_RETRIES;

   transmitBuffer[i++] = Adc1115WriteAddress;
   transmitBuffer[i++] = (byte) (address);

   for(j = 0; j < length; j++)
   {
      transmitBuffer[i++] = buffer[j];
   }

  if (smbSend(transmitBuffer, i) > 0 )
  retAdc1115=TRUE;
  else retAdc1115=FALSE;

  if (retAdc1115==FALSE){
    HalLedSet (HAL_LED_2, HAL_LED_MODE_ON);  // led rosso ON per segnalare malfunzionamento
  }
  
  // stop timer error
  osal_stop_timerEx(Peripheral_TaskID,I2C_ERROR_EVENT);
  
  return retAdc1115;
}


//massimo
uint16 max(uint16 a[], uint8 dim)
{
    uint16 max=a[0]; // default prendo il primo
    uint8 i;
    
  for (i=0; i<dim; i++) {
      if (max<a[i]) 
      {
        max=a[i];
      }       
  }
return max;
}
     
//minimo    
uint16 min(uint16 a[], uint8 dim)   
{   
  uint8 i;
  uint16 min=a[0];  // default prendo il primo
  
  for (i=0; i<dim; i++) {
      if (min>a[i]) 
      {
          min=a[i];
      }
  }
    return min;
}
 


#endif
