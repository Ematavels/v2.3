/******************************************************************************
*                                                                             *
*        **********                                                           *
*       ************                                                          *
*      ***        ***                                                         *
*     ***    ++    ***                                                        *
*     ***   +  +   ***                      CHIPCON                           *
*     ***   +                                                                 *
*     ***   +  +   ***                                                        *
*     ***    ++    ***                                                        *
*      ***        ***                                                         *
*       ************                                                          *
*        **********                                                           *
*                                                                             *
*******************************************************************************

Filename:     astronomic.h
Target:       cc2430
Author:       EFU
Revised:      1/3-2007
Revision:     1.1

Description:
Function declarations for common astronomic functions for use with the CC2430DB.

All functions defined here are implemented in astronomic.c.

******************************************************************************/

#ifdef __cplusplus
extern "C"
{
#endif


  /**************************************************************************************************
 * INCLUDES
 **************************************************************************************************/
#include "ZComDef.h"
   
/* General define */
#define ATTINY85_WIND_W   0x08
#define ATTINY85_WIND_R   0x09
  
#define ATTINY85_RAIN_W   0X10
#define ATTINY85_RAIN_R   0x11
  
#define ATTINY85_PRESS0_W 0x0E
#define ATTINY85_PRESS0_R 0x0F
  
#define ATTINY85_PRESS1_W 0x12
#define ATTINY85_PRESS1_R 0x13
  
#define ATTINY85_PRESS2_W 0x0A
#define ATTINY85_PRESS2_R 0x0B
  
#define ATTINY85_PRESS3_W 0x0C
#define ATTINY85_PRESS3_R 0x0D

extern bool readATTiny;   
  
#if defined (PIOGGIA) || defined (VENTO) || defined (PRESS0) || defined (PRESS1) || defined (PRESS2) || defined (PRESS3)
 
extern uint8 readAttiny85(byte* buffer, byte address, uint8 length);

extern byte writeAttiny85(byte* buffer, byte address, int8 length);

extern uint8 longReadAttiny85(byte devAddr,byte* buffer,byte byteAddr,uint8 length);

#endif