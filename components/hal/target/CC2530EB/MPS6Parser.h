/**************************************************************************************************
  Filename:       Decagon_GS3.h
  Revised:        $Date: 2007-09-11 10:58:41 -0700 (Tue, 11 Sep 2007) $
  Revision:       $Revision: 15371 $

  Description:    Describe the purpose and contents of the file.

**************************************************************************************************
 **************************************************************************************************/

#ifdef __cplusplus
extern "C"
{
#endif

/**************************************************************************************************
 * INCLUDES
 **************************************************************************************************/
#include "ioCC2530.h"
#include "hal_types.h"
#include "ZComDef.h"
#include "hal_board_cfg.h"

extern void Mps6Parser(byte *buffer2, int16 *resbuf);
extern void hexMPS6Multiplier (byte *databuf,uint16 dotpos, uint8 TYPE, int16 *resbuf);



#define TENS 1
#define TEMP 2

