/******************************************************************************
*                                                                             *
*        **********                                                           *
*       ************                                                          *
*      ***        ***                                                         *
*     ***    ++    ***                                                        *
*     ***   +  +   ***                      CHIPCON                           *
*     ***   +                                                                 *
*     ***   +  +   ***                                                        *
*     ***    ++    ***                                                        *
*      ***        ***                                                         *
*       ************                                                          *
*        **********                                                           *
*                                                                             *
*******************************************************************************

Filename:     astronomic.c
Target:       cc2430
Author:       EFU
Revised:      1/3-2007
Revision:     1.1

Description:
Function implementations for common astronomic functions for use with the CC2430DB.

******************************************************************************/
#include "ov7670.h"
#include "ioCC2530.h"
#include "hal_timer.h"
#include "ZComDef.h"
#include "hal_i2c.h"
#include "hal_led.h"

#include "hal_led.h"

/* Includes -------------------------------------------------- ----------------*/
#include "OV7670.h"

uint8 SCCB_Read(void);
uint8 SCCB_Write(uint8 Data);
void SCCB_Start(void);
void SCCB_Stop(void);
void noAck(void);

uint8 WriteReg(uint8 address,uint8 val)
{
SCCB_Start();
SCCB_Write(OvWriteAddress);
SCCB_Write(address);
halWait(1);
SCCB_Write(val);
SCCB_Stop();
}


uint8 ReadReg(byte address)
{
  uint8 Data;

SCCB_Start();
SCCB_Write(OvWriteAddress);
SCCB_Write(address);
SCCB_Stop();
halWait(1);
SCCB_Start();
SCCB_Write(OvReadAddress);
Data = SCCB_Read();
noAck();
SCCB_Stop();

return Data;
}

uint8 SCCB_Write(uint8 Data)
{
	uint8 i;
	uint8 Ack;

	// Configure SIO_D of SCCB/I2C interface as output for write
	IO_DIR_PORT_PIN(DATA_PORT, DATA_PIN, IO_OUT);

	// Write data bit by bit on SCCB/I2C
	for(i=0;i<8;i++)
	{
		if((Data & 0x80) == 0x80)		// If bit in Data is high, write high on SCCB/I2C
		{
			 DATA_OUT_HIGH();
		}
		else							// If bit in Data is low, write low on SCCB/I2C
		{
			 DATA_LOW();
		}
		Data <<= 1;						// Rotate Data for write next bit
		halWaitUs(100);

		// Create clock pulse on SCCB/I2C
		// ___  On SIO_C pin (SCCB clock)
		//    \___
		smbClock(1);
		halWaitUs(100);
		smbClock(0);
		halWaitUs(100);
	}

	// Read acknowladge from Camera Module to confirm received data
	halWaitUs(100);
	// Configure SIO_D of SCCB/I2C interface as input for read
	DATA_HIGH();
	halWaitUs(100);
	smbClock(1);
	halWaitUs(100);
        Ack = !SDA;
        // Pulse on SCCB/I2C fall down from high
	smbClock(0);
	halWaitUs(100);
	// Configure SIO_D of SCCB/I2C interface back to output for write
	IO_DIR_PORT_PIN(DATA_PORT, DATA_PIN, IO_OUT);

	return (Ack);
}

uint8 SCCB_Read(void)
{
	uint8 Data, i;

	// Write to Data zero for correct return data
	Data = 0;
	// Configure SIO_D of SCCB/I2C interface as input for read
	IO_DIR_PORT_PIN(DATA_PORT, DATA_PIN, IO_IN);
	// Delay for  SCCB/I2C interface
	halWaitUs(500);
	// Read data from SCCBI/I2C interface
	for(i=8;i>0;i--)
	{
		halWaitUs(500);							// Delay for  SCCB/I2C
		smbClock(1);						// Clock high on SIO_C
		halWaitUs(500);
		Data = Data << 1;						// Rotate Data << 1
		if(SDA){Data = Data + 1;}		      // Read SIO_D pin value
		smbClock(0);							// Clock low on SIO_C
		halWaitUs(500);
	}
	// Return received data from SCCBI/I2C interface
	return(Data);
}

void SCCB_Start(void)
{
	IO_DIR_PORT_PIN(DATA_PORT, DATA_PIN, IO_OUT);	// Configure SIO_D of SCCB/I2C interface as output for write
	SDA=0x01;
	halWaitUs(500);

	smbClock(1);	
	halWaitUs(500);

	SDA=0x00;
	halWaitUs(500);

	smbClock(0);	
	halWaitUs(500);
}



/**
  * @brief  Create on  SCCB/I2C interface "STOP" condition of transmission
  * 	             |-> Stop
  * 				 |	 __
  * 		SIO_D ______/  \__
  * 			      ________
  * 		SIO_C ___/
  * @param  None
  * @retval None
  */
void SCCB_Stop(void)
{
	IO_DIR_PORT_PIN(DATA_PORT, DATA_PIN, IO_OUT);			// Configure SIO_D of SCCB/I2C interface as output for write
	SDA=0x00;
	halWaitUs(500);

	smbClock(1);	
	halWaitUs(500);

	SDA=0x01;
	halWaitUs(500);
}

void noAck()
{


IO_DIR_PORT_PIN(DATA_PORT, DATA_PIN, IO_OUT); // Configure SIO_D of SCCB/I2C interface as output for write
SDA=0x01;
halWaitUs(500);

smbClock(1);
halWaitUs(500);

smbClock(0);
halWaitUs(500);

SDA=0x00;
halWaitUs(500);


}