/******************************************************************************
*                                                                             *
*        **********                                                           *
*       ************                                                          *
*      ***        ***                                                         *
*     ***    ++    ***                                                        *
*     ***   +  +   ***                      CHIPCON                           *
*     ***   +                                                                 *
*     ***   +  +   ***                                                        *
*     ***    ++    ***                                                        *
*      ***        ***                                                         *
*       ************                                                          *
*        **********                                                           *
*                                                                             *
*******************************************************************************

Filename:     SHT3x.c
Target:       cc2430
Author:       EFU
Revised:      1/3-2007
Revision:     1.1

Description:
Function implementations for common SHT3x_Winet functions for use with the CC2430DB.

******************************************************************************/

#include "SHT3x.h"
#include "ioCC2530.h"
#include "hal_timer.h"
#include "ZComDef.h"
#include "hal_i2c.h"
#include "hal_led.h"
#include "hal_led.h"
#include "OSAL_Timers.h"

/******************************************************************************
* See Sht3x.h for a description of this function.
******************************************************************************/
void sht3xResetI2c()
{ 
   DATA_HIGH();
   wait();
   smbClock(0);
   for (uint8 i=0;i<10;i++)
   {
    smbClock(1);
    wait();
    smbClock(0);
   }    
}

bool readSht3x(byte* buffer, uint16 address, uint8 length)
{
   byte i;
   byte transmitBuffer[3];
   uint8 retSht;

   // timer error avviato
   osal_start_timerEx(Peripheral_TaskID, I2C_ERROR_EVENT, I2C_ERROR_TIME);
      
   // init the retries
   i2cRetries=I2C_RETRIES;

   transmitBuffer[0] = SHT3X_WRITEADDRESS;
   transmitBuffer[1] = (byte) (address >> 8);
   transmitBuffer[2] = (byte) (address);

   smbStart();
  
   for(i = 0; i < 3; i++)
   {
     i2cAck=0; // init della variabile
     while ( (i2cAck==0) && (i2cRetries>0)) //Keep sending until ACK received
     {
     smbSendbyte(transmitBuffer[i]);
     halWaitUs(400); // ritardo per attiny
     }
   }

   if (i2cRetries>0) {
   smbClock(0);
   wait();
   DATA_HIGH();
   smbClock(1);

   halWait(25); // tempo massimo lettura 15ms
   
   retSht=smbReceive(SHT3X_READADDRESS, length, buffer); // se >0 Ok
   }
   else { 
     smbStop(); 
   } // se va male � bene chiudere la lettura con uno stop
   
    // stop timer error
   osal_stop_timerEx(Peripheral_TaskID,I2C_ERROR_EVENT);
   
   return retSht;
}

/******************************************************************************
* See SHT3X.h for a description of this function.
* length must be less or equal 64
******************************************************************************/
byte writeSht3x(byte* buffer, byte address, int8 length)
{
   uint8 i = 0,ret;
   uint8 j;
   byte transmitBuffer[3];
   
   // timer error avviato
   osal_start_timerEx(Peripheral_TaskID, I2C_ERROR_EVENT, I2C_ERROR_TIME);

    // init the retries
    i2cRetries=I2C_RETRIES;

   transmitBuffer[i++] = SHT3X_WRITEADDRESS;
   transmitBuffer[i++] = (byte) (address);

   for(j = 0; j < length; j++)
   {
      transmitBuffer[i++] = buffer[j];
   }

  if (smbSend(transmitBuffer, i) > 0 )
  ret=TRUE;
  else ret=FALSE;

   // stop timer error
   osal_stop_timerEx(Peripheral_TaskID,I2C_ERROR_EVENT);

  return ret;
}

byte resetSht3x()
{
   uint8 ret;
   byte transmitBuffer[3];

    // init the retries
    i2cRetries=I2C_RETRIES;

    // timer error avviato
    osal_start_timerEx(Peripheral_TaskID, I2C_ERROR_EVENT, I2C_ERROR_TIME);

   transmitBuffer[0] = SHT3X_WRITEADDRESS;
   transmitBuffer[1] = RESET_SHT3X;

  if (smbSend(transmitBuffer, 2) > 0 )
  ret=TRUE;
  else ret=FALSE;

  halWait(50); // tempo per il reset + margine

  // stop timer error
   osal_stop_timerEx(Peripheral_TaskID,I2C_ERROR_EVENT);

  return ret;
}
