#if defined SPI

#include "hal_types.h"

/******************
    SD COMMANDS
*******************/
  
#define CMD_GO_IDLE_STATE         0x00    // CMD0   -
#define CMD_SEND_OP_COND          0x01    // CMD1   reserved
#define CMD_SEND_IF_COND          0x08    // CMD8   R7
#define CMD_SEND_CSD              0x09    // CMD9   R2
#define CMD_SEND_CID              0x0A    // CMD10  R2
#define CMD_STOP_TRANSMISSION     0x0C    // CMD12  reserved
#define CMD_SEND_STATUS           0x0D    // CMD13  R1
#define CMD_SET_BLOCKLEN          0x10    // CMD16  R1
#define CMD_READ_SINGLE_BLOCK     0x11    // CMD17  R1
#define CMD_READ_MULTIPLE_BLOCK   0x12    // CMD18  R1
#define CMD_WRITE_SINGLE_BLOCK    0x18    // CMD24  R1
#define CMD_WRITE_MULTIPLE_BLOCK  0x19    // CMD25  R1
#define CMD_PROGRAM_CSD           0x1B    // CMD27  R1
#define CMD_SET_WRITE_PROT        0x1C    // CMD28  R1b
#define CMD_CLR_WRITE_PROT        0x1D    // CMD29  R1b
#define CMD_SEND_WRITE_PROT       0x1E    // CMD30  R1
#define CMD_TAG_SECTOR_START      0x20    // CMD32  R1
#define CMD_TAG_SECTOR_END        0x21    // CMD33  R1
#define CMD_UNTAG_SECTOR          0x22    // CMD34
#define CMD_TAG_ERASE_GROUP_START 0x23    // CMD35
#define CMD_TAG_ERASE_GROUP_END   0x24    // CMD36
#define CMD_UNTAG_ERASE_GROUP     0x25    // CMD37
#define CMD_ERASE                 0x28    // CMD38  R1b
#define CMD_LOCK_UNLOCK           0x2A    // CMD42  R1
#define CMD_APP_CMD               0x37    // CMD55  R1
#define CMD_READ_OCR              0x3A    // CMD58  reserved
#define CMD_CRC_ON_OFF            0x3B    // CMD59  reserved
#define ACMD_SEND_OP_COND         0x29    // CMD41  

typedef struct {                        // struct to describe sd card
     bool         init_error1;    
     bool         init_error2;
     bool         init_error3; 
     bool         init_error4; 
     bool         init_error5; 
     bool         init_error6; 
     bool         wsblock_error;      
     bool         rsblock_error;       
     bool         hc;
     uint8        timeout;    // timeout value when ops timeout
     uint32       size;        // card size in 512kbyte blocks (ie actual capacity is 1000 * m_size / 2 bytes)        
  } SD_CARD;

bool SDInit(SD_CARD *sd_card);
uint8 SD_Command(uint8 cmd, uint32 arg, uint8 crc);
uint8 SD_ReadSector( uint32 SectorNumber, uint8 *Buffer );
uint8 SD_WriteSector( uint32 SectorNumber, uint8 *Buffer);
uint8 SD_readMultipleBlock (uint32 startBlock, uint32 totalBlocks, uint8 *buffer);
uint8 SD_writeMultipleBlock(uint32 startBlock, uint32 totalBlocks, uint8 *buffer);

#endif