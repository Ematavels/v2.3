/******************************************************************************
*                                                                             *
*        **********                                                           *
*       ************                                                          *
*      ***        ***                                                         *
*     ***    ++    ***                                                        *
*     ***   +  +   ***                      CHIPCON                           *
*     ***   +                                                                 *
*     ***   +  +   ***                                                        *
*     ***    ++    ***                                                        *
*      ***        ***                                                         *
*       ************                                                          *
*        **********                                                           *
*                                                                             *
*******************************************************************************

Filename:     SHT21_Winet.c
Target:       cc2430
Author:       EFU
Revised:      1/3-2007
Revision:     1.1

Description:
Function implementations for common SHT21_Winet functions for use with the CC2430DB.

******************************************************************************/

#include "SHT21_Winet.h"
#include "ioCC2530.h"
#include "hal_timer.h"
#include "ZComDef.h"
#include "hal_i2c.h"
#include "hal_led.h"
#include "hal_led.h"
#include "OSAL_Timers.h"

/******************************************************************************
* See Sht21.h for a description of this function.
******************************************************************************/
bool readSht21(byte* buffer, byte address, uint8 length)
{
   byte i;
   byte transmitBuffer[3];
   uint8 retSht;

   // timer error avviato
   osal_start_timerEx(Peripheral_TaskID, I2C_ERROR_EVENT, I2C_ERROR_TIME);
   
   // init the retries
   i2cRetries=I2C_RETRIES;

   transmitBuffer[0] = SHT21_WRITEADDRESS;
   transmitBuffer[1] = (byte) (address);

   smbStart();

   for(i = 0; i < 2; i++)
   {
     i2cAck=0; // init della variabile
     while ( (i2cAck==0) && (i2cRetries>0)) //Keep sending until ACK received
     smbSendbyte(transmitBuffer[i]);
     halWaitUs(400); // ritardo per attiny
   }

   smbClock(0);
   wait();
   DATA_HIGH();
   smbClock(1);

   if (address==TEMP_MEASURE) halWait(100); // attendo il termine della misura con sicurezzza
   else halWait(50); // attendo il termine della misura con sicurezzza

   retSht=smbReceive(SHT21_READADDRESS, length, buffer); // se >0 Ok
   
    // stop timer error
   osal_stop_timerEx(Peripheral_TaskID,I2C_ERROR_EVENT);
   
   return retSht;
}

/******************************************************************************
* See SHT21.h for a description of this function.
* length must be less or equal 64
******************************************************************************/
byte writeSht21(byte* buffer, byte address, int8 length)
{
   uint8 i = 0,ret;
   uint8 j;
   byte transmitBuffer[3];
   
   // timer error avviato
   osal_start_timerEx(Peripheral_TaskID, I2C_ERROR_EVENT, I2C_ERROR_TIME);

    // init the retries
    i2cRetries=I2C_RETRIES;

   transmitBuffer[i++] = SHT21_WRITEADDRESS;
   transmitBuffer[i++] = (byte) (address);

   for(j = 0; j < length; j++)
   {
      transmitBuffer[i++] = buffer[j];
   }

  if (smbSend(transmitBuffer, i) > 0 )
  ret=TRUE;
  else ret=FALSE;

   // stop timer error
   osal_stop_timerEx(Peripheral_TaskID,I2C_ERROR_EVENT);

  return ret;
}

byte resetSht21()
{
   uint8 ret;
   byte transmitBuffer[3];

    // init the retries
    i2cRetries=I2C_RETRIES;

    // timer error avviato
    osal_start_timerEx(Peripheral_TaskID, I2C_ERROR_EVENT, I2C_ERROR_TIME);

   transmitBuffer[0] = SHT21_WRITEADDRESS;
   transmitBuffer[1] = RESET_SHT21;

  if (smbSend(transmitBuffer, 2) > 0 )
  ret=TRUE;
  else ret=FALSE;

  halWait(50); // tempo per il reset + margine

  // stop timer error
   osal_stop_timerEx(Peripheral_TaskID,I2C_ERROR_EVENT);

  return ret;
}
