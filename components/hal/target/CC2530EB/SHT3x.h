/******************************************************************************
*                                                                             *
*        **********                                                           *
*       ************                                                          *
*      ***        ***                                                         *
*     ***    ++    ***                                                        *
*     ***   +  +   ***                      CHIPCON                           *
*     ***   +                                                                 *
*     ***   +  +   ***                                                        *
*     ***    ++    ***                                                        *
*      ***        ***                                                         *
*       ************                                                          *
*        **********                                                           *
*                                                                             *
*******************************************************************************

Filename:     sht3x.h
Target:       cc2430
Author:       EFU
Revised:      1/3-2007
Revision:     1.1

Description:
Function declarations for common sht3x functions for use with the CC2430DB.

All functions defined here are implemented in sht3x.c.

******************************************************************************/

#ifdef __cplusplus
extern "C"
{
#endif


  /**************************************************************************************************
 * INCLUDES
 **************************************************************************************************/
#include "ZComDef.h"

/* General define */
#define SHT3X_WRITEADDRESS   0x88  // 0x44
#define SHT3X_READADDRESS    0x89  // 0x45

#define MEASURE_SHOT 0x24
#define TEMP_MEASURE 0xF3
#define HUM_MEASURE  0xF5
#define WRITE_REGISTER  0xE6
#define READ_REGISTER  0xE7
#define RESET_SHT3X  0xFE
#define MEASUE_TEMP_HUM 0x2C06


/******************************************************************************
* @fn  readSht3x
*
* @brief
*      Initiates sequential read of Sht3x content from the given address. The
*      read operation transfer _length_ bytes to _buffer_, starting at Sht3x
*      address _address_, ending at _adress_ + _length_.
*
* Parameters:
*
* @param  BYTE*    buffer
*	  Pointer to buffer for storing read values.
*	
* @param  WORD     address
*	  Start address for read operation. Valid range: 0 - 0xFFF.
*
* @param  UINT8    length
*	  Number of bytes to be read.
*
* @return BOOL
*         TRUE: Read operation successful.
*         FALSE: Read operation NOT successful.
*
******************************************************************************/
extern bool readSht3x(byte* buffer, uint16 address, uint8 length);

extern byte writeSht3x(byte* buffer, byte address, int8 length);

extern byte resetSht3x(void);

extern void sht3xResetI2c(void);

