/******************************************************************************
*                                                                             *
*        **********                                                           *
*       ************                                                          *
*      ***        ***                                                         *
*     ***    ++    ***                                                        *
*     ***   +  +   ***                      CHIPCON                           *
*     ***   +                                                                 *
*     ***   +  +   ***                                                        *
*     ***    ++    ***                                                        *
*      ***        ***                                                         *
*       ************                                                          *
*        **********                                                           *
*                                                                             *
*******************************************************************************

Filename:     Decagon_GS3.c
Target:       cc2430
Author:       EFU
Revised:      1/3-2007
Revision:     1.1
******************************************************************************/

#if defined FLORA_1 || DEC5TE_P04 || defined SENTEK_10A || defined SENTEK_10B || defined SENTEK_10C || defined SENTEK_10D || defined SENTEK_10E || defined SENTEK_10F || defined SENTEK_90 || defined SENTEK_120 || defined SENTEK_60 || defined SENTEK_30 || defined MPS6_P04 || defined DECGS3_P04 || defined DECCDT || defined ACC_TDR || defined ACC_TDTP

#include "SdiCore.h"
#include "ioCC2530.h"
#include "hal_types.h"
#include "hal_sleep.h"
#include "OSAL_Timers.h"
#include "mac_api.h"
#include "astronomic.h"


void sdiClock(bool);
void sdiSendbyte(byte);
void sdiWrite(bool);
void sdiStart(void);
void sdiStop(void);
void markingSdi(void);
uint8 receiveChar(byte* buffer,float trim);
void resetSdibuffer(void);
void processSdiResponse(void);
bool readSdi(uint8 sdiAddress,byte* buffer,float trim);
bool readSentek(uint8 sdiAddress,byte* buffer,byte *buffer2,byte *buffer3,byte *buffer4 ,float trim,uint8 nprobes, uint8 readType);

bool sdiSanityCheck(byte *buffer, uint8 typeAcc);
bool sdiSanityCheckSentek(byte *buffer);
void startMeasurement(uint8 indirizzo,uint8 tipo);
void getResponse(uint8 indirizzo,char index,byte *buffersdi, float trim);

uint8 askSdi(uint8 sdiAddress,byte *buffer,float trim);
uint8 askSentek(uint8 sdiAddress,byte *buffer,byte *buffer2,byte *buffer3, byte *buffer4,float trim,uint8 nprobes, uint8 readType);

void sdiParser(byte *buffer2,int16 *resbuf);
uint16 finddot(byte *databuff,uint8 len);
uint8 reverse_bit8(uint8 x);

uint16 LAST_VALID_SDI_CLK=SDI_DEF_CLOCK;
uint16 SDI_CLK_TIME;

uint8 sdiGrafic[40]={
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,
};

uint8 leggo;

uint8 aft,bef;  //contatori caratteri prima e dopo il punto
uint8 newChar = 0; //char per ricezione carattere da seriale
uint8 sensAddr;   //Indirizzo Sensore
uint8 chAq=0xFF;  //Variabile per check carattere
uint32 counter=0;   //Variabile per uscire da ciclo in attesa di caratteri
//uint8 u=0;
uint8 san=0;      // risultato sanity check 
uint8 si=0;       //Sanity index, se ok = 0x03
uint8 sdi_retr;   //numero di retries at runtime
bool sdi_succ;    //risultato di readSdi

uint8 c;        //indici per buffer
uint8 ci=0;

//P0INP =0x20; 

 uint16 p=0;

 uint8 zi=0;
 uint8 ti=0;
 
 
/// CLOCK con deriva di 100 us
void sdiClock(bool value)
{
   if(!value){
      IO_DIR_PORT_PIN( SDI_PORT, SDI_PIN, IO_OUT );
      SDI = 0;
   }
   else {
      IO_DIR_PORT_PIN( SDI_PORT, SDI_PIN, IO_OUT );
      SDI = 1;
   }
   halWaitUs(SDI_CLK_TIME);
  
}



/* ADATTA UN BYTE AL FORMATO SDI-12 E LO INVIA SULLA LINEA */
void sdiSendbyte(byte b){
   uint8 i;
   p=0;
   b=reverse_bit8(b);          // inverto l'ordine dei bit per avere LSB first
   
   sdiStart();                 // Start Bit
   
   for (i = 0; i < 7; i++){    // Mando solo 7 bit di info
     //if(i==0) b=(b<<1);      // Mando solo 7 bit di inf
     sdiWrite(!(b & 0x80));    // Scrivo il bit
     if(!(b & 0x80)) p++;          // conto gli 1 per stima parit� (PARI)
     b = (b <<  1);
   }
   //asm("NOP");                             
   if(p%2==0) sdiWrite(0);     //Scrivo il bit di parit�
   else if(p%2!=0) sdiWrite(1);
   //asm("NOP");
   sdiStop();                  //Mando lo stop
   
}


void sdiWrite(bool value){
   
   if(value){
     SDI_DATA_OUT_HIGH(); // scrivo 1
     halWaitUs(SDI_CLK_TIME);
   }
   else{
      SDI_DATA_LOW();   // scrivo 0
      halWaitUs(SDI_CLK_TIME);
   }

}


/* START bit SDI */
void sdiStart(){
  SDI_DATA_OUT_HIGH();
  halWaitUs(SDI_CLK_TIME);
}

/* STOP bit SDI*/
void sdiStop(){
 SDI_DATA_LOW();
 halWaitUs(SDI_CLK_TIME);
 SDI_DATA_HIGH();
}


/* INVERTE UN BYTE IN LSB-FIRST */
uint8 reverse_bit8(uint8 x){
  x = ((x & 0x55) << 1) | ((x & 0xAA) >> 1);
  x = ((x & 0x33) << 2) | ((x & 0xCC) >> 2);
  return (x << 4) | (x >> 4);
}


/* MARKING SDI RICHIESTO PER INIZIO COMUNICAZIONE */
void markingSdi(){
 SDI_DATA_LOW();  ///////
 halWaitUs(745);
 SDI_DATA_OUT_HIGH();
 halWaitms(22);    //12 ms ma non preciso
 halWaitUs(400);
 SDI_DATA_LOW();
 halWaitms(15);     //8 ms ma non preciso            
}


/* RICEVE LA RISPOSTA DEL SENSORE CARATTERE PER CARATTERE */


/* SVUOTO IL BUFFER */
void resetSdiBuffer(byte *buffer){
   for(uint8 k=0;k<SDI_BUFFER_LENGTH;k++) buffer[k]=0x00; 
}



bool sdiSanityCheckSentek(byte *buffer){
uint8 u=2;
uint8 si=0;

while(u<10 && u>1){
  if(buffer[u]==0x00)
    si++;
    u++;
}

if(si>=7) return false;
else return true;
}


bool sdiSanityCheck(byte *buffer,uint8 typeAcc){
uint8 u=0;
uint8 si=0;

while(u<SDI_BUFFER_LENGTH && buffer[u]!=0xFF){
  if(buffer[u]==0x2B || buffer[u]==0x2D)
    si++;
    u++;
}

switch (typeAcc)
{
  // Florapulse
  case 0x30:
   if(si==3) 
     return true;
  break;
  
  
  // MPS
  case 0x36:
   if(si==2) 
     return true;
  break;
  
 // Acclima TDT
   case 0x38:
   case 0x48:
   case 0x58: 
   if(si==4) 
     return true;
  break;
  
  case 0x37: // ACCLIMA TDR310H
  case 0x47:
  case 0x57:
  case 0x69:
  case 0x6A:
  case 0x6B:
   if(si==5) 
     return true;
   break;
   
  default:
    if(si==3) 
      return true;
  break;  
}

return false;

}



//RICEVE LA RISPOSTA DEL SENSORE CARATTERE PER CARATTERE 
uint8 receiveChar(byte *buffer,float trim)
{ 
  ci=0; // inizializzo la posizione del vettore sul primo byte
  counter=0;    //variabile per uscire dal ciclo se non ci sono risposte
  SDI_DATA_HIGH();  // mi metto in input per campionare risposta
  while (SDI==0 && counter<MAX_SDI_COUNTER) counter++;
  
  if(SDI==1)				// Start bit?
  {
    newChar = 0;				
    halWaitUs(SDI_CLK_TIME/trim);       // Aspetto circa mezzo tempo di bit

    for (uint8 i=0x1; i<0x80; i <<= 1)	// Leggo i 7 bit del dato interessante
    {
      halWaitUs(SDI_CLK_TIME);
      uint8 noti = ~i;
      if (SDI==0)
        newChar |= i;
      else 
        newChar &= noti;
    }
    
    halWaitUs(SDI_CLK_TIME);	        //Lascio passare il bit di parita 
    halWaitUs(SDI_CLK_TIME);	        //Lascio passere il bit di Stop 
    buffer[ci]=newChar;                 //Salvo il carattere nel buffer
    ci++;                               //incremento indice buffer
    
  }
  else
  {
    newChar=0xDD;       //Se non ho ottenuto comunque risposta uso 0xDD come flag  
  }
  return newChar;
}


//FUNZIONE CHE INVIA I COMANDI E RICEVE LE RISPOSTE SU BUS SDI-12
bool readSdi(uint8 sdiAddress, byte *buffer,float trim){
   
   leggo=sdiAddress;
   c=0;
   sensAddr=sdiAddress + '0'; // � come se sommasse 0x30 al sensAddr
 
   switch (sensAddr)
   {
   // Florapulse
   case 0x30:
     markingSdi();
     sdiSendbyte(sensAddr);
     sdiSendbyte('M');
     sdiSendbyte('!');
     
     halWaitSec(2); // aspetta l'elaborazione 
     
     sdiSendbyte(sensAddr);
     sdiSendbyte('D');
     sdiSendbyte('0');
     sdiSendbyte('!');  
     SDI_DATA_HIGH();    
   break;
     
   // Acclima TDR  
   case 0x37:
   case 0x47:
   case 0x57:
   case 0x69:
   case 0x6A:
   case 0x6B:
   // Acclima TDT
   case 0x38:
   case 0x48:
   case 0x58: 
     markingSdi();
     sdiSendbyte(sensAddr);
     sdiSendbyte('M');
     sdiSendbyte('!');
     SDI_DATA_HIGH();
     
     halWaitSec(1); // aspetta l'elaborazione 
     c=0;
     
     //Leggi risposta
     markingSdi();
     sdiSendbyte(sensAddr);
     sdiSendbyte('D');
     sdiSendbyte('0');
     sdiSendbyte('!');  
     SDI_DATA_HIGH();   
   break;
   
   // Decagon 
   default:
     // Invia Comando
     markingSdi();
     sdiSendbyte(sensAddr);
     sdiSendbyte('R');
     sdiSendbyte('0');
     sdiSendbyte('!');
     SDI_DATA_HIGH();
   break;
   }
   
   c=0;
   
   chAq=0xEE; // inizializzo il carattero su uno valido e ricevo
   while(chAq!=0x0D && chAq!=0x00 && chAq!=0xDD){
        chAq=receiveChar(buffer,trim);
      //se diverso da quelli vietati , lo scrivo nel buffer
      if(chAq!=0x0D && chAq!=0x00 && chAq!=0xDD){
        buffer[c]=chAq;
        c++;
      }
      else{
        buffer[c]=0x0D;
      }
   }
   
   switch(sensAddr)
    {
    // Florapulse
    case 0x30:
      san=sdiSanityCheck(buffer,sensAddr);
    break;
     
    case 0x36:  // MPS checker 
     san=sdiSanityCheck(buffer,sensAddr);
    break;
   
   // Acclima TDR  
   case 0x37:
   case 0x47:
   case 0x57:
   case 0x69:
   case 0x6A:
   case 0x6B:
   // Acclima TDT
   case 0x38:
   case 0x48:
   case 0x58:   
     san=sdiSanityCheck(buffer,sensAddr);
    break;
   
    default:
       san=sdiSanityCheck(buffer,sensAddr);
    break;
   }
   
   return san;  
}

//FUNZIONE CHE INVIA I COMANDI E RICEVE LE RISPOSTE SU BUS SDI-12
bool readSentek(uint8 sdiAddress, byte *buffer1,byte *buffer2, byte *buffer3,byte *buffer4,float trim,uint8 nprobes,uint8 readType){
   
   // Genero l'indirizzo
   c=0;
   sensAddr=sdiAddress + '0';
   
   if(readType==SOIL){
       startMeasurement(sensAddr,0x00);
       getResponse(sensAddr,'0',buffer1,trim);

       if(nprobes>3)
         getResponse(sensAddr,'1',buffer2,trim);
       
       if(nprobes>6)
       getResponse(sensAddr,'2',buffer3,trim);
       
       if(nprobes>9){
          halWaitSec(1);
          halWaitms(500); 
          startMeasurement(sensAddr,0x01);
          getResponse(sensAddr,'0',buffer4,trim);
       }
   }
   else if(readType==TEMP)
   {
       startMeasurement(sensAddr,0x04);
       getResponse(sensAddr,'0',buffer1,trim);
   
       if(nprobes>3)
       getResponse(sensAddr,'1',buffer2,trim);
       
       if(nprobes>6)
       getResponse(sensAddr,'2',buffer3,trim);
       
       if(nprobes>9){
         halWaitSec(1);
         halWaitms(500); 
         startMeasurement(sensAddr,0x05);
         getResponse(sensAddr,'0',buffer4,trim);
       }
      
   }
   else if(readType==SAL)
   {
       startMeasurement(sensAddr,0x02);
       getResponse(sensAddr,'0',buffer1,trim);
       
       if(nprobes>3)
       getResponse(sensAddr,'1',buffer2,trim);
       
       if(nprobes>6)
       getResponse(sensAddr,'2',buffer3,trim);
       
       if(nprobes>9){
          halWaitSec(1);
          halWaitms(500);
          startMeasurement(sensAddr,0x03);
          getResponse(sensAddr,'0',buffer4,trim);
       }
   }
   
   //eseguo controllo per vedere se lettura buona
   san=sdiSanityCheckSentek(buffer1);  
   return san;  
}

/* FUNZIONE GLOBALE PER INTERROGARE IL SENSORE, RESTITUISCE IL NUMERO DI TENTATIVI*/
uint8 askSentek(uint8 sdiAddress,byte *buffer,byte *buffer2, byte *buffer3, byte *buffer4,float trim,uint8 nprobes,uint8 readType){
   
   SDI_CLK_TIME=LAST_VALID_SDI_CLK;
   sdi_retr=SDI_RETRIES;
   //Inizializzo il risultato a falso
   sdi_succ=0x00;
   sdi_succ=readSentek(sdiAddress,buffer,buffer2,buffer3,buffer4,trim,nprobes,readType);  
   SDI_CLK_TIME=LAST_VALID_SDI_CLK;
  
return sdi_succ;
}


/* FUNZIONE GLOBALE PER INTERROGARE IL SENSORE, RESTITUISCE IL NUMERO DI TENTATIVI*/
uint8 askSdi(uint8 sdiAddress,byte *buffer,float trim){
   
   SDI_CLK_TIME=LAST_VALID_SDI_CLK;
   sdi_retr=SDI_RETRIES;
   uint8 u=0;
   //Inizializzo il risultato a falso
   sdi_succ=0x00;
   sdi_succ=readSdi(sdiAddress,buffer,trim);  
   //asm("NOP");
   //ripeto "sdiRetries"-volte finche non scade o non leggo giusto
   while(!sdi_succ && sdi_retr>0x00){
      sdi_succ=readSdi(sdiAddress,buffer,trim);
      if(!sdi_succ){
        sdi_retr--;
        resetSdiBuffer(buffer);
        SDI_CLK_TIME+=30;
        LAST_VALID_SDI_CLK=SDI_CLK_TIME;
        if(LAST_VALID_SDI_CLK>=SDI_DEF_CLOCK+60)
        {
          LAST_VALID_SDI_CLK=SDI_DEF_CLOCK;
        }
      }
   }
   
   SDI_CLK_TIME=LAST_VALID_SDI_CLK;
   
   //asm("NOP");
   //costruisco il vettore coi byte
   for(u=0;u<SDI_BUFFER_LENGTH;u++)
   {
     if(buffer[u]<0x30 || buffer[u]==0xFF){
        buffer[u]=buffer[u];       //era buffer2
        sdiGrafic[u]=buffer[u]; ///Debug
        //asm("NOP");
     }
     else{
      sdiGrafic[u]=buffer[u]; ///Debug
      buffer[u]=buffer[u]-'0';
      
      //asm("NOP");
    }
         
   }
  
return sdi_retr;
}


uint16 finddot(byte *databuff,uint8 len)
{ 
 uint8 idn=0;
 bef=0;       //Contatore dei caratteri prima del punto
 aft=0;       //Contatore dei caratteri dopo il punto
 bool dot_found=false;
 
  for(idn=0;idn<len;idn++){
   
    //Se il carattere � diverso dal punto e ancora non ho trovato il punto
    if(databuff[idn]!=0x2E && dot_found==false && databuff[idn]!=0xFF)
    {
      bef++;      //Incremento il contatore BEFORE
    }
    
    //Se il carattere � il punto , segno che l'ho trovato
    else if(databuff[idn]==0x2E) dot_found=true;
    
    //Se il carattere � diverso dal punto e l'ho gia trovato 
    else if(databuff[idn]!=0x2E && dot_found==true && databuff[idn]!=0xFF)
    {
      aft++;      //Incremento il contatore AFTER
    }
  }
  asm("NOP");
  
  return BUILD_UINT16(aft,bef);
}



void startMeasurement(uint8 indirizzo,uint8 tipo)
{
   markingSdi();
   sdiSendbyte(indirizzo);
   sdiSendbyte('M');  // Soil Moisture --->M
   
   if(tipo==1) sdiSendbyte('1'); // Salinity ---> M2
   else if(tipo==2) sdiSendbyte('2'); // Salinity ---> M2
   else if(tipo==3) sdiSendbyte('3'); // Salinity ---> M2
   else if(tipo==4) sdiSendbyte('4'); //Temperature --->M4
   else if(tipo==5) sdiSendbyte('5'); //Temperature --->M4
   
   
   sdiSendbyte('!');
   SDI_DATA_HIGH();
   
   halWaitSec(3); // I sentek v1.0.4 hanno bisogno di 3 secondi, di default sarebbe 2
   halWaitms(200);
   c=0;
}

void getResponse(uint8 indirizzo,char index,byte *buffersdi,float trim){
  
   halWaitSec(1);
   halWaitms(500);
       
  //Leggi risposta
   markingSdi();
   sdiSendbyte(indirizzo);
   sdiSendbyte('D');
   sdiSendbyte(index);
   sdiSendbyte('!');  
   SDI_DATA_HIGH();
 
   c=0;
   
   chAq=0xEE; // inizializzo il carattero su uno valido e ricevo
   while(chAq!=0x0D && chAq!=0x00 && chAq!=0xDD){
        chAq=receiveChar(buffersdi,trim);
      //se diverso da quelli vietati , lo scrivo nel buffer
      if(chAq!=0x0D && chAq!=0x00 && chAq!=0xDD){
        buffersdi[c]=chAq;
        c++;
      }
      else{
        buffersdi[c]=0x0D;
      }
   }
   asm("NOP");
   halWaitms(500);
}




#endif