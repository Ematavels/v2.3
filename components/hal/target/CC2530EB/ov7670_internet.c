/******************************************************************************
*                                                                             *
*        **********                                                           *
*       ************                                                          *
*      ***        ***                                                         *
*     ***    ++    ***                                                        *
*     ***   +  +   ***                      CHIPCON                           *
*     ***   +                                                                 *
*     ***   +  +   ***                                                        *
*     ***    ++    ***                                                        *
*      ***        ***                                                         *
*       ************                                                          *
*        **********                                                           *
*                                                                             *
*******************************************************************************

Filename:     astronomic.c
Target:       cc2430
Author:       EFU
Revised:      1/3-2007
Revision:     1.1

Description:
Function implementations for common astronomic functions for use with the CC2430DB.

******************************************************************************/
#include "ov7670.h"
#include "ioCC2530.h"
#include "hal_timer.h"
#include "ZComDef.h"
#include "hal_i2c.h"
#include "hal_led.h"

#include "hal_led.h"

/* Includes -------------------------------------------------- ----------------*/
#include "OV7670.h"


// Address of Camera module (OV7670) for SCCB/I2C interface
uint8 ReadAddress=0x43;
uint8 WriteAddress=0x42;


/* Private typedef -------------------------------------------------- ---------*/
/* Private define -------------------------------------------------- ----------*/
/* Private macro -------------------------------------------------- -----------*/
/* Private variables -------------------------------------------------- -------*/

const uint8 Camera_REG[CHANGE_REG][2]=
{
	{0x3a, 0x04},
	{0x40, 0xd0},
	{0x12, 0x14},
	{0x32, 0x80},
	{0x17, 0x16},
	{0x18, 0x04},
	{0x19, 0x02},
	{0x1a, 0x7b},
	{0x03, 0x06},
	{0x0c, 0x00}, //0x00
	{0x3e, 0x00},
	{0x70, 0x00},
	{0x71, 0x00},
	{0x72, 0x11},
	{0x73, 0x00},
	{0xa2, 0x02},
	{0x11, 0x40},
	{0x7a, 0x20},
	{0x7b, 0x1c},
	{0x7c, 0x28},
	{0x7d, 0x3c},
	{0x7e, 0x55},
	{0x7f, 0x68},
	{0x80, 0x76},
	{0x81, 0x80},
	{0x82, 0x88},
	{0x83, 0x8f},
	{0x84, 0x96},
	{0x85, 0xa3},
	{0x86, 0xaf},
	{0x87, 0xc4},
	{0x88, 0xd7},
	{0x89, 0xe8},
	{0x13, 0xe0},
	{0x00, 0x00},
	{0x10, 0x00},
	{0x0d, 0x00},
	{0x14, 0x20},
	{0xa5, 0x05},
	{0xab, 0x07},
	{0x24, 0x75},
	{0x25, 0x63},
	{0x26, 0xA5},
	{0x9f, 0x78},
	{0xa0, 0x68},
	{0xa1, 0x03},
	{0xa6, 0xdf},
	{0xa7, 0xdf},
	{0xa8, 0xf0},
	{0xa9, 0x90},
	{0xaa, 0x94},
	{0x13, 0xe5},
	{0x0e, 0x61},
	{0x0f, 0x4b},
	{0x16, 0x02},
	{0x1e, 0x07},//0x07,
	{0x21, 0x02},
	{0x22, 0x91},
	{0x29, 0x07},
	{0x33, 0x0b},
	{0x35, 0x0b},
	{0x37, 0x1d},
	{0x38, 0x71},
	{0x39, 0x2a},
	{0x3c, 0x78},
	{0x4d, 0x40},
	{0x4e, 0x20},
	{0x69, 0x0c},
	{0x6b, 0x60},
	{0x74, 0x19},
	{0x8d, 0x4f},
	{0x8e, 0x00},
	{0x8f, 0x00},
	{0x90, 0x00},
	{0x91, 0x00},
	{0x92, 0x00},
	{0x96, 0x00},
	{0x9a, 0x80},
	{0xb0, 0x84},
	{0xb1, 0x0c},
	{0xb2, 0x0e},
	{0xb3, 0x82},
	{0xb8, 0x0a},
	{0x43, 0x14},
	{0x44, 0xf0},
	{0x45, 0x34},
	{0x46, 0x58},
	{0x47, 0x28},
	{0x48, 0x3a},
	{0x59, 0x88},
	{0x5a, 0x88},
	{0x5b, 0x44},
	{0x5c, 0x67},
	{0x5d, 0x49},
	{0x5e, 0x0e},
	{0x64, 0x04},
	{0x65, 0x20},
	{0x66, 0x05},
	{0x94, 0x04},
	{0x95, 0x08},
	{0x6c, 0x0a},
	{0x6d, 0x55},
	{0x6e, 0x11},
	{0x6f, 0x9f},
	{0x6a, 0x40},
	{0x01, 0x40},	//0x40
	{0x02, 0x40},
	{0x13, 0xe7},
	{0x15, 0x00},
	{0x4f, 0x80},
	{0x50, 0x80},
	{0x51, 0x00},
	{0x52, 0x22},
	{0x53, 0x5e},
	{0x54, 0x80},
	{0x58, 0x9e},	
	{0x41, 0x08},
	{0x3f, 0x00},
	{0x75, 0x05},
	{0x76, 0xe1},
	{0x4c, 0x00},
	{0x77, 0x01},
	{0x3d, 0xc2},	
	{0x4b, 0x09},
	{0xc9, 0x60},
	{0x41, 0x38},
	{0x56, 0x40},	
	{0x34, 0x11},
	{0x3b, 0x02},
	{0xa4, 0x89},
	{0x96, 0x00},
	{0x97, 0x30},
	{0x98, 0x20},
	{0x99, 0x30},
	{0x9a, 0x84},
	{0x9b, 0x29},
	{0x9c, 0x03},
	{0x9d, 0x4c},
	{0x9e, 0x3f},
	{0x78, 0x04},	
	{0x79, 0x01},
	{0xc8, 0xf0},
	{0x79, 0x0f},
	{0xc8, 0x00},
	{0x79, 0x10},
	{0xc8, 0x7e},
	{0x79, 0x0a},
	{0xc8, 0x80},
	{0x79, 0x0b},
	{0xc8, 0x01},
	{0x79, 0x0c},
	{0xc8, 0x0f},
	{0x79, 0x0d},
	{0xc8, 0x20},
	{0x79, 0x09},
	{0xc8, 0x80},
	{0x79, 0x02},
	{0xc8, 0xc0},
	{0x79, 0x03},
	{0xc8, 0x40},
	{0x79, 0x05},
	{0xc8, 0x30},
	{0x79, 0x26},
	{0x09, 0x01},	 //0x03
	{0x55, 0x00},
	{0x56, 0x40},	
	{0x3b, 0x42},	
};

const uint8 InitBuffer2[4][2]=
{
	{0x11, 0xc0},
	{0x12, 0x02},
	{0x70, 0xba},
	{0x71, 0x35},
};

/* Private function prototypes -----------------------------------------------*/
static void Camera_HW_Init(void);
static void SCCB_Start(void);
static void SCCB_Stop(void);
static void NoAck(void);
static uint8 OV7670_XCLK_Conf(void);
uint8 Ack;
/* Private functions -------------------------------------------------- -------*/


/**
  * @brief  Resets the OV7670 camera.
  * @param  None
  * @retval None
  */
void Camera_Reset(void)
{
	Camera_WriteReg(OV7670_COM7, 0x80);
}



/**
  * @brief 	Write data to specific register in Camera Module OV7670
  * @param	Address: address of register
  * 		Value: value of data to be written into register
  * @retval	ERROR, SUCCESS
 */
uint8 Camera_WriteReg(uint8 Address, uint8 Value)
{
	// Create start condition on SCCB/I2C interface
	SCCB_Start();
	// Write data (Address of slave device for Write) on SCCB/I2C interface
	if((SCCB_Write(WriteAddress)) == Error)
	{
		SCCB_Stop();			// Create stop condition on SCCB/I2C interface
		return(Error);			// Return error and cancel the communication
	}
	halWaitUs(100);
	// Write data (Address of register in Camera Module)on SCCB/I2C interface
	if((SCCB_Write(Address)) == Error)
	{
		SCCB_Stop();			// Create stop condition on SCCB/I2C interface
		return(Error);			// Return error and cancel the communication
	}
	// Write data (Data to write into register in Camera Module)on SCCB/I2C interface
	if((SCCB_Write(Value)) == Error)
	{
		SCCB_Stop();			// Create stop condition on SCCB/I2C interface
		return(Error);			// Return error and cancel the communication
	}
	// Create stop condition on SCCB/I2C interface
	SCCB_Stop();

	return(Success);
}



/**
  * @brief 	Read data form specific register in Camera Module OV7670
  * @param	Address: address of register in Camera Module
  * @retval	ReturnState.Data -> Return read data from register
  * 		ReturnState.State -> Return state of the transmission
 */
uint8 Camera_ReadReg(uint8 Address)
{
	uint8 Data=0x00;
        uint8 State=Error;

	// Create start condition on SCCB/I2C interface
	SCCB_Start();
	// Write data (Address of slave device for Write) on SCCB/I2C interface
	if(Error == (SCCB_Write(WriteAddress)))
	{
		SCCB_Stop();			// Create stop condition on SCCB/I2C interface
		return(Error);	// Return error and cancel the communication
	}
	halWaitUs(100);
	// Write data (Address of register in Camera Module)on SCCB/I2C interface
	if(Error == (SCCB_Write(Address)))
	{
		SCCB_Stop();			// Create stop condition on SCCB/I2C interface
		return(Error);	// Return error and cancel the communication
	}
	// Create stop condition on SCCB/I2C interface
	SCCB_Stop();
	// Delay for SCCB/I2C
	halWaitUs(100);
	// Create start condition on SCCB/I2C interface
	SCCB_Start();
	// Write data (Address of slave device for Read) on SCCB/I2C interface
	if(Error == (SCCB_Write(ReadAddress)))
	{
		SCCB_Stop();			// Create stop condition on SCCB/I2C interface
		return(Error);	// Return error and cancel the communication
	}
	halWaitUs(500);

	// Received data from Camera Module (SCCB/I2C)
	Data = SCCB_Read();
	// No acknowlage on SCCB/I2C interface
	NoAck();
	// Create stop condition on SCCB/I2C interface
	SCCB_Stop();
	// If everything is done correct return success
	State = SUCCESS;

        asm("NOP");
	return(State);
}


/**
  * @brief 	Read data form  SCCB/I2C interface
  * @param	None
  * @retval	Data: received data from SCCB/I2C interface
 */
uint8 SCCB_Read(void)
{
	uint8 Data, i;

	// Write to Data zero for correct return data
	Data = 0;
	// Configure SIO_D of SCCB/I2C interface as input for read
	DATA_HIGH();
	// Delay for  SCCB/I2C interface
	halWaitUs(500);
	// Read data from SCCBI/I2C interface
	for(i=8;i>0;i--)
	{
		halWaitUs(500);							// Delay for  SCCB/I2C
		smbClock(1);								// Clock high on SIO_C
		halWaitUs(500);
		Data = Data << 1;						// Rotate Data << 1
		if(SDA){Data = Data + 1;}		// Read SIO_D pin value
		smbClock(0);								// Clock low on SIO_C
		halWaitUs(500);
	}
	// Return received data from SCCBI/I2C interface
	return(Data);
}



/**
  * @brief  Write data on SCCB/I2C interface
  * @param  Data: data for write on SCCBI/I2C interface
  * @retval Ack: ERROR, SUCCESS
  */
uint8 SCCB_Write(uint8 Data)
{
	uint8 i;
	
	// Configure SIO_D of SCCB/I2C interface as output for write
	IO_DIR_PORT_PIN(DATA_PORT, DATA_PIN, IO_OUT);

	// Write data bit by bit on SCCB/I2C
	for(i=0;i<8;i++)
	{
		if((Data & 0x80) == 0x80)		// If bit in Data is high, write high on SCCB/I2C
		{
			DATA_OUT_HIGH();
		}
		else							// If bit in Data is low, write low on SCCB/I2C
		{
			DATA_LOW();
		}
		Data <<= 1;						// Rotate Data for write next bit
		halWaitUs(500);

		// Create clock pulse on SCCB/I2C
		// ___  On SIO_C pin (SCCB clock)
		//    \___
		smbClock(1);
		halWaitUs(500);
		smbClock(0);
		halWaitUs(500);
	}

	// Read acknowladge from Camera Module to confirm received data
	halWaitUs(100);
	// Configure SIO_D of SCCB/I2C interface as input for read
	DATA_HIGH();
	halWaitUs(500);
	smbClock(1);
	halWaitUs(500);
       // If acknowladge is OK return SUCCESS else if is incorrect return ERROR
	if(SDA){Ack = Error;}
	else {Ack = Success;}
	// Pulse on SCCB/I2C fall down from high
	smbClock(0);
	halWaitUs(500);
	// Configure SIO_D of SCCB/I2C interface back to output for write
	DATA_OUT_HIGH();

	return (Ack);
}



/**
  * @brief  Create on  SCCB/I2C interface "START" condition of transmission
  * 	             |-> Start
  * 			  ___|
  * 		SIO_D    \______
  * 			  ______
  * 		SIO_C       \___
  * @param  None
  * @retval None
  */
static void SCCB_Start(void)
{
	DATA_OUT_HIGH();		// Configure SIO_D of SCCB/I2C interface as output for write
	halWaitUs(500);

	smbClock(1);
	halWaitUs(500);

	DATA_LOW();
	halWaitUs(500);

	smbClock(0);
	halWaitUs(500);
}



/**
  * @brief  Create on  SCCB/I2C interface "STOP" condition of transmission
  * 	             |-> Stop
  * 				 |	 __
  * 		SIO_D ______/  \__
  * 			      ________
  * 		SIO_C ___/
  * @param  None
  * @retval None
  */
static void SCCB_Stop(void)
{
	DATA_OUT_HIGH();		// Configure SIO_D of SCCB/I2C interface as output for write
	DATA_LOW();
	halWaitUs(500);

	smbClock(1);
	halWaitUs(500);

	DATA_OUT_HIGH();
	halWaitUs(500);
}



/**
  * @brief  Create on SCCB/I2C interface "NoACK"
  * 	          _____
  * 		SIO_D 	   \___
  * 			  ___
  * 		SIO_C 	 \_____
  * @param  None
  * @retval None
  */
static void NoAck(void)
{
	DATA_OUT_HIGH();		// Configure SIO_D of SCCB/I2C interface as output for write
	DATA_OUT_HIGH();
	halWaitUs(500);

	smbClock(1);
	halWaitUs(500);

	smbClock(0);
	halWaitUs(500);

	DATA_LOW();
	halWaitUs(500);
}
