/******************************************************************************
*                                                                             *
*        **********                                                           *
*       ************                                                          *
*      ***        ***                                                         *
*     ***    ++    ***                                                        *
*     ***   +  +   ***                      CHIPCON                           *
*     ***   +                                                                 *
*     ***   +  +   ***                                                        *
*     ***    ++    ***                                                        *
*      ***        ***                                                         *
*       ************                                                          *
*        **********                                                           *
*                                                                             *
*******************************************************************************

Filename:     counter.c
Target:       cc2430
Author:       EFU
Revised:      1/3-2007
Revision:     1.1

Description:
Function implementations for common counter functions for use with the CC2430DB.

******************************************************************************/
#if defined (DS18B20)

#include "DS18B20.h"
#include "ioCC2530.h"
#include "hal_timer.h"
#include "ZComDef.h"
#include "hal_i2c.h"
#include "hal_led.h"
#include "OSAL_Timers.h"
#include "OSAL.h"

uint8 ds18b20_reset(void);
void ds18b20_writebit(uint8 bit);
uint8 ds18b20_readbit(void);
void ds18b20_writebyte(uint8 byte);
uint8 ds18b20_readbyte(void);

/*
 * ds18b20 init
 */

uint8 i;
uint8 ds18b20_reset() {

  //low for 480us

  DS18B20_DDR |= (1<<DS18B20_DQ); //output
  DS18B20_PORT &= ~ (1<<DS18B20_DQ); //low
  halWaitUs(480);

  //release line and wait for 60uS
  DS18B20_DDR &= ~(1<<DS18B20_DQ); //input
  halWaitUs(70);

  i=DS18B20_PIN;
   asm("NOP");
  //get value and wait 420us
  halWaitUs(420);

  //return the read value, 0=ok, 1=error
  return i;
}

/*
* write one bit
*/
void ds18b20_writebit(uint8 bit){

  //low for 1uS
  DS18B20_DDR |= (1<<DS18B20_DQ); //output
  DS18B20_PORT &= ~ (1<<DS18B20_DQ); // low
  halWaitUs(1);

  //if we want to write 1, release the line (if not will keep low)
  if(bit){
    DS18B20_DDR &= ~(1<<DS18B20_DQ); //input
  }
  //wait 60uS and release the line
  halWaitUs(60);
  DS18B20_DDR &= ~(1<<DS18B20_DQ); //input
}

/*
* read one bit
*/

uint8 ds18b20_readbit(void){
  
  uint8 bit=0;

  //low for 1uS
  DS18B20_DDR |= (1<<DS18B20_DQ); //output
  halWaitUs(1);
  //release line and wait for 14uS
  DS18B20_DDR &= ~(1<<DS18B20_DQ); //input
  halWaitUs(7);

  //read the value	
  if(DS18B20_PIN)
    bit=1;
  else 
    asm("NOP");

  //wait 45uS and return read value	
  halWaitUs(45);	
  return bit;
}

/*
* write one byte
*/
uint8 temp;
void ds18b20_writebyte(uint8 byte){
  
  uint8 i=8;
	
  while(i--){
    temp = byte&1;
    ds18b20_writebit(byte&1);
    byte >>= 1;
    temp=byte;
  }
}

/*
* read one byte
*/

uint8 n;
uint8 ds18b20_readbyte(void){
  uint8 i=8;
  while(i--){
    n >>= 1;
    n |= (ds18b20_readbit()<<7);
  }	
  return n;
}

/*
* get temperature
* 0xFFFF indica lettura errata
*/
uint16 ds18b20_gettemp() {
  uint8 temperature[2];
  uint16 retd = 0xFFFF; 
  
  ds18b20_reset(); //reset
  ds18b20_writebyte(DS18B20_CMD_SKIPROM); //skip ROM
  ds18b20_writebyte(DS18B20_CMD_CONVERTTEMP); //start temperature conversion

  halWaitms(800);
  
  ds18b20_reset(); //reset
  ds18b20_writebyte(DS18B20_CMD_SKIPROM); //skip ROM
  ds18b20_writebyte(DS18B20_CMD_RSCRATCHPAD); //read scratchpad

  //read 2 byte from scratchpad
  temperature[0] = ds18b20_readbyte();
  temperature[1] = ds18b20_readbyte();

  //convert the 12 bit value obtained
  retd = (( temperature[1] << 8 ) + temperature[0] );//* 0.0625;

  return retd;
}

#endif
