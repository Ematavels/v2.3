/******************************************************************************
*                                                                             *
*        **********                                                           *
*       ************                                                          *
*      ***        ***                                                         *
*     ***    ++    ***                                                        *
*     ***   +  +   ***                      CHIPCON                           *
*     ***   +                                                                 *
*     ***   +  +   ***                                                        *
*     ***    ++    ***                                                        *
*      ***        ***                                                         *
*       ************                                                          *
*        **********                                                           *
*                                                                             *
*******************************************************************************

Filename:     FloraParser.c
Target:       cc2430
Author:       EFU
Revised:      1/3-2007
Revision:     1.1
******************************************************************************/


#if defined FLORA_1

#include "SdiCore.h"
#include "FloraParser.h"
#include "ioCC2530.h"
#include "hal_types.h"
#include "hal_sleep.h"
#include "OSAL_Timers.h"
#include "mac_api.h"
#include "astronomic.h"


void FloraParser(byte *buffer2,int16 *resbuf);

uint16 findACCdot(byte *databuff,uint8 len);
void hexFloraMultiplier (byte *databuf,uint16 dotpos,uint8 TYPE,int16 *resbuf);

uint8 ipb_Flora;    //indice di buffFloraPars
//uint8 aft,bef;  //contatori caratteri prima e dopo il punto
uint8 split_Flora[2]; //vettore contenten prima e dopo da passare a hexmultiplier
uint16 somma_Flora;
bool negtemp_Flora=false;   //tiene traccia del segno "-" nella temperatura


uint16 d1_Flora;    //risultato posizione del punto nel primo dato (AAAAAVWC per Flora), unisce "bef" e "aft"
uint16 d2_Flora;    //risultato posizione del punto nel sAECondo dato (ATEMP per Flora)  unisce "bef" e "aft"
uint16 d3_Flora;    //risultato posizione del punto nel terzo dato (AEC per Flora) unisce "bef" e "aft"  


//uint8 pi;

uint8 buffFloraPars[6]={0xFF,0xFF,0xFF,0xFF,0xFF,0xFF}; // vettore degli indici posizioni "+" e "-"
uint8 dotn_Flora=0;
uint8 idn_Flora;    // indice per copiare valori nei vari data1_Flora,data2_Flora,data3_Flora

uint8 data1_Flora[5]={0xFF,0xFF,0xFF,0xFF,0xFF};      // AAAAAVWC  (1-80) � [0,01]
uint8 data2_Flora[5]={0xFF,0xFF,0xFF,0xFF,0xFF};      // ATEMP (-40 , +50) �C [0,1]
uint8 data3_Flora[8]={0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};      // AEC  (0 - 25000) ms/cm


uint16 test1_Flora;   //Variabili di appoggio per BUILD risultato finale misura
uint16 test2_Flora;
uint16 test3_Flora;


void FloraParser(byte *buffer2,int16 *resbuf)
{
  //pi=0; // indice del for e if
  uint8 u=0;
  ipb_Flora=0;
 
  
  //Inserire reset delle misure
  
  negtemp_Flora=false;
  //Ricavo i pivot dei dati ( le posizioni dei segni "+" e "-" della stringa
  for(u=0;u<SDI_BUFFER_LENGTH;u++)
  {
    
    //Se uguale a "+", "-" , o "CR" quando la posizione � diversa da 0 ( comincia sempre per CR)
    if(buffer2[u]==0x2B || buffer2[u]==0x2D || (buffer2[u]==0x0D && u!=0))
    {
      buffFloraPars[ipb_Flora]=u;     //Salvo le posizioni in un buffer per il parsing  
      ipb_Flora++;
      if(buffer2[u]==0x2D)negtemp_Flora=true;
    }
  }
  
  for(u=0;u<sizeof(data1_Flora);u++)
  {
    data1_Flora[u]=0xFF;
  }
  
  for(u=0;u<sizeof(data2_Flora);u++)
  {
    data2_Flora[u]=0xFF;
  }
  
  for(u=0;u<sizeof(data3_Flora);u++)
  {
    data3_Flora[u]=0xFF;
  }
  
  
  //Separo i 3 diversi valori in 2 buffer separati
  //data1_Flora=AVWC
  idn_Flora=0;
  for(u=buffFloraPars[0]+1;u<buffFloraPars[1];u++){
    data1_Flora[idn_Flora]=buffer2[u];
    idn_Flora++;
  }
  
  idn_Flora=0;
  //data2_Flora=TEMPERATURA
  for(u=buffFloraPars[1]+1;u<buffFloraPars[2];u++){
    data2_Flora[idn_Flora]=buffer2[u];
    idn_Flora++;
  }
  
  idn_Flora=0;
  //data3_Flora=PErmettivity
  for(u=buffFloraPars[2]+1;u<buffFloraPars[3];u++){
    data3_Flora[idn_Flora]=buffer2[u];
    idn_Flora++;
  }
  
  //Falsifico i risultati per test
  asm("NOP");
  
  // Trovo il punto  per i 3 buffer dati 
  // SWP - Water Potential 
  d1_Flora=finddot(data1_Flora,sizeof(data1_Flora));
  // Lo converto in un valore esadecimale buono per essere spedito via radio
  hexFloraMultiplier(data1_Flora,d1_Flora,FPOT,resbuf);
  
  // mv/V
  d2_Flora=finddot(data2_Flora,sizeof(data2_Flora));
  hexFloraMultiplier(data2_Flora,d2_Flora,FMV,resbuf);
  asm("NOP");
  
  // Resistence
  d3_Flora=finddot(data3_Flora,sizeof(data3_Flora));
  hexFloraMultiplier(data3_Flora,d3_Flora,FRES,resbuf);
  
}

  

void hexFloraMultiplier (byte *databuf,uint16 dotpos, uint8 TYPE,int16 *resbuf)
{
  
  //Spezzo il valore uint16 del contatore caratteri in prima e dopo.
  split_Flora[0] = (dotpos >> 8) & 0xff;  //Parte alta
  split_Flora[1] = dotpos & 0xff;         //Parte Bassa
  
  
  // 2:2  
  if(split_Flora[0]==0x02 && split_Flora [1]==0x02){
    test1_Flora=BUILD_UINT8(databuf[0],databuf[1]);
    test1_Flora=HextoBCD(test1_Flora);
    test2_Flora=BUILD_UINT8(databuf[3],databuf[4]);
    test2_Flora=HextoBCD(test2_Flora);
    test1_Flora=test1_Flora*0x64;
    somma_Flora=test1_Flora + test2_Flora;
    asm("NOP");
  }
  
  //2:1
  else if (split_Flora[0]==0x02 && split_Flora [1]==0x01)
  {
    test1_Flora=BUILD_UINT8(databuf[0],databuf[1]);
    test1_Flora=HextoBCD(test1_Flora);
    test2_Flora=BUILD_UINT8(databuf[3],0x00);
    test2_Flora=HextoBCD(test2_Flora);
    test1_Flora=test1_Flora*0x64;
    somma_Flora=test1_Flora + test2_Flora;
    asm("NOP");
  }
  
  //2:0
  else if (split_Flora[0]==0x02 && split_Flora [1]==0x00)
  {
    if(TYPE==FPOT || TYPE==FMV)
    {
    test1_Flora=BUILD_UINT8(databuf[0],databuf[1]);
    test1_Flora=HextoBCD(test1_Flora);
    test1_Flora=test1_Flora*0x64;
    test2_Flora=BUILD_UINT16(0x00,0x00);
    somma_Flora=test1_Flora+test2_Flora;
    }
    else
    {
    test1_Flora=BUILD_UINT8(databuf[0],databuf[1]);
    test1_Flora=HextoBCD(test1_Flora);
    somma_Flora=test1_Flora;
    }
  
  }
  
  //1:2
  else if(split_Flora[0]==0x01 && split_Flora [1]==0x02)
  {
    test1_Flora=BUILD_UINT8(0x00,databuf[0]);
    test1_Flora=HextoBCD(test1_Flora);
    test2_Flora=BUILD_UINT8(databuf[2],databuf[3]);
    test2_Flora=HextoBCD(test2_Flora);
    test1_Flora=test1_Flora*0x64;
    somma_Flora=test1_Flora + test2_Flora;
  }
  
  
  //1:1
  else if(split_Flora[0]==0x01 && split_Flora [1]==0x01)
  {
      test1_Flora=BUILD_UINT8( 0x00,databuf[0]);
      test1_Flora=HextoBCD(test1_Flora);
      test2_Flora=BUILD_UINT8(databuf[2],0x00);
      test2_Flora=HextoBCD(test2_Flora);
      test1_Flora=test1_Flora*0x64;
      somma_Flora=test1_Flora+test2_Flora;
  }
  
  //1:0
  else if(split_Flora[0]==0x01 && split_Flora [1]==0x00)
  {
    
    if(TYPE==FPOT || TYPE==FMV)
    {
      test1_Flora=BUILD_UINT8(0x00,databuf[0]);
      test1_Flora=HextoBCD(test1_Flora);
      test1_Flora=test1_Flora*0x64;
      somma_Flora=test1_Flora;
    }
    else
    {
      test1_Flora=BUILD_UINT8(0x00,databuf[0]);
      test1_Flora=HextoBCD(test1_Flora);
      somma_Flora=test1_Flora;
      //asm("NOP");
    }
  }
  
  //FUNZIONANO SOLO PER AEC
  //3:0
  else if (split_Flora[0]==0x03 && split_Flora[1]==0x00)
  {
    test1_Flora=BUILD_UINT8(0x00,databuf[0]);
    test1_Flora=HextoBCD(test1_Flora);
    test2_Flora=BUILD_UINT8(databuf[1],databuf[2]);
    test2_Flora=HextoBCD(test2_Flora);
    test1_Flora=test1_Flora*0x64;
    somma_Flora=test1_Flora + test2_Flora + test3_Flora;
  }
  
   //3:1
  else if (split_Flora[0]==0x03 && split_Flora[1]==0x01)
  {
    asm("NOP");
    somma_Flora=9999;
  }
  
  //4:0
  else if (split_Flora[0]==0x04 && split_Flora[1]==0x00)
  {
    test1_Flora=BUILD_UINT8(databuf[0],databuf[1]);
    test1_Flora=HextoBCD(test1_Flora);
    test2_Flora=BUILD_UINT8(databuf[2],databuf[3]);
    test2_Flora=HextoBCD(test2_Flora);
    test1_Flora=test1_Flora*0x64;
    somma_Flora=test1_Flora + test2_Flora;
  }
   //4:2
  else if (split_Flora[0]==0x04 && split_Flora[1]==0x02)
  {
    test1_Flora=BUILD_UINT8(databuf[0],databuf[1]);
    test1_Flora=HextoBCD(test1_Flora);
    test2_Flora=BUILD_UINT8(databuf[2],databuf[3]);
    test2_Flora=HextoBCD(test2_Flora);
    test1_Flora=test1_Flora*0x64;
    somma_Flora=test1_Flora + test2_Flora;
    asm("NOP");
  }
  //5:0
  else if(split_Flora[0]==0x05)
  {
    test1_Flora=BUILD_UINT8(databuf[1],databuf[2]);
    test1_Flora=HextoBCD(test1_Flora);
    test2_Flora=BUILD_UINT8(databuf[3],databuf[4]);
    test2_Flora=HextoBCD(test2_Flora);
    test1_Flora=test1_Flora*0x64;
    test3_Flora=BUILD_UINT8(0x00,databuf[0]);
    test3_Flora=HextoBCD(test3_Flora);
    test3_Flora=test3_Flora*0x2710;
    
    somma_Flora=test1_Flora + test2_Flora + test3_Flora;
  }
  
  
 if(TYPE==FPOT) 
 { 
   if(negtemp_Flora==false)
   resbuf[0]=somma_Flora;
   else
   {
    somma_Flora=-somma_Flora;
    resbuf[0]=somma_Flora;
   }
 }
 else if (TYPE==FMV)
 { 
   if(negtemp_Flora==false)
   resbuf[1]=somma_Flora;
   else
   {
    somma_Flora=-somma_Flora;
    resbuf[1]=somma_Flora;
   }
 }
 else if (TYPE==FRES) resbuf[2]=somma_Flora;
 
}


uint16 findFloradot(byte *databuff,uint8 len)
{ 
 uint8 idn=0;
 uint8 bef=0;       //Contatore dei caratteri prima del punto
 uint8 aft=0;       //Contatore dei caratteri dopo il punto
 bool dot_found=false;
 
  for(idn=0;idn<len;idn++){
   
    //Se il carattere � diverso dal punto e ancora non ho trovato il punto
    if(databuff[idn]!=0x2E && dot_found==false && databuff[idn]!=0xFF)
    {
      bef++;      //Incremento il contatore BEFORE
    }
    
    //Se il carattere � il punto , segno che l'ho trovato
    else if(databuff[idn]==0x2E) dot_found=true;
    
    //Se il carattere � diverso dal punto e l'ho gia trovato 
    else if(databuff[idn]!=0x2E && dot_found==true && databuff[idn]!=0xFF)
    {
      aft++;      //Incremento il contatore AFTER
    }
  }
  asm("NOP");
  
  return BUILD_UINT16(aft,bef);
}

#endif