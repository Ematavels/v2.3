/******************************************************************************
*                                                                             *
*        **********                                                           *
*       ************                                                          *
*      ***        ***                                                         *
*     ***    ++    ***                                                        *
*     ***   +  +   ***                      CHIPCON                           *
*     ***   +                                                                 *
*     ***   +  +   ***                                                        *
*     ***    ++    ***                                                        *
*      ***        ***                                                         *
*       ************                                                          *
*        **********                                                           *
*                                                                             *
*******************************************************************************

Filename:     sps30.h
Target:       cc2430
Author:       EFU
Revised:      1/3-2007
Revision:     1.1

Description:
Function declarations for common sps30 functions for use with the CC2430DB.

All functions defined here are implemented in sps30.c.

******************************************************************************/
#if defined SPS30

#ifdef __cplusplus
extern "C"
{
#endif


  /**************************************************************************************************
 * INCLUDES
 **************************************************************************************************/
#include "ZComDef.h"

/* General define */
  
// address I2C 0x69 
#define SPS30_ADDRESS 0x69 // Datasheet with A0,A1,A2 to GND 
#define SPS30_WRITEADDRESS     SPS30_ADDRESS<<1
#define SPS30_READADDRESS     (SPS30_ADDRESS<<1)|0x01 

#define SPS_CMD_START_MEASUREMENT 0x0010
#define SPS_CMD_START_MEASUREMENT_ARG 0x05
#define SPS_CMD_START_MEASUREMENT_IEEE_ARG 0x03  
#define SPS_CMD_START_UINT_CRC 0xF6
#define SPS_CMD_START_IEEE754_CRC 0xAC
#define SPS_CMD_STOP_MEASUREMENT 0x0104
#define SPS_CMD_READ_MEASUREMENT 0x0300
#define SPS_CMD_START_MANUAL_FAN_CLEANING 0x5607
#define SPS_CMD_READ_FW 0xD100
  
#define SPS30_MAX_SERIAL_LEN 32  
  
#define CRC8_POLYNOMIAL 0x31
#define CRC8_INIT 0xFF
#define CRC8_LEN 1
  
  
/******************************************************************************
* @fn  readSps30
*
* @brief
*      Initiates sequential read of sps30 content from the given address. The
*      read operation transfer _length_ bytes to _buffer_, starting at sps30
*      address _address_, ending at _adress_ + _length_.
*
* Parameters:
*
* @param  BYTE*    buffer
*	  Pointer to buffer for storing read values.
*	
* @param  WORD     address
*	  Start address for read operation. Valid range: 0 - 0xFFF.
*
* @param  UINT8    length
*	  Number of bytes to be read.
*
* @return BOOL
*         TRUE: Read operation successful.
*         FALSE: Read operation NOT successful.
*
******************************************************************************/
uint8 sensirion_common_generate_crc(const uint8* data, uint16 count);

extern uint8 readSps30Data(byte* buffer,uint8 fanClean);

bool readSps30(byte* buffer, uint16 address, uint8 length);

byte writeSps30(byte* buffer, uint16 address, int8 length);

#endif