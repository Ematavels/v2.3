/******************************************************************************
*                                                                             *
*        **********                                                           *
*       ************                                                          *
*      ***        ***                                                         *
*     ***    ++    ***                                                        *
*     ***   +  +   ***                      CHIPCON                           *
*     ***   +                                                                 *
*     ***   +  +   ***                                                        *
*     ***    ++    ***                                                        *
*      ***        ***                                                         *
*       ************                                                          *
*        **********                                                           *
*                                                                             *
*******************************************************************************

Filename:     eeprom.c
Target:       cc2430
Author:       EFU
Revised:      1/3-2007
Revision:     1.1

Description:
Function implementations for common EEPROM functions for use with the CC2430DB.

******************************************************************************/
#if defined (EPROM) || (EPROM512)

#include "eeprom.h"
#include "ioCC2530.h"
#include "hal_timer.h"
#include "ZComDef.h"
#include "hal_i2c.h"
#include "hal_led.h"
#include "OSAL_Timers.h"
#include "OSAL.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

uint8 ret;
byte writeEEPromBlock(byte* buffer, int8 length, word address);

/******************************************************************************
* See eeprom.h for a description of this function.
******************************************************************************/
bool readEEProm(byte* buffer, word address, uint8 length)
{ 
  
   byte i;
   byte transmitBuffer[3];

   // timer error avviato
   if (systemStart) 
    osal_start_timerEx(Peripheral_TaskID, I2C_ERROR_EVENT, I2C_ERROR_TIME);
   
   // init the retries
   i2cRetries=I2C_RETRIES;

   transmitBuffer[0] = EEPROM_WRITEADDRESS;
   transmitBuffer[1] = (byte) (address >> 8);
   transmitBuffer[2] = (byte) (address);

   smbStart();

   for(i = 0; i < 3; i++)
   {
     i2cAck=0; // init della variabile
     while ( (i2cAck==0) && (i2cRetries>0)) //Keep sending until ACK received
     {
      smbSendbyte(transmitBuffer[i]);
      halWaitUs(400); //Ritardo ATTINY
     }            
   }

   if (i2cRetries>0)
   {
     smbClock(0);
     halWaitUs(300); //Ritardo ATTINY
     DATA_HIGH();
     smbClock(1);

     ret=smbReceive(EEPROM_READADDRESS, length, buffer); // se > 0 allora ok
   }
   else { smbStop(); ret=0; } // se va male � bene chiudere la lettura con uno stop

   // stop timer error
   if (systemStart) 
    osal_stop_timerEx(Peripheral_TaskID,I2C_ERROR_EVENT);
    
   return ret;
}


/******************************************************************************
* See eeprom.h for a description of this function.
******************************************************************************/
byte writeEEProm(byte* buffer, word address, int8 length)
{
   uint8 i;
   uint8 currentPageLength;
   uint8 pageOffset;
   uint8 ret;

   // timer error avviato
    if (systemStart) 
     osal_start_timerEx(Peripheral_TaskID, I2C_ERROR_EVENT, I2C_ERROR_TIME);

      // init the retries
   i2cRetries=I2C_RETRIES;

   if(((dword)address + length) > (word)PROM_SIZE){
      // Trying to write to a higher address than max address
     return FALSE;
   }

  if( pageOffset = (address & (PAGE_SIZE-1)) ){
      // Write to first page if address is not 32 bytes aligned
      if( pageOffset + length > PAGE_SIZE ){
         currentPageLength = PAGE_SIZE - pageOffset;
      }
      else{
         currentPageLength = length;
      }

      ret=writeEEPromBlock(&buffer[0], currentPageLength, address);
      // Wait for internal write cycle to finish
      halWait(WAIT_FOR_WRITE_DELAY);
   }
   else{
      // Address is 32 bytes aligned
      pageOffset = 0;
      currentPageLength = 0;
   }

   for( i = currentPageLength; i < length; i += PAGE_SIZE ){
      if( i + PAGE_SIZE < length ) {
         currentPageLength = PAGE_SIZE;
      }
      else{
         currentPageLength = length - i;
      }
      ret=writeEEPromBlock(&buffer[i], currentPageLength, address + i);
      // Wait for internal write cycle to finish
      halWait(WAIT_FOR_WRITE_DELAY);
   }

   if (ret==0)
   {
     HalLedSet (HAL_LED_2, HAL_LED_MODE_ON);  // led rosso ON per segnalare malfunzionamento
     // se non scrivo correttamente le eeprom � un errore grave
     resetBoard();
   }

   // stop timer error
   if (systemStart) 
     osal_stop_timerEx(Peripheral_TaskID,I2C_ERROR_EVENT);
   
   return ret; // se > 0 ok
}


/******************************************************************************
* See eeprom.h for a description of this function.
* length must be less or equal 32 ( 128 )
******************************************************************************/
byte writeEEPromBlock(byte* buffer, int8 length, word address)
{
   uint8 i = 0;
   uint8 j;
   byte transmitBuffer[BUFFER_SIZE_EEPROM];

   transmitBuffer[i++] = EEPROM_WRITEADDRESS;
   transmitBuffer[i++] = (byte) (address >> 8);
   transmitBuffer[i++] = (byte) (address);

   for(j = 0; j < length; j++)
   {
      transmitBuffer[i++] = buffer[j];
   }

   if (smbSend(transmitBuffer, i) > 0 )
   ret=TRUE;
   else ret=FALSE;

  return ret; // se > 0 ok
}

void eraseSelectedEprom(word address, uint16 length)
{
  
  uint16 eraseLenght=length;
  byte eraseBuffer[PAGE_SIZE+1];
  uint16 timeOut=0x1FFE; // timeout per evitare di rimanere bloccati nel while in caso di malfunzionamenti
  
   // lo riempio di tutti 0xFF
   memset(eraseBuffer,0xFF,sizeof(eraseBuffer)); 
   
   while ( (eraseLenght!=0) && (timeOut!=0) ) 
   {
     
    if (eraseLenght<=PAGE_SIZE)
    {
      writeEEProm((uint8*)&eraseBuffer,address,eraseLenght);  
      eraseLenght=0;
      wait();
    }
    else
    {
 
     writeEEProm((uint8*)&eraseBuffer,address,PAGE_SIZE);
  
     address=address+PAGE_SIZE;
     eraseLenght=eraseLenght-PAGE_SIZE;
     wait();
   }
   
   timeOut--;
  }
}
#endif