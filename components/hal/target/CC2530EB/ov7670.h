/******************************************************************************
*                                                                             *
*        **********                                                           *
*       ************                                                          *
*      ***        ***                                                         *
*     ***    ++    ***                                                        *
*     ***   +  +   ***                      CHIPCON                           *
*     ***   +                                                                 *
*     ***   +  +   ***                                                        *
*     ***    ++    ***                                                        *
*      ***        ***                                                         *
*       ************                                                          *
*        **********                                                           *
*                                                                             *
*******************************************************************************

Filename:     astronomic.h
Target:       cc2430
Author:       EFU
Revised:      1/3-2007
Revision:     1.1

Description:
Function declarations for common astronomic functions for use with the CC2430DB.

All functions defined here are implemented in astronomic.c.

******************************************************************************/

#ifdef __cplusplus
extern "C"
{
#endif


  /**************************************************************************************************
 * INCLUDES
 **************************************************************************************************/
#include "ZComDef.h"
#ifndef OV7670_H_
#define OV7670_H_

// Address of Camera module (OV7670) for SCCB/I2C interface

#define OvWriteAddress   0x42
#define OvReadAddress    0x43


/* Exported function prototypes -----------------------------------------------*/

uint8 WriteReg(uint8 address,uint8 val);
uint8 ReadReg(byte address);

/* Exported functions -------------------------------------------------- -------*/

#endif /* OV7670_H_ */
