/******************************************************************************
*                                                                             *
*        **********                                                           *
*       ************                                                          *
*      ***        ***                                                         *
*     ***    ++    ***                                                        *
*     ***   +  +   ***                      CHIPCON                           *
*     ***   +                                                                 *
*     ***   +  +   ***                                                        *
*     ***    ++    ***                                                        *
*      ***        ***                                                         *
*       ************                                                          *
*        **********                                                           *
*                                                                             *
*******************************************************************************

Filename:     counter.c
Target:       cc2430
Author:       EFU
Revised:      1/3-2007
Revision:     1.1

Description:
Function implementations for common counter functions for use with the CC2430DB.

******************************************************************************/
#if defined (BMP180)

#include "bmp180.h"
#include "ioCC2530.h"
#include "hal_timer.h"
#include "ZComDef.h"
#include "hal_i2c.h"
#include "hal_led.h"
#include "OSAL_Timers.h"
#include "OSAL.h"

bool readBmp180( byte* buffer, byte address, uint8 length);
byte writeBmp180( uint16 buffer, byte address, int8 length);
uint8 retCount;

/******************************************************************************
* See counter.h for a description of this function.
******************************************************************************/
bool readBmpTemp( byte* buffer)
{
  // configuro la lettura della temparatura
  writeBmp180(0x2E,0xF4,1);
  halWait(5); // aspetto 4.5ms + margine
  // leggo dal registro 0xF6 e 0xF7 il risultato
  readBmp180(buffer,0xF6,2);  

  return 1;  
}

bool readBmpPress( byte* buffer)
{
  // configuro la lettura della temparatura
  writeBmp180((0x34+(oss<<6)),0xF4,1);
  halWait(5); // aspetto 25.5ms + margine
  // leggo dal registro 0xF6 e 0xF7 il risultato
  readBmp180(buffer,0xF6,3);  

  return 1;  
}


bool readBmp180( byte* buffer, byte address, uint8 length)
{
   byte i;
   byte transmitBuffer[2];
   
   // timer error avviato
  if (systemStart) 
   osal_start_timerEx(Peripheral_TaskID, I2C_ERROR_EVENT, I2C_ERROR_TIME);
 
   // init the retries
   i2cRetries=I2C_RETRIES;

   // imposto il buffer del counter scelto
   transmitBuffer[0] = BMP180_WRITEADDRESS;
   transmitBuffer[1] = address;

   for(i = 0; i < 2; i++)
   {
     i2cAck=0; // init della variabile
     while ( (i2cAck==0) && (i2cRetries>0)) //Keep sending until ACK received
     smbSendbyte(transmitBuffer[i]);
   }

   smbClock(0);
   wait();
   DATA_HIGH();
   smbClock(1);

   retCount=smbReceive(BMP180_READADDRESS, length, buffer); // se >0 Ok

   if (retCount==0) HalLedSet (HAL_LED_2, HAL_LED_MODE_ON);  // led rosso ON per segnalare malfunzionamento

   // stop timer error
  if (systemStart) 
   osal_stop_timerEx(Peripheral_TaskID,I2C_ERROR_EVENT);
   
   return retCount;
}


byte writeBmp180( uint16 buffer, byte address, int8 length)
{
   uint8 i = 0;
   byte transmitBuffer[10];

   // timer error avviato
  if (systemStart) 
   osal_start_timerEx(Peripheral_TaskID, I2C_ERROR_EVENT, I2C_ERROR_TIME);

   // init the retries
   i2cRetries=I2C_RETRIES;

   transmitBuffer[i++] = BMP180_WRITEADDRESS;
   transmitBuffer[i++] = (byte) (address);
   transmitBuffer[i++] = (uint8)buffer;
   transmitBuffer[i++] = buffer >>8;
   
    // scrivo sul counter scelto
   if (smbSend(transmitBuffer, i) > 0 )  retCount=TRUE;
    else retCount=FALSE;

  if (retCount==FALSE) HalLedSet (HAL_LED_2, HAL_LED_MODE_ON);  // led rosso ON per segnalare malfunzionamento

   // stop timer error
  if (systemStart) 
   osal_stop_timerEx(Peripheral_TaskID,I2C_ERROR_EVENT);

  return retCount;
}


  
/** SELECT -> 0: temperature | 1: pressure **/
/** return 0xFFF se legge male **/
int16 bmp180_read_param(uint8 SELECT)
 {
    uint8 bmp180_buff[22];
 
    /** VARIABILI **/
    int16   AC1,AC2,AC3,B1,B2,MB,MC,MD;
    uint16 AC4,AC5,AC6;
    int32  UT,UP,X1,X2,B5,T,B6,X3,B3,p; 
    uint32  B4,B7;  

    /** Read Calibration Data **/
    if (readBmp180( bmp180_buff, 0xAA, 22)==0) 
        return 0xFFFF; // se i2c non ok
     
    AC1 = bmp180_buff[0]<<8 | bmp180_buff[1];
    AC2 = bmp180_buff[2]<<8 | bmp180_buff[3];
    AC3 = bmp180_buff[4]<<8 | bmp180_buff[5];
    AC4 = bmp180_buff[6]<<8 | bmp180_buff[7];
    AC5 = bmp180_buff[8]<<8 | bmp180_buff[9];
    AC6 = bmp180_buff[10]<<8 | bmp180_buff[11];
    B1  = bmp180_buff[12]<<8 | bmp180_buff[13];
    B2  = bmp180_buff[14]<<8 | bmp180_buff[15];
    MB  = bmp180_buff[16]<<8 | bmp180_buff[17];
    MC  = bmp180_buff[18]<<8 | bmp180_buff[19];
    MD  = bmp180_buff[20]<<8 | bmp180_buff[21];

    /** Read Temperature Value **/
    if (readBmpTemp(bmp180_buff)==0)
      return 0xFFFF;  
    UT=bmp180_buff[0]<<8 | bmp180_buff[1];  
    
    /** Read Pressure Value **/ 
    if (readBmpPress(bmp180_buff)==0)
      return 0xFFFF; 
    UP=((uint32)bmp180_buff[0]<<16 | (uint32)bmp180_buff[1]<<8 | (uint32)bmp180_buff[2])>>8;

    /** Calculate True Temperature **/         
    X1=(UT-AC6)*AC5/32768;          
    X2=(int32)MC*2048/(X1+MD);          
    B5=X1+X2;       
    T= (B5+8)/16;
                    
    /** Calculate True pressure **/
            
    B6=B5-4000;    
    X1 = (B2*(B6*B6/4096))/2048;      
    X2 = AC2*B6/2048;             
    X3 = X1+X2;        
    B3 = (((int32)AC1*4+X3)+2)/4;   
    X1 = (int32)AC3*B6/8192;      
    X2 = (B1*(B6*B6/4096))/65536;     
    X3 = ((X1+X2)+2)/4;      
    B4 = (int32)AC4*(uint32)(X3+32768)/32768;    
    B7=  ((uint32)(UP-B3))*(50000);       
    if(B7<0x80000000)           
    {              
      p=(B7*2)/B4;       
    }  
    else {     
      p=(B7/B4)*2;       
    }        
    X1=(p/256)*(p/256);    
    X1=((int32)X1*3038)/65536;      
    X2=(-7357*p)/65536;      
    p=p+(X1+X2+3791)/16;
    
    /** Return Selection **/
    
    if(SELECT)
      return p/10;
     else return T;
            
 }
#endif
