/******************************************************************************
*                                                                             *
*        **********                                                           *
*       ************                                                          *
*      ***        ***                                                         *
*     ***    ++    ***                                                        *
*     ***   +  +   ***                      CHIPCON                           *
*     ***   +                                                                 *
*     ***   +  +   ***                                                        *
*     ***    ++    ***                                                        *
*      ***        ***                                                         *
*       ************                                                          *
*        **********                                                           *
*                                                                             *
*******************************************************************************

Filename:     astronomic.h
Target:       cc2430
Author:       EFU
Revised:      1/3-2007
Revision:     1.1

Description:
Function declarations for common astronomic functions for use with the CC2430DB.

All functions defined here are implemented in astronomic.c.

******************************************************************************/

#ifdef __cplusplus
extern "C"
{
#endif

#if defined (BMP180)

  /**************************************************************************************************
 * INCLUDES
 **************************************************************************************************/
#include "ZComDef.h"

/* General define */
  
#define BMP180_WRITEADDRESS  0xEE
#define BMP180_READADDRESS   0xEF
#define oss                  0x00
  
  extern uint8 retCount;
  
  extern bool readBmpPress( byte* buffer);

  extern bool readBmpTemp( byte* buffer);

  int16 bmp180_read_param(uint8 SELECT); /** SELECT -> 0: Temperature | 1: Pressure **/

#endif