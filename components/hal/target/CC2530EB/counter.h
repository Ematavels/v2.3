/******************************************************************************
*                                                                             *
*        **********                                                           *
*       ************                                                          *
*      ***        ***                                                         *
*     ***    ++    ***                                                        *
*     ***   +  +   ***                      CHIPCON                           *
*     ***   +                                                                 *
*     ***   +  +   ***                                                        *
*     ***    ++    ***                                                        *
*      ***        ***                                                         *
*       ************                                                          *
*        **********                                                           *
*                                                                             *
*******************************************************************************

Filename:     astronomic.h
Target:       cc2430
Author:       EFU
Revised:      1/3-2007
Revision:     1.1

Description:
Function declarations for common astronomic functions for use with the CC2430DB.

All functions defined here are implemented in astronomic.c.

******************************************************************************/

#ifdef __cplusplus
extern "C"
{
#endif

#if defined (COUNTER) || defined (PLUVIOMETRO)

  /**************************************************************************************************
 * INCLUDES
 **************************************************************************************************/
#include "ZComDef.h"

/* General define */

#define SELECT_COUNTER_WR     0xE0
#define SELECT_COUNTER_RD     0xE1

#define CHANNEL_0             0x04
#define CHANNEL_1             0x05

#define COUNTER_WRITEADDRESS   0xD6
#define COUNTER_READADDRESS    0xD7
#define COUNTER_PAGE_SIZE 8
#define COUNTER_SIZE (0x001F)
#define COUNTER_BUFFER_SIZE 34

extern byte countBuffer[9];

/******************************************************************************
* @fn  readCounter
*
* @brief
*      Initiates sequential read of astronomic content from the given address. The
*      read operation transfer _length_ bytes to _buffer_, starting at astronomic
*      address _address_, ending at _adress_ + _length_.
*
* Parameters:
*
* @param  BYTE*    buffer
*	  Pointer to buffer for storing read values.
*	
* @param  WORD     address
*	  Start address for read operation. Valid range: 0 - 0xFFF.
*
* @param  UINT8    length
*	  Number of bytes to be read.
*
* @return BOOL
*         TRUE: Read operation successful.
*         FALSE: Read operation NOT successful.
*
******************************************************************************/
extern bool readCounter( byte* buffer, byte address, uint8 length);

extern byte writeCounter( byte* buffer, byte address, int8 length);

extern bool selectCounter(uint8 countSel);

extern bool resetCounter(void);

#endif