/******************************************************************************
*                                                                             *
*        **********                                                           *
*       ************                                                          *
*      ***        ***                                                         *
*     ***    ++    ***                                                        *
*     ***   +  +   ***                      CHIPCON                           *
*     ***   +                                                                 *
*     ***   +  +   ***                                                        *
*     ***    ++    ***                                                        *
*      ***        ***                                                         *
*       ************                                                          *
*        **********                                                           *
*                                                                             *
*******************************************************************************

Filename:     astronomic.h
Target:       cc2430
Author:       EFU
Revised:      1/3-2007
Revision:     1.1

Description:
Function declarations for common astronomic functions for use with the CC2430DB.

All functions defined here are implemented in astronomic.c.

******************************************************************************/

#ifdef __cplusplus
extern "C"
{
#endif

#if (defined M41T11) || (defined M41T81)

  /**************************************************************************************************
 * INCLUDES
 **************************************************************************************************/
#include "ZComDef.h"

/* General define */
#if defined M41T11
#define ASTRONOMIC_WRITEADDRESS   0xD0
#define ASTRONOMIC_READADDRESS    0xD1
#define ASTRONOMIC_SIZE (0x0040)
#define ASTRO_BUFFER_SIZE 67
#endif

#if defined M41T81
#define ASTRONOMIC_WRITEADDRESS   0xD0
#define ASTRONOMIC_READADDRESS    0xD1
#define ASTRONOMIC_SIZE (0x0014)
#define ASTRO_BUFFER_SIZE 23
#endif

extern byte astroBuffer[9];

/******************************************************************************
* @fn  readAstronomic
*
* @brief
*      Initiates sequential read of astronomic content from the given address. The
*      read operation transfer _length_ bytes to _buffer_, starting at astronomic
*      address _address_, ending at _adress_ + _length_.
*
* Parameters:
*
* @param  BYTE*    buffer
*	  Pointer to buffer for storing read values.
*	
* @param  WORD     address
*	  Start address for read operation. Valid range: 0 - 0xFFF.
*
* @param  UINT8    length
*	  Number of bytes to be read.
*
* @return BOOL
*         TRUE: Read operation successful.
*         FALSE: Read operation NOT successful.
*
******************************************************************************/
extern uint8 readAstronomic(byte* buffer, byte address, uint8 length);

extern byte writeAstronomic(byte* buffer, byte address, int8 length);

extern uint8 HextoBCD(uint8 hex);

extern uint8 BcdToHex(uint8 bcd);

extern uint16 Hex16toBCD(uint16 hex);

extern bool setAstronomic(uint8 startBit, uint8 secondi, uint8 minuti,
                   uint8 ore, uint8 giorno, uint8 data, uint8 mese, uint8 anno );

extern void astroConv(byte* tempBuffer);

#if defined M41T81
extern bool setAstroInterrupt(uint8 secondi, uint8 minuti, uint8 ore, uint8 data, uint8 mese );

bool setWDInterrupt(uint8 secondi, uint8 minuti, uint8 ore, uint8 data, uint8 mese );
#endif

extern void checkOsc(void);

extern bool restoreTime(void);

extern uint8 calcDiffCent(uint8* decCent2,uint8* decCent1 );

#endif