/******************************************************************************
*                                                                             *
*        **********                                                           *
*       ************                                                          *
*      ***        ***                                                         *
*     ***    ++    ***                                                        *
*     ***   +  +   ***                      CHIPCON                           *
*     ***   +                                                                 *
*     ***   +  +   ***                                                        *
*     ***    ++    ***                                                        *
*      ***        ***                                                         *
*       ************                                                          *
*        **********                                                           *
*                                                                             *
*******************************************************************************

Filename:     astronomic.c
Target:       cc2430
Author:       EFU
Revised:      1/3-2007
Revision:     1.1

Description:
Function implementations for common astronomic functions for use with the CC2430DB.

******************************************************************************/
#if (defined M41T11) || (defined M41T81)

#include "astronomic.h"
#if defined WINETTX
#include "modem.h"
#endif
#include "ioCC2530.h"
#include "hal_timer.h"
#include "ZComDef.h"
#include "hal_i2c.h"
#include "hal_led.h"
#include "OSAL.h"

byte astroBuffer[9]; // buffer che contiene i valori convertiti
uint16 bcd;

uint8 calcDiffCent(uint8* decCent2,uint8* decCent1 )
{
  uint8 ReadEnd[1],ReadStart[1];
  uint8 diffDecCent;

  ReadEnd[0] = HextoBCD(decCent2[0]); // decimi e centesimi finali
  ReadStart[0] = HextoBCD(decCent1[0]); // decimi e centesimi iniziali

  if ( (ReadStart[0]==0) || (ReadEnd[0]>ReadStart[0]) ) diffDecCent=ReadEnd[0]-ReadStart[0] ;
  else if (ReadEnd[0]<=ReadStart[0]) // se i finali sono <= iniziali devo calcolare il tempo tra inziali e 100
  {
    ReadStart[0]=100-ReadStart[0];
    diffDecCent=ReadStart[0]+ReadEnd[0] ;
  }

  return diffDecCent;

}


bool restoreTime(void)
{
  uint8 clearBuff[1],retV;
  
  /* Ripristino HT */
  readAstronomic(clearBuff,0x0C,0x01); // leggo il valore registro
  clearBuff[0]=(clearBuff[0] & 0xBF);  // setto HT a "zero"
  retV=writeAstronomic(clearBuff,0x0C,1); // ripristino i registri abilitando HT
  halWait(10);
  clearBuff[0]=0x00;
  // setto a zero OF dopo evento restore
  writeAstronomic(clearBuff,0x0F,1); 
  
  return retV;
}


/******************************************************************************
* See astronomic.h for a description of this function.
******************************************************************************/
uint8 readAstronomic(byte* buffer, byte address, uint8 length)
{
   byte i;
   byte transmitBuffer[2];
   uint8 retAstro=0;
   
    // timer error avviato
   if (systemStart) 
     osal_start_timerEx(Peripheral_TaskID, I2C_ERROR_EVENT, I2C_ERROR_TIME);

   // init the retries
   i2cRetries=I2C_RETRIES;

   transmitBuffer[0] = ASTRONOMIC_WRITEADDRESS;
   transmitBuffer[1] = (byte) (address);

   smbStart();

   for(i = 0; i < 2; i++)
   {
     i2cAck=0; // init della variabile
     while ( (i2cAck==0) && (i2cRetries>0)) //Keep sending until ACK received
     {
      smbSendbyte(transmitBuffer[i]);
     }
   }

   if (i2cRetries>0)
   {
   smbClock(0);
   wait();
   DATA_HIGH();
   smbClock(1);

   retAstro=smbReceive(ASTRONOMIC_READADDRESS, length, buffer); // se >0 Ok
   }
   else 
   {
    smbStop();
    err=1;
    #if defined DEBUG
     HalLedSet (HAL_LED_2, HAL_LED_MODE_ON);  // led rosso ON per segnalare malfunzionamento
    #else
     resetBoard();
    #endif
   }
   
   if (length==0x08) // leggo la data e ora ( non leggo flag WD o Interrupt )
   {  
     // controllo anche che i valori siano sensati per evitare problemi di letture falsate
     if ( ( HextoBCD(buffer[0])>99) // centesimi
      || (HextoBCD(buffer[1])>59) // secondi
        || ( HextoBCD(buffer[2] & 0x7F)>59) // minuti
            || (HextoBCD(buffer[3])>23) // ore ( non maschero le ore per leggere il CEB/CB in modo da fare setAstro su richiesta reset )
              || (HextoBCD(buffer[4] & 0x07)>7) // giorno della settimana
                || (HextoBCD(buffer[4] & 0x07)==0) // giorno della settimana
                 || (HextoBCD(buffer[5] & 0x3F)>31) // giorno del mese
                   || (HextoBCD(buffer[5] & 0x3F)==0) // giorno del mese
                     || (HextoBCD(buffer[6] & 0x1F)>12) // mese
                       || (HextoBCD(buffer[6] & 0x1F)==0) ) // mese
                        
      {
      #if defined WINETTX
       if (firstOn!=TRUE) // se � il primo avvio dove non ha in memoria la data e ora ignoro perch� poi la setto
       {
         err=2;
         #if defined DEBUG
           HalLedSet (HAL_LED_2, HAL_LED_MODE_ON);  // led rosso ON per segnalare malfunzionamento
          #else
           resetBoard();
          #endif
       }
       else return 0xAA; // codice che mi dice ora non settata
      #else
       err=8;
       resetBoard();
      #endif
     }
   }
   
   // stop timer error
   if (systemStart) 
   osal_stop_timerEx(Peripheral_TaskID,I2C_ERROR_EVENT);
   
   return retAstro;
}

/******************************************************************************
* See astronomic.h for a description of this function.
* length must be less or equal 64
******************************************************************************/
byte writeAstronomic(byte* buffer, byte address, int8 length)
{
   uint8 i = 0;
   uint8 j;
   byte transmitBuffer[ASTRO_BUFFER_SIZE];
   uint8 retAstro=0;
   
   // timer error avviato
    if (systemStart) 
      osal_start_timerEx(Peripheral_TaskID, I2C_ERROR_EVENT, I2C_ERROR_TIME);

    // init the retries
    i2cRetries=I2C_RETRIES;

   transmitBuffer[i++] = ASTRONOMIC_WRITEADDRESS;
   transmitBuffer[i++] = (byte) (address);

   for(j = 0; j < length; j++)
   {
      transmitBuffer[i++] = buffer[j];
   }

  if (smbSend(transmitBuffer, i) > 0) retAstro=0x01;
  else 
  {
    err=3;
    #if defined DEBUG
     HalLedSet (HAL_LED_2, HAL_LED_MODE_ON);  // led rosso ON per segnalare malfunzionamento
    #else
     resetBoard();
    #endif
  }
  
  // stop timer error
   if (systemStart) 
    osal_stop_timerEx(Peripheral_TaskID,I2C_ERROR_EVENT);
   
  return retAstro;
}


/******************************************************************************
* @fn  setAstronomic
*
* @brief
*
*
* Parameters:
*
* @param  uint8   startBit
*         uint8   secondi
*	  uint8   minuti
******************************************************************************/
bool setAstronomic(uint8 startBit, uint8 secondi, uint8 minuti, uint8 ore,
                   uint8 giorno, uint8 data, uint8 mese, uint8 anno )
{
  uint8 writeBuffer[8];
  bool retVal=0;

#if defined M41T11
  writeBuffer[0]= startBit | secondi;
  writeBuffer[1]= minuti;
  writeBuffer[2]= ore;
  writeBuffer[3]= giorno;
  writeBuffer[4]= data;
  writeBuffer[5]= mese;
  writeBuffer[6]= anno;

  retVal=writeAstronomic(writeBuffer,0x00,7);
#endif

#if defined M41T81

  writeBuffer[0]= 0x00;  // decimi e centesimi di secondo ( ogni scrittura li rimette comunque a zero di default )
  writeBuffer[1]= startBit | secondi;
  writeBuffer[2]= minuti;
  writeBuffer[3]= ore;
  writeBuffer[4]= giorno;
  writeBuffer[5]= data;
  writeBuffer[6]= mese;
  writeBuffer[7]= anno;
 
  writeAstronomic(writeBuffer,0x00,8);
  
  // check oscillatore 
  checkOsc(); 
  
  retVal=readAstronomic(writeBuffer,0x00,0x08);  // verifica check
  
   // controllo anche che i valori siano sensati per evitare problemi di impostazioni errate
   if ( ( HextoBCD(writeBuffer[0])>99) // centesimi
      || (HextoBCD(writeBuffer[1])>59) // secondi
        || ( HextoBCD(writeBuffer[2] & 0x7F)>59) // minuti
          || (HextoBCD(writeBuffer[3] & 0x3F)>23) // ore
            || (HextoBCD(writeBuffer[4] & 0x07)>7) // giorno della settimana
              || ( (HextoBCD(writeBuffer[5] & 0x3F)>31) || (HextoBCD(writeBuffer[5] & 0x3F)==0) ) // giorno del mese
                || ( (HextoBCD(writeBuffer[6] & 0x1F)>12) || (HextoBCD(writeBuffer[6] & 0x1F)==0) ) ) // mese
  {
   err=4;
   #if defined DEBUG
     HalLedSet (HAL_LED_2, HAL_LED_MODE_ON);  // led rosso ON per segnalare malfunzionamento
    #else
     resetBoard();
    #endif
  }
   
#endif

  return retVal;

}

#if defined M41T81
bool setAstroInterrupt(uint8 secondi, uint8 minuti, uint8 ore, uint8 data, uint8 mese )
{
  bool retVal;
  uint8 writeBuffer[5];

  writeBuffer[0]= 0x20 | 0x80 | mese;  // set ABE A1IE e mese
  writeBuffer[1]= data;
  writeBuffer[2]= ore;
  writeBuffer[3]= minuti;
  writeBuffer[4]= secondi;
  
  retVal=writeAstronomic(writeBuffer,0x0A,5); // abilito allarme e ABE
  halWait(5); 
  readAstronomic(writeBuffer,0x0000,0x01);  // sblocco l'indice come da manuale 
  halWait(5); 
  // check oscillatore 
  checkOsc(); 
  
  readAstronomic(writeBuffer,0x0A,0x05);  // verifica check
   
   // controllo anche che i valori siano sensati per evitare problemi di impostazioni errate
   if ( ( HextoBCD(writeBuffer[4] & 0x7F)>59) // secondi
    || (HextoBCD(writeBuffer[3]& 0x7F)>59) // minuti
      || (HextoBCD(writeBuffer[2] & 0x3F)>23) // ore
         || ( (HextoBCD(writeBuffer[1] & 0x3F)>31) || (HextoBCD(writeBuffer[1] & 0x3F)==0x00) ) // giorno del mese
            || ((HextoBCD(writeBuffer[0] & 0x1F)>12) || (HextoBCD(writeBuffer[0] & 0x1F)==0x00)) ) // mese
      {
      
      err=4;
      #if defined DEBUG
       HalLedSet (HAL_LED_2, HAL_LED_MODE_ON);  // led rosso ON per segnalare malfunzionamento
      #else
        resetBoard(); 
       #endif   
      }

  return retVal;

}




void checkOsc(void)
{
  uint8 readBuf[1];
  uint8 stopBit[1];
  
  // check oscillator ok or fail
  readAstronomic(readBuf,0x0F,0x01); // leggo il valore registro
  if (readBuf[0] & 0x04)
  {
    // impostare lo STOP a 1 e poi a 0
    stopBit[0]=0x80;
    writeAstronomic(stopBit,0x01,0x01);
    halWait(5);
    stopBit[0]=0x00;
    writeAstronomic(stopBit,0x01,0x01);
    halWait(5);
    writeAstronomic(stopBit,0x0F,1); // setto a zero OF 
    err=5;
    #if defined DEBUG
     HalLedSet (HAL_LED_2, HAL_LED_MODE_ON);  // led rosso ON per segnalare malfunzionamento
    #else
     resetBoard();
    #endif
   // si potrebbe riavviare oscillatore ma ho sicuramente perso il sincronismo
  }
}

bool setWDInterrupt(uint8 secondi, uint8 minuti, uint8 ore, uint8 data, uint8 mese )
{
  bool retVal;
  uint8 writeBuffer[5];

  writeBuffer[0]= 0x80 | mese;  // set ABE A1IE e mese
  writeBuffer[1]= data;
  writeBuffer[2]= ore;
  writeBuffer[3]= minuti;
  writeBuffer[4]= secondi;

  retVal=writeAstronomic(writeBuffer,0x14,5); // abilito allarme
  // attivo l'allarme
  writeBuffer[0]=0x02;
  retVal=writeAstronomic(writeBuffer,0x13,1); // attivo interrupt allarme 
  readAstronomic(writeBuffer,0x0000,0x01);  // sblocco l'indice come da manuale
 
  return retVal;

}
#endif

/* Converte i valori letti dall'astronomico nel formato BCD */
void astroConv(byte* tempBuffer)
{

  astroBuffer[0] = HextoBCD(tempBuffer[0]); // decimi e centesimi
  astroBuffer[1] = HextoBCD(tempBuffer[1]);   // secondi
  astroBuffer[2] = HextoBCD(tempBuffer[2] & 0x7F); // minuti
  astroBuffer[3] = HextoBCD(tempBuffer[3] & 0x3F); // ore
  astroBuffer[4] = HextoBCD(tempBuffer[4] & 0x07); // giorno della settimana
  astroBuffer[5] = HextoBCD(tempBuffer[5] & 0x3F); // giorno del mese
  astroBuffer[6] = HextoBCD(tempBuffer[6] & 0x1F); // mese
  astroBuffer[7] = HextoBCD(tempBuffer[7]); // anno
  astroBuffer[8] = HextoBCD(tempBuffer[8]); // calibration etc..

  // controllo anche che i valori siano sensati per evitare problemi di letture falsate
  if  ( (astroBuffer[0]>99) // centesimi
       || (astroBuffer[1]>59) // secondi
         || (astroBuffer[2]>59) // minuti
           || (astroBuffer[3]>23) // ore
             || (astroBuffer[4]>7) // giorno della settimana
               || (astroBuffer[5]>31) // giorno del mese
                 || (astroBuffer[6]>12) ) // mese
                  { 
                   err=6;
                   #if defined DEBUG
                     HalLedSet (HAL_LED_2, HAL_LED_MODE_ON);  // led rosso ON per segnalare malfunzionamento
                    #else
                     resetBoard();
                    #endif
                  }
}


/**********************************************************************************************
* Convert Hex to BCD
**********************************************************************************************/
uint8 HextoBCD(uint8 hex)
{
  uint8 bcd;

  bcd  = hex & 0x0F;
  bcd += (hex >> 4) * 0x0A;
  return bcd;
}

uint16 Hex16toBCD(uint16 hex)
{

  bcd  = hex & 0x0F;
  bcd = bcd + (hex >> 4) * 0x0A;

  return bcd;
}

uint8 BcdToHex(uint8 bcd)
{
 uint8 hex,tempHex;

 tempHex=bcd/10; // calcolo le decine
 hex = BUILD_UINT8(tempHex,(bcd-(tempHex *10))); // costruisco con decine | unit�

 return hex;
}

#endif
 