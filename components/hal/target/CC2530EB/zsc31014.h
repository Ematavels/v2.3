/******************************************************************************
*                                                                             *
*        **********                                                           *
*       ************                                                          *
*      ***        ***                                                         *
*     ***    ++    ***                                                        *
*     ***   +  +   ***                      CHIPCON                           *
*     ***   +                                                                 *
*     ***   +  +   ***                                                        *
*     ***    ++    ***                                                        *
*      ***        ***                                                         *
*       ************                                                          *
*        **********                                                           *
*                                                                             *
*******************************************************************************

Filename:     zsc31014.h
Target:       cc2430
Author:       EFU
Revised:      1/3-2007
Revision:     1.1

Description:
Function declarations for common EEPROM functions for use with the CC2430DB.

All functions defined here are implemented in eeprom.c.

******************************************************************************/

#ifdef __cplusplus
extern "C"
{
#endif
  
#if defined (GUARDIAN)
  
/**************************************************************************************************
 * INCLUDES
 **************************************************************************************************/
#include "ZComDef.h"
  
/* General Define */
  
#define ZSC_WRITEADDRESS_1   0x4E  // 0x27 + 0=write  0x4E
#define ZSC_READADDRESS_1    0x4F  // 0x27 + 1=read   0x4F  // modificato da me
  
#define ZSC_WRITEADDRESS_2   0x50  // 0x28 + 0=write
#define ZSC_READADDRESS_2    0x51  // 0x28 + 1=read       // default address

#define PAGE_SIZE 19
#define PROM_SIZE (0x13)  
#define BUFFER_SIZE_ZSC 22 

#define WAIT_FOR_WRITE_DELAY 0x03
  
// command mode
#define COMMAND_MODE 0xA0
#define NORMAL_MODE 0x80

// 0x40 to 0x53 equivale a scrivere nel registro 0x00 to 0x13
  
// Define per la lettura dell'ADC  
// lettura del ponte
#define LIVELLI_ZSC 8192
#define GAIN 1.5
#define OFFSET 595
#define VREF 5250 // in mVolt cosi evito il float

// lettura della temperatura  
#define VREF_TEMP 20000
#define OFFSET_TEMP 5000
#define LIVELLI_ZSC_TEMP 2048

/******************************************************************************
* @fn  readZsc
*
* @brief
*      Initiates sequential read of EEPROM content from the given address. The
*      read operation transfer _length_ bytes to _buffer_, starting at EEPROM
*      address _address_, ending at _adress_ + _length_.
*
* Parameters:
*
* @param  BYTE*    buffer
*	  Pointer to buffer for storing read values.
*	
* @param  WORD     address
*	  Start address for read operation. Valid range: 0 - 0xFFF.
*
* @param  UINT8    length
*	  Number of bytes to be read.
*
* @return BOOL
*         TRUE: Read operation successful.
*         FALSE: Read operation NOT successful.
*
******************************************************************************/
extern void selectZsc(uint8 numbers);
  
extern uint8 readZsc(byte* buffer);

extern uint16 readAddressZsc(uint8 address);

extern byte writeAddressZsc(uint16 value, byte address);

extern uint8 zscWriteAddress,zscReadAddress;

extern byte startModeZsc(uint8 command);

extern uint16 readBridgeZsc(void);

extern uint16 readTempZsc(void);

extern void zscInit(uint8 setValue);

extern uint8 setZscParameters(void);

extern void setZscPhyAddress(void);

#endif