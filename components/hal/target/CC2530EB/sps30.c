/******************************************************************************
*                                                                             *
*        **********                                                           *
*       ************                                                          *
*      ***        ***                                                         *
*     ***    ++    ***                                                        *
*     ***   +  +   ***                      CHIPCON                           *
*     ***   +                                                                 *
*     ***   +  +   ***                                                        *
*     ***    ++    ***                                                        *
*      ***        ***                                                         *
*       ************                                                          *
*        **********                                                           *
*                                                                             *
*******************************************************************************

Filename:     sps30.c
Target:       cc2430
Author:       EFU
Revised:      1/3-2007
Revision:     1.1

Description:
Function implementations for common sps30_Winet functions for use with the CC2430DB.

******************************************************************************/

#if defined SPS30


#include "sps30.h"
#include "ioCC2530.h"
#include "hal_timer.h"
#include "ZComDef.h"
#include "hal_i2c.h"
#include "hal_led.h"
#include "hal_led.h"
#include "OSAL_Timers.h"

uint8 crc;
uint8 retG;

bool readSps30(byte* buffer, uint16 address, uint8 length)
{
   byte i;
   byte transmitBuffer[3];
   uint8 ret;

   // timer error avviato
   osal_start_timerEx(Peripheral_TaskID, I2C_ERROR_EVENT, I2C_ERROR_TIME);
      
   // init the retries
   i2cRetries=I2C_RETRIES;

   transmitBuffer[0] = SPS30_WRITEADDRESS;
   transmitBuffer[1] = (byte) (address >> 8);
   transmitBuffer[2] = (byte) (address);

   smbStart();
  
   for(i = 0; i < 3; i++)
   {
     i2cAck=0; // init della variabile
     while ( (i2cAck==0) && (i2cRetries>0)) //Keep sending until ACK received
     {
     smbSendbyte(transmitBuffer[i]);
     halWaitUs(400); // ritardo per attiny
     }
   }

   if (i2cRetries>0) {
   smbClock(0);
   wait();
   smbClock(1);
   DATA_HIGH();
   
   halWait(100);
   
   ret=smbReceive(SPS30_READADDRESS, length, buffer); // se >0 Ok
   }
   else { 
     smbStop(); 
   } // se va male � bene chiudere la lettura con uno stop
   
    // stop timer error
   osal_stop_timerEx(Peripheral_TaskID,I2C_ERROR_EVENT);
   
   return ret;
}

/******************************************************************************
* See sps30.h for a description of this function.
* length must be less or equal 64
******************************************************************************/
byte writeSps30(byte* buffer, uint16 address, int8 length)
{
   uint8 i = 0,ret;
   uint8 j;
   byte transmitBuffer[10];
   
   // timer error avviato
   osal_start_timerEx(Peripheral_TaskID, I2C_ERROR_EVENT, I2C_ERROR_TIME);

    // init the retries
    i2cRetries=I2C_RETRIES;

   transmitBuffer[i++] = SPS30_WRITEADDRESS;
   transmitBuffer[i++] = (byte) (address >> 8);
   transmitBuffer[i++] = (byte) (address);

   for(j = 0; j < length; j++)
   {
      transmitBuffer[i++] = buffer[j];
      halWaitUs(400); // ritardo per attiny
   }

  if (smbSend(transmitBuffer, i) > 0 )
  ret=TRUE;
  else ret=FALSE;

   // stop timer error
   osal_stop_timerEx(Peripheral_TaskID,I2C_ERROR_EVENT);

  return ret;
}


uint8 readSps30Data(byte* buffer,uint8 fanClean) {
   
    uint8 ret=0;
    uint8 arg[3] = {SPS_CMD_START_MEASUREMENT_ARG,0x00,SPS_CMD_START_UINT_CRC}; // start cmd with CRC calculdated
    
    retG = writeSps30(arg,SPS_CMD_START_MEASUREMENT,0x03);
    
    halWaitSec(25); // 25 secondi di aerosol
    
    ret = readSps30(buffer,SPS_CMD_READ_MEASUREMENT,30);
    
    if (fanClean)    
    {
     ret = writeSps30(arg,SPS_CMD_START_MANUAL_FAN_CLEANING,SPS30_MAX_SERIAL_LEN);
    
     halWaitSec(10);
    }
    
    return ret;
}

uint8 sensirion_common_generate_crc(const uint8* data, uint16 count) {
    uint16 current_byte;
    uint8 crc = CRC8_INIT;
    uint8 crc_bit;

    /* calculates 8-Bit checksum with given polynomial */
    for (current_byte = 0; current_byte < count; ++current_byte) {
        crc ^= (data[current_byte]);
        for (crc_bit = 8; crc_bit > 0; --crc_bit) {
            if (crc & 0x80)
                crc = (crc << 1) ^ CRC8_POLYNOMIAL;
            else
                crc = (crc << 1);
        }
    }
    return crc;
}

#endif