/******************************************************************************
*                                                                             *
*        **********                                                           *
*       ************                                                          *
*      ***        ***                                                         *
*     ***    ++    ***                                                        *
*     ***   +  +   ***                      CHIPCON                           *
*     ***   +                                                                 *
*     ***   +  +   ***                                                        *
*     ***    ++    ***                                                        *
*      ***        ***                                                         *
*       ************                                                          *
*        **********                                                           *
*                                                                             *
*******************************************************************************

Filename:     i2c.c
Target:       cc2430
Author:       EFU
Revised:      1/3-2007
Revision:     1.1
******************************************************************************/
#include "hal_i2c.h"
#include "ioCC2530.h"
#include "hal_types.h"
#include "hal_sleep.h"
#include "OSAL_Timers.h"
#include "OSAL.h"
#include "mac_api.h"
#include "SHT11_Davis.h"
#include "attiny85.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

uint8 err=0;
uint8 i2cRetries=I2C_RETRIES;  // retry per la spedizione o lettura su i2c
uint8 i2cAck=0;
uint8 Peripheral_TaskID;

#if defined SPI || defined COORDINATOR // buffer che uso sia nel modem che nel SD Card
char tempBuffer[205];  // utilizzato nella strtohex per memorizzare il valore convertito e nella SD CARD packet_lenght*2 + 1 terminatore
#endif

void Peripheral_Init( uint8 task_id )
{
  /* Register task ID */
  Peripheral_TaskID = task_id;
}


uint16 Peripheral_ProcessEvent( uint8 task_id, uint16 events )
{

if (events & I2C_ERROR_EVENT)
  {
    err=13;
    resetBoard();
    
    return events ^ I2C_ERROR_EVENT;
  }

return 0;
}



void resetBoard()
{
  
  asm("NOP");
  
   /* spengo la radio */
   #if defined HAL_PA_LNA
    OBSSEL2 &=~0xFF;   
    OBSSEL3 &=~0xFF;
    do { P2DIR|=0x01; P2_0 = 0x00;  } while (0);
    halWait(5);   
   #endif
    MAC_MlmeSetReq(MAC_RX_ON_WHEN_IDLE, 0x00);
    
  /* spengo le sezioni della scheda */
   DISABLE_SENSTH();  // disattiva sensori
   halWait(200);
   DISABLE_PERIPH();  // disattiva le periferiche
   halWait(200);
   HAL_TURN_OFF_LED1(); // spegni led  
   halWait(100);
   HAL_TURN_OFF_LED2(); // spegni led 
     
   halWaitSec(2); // aspetto 5 secondi
   HAL_SYSTEM_RESET(); // resetto 
}


/* I2C Init */
void initI2C()
{
      // Set port pins as inputs
    IO_DIR_PORT_PIN( CLK_PORT, CLK_PIN, IO_OUT );
    IO_DIR_PORT_PIN( DATA_PORT, DATA_PIN, IO_IN );

    // Set for general I/O operation
    IO_FUNC_PORT_PIN( CLK_PORT, CLK_PIN, IO_GIO );
    IO_FUNC_PORT_PIN( DATA_PORT, DATA_PIN, IO_GIO );

    // Set I/O mode for pull-up/pull-down
    IO_IMODE_PORT_PIN( CLK_PORT, CLK_PIN, IO_PUP );
    IO_IMODE_PORT_PIN( DATA_PORT, DATA_PIN, IO_PUP );

    // Set pins to pull-up
    IO_PUD_PORT( CLK_PORT, IO_PUP );
    IO_PUD_PORT( DATA_PORT, IO_PUP );
}

/******************************************************************************
* Internal function for I2C
******************************************************************************/
uint8 smbSend(byte *buffer, const uint8 n){
   
  uint8 i = 0;
  
  smbStart();

  for(i = 0; i < n; i++)
  {
     i2cAck=0; // init della variabile
     while ( (i2cAck==0) && (i2cRetries>0)) //Keep sending until ACK received
     {
       smbSendbyte(buffer[i]);
       if (i2cRetries!=I2C_RETRIES) // se devo rifare dei tentativi reinit lo start i2c
       {
        i=0;  // rimando tutta la sequenza
        smbStop();
        smbStart();
       }
    }
  }

   smbClock(0);
   smbClock(1);
   DATA_LOW();
   halWaitUs(20);
  
   smbStop();

   return i2cRetries; // se maggiore di zero ok altrimenti non � andato a buon fine
}




/******************************************************************************
* Internal function for I2C
******************************************************************************/
bool smbSendbyte(byte b){
   uint8 i;

   for (i = 0; i < 8; i++){
      smbWrite(b & 0x80);
      b = (b <<  1);
   }
    smbClock(0);
    DATA_HIGH(); // input per campionare l'ack
    smbClock(1);
    halWaitUs(200); // Ritardo ATTINY
  
    i2cAck = !SDA;
    DATA_LOW();
    if (i2cAck) i2cRetries=I2C_RETRIES;
    else i2cRetries--; // aggiorno i tentativi di retries
    halWaitUs(400);
   
    return i2cAck; //high = ACK received, else ACK not received
}


/******************************************************************************
* Internal function for I2C
* Function for writing bit to the data line. Setting the port as input
* make the SMBus go high because of the pull-up resistors.
*
******************************************************************************/
void smbWrite(bool value){
   smbClock(0);
   if(value){
      DATA_OUT_HIGH(); // scrivo 1
   }
   else{
      DATA_LOW();   // scrivo 0
   }
   smbClock(1);
}


/******************************************************************************
* Internal function for I2C
******************************************************************************/
uint8 smbReceive(byte address, uint16 length, byte *buffer)
{
   uint8 i;
   i2cAck=0; //init
   i2cRetries=I2C_RETRIES;   // init the retries

   smbStart();
 
   while ( (i2cAck==0) && (i2cRetries>0)) //Keep sending until ACK received
   smbSendbyte(address);

   smbClock(0);
   halWaitUs(300);// Ritardo ATTINY
 
   // (CLK is high and data is high)
   for(i = 0; i < length; i++){
      buffer[i] = smbReceivebyte();
      halWaitUs(500);
   }

   smbClock(1);
   halWaitUs(400);
 
   smbStop();

   return i2cRetries; // se maggiore di zero allora ok
}


/******************************************************************************
* Internal function for I2C
******************************************************************************/
byte smbReceivebyte(){

  int8 i;
  byte inData;

   DATA_HIGH(); // input per leggere i dati

   for(i = 0; i < 8 * 2 ; i++)
   {
      if(SCL == 1)
      { 	//clk high
         if(SDA) 	//read '1'
           inData = (inData<<1) | 0x01;
          else 
           inData = (inData<<1) & ~0x01;
         
         smbClock(0);
      }
      else smbClock(1); 
   }

   smbClock(0);

     if(readATTiny==FALSE) { DATA_LOW(); }
     else { asm("NOP"); }
     
   smbClock(1);
   wait();
   smbClock(0);

   /* originale senza attiny
   smbClock(0);
   DATA_LOW();
   smbClock(1);
   wait();
   smbClock(0);       */ 
   return inData;

}

/******************************************************************************
* Internal function for I2C
* If a negative edge is to follow, the pin is set as an output and driven low.
* If a positive edge is to follow, the pin is set as an input and the pull-up resistor
* is to drive the node high. This way the slave device can hold the node low if
* a longer setup time is desired.
*
******************************************************************************/
void smbClock(bool value)
{
   if(!value){
      IO_DIR_PORT_PIN( CLK_PORT, CLK_PIN, IO_OUT );
      SCL = 0;
   }
   else {
      IO_DIR_PORT_PIN( CLK_PORT, CLK_PIN, IO_OUT );
      SCL = 1;
   }
   
   if(newsht11)
     halWaitUs(20);
   else
     halWaitUs(4);
  
}

/******************************************************************************
* Internal function for I2C
* This function initiates SMBus communication. It makes sure that both the
* data and the clock of the SMBus are high. Then the data pin is set low
* while the clock is kept high. This initializes SMBus transfer.
*
******************************************************************************/
void smbStart(){

 #if defined HAL_PA_LNA
 if (P2_0!=0x00)  // ulteriore controllo del settaggio corretto x amplificato
 {
    OBSSEL2 &=~0xFF;   
    OBSSEL3 &=~0xFF;
    do { P2DIR|=0x01; P2_0 = 0x00;  } while (0);
    halWait(2); 
 }
 #endif
  
  while (!(SDA && SCL) )
  {
      DATA_OUT_HIGH();
      wait();
      smbClock(0);
      smbClock(1);
      wait();
   }//wait for Data and clk high
   DATA_LOW();
   halWaitUs(20);  // Ritardo ATTINY
   smbClock(0);
}

/******************************************************************************
* Internal function for I2C
* This function terminates SMBus communication. It makes sure that the data
* and clock of the SMBus are low and high, respectively. Then the data pin is
* set high while the clock is kept high. This terminates SMBus transfer.
*
******************************************************************************/
void smbStop(){
   while (! (!SDA && SCL))
   {
      DATA_LOW();
      wait();
      smbClock(0);
      smbClock(1);
      wait();
   }
   DATA_OUT_HIGH();
   smbClock(1);
   halWaitUs(200);
 
  smbReceivebyte(); // i2c bug
  
  #if defined PIOGGIA  || PRESS0 || PRESS2
   halWait(30); // ritardo per attiny deep sleep
  #else
   halWait(5); 
  #endif
  
}


/******************************************************************************
* Internal function for I2c
******************************************************************************/
void wait(){
   uint8 j = 0x01;
   while(j--);
}

//-----------------------------------------------------------------------------
// See hal_i2c.h for a description of this function.
//-----------------------------------------------------------------------------
void halWait(uint8 wait){
   uint32 largeWait;

   if(wait == 0)
   {return;}
   largeWait = ((uint16) (wait << 7));
   largeWait += 114*wait;


   largeWait = (largeWait >> CLKSPEED);
   while(largeWait--);

   return;
}

void halWaitUs(uint16 microSecs)
{
  while(microSecs--)
  {
    /* 3 NOPs == 1 usecs */
    asm("nop"); asm("nop"); asm("nop");
  }
}

void halWaitms(uint16 wait){

  while(wait--){
  halWait(1); 

  }
}

void halWaitSec(uint16 wait){

  while(wait--){
  halWait(250); halWait(250); halWait(250); halWait(250);

  }
}

#if defined SPI || defined COORDINATOR // buffer che uso sia nel modem che nel SD Card
void BytesToChar(uint8* buffer,uint16 lenghtBytes)
{
  uint8 k=0;
  char tempSupport[3]={0x00,0x00,0x00}; // init + terminatore
  memset(tempBuffer,0x00,sizeof(tempBuffer)); 
  
  for(uint8 i=0;i<lenghtBytes;i++)
  {
   tempSupport[0]=HI_UINT8(buffer[i]);
   tempSupport[1]=LO_UINT8(buffer[i]);
  
   if ((tempSupport[0]!=0x0A) && (tempSupport[0]!=0x0B) && (tempSupport[0]!=0x0C)
   && (tempSupport[0]!=0x0D) && (tempSupport[0]!=0x0E) && (tempSupport[0]!=0x0F))
   tempBuffer[k++]=tempSupport[0]+48;  // per avere il codice ascii del valore memorizzato
   else tempBuffer[k++]=tempSupport[0]+55;

   if ((tempSupport[1]!=0x0A) && (tempSupport[1]!=0x0B) && (tempSupport[1]!=0x0C)
   && (tempSupport[1]!=0x0D) && (tempSupport[1]!=0x0E) && (tempSupport[1]!=0x0F))
   tempBuffer[k++]=tempSupport[1]+48;  // per avere il codice ascii del valore memorizzato
   else tempBuffer[k++]=tempSupport[1]+55;
  }
}
#endif




