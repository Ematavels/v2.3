/******************************************************************************
*                                                                             *
*        **********                                                           *
*       ************                                                          *
*      ***        ***                                                         *
*     ***    ++    ***                                                        *
*     ***   +  +   ***                      CHIPCON                           *
*     ***   +                                                                 *
*     ***   +  +   ***                                                        *
*     ***    ++    ***                                                        *
*      ***        ***                                                         *
*       ************                                                          *
*        **********                                                           *
*                                                                             *
*******************************************************************************

Filename:     astronomic.c
Target:       cc2430
Author:       EFU
Revised:      1/3-2007
Revision:     1.1

Description:
Function implementations for common astronomic functions for use with the CC2430DB.

******************************************************************************/

#include "attiny85.h"
#include "ioCC2530.h"
#include "hal_timer.h"
#include "ZComDef.h"
#include "hal_i2c.h"
#include "hal_led.h"
#include "OSAL.h"

bool readATTiny=FALSE;

#if defined (PIOGGIA) || defined (VENTO) || defined (PRESS0) || defined (PRESS1) || defined (PRESS2) || defined (PRESS3)

/******************************************************************************
* See astronomic.h for a description of this function.
******************************************************************************/
uint8 readAttiny85(byte* buffer, byte address, uint8 length)
{
   uint8 ret=0;
   readATTiny=FALSE;
   
    // timer error avviato
   if (systemStart) 
     osal_start_timerEx(Peripheral_TaskID, I2C_ERROR_EVENT, I2C_ERROR_TIME);

   // init the retries
   i2cRetries=I2C_RETRIES;

   if (i2cRetries>0)
   {
      ret=smbReceive(ATTINY85_WIND_R, length, buffer); // se >0 Ok
   }
   
   // stop timer error
   if (systemStart) 
   osal_stop_timerEx(Peripheral_TaskID,I2C_ERROR_EVENT);
   
   readATTiny=FALSE;
   
   return ret;
}

/******************************************************************************
* See astronomic.h for a description of this function.
* length must be less or equal 64
******************************************************************************/
byte writeAttiny85(byte* buffer, byte address, int8 length)
{
   uint8 i = 0;
   uint8 j;
   byte transmitBuffer[10];
   uint8 ret=0;
   
   readATTiny=TRUE;
   
   // timer error avviato
    if (systemStart) 
      osal_start_timerEx(Peripheral_TaskID, I2C_ERROR_EVENT, I2C_ERROR_TIME);

    // init the retries
    i2cRetries=I2C_RETRIES;

   transmitBuffer[i++] = ATTINY85_WIND_W;
   transmitBuffer[i++] = (byte) (address);

   for(j = 0; j < length; j++)
   {
      transmitBuffer[i++] = buffer[j];
   }

  if (smbSend(transmitBuffer, i) > 0 ) ret=0x01;
  else 
  {
    HalLedSet (HAL_LED_2, HAL_LED_MODE_ON);  // led rosso ON per segnalare malfunzionamento
  }
  
  // stop timer error
   if (systemStart) 
    osal_stop_timerEx(Peripheral_TaskID,I2C_ERROR_EVENT);
  
   readATTiny=FALSE;
   
  return ret;
}




/******************************************************************************
* See astronomic.h for a description of this function.
******************************************************************************/
uint8 longReadAttiny85(byte devAddr,byte* buffer,byte byteAddr,uint8 length)
{
   uint8 ret=0;
   readATTiny=TRUE;
   uint8 indexB=0;
   
   // timer error avviato
   if (systemStart) 
     osal_start_timerEx(Peripheral_TaskID, I2C_ERROR_EVENT, I2C_ERROR_TIME);

   // init the retries
   i2cRetries=I2C_RETRIES;
   uint8 j=0;
 
    //Per il numero di byte richiesto, incremento il byteAddr ogni volta dopo
      //aver letto.
      for(j=0; j<length; j++){
      ret=smbReceiveLongAtTiny(devAddr, byteAddr, (uint8*)&buffer[indexB]); // se >0 Ok
      byteAddr++;
      indexB++;
      //Se sono richiesti pi� byte, inserisco un intervallo di tempo tra
      //una richiesta e l'altra
      if (j<length-1) halWaitms(30);
      }
    
      readATTiny=FALSE;
   
   // stop timer error
   if (systemStart) 
   osal_stop_timerEx(Peripheral_TaskID,I2C_ERROR_EVENT);
   
   if (ret==0) initI2C(); // init del bus
   
   return ret;
} 


/*******************************************************************
LETTURA ESTESA ATTINY85: sintassi-> [ 8 0 [ 9 r ]
Dove:
[ = START
8 = INDIRIZZO SCRITTURA ATTINY
0 = INDIRIZZO DEL BYTE DA LEGGERE
[ = SECONDO START 
9 = INDIRIZZO LETTURA ATTINY
r = RICHIESTA LETTURA
] = STOP
*****************************************************************/
uint8 smbReceiveLongAtTiny(byte addressDevice,byte addressByte, byte *buffer)
{
   i2cAck=0; //init
   
   smbStart();
   halWaitUs(300);
   
   while ( (i2cAck==0) && (i2cRetries>0)) //Keep sending until ACK received
     smbSendbyte(addressDevice-0x01);
 
   smbClock(0);
   //RICHIESTO PER ATTINY CON CLOCK 4us
   halWaitUs(300);
   i2cAck=0;
   
   while ( (i2cAck==0) && (i2cRetries>0))
   smbSendbyte(addressByte);
   
   smbClock(0);
   halWaitUs(300);
   
   i2cAck=0; //init
   
   smbStart();
   halWaitUs(650);
   while ( (i2cAck==0) && (i2cRetries>0)) //Keep sending until ACK received
   smbSendbyte(addressDevice);
   
   smbClock(0);
   halWaitUs(650); 
   
   // (CLK is high and data is high)
   for(uint8 i = 0; i < 1; i++){
      buffer[i] = smbReceivebyte();
      halWaitUs(550); 
   }
   
   smbClock(1);
   halWaitUs(800);
   smbStop();
   
   return i2cRetries; // se maggiore di zero allora ok
} 

#endif


