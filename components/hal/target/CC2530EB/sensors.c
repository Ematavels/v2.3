/******************************************************************************
*                                                                             *
*        **********                                                           *
*       ************                                                          *
*      ***        ***                                                         *
*     ***    ++    ***                                                        *
*     ***   +  +   ***                      CHIPCON                           *
*     ***   +                                                                 *
*     ***   +  +   ***                                                        *
*     ***    ++    ***                                                        *
*      ***        ***                                                         *
*       ************                                                          *
*        **********                                                           *
*                                                                             *
*******************************************************************************

Filename:     sensors.c
Target:       cc2430
Author:       EFU
Revised:      1/3-2007
Revision:     1.1


******************************************************************************/

#include "ioCC2530.h"
#include "hal_timer.h"
#include "ZComDef.h"
#include "hal_i2c.h"
#include "hal_led.h"
#include "hal_adc.h"
#include "SHT11_Davis.h"
#include "SHT21_Winet.h"
#include "astronomic.h"
#include "OSAL.h"
#include <string.h>

#if defined TEST_BOARD
 uint16 HP0,HP1,HP2,HP3,HP4,HP5,HP6,HP7;
#endif

#if defined SHT3X
#include "SHT3X.h"
#endif

#if defined DS18B20
 #include "DS18B20.h"
#endif

#if defined BMP180
 #include "bmp180.h"
#endif

#if defined WINETHP
#include "adc1115.h"
#endif

#if defined RS485
#include "rs485.h"
#endif

#include  "i2c_multiplexer.h"

#if defined (PIOGGIA) || defined (VENTO) || defined (PRESS0) || defined (PRESS1) || defined (PRESS2) || defined (PRESS3)
 #include "attiny85.h"
#endif

#if defined COORDINATOR
 #include "CoordFrane.h"
#endif

#if defined ROUTER
  #include "RouterFrane.h"
#endif

uint8 readSensor(uint32 sensorId);

#if defined (VENTO)
 uint8 vento[10];
 uint32 readSensor32;
 uint32 tempWind=0;
#endif
#if defined (PIOGGIA)
 uint8 pioggia[2];
#endif
#if defined PRESS0
 uint8 press0[]={0x00,0x00,0x00,0x00,0x00,0x00};
#endif
#if defined PRESS1
 uint8 press1[]={0x00,0x00,0x00,0x00,0x00,0x00};
#endif
#if defined PRESS2
 uint8 press2[]={0x00,0x00,0x00,0x00,0x00,0x00};
#endif
#if defined PRESS3
 uint8 press3[]={0x00,0x00,0x00,0x00,0x00,0x00};
#endif 
 
#if defined FLOW0
 uint8 flow0[8];
 uint32 totFlowTime=0;
#endif
#if defined FLOW1
 uint8 flow1[8];
 uint32 totFlowTime1=0;
#endif
#if defined WINETHP
   uint16 setAdcGainIn;
#endif
#if defined SHT3X
 uint8 readSensSht3x[6]={0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
#endif
#if defined SPS30
#include "SPS30.h"
uint8 readSpsVal[32];
uint16 fanClean=0;
#endif

#if !defined SENTEK_120
uint16 readSensorM[10]; // imposto 0xFFFF che indica lettura errata ( es.i2c... )
#else
uint16 readSensorM[13]; // imposto 0xFFFF che indica lettura errata ( es.i2c... )
#endif

uint8 numSensTot=0x00;

uint8 readSensor (uint32 sensorId)
{
 memset(readSensorM,0xFF,sizeof(readSensorM)); // init del buffer
 
 #if defined SHT1X || defined SHT1xBEN// SHT11
  uint8 readSens[3]={0xFF,0xFF,0xFF};
 #endif
 uint8 lenMeasure=0x01;

 switch(sensorId)
 {

  // WINETAQIN0 - PORTA 0 - Sentek Humidity 
    case 0x00000001:
    #if defined RS485
     if (saveMeasure1!=0xFFFF) readSensorM[0]=saveMeasure1; // frequenza primo convertitore
     saveMeasure1=0xFFFF;
    #elif defined SENTEK_120
      osal_memcpy16(readSensorM,resSentekHum,0x06); 
      osal_memcpy16(&readSensorM[0x06],resSentekHumB,0x06);
      lenMeasure=0x0C;
    #elif defined SENTEK_90
      osal_memcpy16(readSensorM,resSentekHum,0x06); 
      osal_memcpy16(&readSensorM[0x06],resSentekHumB,0x03);
      lenMeasure=0x09;
    #elif defined SENTEK_60
    osal_memcpy16(readSensorM,resSentekHum,0x06); 
    lenMeasure=0x06;
    #elif defined SENTEK_30
     osal_memcpy16(readSensorM,resSentekHum,0x03); 
     lenMeasure=0x03;
    #elif defined ACC_TDR
     // VWC - TEMP - PERM - EC - ECPORE
     osal_memcpy16(readSensorM,accResBuffer,0x05);  
     lenMeasure=0x05;
    #elif defined FLORA_1
     // SWP - mv/v - Resistence
     osal_memcpy16(readSensorM,floraResBuffer,0x03);  
     lenMeasure=0x03;
    #endif
    break;

    //WINETAQIN1 -PORTA 1
   case 0x00000002:
     #if defined RS485
      if (saveTemperature1!=0xFFFF) readSensorM[0]=saveTemperature1; // frequenza primo convertitore
      saveTemperature1=0xFFFF;
     #elif defined SENTEK_120
      osal_memcpy16(readSensorM,resSentekTemp,0x06); 
      osal_memcpy16(&readSensorM[0x06],resSentekTempB,0x06);
      lenMeasure=0x0C;
     #elif defined SENTEK_90
      osal_memcpy16(readSensorM,resSentekTemp,0x06); 
      osal_memcpy16(&readSensorM[0x06],resSentekTempB,0x03);
      lenMeasure=0x09;
      asm("NOP");
     #elif defined SENTEK_60
      osal_memcpy16(readSensorM,resSentekTemp,0x06);
      lenMeasure=0x06;
     #elif defined SENTEK_30
      osal_memcpy16(readSensorM,resSentekTemp,0x03);
      lenMeasure=0x03;
     #elif defined SENTEK_10A
      // VWC - TEMP
      osal_memcpy16(readSensorM,resSentek10A,0x02); 
      lenMeasure=0x02;
     #elif defined ACC_TDR_2
     // VWC - TEMP - PERM - EC - ECPORE
     osal_memcpy16(readSensorM,accResBuffer2,0x05);  
     lenMeasure=0x05;
     #endif
    break;

   //WINETAQIN2 - PORTA2
   case 0x00000004:
     #if defined RS485
        if (saveMeasure2!=0xFFFF) readSensorM[0]=saveMeasure2; // frequenza primo convertitore
        saveMeasure2=0xFFFF;
     #else
        readSensorM[0]=getAdcChannel(0x02);
     #endif
   break;

  //WINETAQIN3 - PORTA 3
  case 0x00000008:
    #if defined RS485
      if (saveTemperature2!=0xFFFF)  readSensorM[0]=saveTemperature2; // frequenza primo convertitore
      saveTemperature2=0xFFFF;
     #else    
      readSensorM[0]=getAdcChannel(0x03);
     #endif
  break;

  //WINETAQIN4 - PORTA 4
  case 0x00000010:
    readSensorM[0]=getAdcChannel(0x04);
  break;

   //WINETAQIN5 - PORTA 5
    case 0x00000020:
    #if defined VENTO
      readSensor32=BUILD_UINT32(vento[9],vento[8],vento[7],vento[6]);
      //tempWind=((uint16)(((vento[1]) & 0x00FF) + (((vento[0]) & 0x00FF) << 8)));  // BUILD_UINT16 TI
      //tempWind=((uint16)vento[0] << 8 | vento[1]);
      tempWind=BUILD_UINT32(vento[1],vento[0],0x00,0x00);
      
      if(tempWind > 0x00) 
      {
        readSensor32=readSensor32/tempWind;
        readSensorM[0]=(uint16)readSensor32;
      }
      else readSensorM[0]=0x00;
    #else
      readSensorM[0]=getAdcChannel(0x05);
    #endif
   break;

   
   //WINETAQIN6 - PORTA 6
   case 0x00000040:
   #if defined SENTEK_120 && defined SENTEK_120_SAL
      osal_memcpy16(readSensorM,resSentekSal,0x06); 
      osal_memcpy16(&readSensorM[0x06],resSentekSalB,0x06);
      lenMeasure=0x0C;
   #elif defined SENTEK_90 && defined SENTEK_90_SAL
    osal_memcpy16(readSensorM,resSentekSal,0x06); 
    osal_memcpy16(&readSensorM[0x06],resSentekSalB,0x03);
    lenMeasure=0x09;
   #elif defined SENTEK_60 && defined SENTEK_60_SAL
    osal_memcpy16(readSensorM,resSentekSal,0x06);
    lenMeasure=6;
   #elif  defined SENTEK_30 && defined SENTEK_30_SAL
     osal_memcpy16(readSensorM,resSentekSal,0x03);
     lenMeasure=3;
    #endif
     break;
     
     
 //ATTINY2 pluviomentro - porta 7
  case 0x00000080:
  #if defined PIOGGIA
   if ( (pioggia[0]!=0xFF) && (pioggia[1]!=0xFF) )
    {
      readSensorM[0]=BUILD_UINT16(pioggia[1],pioggia[0]);
    } 
  #endif
  break;

  //ATTINY1 anemometro velocit� e raffica che poi dopo sposto in readSensorM - porta 8
  case 0x00000100:
  #if defined VENTO
    readSensorM[0]=BUILD_UINT16(vento[1],vento[0]);
  #endif
  break;

   //WINETHPIN0 - PORTA 9
  case 0x00000200:
    #if defined WINETHP
      #if defined WHPIN0_1024
        setAdcGainIn=0xC6E0;
      #elif defined WHPIN0_2048
        setAdcGainIn=0xC4E0;
      #elif defined WHPIN0_4096
        setAdcGainIn=0xC2E0;
      #endif

      #if defined WHPIN0DIFF_2048
        setAdcGainIn=0x84E0;
      #elif defined WHPIN0DIFF_4096
        setAdcGainIn=0x82E0;
      #endif

      readSensorM[0]=readSensorsAdc1115(0x01,setAdcGainIn);
      
    
      #if defined TEST_BOARD
      HP0=readSensorM[0];
      #endif
      
    #endif
  break;

    //WINETHPIN1 - PORTA 10
  case 0x00000400:
   #if defined WINETHP
      #if defined WHPIN1_1024
        setAdcGainIn=0xD6E0;
      #elif defined WHPIN1_2048
        setAdcGainIn=0xD4E0;
      #elif defined WHPIN1_4096
        setAdcGainIn=0xD2E0;
      #endif

      readSensorM[0]=readSensorsAdc1115(0x01,setAdcGainIn);
       #if defined TEST_BOARD
      HP1=readSensorM[0];
      #endif
  #endif
  break;

     //WINETHPIN2 - INPUT2 o INPUT2 Differenziale - porta 11
    case 0x00000800:
     #if defined WINETHP
  
      #if defined WHPIN2_1024
        setAdcGainIn=0xE680;  // 128 SPS
      #elif defined WHPIN2_2048
        setAdcGainIn=0xE4E0;
      #elif defined WHPIN2_4096
        setAdcGainIn=0xE2E0;
      #endif

      #if defined WHPIN1DIFF_2048
        setAdcGainIn=0xB4E0;
      #elif defined WHPIN1DIFF_4096
        setAdcGainIn=0xB2E0;
      #endif
        
      readSensorM[0]=readSensorsAdc1115(0x01,setAdcGainIn);
      #if defined TEST_BOARD
      HP2=readSensorM[0];
      #endif
    #endif
    break;

   //WINETHPIN3 - IPNUT3 - - porta 12
   case 0x00001000:
     #if defined WINETHP
     
      #if defined WHPIN3_1024
        setAdcGainIn=0xF680;   // 128 SPS
      #elif defined WHPIN3_2048
        setAdcGainIn=0xF4E0;
      #elif defined WHPIN3_4096
        setAdcGainIn=0xF2E0;
      #endif
    readSensorM[0]=readSensorsAdc1115(0x01,setAdcGainIn);
     #if defined TEST_BOARD
      HP3=readSensorM[0];
      #endif
    #endif
    break;

    //WINETHPIN4 - INPUT4 o INPUT4 Differenziale ( Input0 ADC2 )
    case 0x00002000:
     #if defined WINETHP
         #if defined WHPIN4_2048
        setAdcGainIn=0xC4E0;
      #elif defined WHPIN4_4096
        setAdcGainIn=0xC2E0;
      #endif

      #if defined WHPIN2DIFF_2048
        setAdcGainIn=0x84E0;
      #elif defined WHPIN2DIFF_4096
        setAdcGainIn=0x82E0;
      #endif
     readSensorM[0]=readSensorsAdc1115(0x02,setAdcGainIn);
     #if defined TEST_BOARD
      HP4=readSensorM[0];
      #endif
    #endif
    break;

    //WINETHPIN5
    case 0x00004000:
     #if defined WINETHP
      #if defined WHPIN5_2048
        setAdcGainIn=0xD4E0;
      #endif
      #if defined WHPIN5_4096
        setAdcGainIn=0xD2E0;
      #endif
    readSensorM[0]=readSensorsAdc1115(0x02,setAdcGainIn);
     #if defined TEST_BOARD
      HP5=readSensorM[0];
     #endif
    #endif
    break;

   //WINETHPIN6
    case 0x00008000:
     #if defined WINETHP
         #if defined WHPIN6_2048
        setAdcGainIn=0xE4E0;
      #elif defined WHPIN6_4096
        setAdcGainIn=0xE2E0;
      #endif

      #if defined WHPIN3DIFF_2048
        setAdcGainIn=0xB4E0;
      #elif defined WHPIN3DIFF_4096
        setAdcGainIn=0xB2E0;
      #endif

    readSensorM[0]=readSensorsAdc1115(0x02,setAdcGainIn);
    #if defined TEST_BOARD
     HP6=readSensorM[0];
    #endif
   #endif
    break;

     //WINETHPIN7
    case 0x00010000: 
     #if defined WINETHP
      #if defined WHPIN7_2048
        setAdcGainIn=0xF4E0;
      #elif defined WHPIN7_4096
        setAdcGainIn=0xF2E0;
      #endif

    readSensorM[0]=readSensorsAdc1115(0x02,setAdcGainIn);
     #if defined TEST_BOARD
      HP7=readSensorM[0];
      #endif
    #endif
    break;

    
   // SENSORE I2C SHT11 Davis Temperatura/umidit� - porta 17
   case 0x00020000:
    #if defined SHT3X
    lenMeasure=2; 
    if (countSht3x[0]) 
    {  
     if (checkMux) multiplexerChannel(CH_1); 
     // SHT35
     sht3xResetI2c();
     readSht3x(readSensSht3x,MEASUE_TEMP_HUM,0x06); // leggo temperatura e umidit�
     asm("NOP");
     if ( (readSensSht3x[2]!=0xFF) && (readSensSht3x[5]!=0xFF) )
     readSensorM[0]=BUILD_UINT16(readSensSht3x[1],readSensSht3x[0]);
     readSensorM[1]=BUILD_UINT16(readSensSht3x[4],readSensSht3x[3]);
     memset(readSensSht3x,0xFF,sizeof(readSensSht3x));   
    }
    #elif defined SHT1X // SHT11
     sensorReset();
     checkSensor(readSens,ASK_TEMP);
     if (readSens[2]!=0xFF)
     readSensorM[0]=BUILD_UINT16(readSens[1],readSens[0]);
     checkSensor(readSens,ASK_HUM);
     if (readSens[2]!=0xFF)
     readSensorM[1]=BUILD_UINT16(readSens[1],readSens[0]);
     lenMeasure=2;
     // se legge 0xFFFF significa errore i2c
    #endif
    #if defined SHT3X
      if (checkMux) 
        multiplexerChannel(NO_CHANNEL); // Ripristino l'expander in standby�
    #endif
    break;

    // SENSORE I2C  Temperatura/Umidit� - porta 18
    case 0x00040000:
    #if defined SHT3X
    lenMeasure=2; 
    if (countSht3x[1]) 
    {  
     multiplexerChannel(CH_2); 
     // SHT35
     sht3xResetI2c();
     readSht3x(readSensSht3x,MEASUE_TEMP_HUM,0x06); // leggo temperatura e umidit�
     if ( (readSensSht3x[2]!=0xFF) && (readSensSht3x[5]!=0xFF) )
     readSensorM[0]=BUILD_UINT16(readSensSht3x[1],readSensSht3x[0]);
     readSensorM[1]=BUILD_UINT16(readSensSht3x[4],readSensSht3x[3]);
     memset(readSensSht3x,0xFF,sizeof(readSensSht3x));
    }
    #elif defined SHT1X // SHT11
    sensorReset();
     checkSensor(readSens,ASK_TEMP);
     if (readSens[2]!=0xFF)
     readSensorM[0]=BUILD_UINT16(readSens[1],readSens[0]);
     checkSensor(readSens,ASK_HUM);
     if (readSens[2]!=0xFF)
     readSensorM[1]=BUILD_UINT16(readSens[1],readSens[0]);
     lenMeasure=2;
     // se legge 0xFFFF significa errore i2c
    #endif
     #if defined SHT3X
      if (checkMux) 
        multiplexerChannel(NO_CHANNEL); // Ripristino l'expander in standby�
    #endif
    break;

    // SENSORE I2C SHT21 - PORTA 19
   case 0x00080000:
    #if defined SHT3X
    lenMeasure=2;
    if (countSht3x[2]) 
    {  
     multiplexerChannel(CH_3); 
     // SHT35
      sht3xResetI2c();
     readSht3x(readSensSht3x,MEASUE_TEMP_HUM,0x06); // leggo temperatura e umidit�
     if ( (readSensSht3x[2]!=0xFF) && (readSensSht3x[5]!=0xFF) )
     readSensorM[0]=BUILD_UINT16(readSensSht3x[1],readSensSht3x[0]);
     readSensorM[1]=BUILD_UINT16(readSensSht3x[4],readSensSht3x[3]);
     memset(readSensSht3x,0xFF,sizeof(readSensSht3x));
    }
    #elif defined SHT1X // SHT11
     sensorReset();
     checkSensor(readSens,ASK_TEMP);
     if (readSens[2]!=0xFF)
     readSensorM[0]=BUILD_UINT16(readSens[1],readSens[0]);
     checkSensor(readSens,ASK_HUM);
     if (readSens[2]!=0xFF)
     readSensorM[1]=BUILD_UINT16(readSens[1],readSens[0]);
     lenMeasure=2;
     // se legge 0xFFFF significa errore i2c
    #endif
     #if defined SHT3X
      if (checkMux) 
        multiplexerChannel(NO_CHANNEL); // Ripristino l'expander in standby�
    #endif
  break;

    // SENSORE I2C SHT21 - PORTA 20
   case 0x00100000:
    #if defined SHT3X
    lenMeasure=2;
    if (countSht3x[3]) 
    {  
     multiplexerChannel(CH_4); 
     // SHT35
      sht3xResetI2c();
     readSht3x(readSensSht3x,MEASUE_TEMP_HUM,0x06); // leggo temperatura e umidit�
     if ( (readSensSht3x[2]!=0xFF) && (readSensSht3x[5]!=0xFF) )
     readSensorM[0]=BUILD_UINT16(readSensSht3x[1],readSensSht3x[0]);
     readSensorM[1]=BUILD_UINT16(readSensSht3x[4],readSensSht3x[3]);
     memset(readSensSht3x,0xFF,sizeof(readSensSht3x));
    }
    #elif defined SHT1X // SHT11
     sensorReset();
     checkSensor(readSens,ASK_TEMP);
     if (readSens[2]!=0xFF)
     readSensorM[0]=BUILD_UINT16(readSens[1],readSens[0]);
     checkSensor(readSens,ASK_HUM);
     if (readSens[2]!=0xFF)
     readSensorM[1]=BUILD_UINT16(readSens[1],readSens[0]);
     lenMeasure=2;
     // se legge 0xFFFF significa errore i2c
    #endif
     #if defined SHT3X
      if (checkMux) 
        multiplexerChannel(NO_CHANNEL); // Ripristino l'expander in standby�
    #endif
   break;

    case 0x00200000: // raffica del vento porta 21
    #if defined VENTO
      // vento[2] � MSB , vento[3] LSB - Massimo valore possibile 2000 (0x07D0)
      if ( (vento[2]==0xFF) && (vento[3]==0xFF) ) readSensorM[0]=0x00; // no raffica
      else if ( vento[2]!=0x7F) readSensorM[0]=BUILD_UINT16(vento[3],vento[2]);
      // else 0xFFFF ovvero errore i2c 
    #endif
    break; 
    
    case 0x00400000: // direzione raffica del vento porta 22
    #if defined VENTO
      readSensorM[0]=BUILD_UINT16(vento[5],vento[4]);
    #endif 
    break; 
  
   // porta 23 - Sentek60_B
   case 0x00800000:  
   #if defined SENTEK_60_B
    osal_memcpy16(readSensorM,resSentekHumB,0x06); 
    lenMeasure=0x06; 
   #elif defined SENTEK_30_B
    osal_memcpy16(readSensorM,resSentekHumB,0x03); 
    lenMeasure=0x03; 
   #endif
   break;
    
    // PORTA 24  - Sentek60_B Temperatura
    case 0x01000000:
   #if defined SENTEK_60_B 
    osal_memcpy16(readSensorM,resSentekTempB,0x06); 
    lenMeasure=0x06; 
   #elif defined SENTEK_30_B
    osal_memcpy16(readSensorM,resSentekTempB,0x03); 
    lenMeasure=0x03; 
   #elif defined ACC_TDR_3
    // VWC - TEMP - PERM - EC - ECPORE
    osal_memcpy16(readSensorM,accResBuffer3,0x05);  
    lenMeasure=0x05;
   #endif
   break;
       
    // PORTA 25 
    case 0x02000000: 
    #if defined ACC_TDR_4
     // VWC - TEMP - PERM - EC - ECPORE
     osal_memcpy16(readSensorM,accResBuffer4,0x05);  
     lenMeasure=0x05;
    #elif defined SENTEK_60_B && defined SENTEK_60_SAL_B
    osal_memcpy16(readSensorM,resSentekSalB,0x06);
    lenMeasure=6;
    #elif defined SENTEK_30_B && defined SENTEK_30_B_SAL
     osal_memcpy16(readSensorM,resSentekSalB,0x03);
     lenMeasure=3;
    #endif
    break;
   
    // PORTA 26 - Decagon eC 5te
    case 0x04000000: 
     #if defined ACC_TDR_5
     // VWC - TEMP - PERM - EC - ECPORE
     osal_memcpy16(readSensorM,accResBuffer5,0x05);  
     lenMeasure=0x05;
    #elif defined SENTEK_30_C
     osal_memcpy16(readSensorM,resSentekHumC,0x03); 
     lenMeasure=0x03; 
    #elif defined SPS30
     lenMeasure=0x05; 
     if (fanClean>=650) {
       readSps30Data(readSpsVal,0x01); 
       fanClean=0;}
     else readSps30Data(readSpsVal,0x00); // mass concentration ug/m3 + typical particle size PM1.0, PM2.5, PM4 and PM10
     readSensorM[0]=BUILD_UINT16(readSpsVal[1],readSpsVal[0]);
     readSensorM[1]=BUILD_UINT16(readSpsVal[4],readSpsVal[3]);
     readSensorM[2]=BUILD_UINT16(readSpsVal[7],readSpsVal[6]);
     readSensorM[3]=BUILD_UINT16(readSpsVal[10],readSpsVal[9]);
     readSensorM[4]=BUILD_UINT16(readSpsVal[28],readSpsVal[27]);
     memset(readSpsVal,0x00,sizeof(readSpsVal));  
    #endif
    break;

    // PORTA 27 - VWC Decagon 5TE P0_5
    case 0x08000000: 
     #if defined BMP180
        readSensorM[0]=bmp180_read_param(1);  // ritorna 0xFFFF se non lo legge  
    #elif defined ACC_TDR_6
     // VWC - TEMP - PERM - EC - ECPORE
     osal_memcpy16(readSensorM,accResBuffer6,0x05);  
     lenMeasure=0x05;
    #elif defined SENTEK_30_C
     osal_memcpy16(readSensorM,resSentekTempC,0x03); 
     lenMeasure=0x03; 
    #endif
    break;
     
   case 0x10000000: // tempo pressostato
  #if defined PRESS0  // porta 28
   if (press0[0]==0xCC) // flusso rilevato ( sta irrigando )
     readSensorM[0]=0xF1F1; 
   else if (press0[0]==0xDD) // flusso non rilevato
   {
      if(press0[1]==0xFE && press0[2]==0xFE && press0[3]==0xFE && press0[4]==0xFE )// Se sono tutti 0xFE , non ci sono stati Flussi dopo ultima lettura
      {
        readSensorM[0]=0x00;
      }
      else 
      {  
        uint32 totFluxTime=BUILD_UINT32(press0[4],press0[3],press0[2],press0[1]);
        readSensorM[0]=totFluxTime/1000; // trasformo in secondi
      }
   }        
   #elif defined MPS6_P04
     readSensorM=resBuffer2[0]; // MPS6 TENSIOMETRIA
   #endif     
   break;  
   
   #if defined SHT1xBEN
    case 0x40000000: // PORTA 30 
     sensorReset();
     checkSensor(readSens,ASK_TEMP);
     if (readSens[2]!=0xFF)
     readSensorM[0]=BUILD_UINT16(readSens[1],readSens[0]);
    break;
     
    case 0x80000000: // PORTA 31 
     checkSensor(readSens,ASK_HUM);
     if (readSens[2]!=0xFF)
     readSensorM[0]=BUILD_UINT16(readSens[1],readSens[0]);
    break;
#endif

     
   case 0x20000000: // tempo pressostato 1 
  #if defined PRESS1  //--- // Porta 29
   if (press1[0]==0xCC) // flusso non rilevato
    readSensorM[0]=0xF1F1;
   else if (press1[0]==0xDD) // flusso rilevato
   {
    if(press1[1]==0xFE && press1[2]==0xFE && press1[3]==0xFE && press1[4]==0xFE)// Se sono tutti 0xFE , non ci sono stati Flussi dopo ultima lettura
    {
     readSensorM[0]=0x00;  
    }
    else 
    {
      uint32 totFluxTime=BUILD_UINT32(press1[4],press1[3],press1[2],press1[1]);
      readSensorM[0]=totFluxTime/1000; // trasformo in secondi
    }   
   }
   #elif defined MPS6_P04 
    readSensorM=resBuffer2[1]; // MPS6 TEMPERATURA
   #endif 
   break;
 
#if defined PRESS2
   case 0x40000000: // PORTA 30 - flussimetro tempo
     if (press2[0]==0xCC) // flusso non rilevato
     readSensorM[0]=0xF1F1;
   else if (press2[0]==0xDD) // flusso rilevato
   {
    if(press2[1]==0xFE && press2[2]==0xFE && press2[3]==0xFE && press2[4]==0xFE)// Se sono tutti 0xFE , non ci sono stati Flussi dopo ultima lettura
    {
      readSensorM[0]=0x00;
    }
    else 
    {
      uint32 totFluxTime=BUILD_UINT32(press2[4],press2[3],press2[2],press2[1]);
      readSensorM[0]=totFluxTime/1000; // trasformo in secondi
    }   
   }  
   #endif 
   break; 

  #if defined PRESS3  
   case 0x80000000: // PORTA 31 - flussimetro tempo
   if (press3[0]==0xCC) // flusso non rilevato
     readSensorM[0]=0xF1F1;
   else if (press3[0]==0xDD) // flusso rilevato
   {
    if(press3[1]==0xFE && press3[2]==0xFE && press3[3]==0xFE && press3[4]==0xFE)// Se sono tutti 0xFE , non ci sono stati Flussi dopo ultima lettura
    {
      readSensorM[0]=0x00;  
    }
    else 
    {
      uint32 totFluxTime=BUILD_UINT32(press3[4],press3[3],press3[2],press3[1]);
      readSensorM[0]=totFluxTime/1000; // trasformo in secondi
    }
   } 
   #endif 
   break; 
  
 default:
   break;

}

// prima lettura salvo il numero dei sensori
 if (firstStart)
    numSensTot+=lenMeasure;

  
 return lenMeasure;
}