/******************************************************************************
*                                                                             *
*        **********                                                           *
*       ************                                                          *
*      ***        ***                                                         *
*     ***    ++    ***                                                        *
*     ***   +  +   ***                      CHIPCON                           *
*     ***   +                                                                 *
*     ***   +  +   ***                                                        *
*     ***    ++    ***                                                        *
*      ***        ***                                                         *
*       ************                                                          *
*        **********                                                           *
*                                                                             *
*******************************************************************************

Filename:     eeprom.h
Target:       cc2430
Author:       EFU
Revised:      1/3-2007
Revision:     1.1

Description:
Function declarations for common EEPROM functions for use with the CC2430DB.

All functions defined here are implemented in eeprom.c.

******************************************************************************/

#ifdef __cplusplus
extern "C"
{
#endif

#if defined WINETTX

/**************************************************************************************************
 * INCLUDES
 **************************************************************************************************/
#include "hal_board.h"

extern uint8 Modem_TaskID;

extern void Modem_Init( uint8 task_id );
extern uint16 Modem_ProcessEvent( uint8 task_id, uint16 events );
extern void modemOnOff(uint8 setOnOff);
extern void saveDataOraGPRS(void);
extern void checkOperatore(void);

extern void rxCB( uint8 port, uint8 event );
extern uint16 lenghtData;
extern uint8 cmdSent;
extern uint8 sendModem;
extern bool firstOn;

/* socket settings */
#define MAX_SOCKET 10000  // massima porta da 3348 a 10000

/* size settings */
#if defined WEBCAM
#define SIZE_SAVE 13  // es. 120,120,1,0 = 11 + 2xterminatore 
#else
#define SIZE_SAVE 11  // es. 120,120,1 = 9 + 2xterminatore 
#endif

#define SIZE_DIM  5  // per il check Size

#if defined WEBCAM
 #define N_PARAMETRI_M 4 // numero di parametri ( fw 2.2.1 3 parametri con coordinatorMode )
#else
 #define N_PARAMETRI_M 3 // numero di parametri ( fw 2.2.1 3 parametri con coordinatorMode )
#endif


/* size settings per supporto conversione tempi */
#define SIZE_SUP  6 // 5 + terminatore

/* UART Settings */
#define UART_BAUD  HAL_UART_BR_115200
#define UART_THRESH  64
#define UART_RX_MAX  255
#define UART_TX_MAX  255
#define UART_IDLE  5

#define RX_CNT  UART_RX_MAX    // dimensione buffer ricezione modem - lo metto come max uart

#define NEXT_COMMAND  50    // tempo per avvio del prossimo comando 50ms
#define TIME_ERROR    25000 // tempo 25 secondi entro il quale il modem e server deve rispondere
#define TIME_OUT_GPRS 3000  // tempo 3 secondi per il retry della verifica GPRS 
#define SEND_TIME     4000  // 4000 se checkSize tempo per spedizione di un burst di dati - dinamico
#define TIME_CARRIER  14000 // tempo per connessione 
#define FAULT_LIMIT 10
#define N_RETRIES 30

#define MAX_BYTES  38           // massimo valore di bytes che posso leggere e memorizzare in una tx, prima del PUSH
                                // ( il massimo scambiabile tra terminale e modem � 80 caratteri ( datasheet modem 3-289) )
#define SIZE (MAX_BYTES*4)+1    // massimo valore di char convertiti in ascii nei vari buffer utilizzati + terminatore
#define MAX_CMD_LENGHT 20       // massimo valore della lunghezza del comando
#define MAX_PACKET_READ 15      // massimo numero di pacchetti consecutivi che carica con la SEND prima di fare il PUSH
                                // teoricamente il buffer contiene al massimo 1372 bytes e se arrivi a quel valore fa PUSH in automatico.
                                // ma voglio evitarlo altrimenti � difficile controllarlo.
                                // ogni pacchetto � grande (MAX_BYTES*2) = 1372/80=17 pacchetti

// eventi modem
#define NEXT_CMD_MODEM 0x0001
#define ERROR_EVENT    0x0002
#define DATA_RX        0x0004
#define RETRY_EVENT    0x0008

/******************************/

#endif //modem

