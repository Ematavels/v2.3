#include "SHT11_Davis.h"
#include "ioCC2530.h"
#include "hal_timer.h"
#include "ZComDef.h"
#include "hal_i2c.h"
#include "hal_sleep.h"

/* OS includes */
#include "OSAL.h"
#include "OSAL_Tasks.h"
#include "OSAL_PwrMgr.h"
#include "OSAL_Nv.h"

/* funzioni */
void sensorStart(void);
uint8 ThSend(byte *buffer, const uint8 n);

bool newsht11;

bool selectTH(uint8 select)
{
   byte selectBuffer[2];
   uint8 retCount;

   // init the retries
   i2cRetries=I2C_RETRIES;

   /* Scelgo il counter */
   selectBuffer[0] = SELECT_COUNTER_WR;

   if (select==0x00)  selectBuffer[1] = CHANNEL_0;
   else if (select==0x01) selectBuffer[1] = CHANNEL_1;
   else selectBuffer[1] = 0x00;

   if (smbSend(selectBuffer, 2) > 0 )   retCount=TRUE;
   else retCount=FALSE;

   halWait(25);

   return retCount;
}

bool checkSensor(byte* buffer, uint8 command)
{
   newsht11=true;
   byte i;
   i2cRetries=I2C_RETRIES;
   i2cAck=0; // init della variabile
  
   // timer error avviato
   osal_start_timerEx(Peripheral_TaskID, I2C_ERROR_EVENT, I2C_ERROR_TIME);
    
   sensorStart();
  
   while ( (i2cAck==0) && (i2cRetries>0)) //Keep sending until ACK received
   {
   smbSendbyte(command);
   halWaitUs(400); // ritardo per attiny
   }
 
   smbClock(0);
   halWaitUs(300);// Ritardo ATTINY
   DATA_HIGH(); // mi metto in ingresso per campionare il termine misura
   
   
   if (command==ASK_TEMP) { halWaitms(200); halWaitms(120); }
   else if (command==ASK_HUM) halWaitms(80);
   
   for(i = 0; i < 3; i++)
   {
    buffer[i] = smbReceivebyte();
    halWaitUs(400); //Ritardo ATTINY
   }
   
   halWaitms(20); // prima di fare la prossima lettura 
   
   // stop timer error
   osal_stop_timerEx(Peripheral_TaskID,I2C_ERROR_EVENT);
   newsht11=false;

   return TRUE;
}


bool writeRegister(uint8 cmdValue, uint8 command)
{
   byte transmitBuffer[2];
   uint8 ret;

   sensorStart();

   transmitBuffer[0] = command;
   transmitBuffer[1] = cmdValue;

  if (ThSend(transmitBuffer, 2) > 0 ) // se > 0 Ok
  ret=TRUE;
  else ret=FALSE;

  return ret;

}

void sensorStart()
{

  while (!(SDA && SCL) )
  {
      DATA_HIGH();
      wait();
      smbClock(0);
      smbClock(1);
      wait();
   }//wait for Data and clk high
 
   DATA_LOW();
   halWaitUs(20);  
   smbClock(0);
   smbClock(1);
   wait();
   DATA_HIGH();
   wait();
   smbClock(0); 
}

void sensorReset()
{
   newsht11=true;
   DATA_HIGH();
   smbClock(0);
   for (uint8 i=0;i<9;i++)
   {
    smbClock(1);
    wait();
    smbClock(0);
   }
   sensorStart();
   halWait(20);
   
   newsht11=false;
}


uint8 ThSend(byte *buffer, const uint8 n)
{
   uint8 i = 0;

   for(i = 0; i < n; i++)
   {
     i2cAck=0; // init della variabile
     while ( (i2cAck==0) && (i2cRetries>0)) //Keep sending until ACK received
     {
       smbSendbyte(buffer[i]);
     }
   }

   smbClock(0);
   smbClock(1);
   DATA_LOW();

   return i2cRetries; // se maggiore di zero ok altrimenti non � andato a buon fine
}
