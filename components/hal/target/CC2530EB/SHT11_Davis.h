#ifdef __cplusplus
extern "C"
{
#endif
  
  
  
/**************************************************************************************************
 * INCLUDES
 **************************************************************************************************/
#include "ZComDef.h"
  
  
/* General Define */
#define SELECT_COUNTER_WR     0xE0
#define SELECT_COUNTER_RD     0xE1

#define CHANNEL_0             0x04
#define CHANNEL_1             0x05
  
#define ASK_TEMP   0x03
#define ASK_HUM    0x05
#define ASK_REG    0x07
#define WRITE_REG  0x06
#define SOFT_RES   0x1E

/* Global Functions */

  
extern bool checkSensor(byte* buffer, uint8 command);

bool writeRegister(uint8 cmdValue, uint8 command);

extern uint8 ThSend(byte *buffer, const uint8 n);

extern void sensorReset(void);

extern bool newsht11;
  
extern bool selectTH(uint8 select);  