/******************************************************************************
*                                                                             *
*        **********                                                           *
*       ************                                                          *
*      ***        ***                                                         *
*     ***    ++    ***                                                        *
*     ***   +  +   ***                      CHIPCON                           *
*     ***   +                                                                 *
*     ***   +  +   ***                                                        *
*     ***    ++    ***                                                        *
*      ***        ***                                                         *
*       ************                                                          *
*        **********                                                           *
*                                                                             *
*******************************************************************************

Filename:     Decagon_GS3.c
Target:       cc2430
Author:       EFU
Revised:      1/3-2007
Revision:     1.1
******************************************************************************/


#include "SdiCore.h"
#include "ioCC2530.h"
#include "hal_types.h"
#include "hal_sleep.h"
#include "OSAL_Timers.h"
#include "mac_api.h"
#include "astronomic.h"


void sdiClock(bool);
void sdiSendbyte(byte);
void sdiWrite(bool);
void sdiStart(void);
void sdiStop(void);
void markingSdi(void);
uint8 receiveChar(byte* buffer,float trim);
void resetSdibuffer(void);
void processSdiResponse(void);
bool readSdi(uint8 sdiAddress,byte* buffer,float trim);
bool sdiSanityCheck(byte* buffer);
bool sdiSanityCheckMPS(byte* buffer);

uint8 askSdi(uint8 sdiAddress,byte *buffer,float trim);
void sdiParser(byte *buffer2,int16 *resbuf);
uint16 finddot(byte *databuff,uint8 len);
uint8 reverse_bit8(uint8 x);


uint8 sdiGrafic[30]={
0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,
0xFF,0xFF,0xFF,0xFF,0xFF
};


uint8 aft,bef;  //contatori caratteri prima e dopo il punto
uint8 newChar = 0; //char per ricezione carattere da seriale
uint8 sensAddr;   //Indirizzo Sensore
uint8 chAq=0xFF;  //Variabile per check carattere



uint16 counter=0;   //Variabile per uscire da ciclo in attesa di caratteri
//uint8 u=0;
uint8 san=0;      // risultato sanity check 
uint8 si=0;       //Sanity index, se ok = 0x03
uint8 sdi_retr;   //numero di retries at runtime
bool sdi_succ;    //risultato di readSdi

uint8 c;        //indici per buffer
uint8 ci=0;


//P0INP =0x20; 

 uint16 p=0;

 uint8 zi=0;
 uint8 ti=0;
 
 
/// CLOCK con deriva di 100 us
void sdiClock(bool value)
{
   if(!value){
      IO_DIR_PORT_PIN( SDI_PORT, SDI_PIN, IO_OUT );
      SDI = 0;
   }
   else {
      IO_DIR_PORT_PIN( SDI_PORT, SDI_PIN, IO_OUT );
      SDI = 1;
   }
   halWaitUs(710);
  
}


/* ADATTA UN BYTE AL FORMATO SDI-12 E LO INVIA SULLA LINEA */
void sdiSendbyte(byte b){
   uint8 i;
   p=0;
   b=reverse_bit8(b);          // inverto l'ordine dei bit per avere LSB first
   
   sdiStart();                 // Start Bit
   
   for (i = 0; i < 7; i++){    // Mando solo 7 bit di info
     //if(i==0) b=(b<<1);      // Mando solo 7 bit di inf
     sdiWrite(!(b & 0x80));    // Scrivo il bit
     if(!(b & 0x80)) p++;          // conto gli 1 per stima parit� (PARI)
     b = (b <<  1);
   }
   //asm("NOP");                             
   if(p%2==0) sdiWrite(0);     //Scrivo il bit di parit�
   else if(p%2!=0) sdiWrite(1);
   //asm("NOP");
   sdiStop();                  //Mando lo stop
   
}


void sdiWrite(bool value){
   
   if(value){
     SDI_DATA_OUT_HIGH(); // scrivo 1
     halWaitUs(SDI_CLK_TIME);
   }
   else{
      SDI_DATA_LOW();   // scrivo 0
      halWaitUs(SDI_CLK_TIME);
   }

}


/* START bit SDI */
void sdiStart(){
  SDI_DATA_OUT_HIGH();
  halWaitUs(SDI_CLK_TIME);
}

/* STOP bit SDI*/
void sdiStop(){
 SDI_DATA_LOW();
 halWaitUs(SDI_CLK_TIME);
 SDI_DATA_HIGH();
}


/* INVERTE UN BYTE IN LSB-FIRST */
uint8 reverse_bit8(uint8 x){
  x = ((x & 0x55) << 1) | ((x & 0xAA) >> 1);
  x = ((x & 0x33) << 2) | ((x & 0xCC) >> 2);
  return (x << 4) | (x >> 4);
}


/* MARKING SDI RICHIESTO PER INIZIO COMUNICAZIONE */
void markingSdi(){
 SDI_DATA_LOW();  ///////
 halWaitUs(745);
 SDI_DATA_OUT_HIGH();
 halWaitms(15);    //12 ms ma non preciso
 halWaitUs(400);
 SDI_DATA_LOW();
 halWaitms(11);     //8 ms ma non preciso            
}


/* RICEVE LA RISPOSTA DEL SENSORE CARATTERE PER CARATTERE */


/* SVUOTO IL BUFFER */
void resetSdiBuffer(byte *buffer){
   for(uint8 k=0;k<SDI_BUFFER_LENGTH;k++) buffer[k]=0xFF; 
}





/* CONTROLLO SE LA RISPOSTA � VALIDA ( DEVE CONTENERE ALMENO 3 TRA "+" E "-")*/

bool sdiSanityCheck(byte *buffer){
uint8 u=0;
uint8 si=0;

while(u<SDI_BUFFER_LENGTH && buffer[u]!=0xFF){
  if(buffer[u]==0x2B || buffer[u]==0x2D)
    si++;
  
    u++;
}

if(si==3) return true;
else return false;

}


/* CONTROLLO SANITA PER MPS6 */
bool sdiSanityCheckMPS(byte *buffer){
uint8 u=0;
uint8 si=0;

while(u<SDI_BUFFER_LENGTH && buffer[u]!=0xFF){
  if(buffer[u]==0x2B || buffer[u]==0x2D)
    si++;
  
    u++;
}

if(si==2) return true;
else return false;

}



//RICEVE LA RISPOSTA DEL SENSORE CARATTERE PER CARATTERE 
uint8 receiveChar(byte *buffer,float trim)
{ 
  ci=0; // inizializzo la posizione del vettore sul primo byte
  counter=0;    //variabile per uscire dal ciclo se non ci sono risposte
  SDI_DATA_HIGH();  // mi metto in input per campionare risposta
  while(SDI==0 && counter<8000)counter++;
 
  
  
  if(SDI==1)				// Start bit?
  {
    newChar = 0;				
    halWaitUs(SDI_CLK_TIME/trim);       // Aspetto circa mezzo tempo di bit

    for (uint8 i=0x1; i<0x80; i <<= 1)	// Leggo i 7 bit del dato interessante
    {
      halWaitUs(SDI_CLK_TIME);
      uint8 noti = ~i;
      if (SDI==0)
        newChar |= i;
      else 
        newChar &= noti;
    }
    
    halWaitUs(SDI_CLK_TIME);	        //Lascio passare il bit di parita 
    halWaitUs(SDI_CLK_TIME);	        //Lascio passere il bit di Stop 
    buffer[ci]=newChar;                 //Salvo il carattere nel buffer
    ci++;                               //incremento indice buffer
    
  }
  else
  {
    newChar=0xDD;       //Se non ho ottenuto comunque risposta uso 0xDD come flag  
  }
  return newChar;
}


//FUNZIONE CHE INVIA I COMANDI E RICEVE LE RISPOSTE SU BUS SDI-12
bool readSdi(uint8 sdiAddress, byte *buffer,float trim){
   
   c=0;
   
   sensAddr=sdiAddress + '0';
   asm("NOP");
   // Invia Comando
   markingSdi();
   sdiSendbyte(sensAddr);
   sdiSendbyte('M');
   sdiSendbyte('!');
   SDI_DATA_HIGH();
              
   halWaitSec(1); // aspetta l'elaborazione
   
   c=0;
   //Leggi risposta
   markingSdi();
   sdiSendbyte(sensAddr);
   sdiSendbyte('D');
   sdiSendbyte('0');
   sdiSendbyte('!');  
   SDI_DATA_HIGH();
   
   chAq=0xEE; // inizializzo il carattero su uno valido e ricevo
   while(chAq!=0x0D && chAq!=0x00 && chAq!=0xDD){
      chAq=receiveChar(buffer,trim);
      //se diverso da quelli vietati , lo scrivo nel buffer
      if(chAq!=0x0D && chAq!=0x00 && chAq!=0xDD){
      buffer[c]=chAq;
      c++;
      }
      else{
      buffer[c]=0x0D;
      }
   }
   //eseguo controllo per vedere se lettura buona
    if(sensAddr==0x36)
    san=sdiSanityCheckMPS(buffer);
    else
    san=sdiSanityCheck(buffer);  
    
   return san;  
}



/* FUNZIONE GLOBALE PER INTERROGARE IL SENSORE, RESTITUISCE IL NUMERO DI TENTATIVI*/
uint8 askSdi(uint8 sdiAddress,byte *buffer,float trim){

   sdi_retr=SDI_RETRIES;
   uint8 u=0;
   //Inizializzo il risultato a falso
   sdi_succ=0x00;
   sdi_succ=readSdi(sdiAddress,buffer,trim);  
   //asm("NOP");
   // ripeto "sdiRetries"-volte finche non scade o non leggo giusto
   while(!sdi_succ && sdi_retr>0x00){
      sdi_succ=readSdi(sdiAddress,buffer,trim);
      if(!sdi_succ){
        
        sdi_retr--;
        resetSdiBuffer(buffer);
        if(sdi_retr%3==0x09)trim=SDI_TRIM_2_0;
        else if (sdi_retr%3==0x06)trim=SDI_TRIM_1_3;
        else if (sdi_retr%3==0x03)trim=SDI_TRIM_1_8;  
      }
   }
   
   //asm("NOP");
   //costruisco il vettore coi byte
   for(u=0;u<SDI_BUFFER_LENGTH;u++)
   {
     if(buffer[u]<0x30 || buffer[u]==0xFF){
        buffer[u]=buffer[u];       //era buffer2
        sdiGrafic[u]=buffer[u]; ///Debug
        //asm("NOP");
     }
     else{
      sdiGrafic[u]=buffer[u]; ///Debug
      buffer[u]=buffer[u]-'0';
      
      //asm("NOP");
    }
         
   }
  
return sdi_retr;
}


uint16 finddot(byte *databuff,uint8 len)
{ 
 uint8 idn=0;
 bef=0;       //Contatore dei caratteri prima del punto
 aft=0;       //Contatore dei caratteri dopo il punto
 bool dot_found=false;
 
  for(idn=0;idn<len;idn++){
   
    //Se il carattere � diverso dal punto e ancora non ho trovato il punto
    if(databuff[idn]!=0x2E && dot_found==false && databuff[idn]!=0xFF)
    {
      bef++;      //Incremento il contatore BEFORE
    }
    
    //Se il carattere � il punto , segno che l'ho trovato
    else if(databuff[idn]==0x2E) dot_found=true;
    
    //Se il carattere � diverso dal punto e l'ho gia trovato 
    else if(databuff[idn]!=0x2E && dot_found==true && databuff[idn]!=0xFF)
    {
      aft++;      //Incremento il contatore AFTER
    }
  }
  //asm("NOP");
  
  return BUILD_UINT16(aft,bef);
}







