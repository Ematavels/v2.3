/******************************************************************************
*                                                                             *
*        **********                                                           *
*       ************                                                          *
*      ***        ***                                                         *
*     ***    ++    ***                                                        *
*     ***   +  +   ***                      CHIPCON                           *
*     ***   +                                                                 *
*     ***   +  +   ***                                                        *
*     ***    ++    ***                                                        *
*      ***        ***                                                         *
*       ************                                                          *
*        **********                                                           *
*                                                                             *
*******************************************************************************

Filename:     ACCTParser.c
Target:       cc2430
Author:       EFU
Revised:      1/3-2007
Revision:     1.1
******************************************************************************/


#if defined ACC_TDT || defined ACC_TDTP || defined ACC_TDR

#include "SdiCore.h"
#include "ACCTParser.h"
#include "ioCC2530.h"
#include "hal_types.h"
#include "hal_sleep.h"
#include "OSAL_Timers.h"
#include "mac_api.h"
#include "astronomic.h"


void AcctParser(byte *buffer2,int16 *resbuf);

uint16 findACCdot(byte *databuff,uint8 len);
void hexACCTMultiplier (byte *databuf,uint16 dotpos,uint8 TYPE,int16 *resbuf);

uint8 ipb_ACCT;    //indice di buffACCTPars
//uint8 aft,bef;  //contatori caratteri prima e dopo il punto
uint8 split_ACCT[2]; //vettore contenten prima e dopo da passare a hexmultiplier
uint16 somma_ACCT;
bool negtemp_ACCT=false;   //tiene traccia del segno "-" nella temperatura


uint16 d1_ACCT;    //risultato posizione del punto nel primo dato (AAAAAVWC per ACCT), unisce "bef" e "aft"
uint16 d2_ACCT;    //risultato posizione del punto nel sAECondo dato (ATEMP per ACCT)  unisce "bef" e "aft"
uint16 d3_ACCT;    //risultato posizione del punto nel terzo dato (AEC per ACCT) unisce "bef" e "aft"
uint16 d4_ACCT;  
uint16 d5_ACCT; 


//uint8 pi;

uint8 buffACCTPars[6]={0xFF,0xFF,0xFF,0xFF,0xFF,0xFF}; // vettore degli indici posizioni "+" e "-"
uint8 dotn_ACCT=0;
uint8 idn_ACCT;    // indice per copiare valori nei vari data1_ACCT,data2_ACCT,data3_ACCT

uint8 data1_ACCT[5]={0xFF,0xFF,0xFF,0xFF,0xFF};       // AAAAAVWC  (1-80) � [0,01]
uint8 data2_ACCT[5]={0xFF,0xFF,0xFF,0xFF,0xFF};            // ATEMP (-40 , +50) �C [0,1]
uint8 data3_ACCT[5]={0xFF,0xFF,0xFF,0xFF,0xFF};      // AEC  (0 - 25000) ms/cm
uint8 data4_ACCT[5]={0xFF,0xFF,0xFF,0xFF,0xFF};
uint8 data5_ACCT[5]={0xFF,0xFF,0xFF,0xFF,0xFF};


uint16 test1_ACCT;   //Variabili di appoggio per BUILD risultato finale misura
uint16 test2_ACCT;
uint16 test3_ACCT;
uint16 test4_ACCT;
uint16 test5_ACCT;

void AcctParser(byte *buffer2,int16 *resbuf)
{
  //pi=0; // indice del for e if
  uint8 u=0;
  ipb_ACCT=0;
 
  
  //Inserire reset delle misure
  
  negtemp_ACCT=false;
  //Ricavo i pivot dei dati ( le posizioni dei segni "+" e "-" della stringa
  for(u=0;u<SDI_BUFFER_LENGTH;u++)
  {
    
    //Se uguale a "+", "-" , o "CR" quando la posizione � diversa da 0 ( comincia sempre per CR)
    if(buffer2[u]==0x2B || buffer2[u]==0x2D || (buffer2[u]==0x0D && u!=0))
    {
      buffACCTPars[ipb_ACCT]=u;     //Salvo le posizioni in un buffer per il parsing  
      ipb_ACCT++;
      if(buffer2[u]==0x2D)negtemp_ACCT=true;
    }
  }
  
  for(u=0;u<sizeof(data1_ACCT);u++)
  {
    data1_ACCT[u]=0xFF;
  }
  
  for(u=0;u<sizeof(data2_ACCT);u++)
  {
    data2_ACCT[u]=0xFF;
  }
  
  for(u=0;u<sizeof(data3_ACCT);u++)
  {
    data3_ACCT[u]=0xFF;
  }
  
  for(u=0;u<sizeof(data4_ACCT);u++)
  {
    data4_ACCT[u]=0xFF;
  }
  
  for(u=0;u<sizeof(data5_ACCT);u++)
  {
    data5_ACCT[u]=0xFF;
  }
  
  //Separo i 3 diversi valori in 2 buffer separati
  //data1_ACCT=AVWC
  idn_ACCT=0;
  for(u=buffACCTPars[0]+1;u<buffACCTPars[1];u++){
    data1_ACCT[idn_ACCT]=buffer2[u];
    idn_ACCT++;
  }
  
  idn_ACCT=0;
  //data2_ACCT=TEMPERATURA
  for(u=buffACCTPars[1]+1;u<buffACCTPars[2];u++){
    data2_ACCT[idn_ACCT]=buffer2[u];
    idn_ACCT++;
  }
  
  idn_ACCT=0;
  //data3_ACCT=PErmettivity
  for(u=buffACCTPars[2]+1;u<buffACCTPars[3];u++){
    data3_ACCT[idn_ACCT]=buffer2[u];
    idn_ACCT++;
  }
  
   idn_ACCT=0;
  //data4_ACCT= AEC
  for(u=buffACCTPars[3]+1;u<buffACCTPars[4];u++){
    data4_ACCT[idn_ACCT]=buffer2[u];
    idn_ACCT++;
  }
  
   idn_ACCT=0;
  //data4_ACCT= AEC
  for(u=buffACCTPars[4]+1;u<buffACCTPars[5];u++){
    data5_ACCT[idn_ACCT]=buffer2[u];
    idn_ACCT++;
  }
  
  //Falsifico i risultati per test
  asm("NOP");
  
  // Trovo il punto  per i 3 buffer dati
  d1_ACCT=finddot(data1_ACCT,sizeof(data1_ACCT));
  // Lo converto in un valore esadecimale buono per essere spedito via radio
  hexACCTMultiplier(data1_ACCT,d1_ACCT,AVWC,resbuf);
  
  d2_ACCT=finddot(data2_ACCT,sizeof(data2_ACCT));
  hexACCTMultiplier(data2_ACCT,d2_ACCT,ATEMP,resbuf);
  asm("NOP");
  
  // permittivity - da 1 a 80
  d3_ACCT=finddot(data3_ACCT,sizeof(data3_ACCT));
  hexACCTMultiplier(data3_ACCT,d3_ACCT,APERM,resbuf);
  
  // bulk conductivity 0 - 5000 us/cm
  d4_ACCT=finddot(data4_ACCT,sizeof(data4_ACCT));
  hexACCTMultiplier(data4_ACCT,d4_ACCT,AEC,resbuf);
  asm("NOP");
  
  // pore conductivity 0 - 55000 us/cm
  d5_ACCT=finddot(data5_ACCT,sizeof(data5_ACCT));
  hexACCTMultiplier(data5_ACCT,d5_ACCT,AECPORE,resbuf);
  asm("NOP");
}

  

void hexACCTMultiplier (byte *databuf,uint16 dotpos, uint8 TYPE,int16 *resbuf)
{
  
  //Spezzo il valore uint16 del contatore caratteri in prima e dopo.
  split_ACCT[0] = (dotpos >> 8) & 0xff;  //Parte alta
  split_ACCT[1] = dotpos & 0xff;         //Parte Bassa
  
  
  // 2:2  
  if(split_ACCT[0]==0x02 && split_ACCT [1]==0x02){
    test1_ACCT=BUILD_UINT8(databuf[0],databuf[1]);
    test1_ACCT=HextoBCD(test1_ACCT);
    test2_ACCT=BUILD_UINT8(databuf[3],databuf[4]);
    test2_ACCT=HextoBCD(test2_ACCT);
    test1_ACCT=test1_ACCT*0x64;
    somma_ACCT=test1_ACCT + test2_ACCT;
    asm("NOP");
  }
  
  //2:1
  else if (split_ACCT[0]==0x02 && split_ACCT [1]==0x01)
  {
    test1_ACCT=BUILD_UINT8(databuf[0],databuf[1]);
    test1_ACCT=HextoBCD(test1_ACCT);
    test2_ACCT=BUILD_UINT8(databuf[3],0x00);
    test2_ACCT=HextoBCD(test2_ACCT);
    test1_ACCT=test1_ACCT*0x64;
    somma_ACCT=test1_ACCT + test2_ACCT;
    asm("NOP");
  }
  
  //2:0
  else if (split_ACCT[0]==0x02 && split_ACCT [1]==0x00)
  {
    if(TYPE==AVWC || TYPE==ATEMP)
    {
    test1_ACCT=BUILD_UINT8(databuf[0],databuf[1]);
    test1_ACCT=HextoBCD(test1_ACCT);
    test1_ACCT=test1_ACCT*0x64;
    test2_ACCT=BUILD_UINT16(0x00,0x00);
    somma_ACCT=test1_ACCT+test2_ACCT;
   
    //asm("NOP");
    }
    else
    {
    test1_ACCT=BUILD_UINT8(databuf[0],databuf[1]);
    test1_ACCT=HextoBCD(test1_ACCT);
    somma_ACCT=test1_ACCT;
    //asm("NOP");
    }
  
  }
  
  //1:2
  else if(split_ACCT[0]==0x01 && split_ACCT [1]==0x02)
  {
    test1_ACCT=BUILD_UINT8(0x00,databuf[0]);
    test1_ACCT=HextoBCD(test1_ACCT);
    test2_ACCT=BUILD_UINT8(databuf[2],databuf[3]);
    test2_ACCT=HextoBCD(test2_ACCT);
    test1_ACCT=test1_ACCT*0x64;
    somma_ACCT=test1_ACCT + test2_ACCT;
  }
  
  
  //1:1
  else if(split_ACCT[0]==0x01 && split_ACCT [1]==0x01)
  {
      test1_ACCT=BUILD_UINT8( 0x00,databuf[0]);
      test1_ACCT=HextoBCD(test1_ACCT);
      test2_ACCT=BUILD_UINT8(databuf[2],0x00);
      test2_ACCT=HextoBCD(test2_ACCT);
      test1_ACCT=test1_ACCT*0x64;
      somma_ACCT=test1_ACCT+test2_ACCT;
  }
  
  //1:0
  else if(split_ACCT[0]==0x01 && split_ACCT [1]==0x00)
  {
    
    if(TYPE==AVWC || TYPE==ATEMP)
    {
      test1_ACCT=BUILD_UINT8(0x00,databuf[0]);
      test1_ACCT=HextoBCD(test1_ACCT);
      test1_ACCT=test1_ACCT*0x64;
      somma_ACCT=test1_ACCT;
    }
    else
    {
      test1_ACCT=BUILD_UINT8(0x00,databuf[0]);
      test1_ACCT=HextoBCD(test1_ACCT);
      somma_ACCT=test1_ACCT;
      //asm("NOP");
    }
  }
  
  //FUNZIONANO SOLO PER AEC
  //3:0
  else if (split_ACCT[0]==0x03 && split_ACCT[1]==0x00)
  {
    test1_ACCT=BUILD_UINT8(0x00,databuf[0]);
    test1_ACCT=HextoBCD(test1_ACCT);
    test2_ACCT=BUILD_UINT8(databuf[1],databuf[2]);
    test2_ACCT=HextoBCD(test2_ACCT);
    test1_ACCT=test1_ACCT*0x64;
    somma_ACCT=test1_ACCT + test2_ACCT + test3_ACCT;
  }
  
   //3:1
  else if (split_ACCT[0]==0x03 && split_ACCT[1]==0x01)
  {
    asm("NOP");
    somma_ACCT=9999;
  }
  
  //4:0
  else if (split_ACCT[0]==0x04 && split_ACCT[1]==0x00)
  {
    test1_ACCT=BUILD_UINT8(databuf[0],databuf[1]);
    test1_ACCT=HextoBCD(test1_ACCT);
    test2_ACCT=BUILD_UINT8(databuf[2],databuf[3]);
    test2_ACCT=HextoBCD(test2_ACCT);
    test1_ACCT=test1_ACCT*0x64;
    somma_ACCT=test1_ACCT + test2_ACCT;
  }
  //5:0
  else if(split_ACCT[0]==0x05)
  {
    test1_ACCT=BUILD_UINT8(databuf[1],databuf[2]);
    test1_ACCT=HextoBCD(test1_ACCT);
    test2_ACCT=BUILD_UINT8(databuf[3],databuf[4]);
    test2_ACCT=HextoBCD(test2_ACCT);
    test1_ACCT=test1_ACCT*0x64;
    test3_ACCT=BUILD_UINT8(0x00,databuf[0]);
    test3_ACCT=HextoBCD(test3_ACCT);
    test3_ACCT=test3_ACCT*0x2710;
    
    somma_ACCT=test1_ACCT + test2_ACCT + test3_ACCT;
  }
  
  
 if(TYPE==AVWC) resbuf[0]=somma_ACCT;
 else if (TYPE==ATEMP)
 { 
   if(negtemp_ACCT==false)
   resbuf[1]=somma_ACCT;
   else
   {
    somma_ACCT=-somma_ACCT;
    resbuf[1]=somma_ACCT;
   }
 }
 else if (TYPE==APERM) resbuf[2]=somma_ACCT;
 else if (TYPE==AEC) resbuf[3]=somma_ACCT;
 else if (TYPE==AECPORE) resbuf[4]=somma_ACCT;
 
}


uint16 findACCdot(byte *databuff,uint8 len)
{ 
 uint8 idn=0;
 uint8 bef=0;       //Contatore dei caratteri prima del punto
 uint8 aft=0;       //Contatore dei caratteri dopo il punto
 bool dot_found=false;
 
  for(idn=0;idn<len;idn++){
   
    //Se il carattere � diverso dal punto e ancora non ho trovato il punto
    if(databuff[idn]!=0x2E && dot_found==false && databuff[idn]!=0xFF)
    {
      bef++;      //Incremento il contatore BEFORE
    }
    
    //Se il carattere � il punto , segno che l'ho trovato
    else if(databuff[idn]==0x2E) dot_found=true;
    
    //Se il carattere � diverso dal punto e l'ho gia trovato 
    else if(databuff[idn]!=0x2E && dot_found==true && databuff[idn]!=0xFF)
    {
      aft++;      //Incremento il contatore AFTER
    }
  }
  asm("NOP");
  
  return BUILD_UINT16(aft,bef);
}

#endif