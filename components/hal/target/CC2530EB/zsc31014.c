/******************************************************************************
*                                                                             *
*        **********                                                           *
*       ************                                                          *
*      ***        ***                                                         *
*     ***    ++    ***                                                        *
*     ***   +  +   ***                      CHIPCON                           *
*     ***   +                                                                 *
*     ***   +  +   ***                                                        *
*     ***    ++    ***                                                        *
*      ***        ***                                                         *
*       ************                                                          *
*        **********                                                           *
*                                                                             *
*******************************************************************************

Filename:     zsc31014.c
Target:       cc2430
Author:       EFU
Revised:      1/3-2007
Revision:     1.1

Description:
Function implementations for common ZSC functions for use with the CC2430DB.

******************************************************************************/
#if defined (GUARDIAN)

#include "zsc31014.h"
#include "ioCC2530.h"
#include "hal_timer.h"
#include "ZComDef.h"
#include "hal_i2c.h"
#include "OSAL.h"
#include "hal_sleep.h"

void resetReceiveBuff(void);

byte writeZscBlock(byte* buffer, int8 length, word address);
byte receiveBuffer[4];
uint8 zscWriteAddress,zscReadAddress; // per indirizzare piu di uno zsc
uint8 respVal,respVal1;

bool setZscAddress=FALSE;

/******************************************************************************
* See eeprom.h for a description of this function.
******************************************************************************/
void resetReceiveBuff()
{
 for (uint8 i=0;i<4;i++)
   receiveBuffer[i]='\0';
}


void zscInit(uint8 setValue)
{
  /* per la scrittura dell'Id OFF=1  ON=0  */
 switch (setValue)
 {
 case 0x00:
  P1_7=0x01;  // blocco il sensore 2 per scrivere l'1 ( anche se controllo di seguito )
              // 0=funzionano entrambi 1=funziona solo il 1
   
  P1_4=0x01; // attivo il sensore 1 OFF
  P1_5=0x01; // controllo sensore 2 OFF
  P1_6=0x01; // controllo sensore 3 OFF
  P1_0=0x01; // controllo sensore 4 OFF
  P1_1=0x01; // controllo sensore 5 OFF   
 break;
 
 case 0x01:
  if ( setZscAddress ) // se � un set parameters
   P1_7=0x01; // 1 = funziona solo il 1
  else P1_7=0x00; // 0 = funzionano entrambi
  P1_4=0x00; // attivo il sensore 1 ON
  P1_5=0x00; // controllo sensore 2 ON
  P1_6=0x01; // controllo sensore 3 OFF
  P1_0=0x01; // controllo sensore 4 OFF
  P1_1=0x01; // controllo sensore 5 OFF   
 break;
  
  default:
   break;
 }
 
  halWait(10); // aspetto 10ms perch� sia stabile
 
}

void selectZsc(uint8 numbers)
{
  switch (numbers) 
  { 
  case 0x01: 
    zscWriteAddress=ZSC_WRITEADDRESS_1;
    zscReadAddress=ZSC_READADDRESS_1;
  break;
  
  case 0x02:
    zscWriteAddress=ZSC_WRITEADDRESS_2;
    zscReadAddress=ZSC_READADDRESS_2;
  break;
  
  case 0x03: // valori di default di lettura/scrittura ( set parameters )
    zscWriteAddress=0x50;
    zscReadAddress=0x51;
  break;
 }  
}


uint8 readZsc(byte* buffer)
{
   uint8 retVal=0x00;
   
   if (smbZscReceive(zscReadAddress, 0x04, receiveBuffer)>0)
   retVal=0x5A;
     
   osal_memcpy(buffer,receiveBuffer,0x04); // copio nel buffer di supporto
        
   //zscInit(0x00);  // spengo  alla fine di tutte le letture

   return retVal;
}


/******************************************************************************
* See eeprom.h for a description of this function.
******************************************************************************/
byte startModeZsc(uint8 command)
{  
   uint8 retstart;
   byte startModeBuffer[4]={zscWriteAddress,command,0x00,0x00};
      
   /* Aspetto la risposta dall'integrato 1 byte */ 
   if ( zscI2cReceive( startModeBuffer,0x04, receiveBuffer, 0x01) > 0 )
    retstart=receiveBuffer[0]; 
   else retstart=0;
   
   return retstart;
}

uint16 readAddressZsc(uint8 address)
{
   uint16 readValue=0x00;
   byte transmitBuffer[4]={zscWriteAddress,address,0x00,0x00};
   
   resetReceiveBuff();
   
   zscInit(0x00);
   halWait(10); // aspetto che sia stabile
   zscInit(0x01);  // spengo e attivo lo zsc per attivarmi in 6ms
   
   if ( startModeZsc(COMMAND_MODE) > 0 ) // init del command mode  
   {
    /* Invio ed aspetto la risposta dall'integrato 3 byte */ 
    if ( zscI2cReceive( transmitBuffer, 0x04, receiveBuffer, 0x03) > 0)
    { 
       if (receiveBuffer[0]==0x5A) // se ho letto correttamente ritorno il valore letto
       { 
         readValue=BUILD_UINT16(receiveBuffer[2],receiveBuffer[1]);
         if (startModeZsc(NORMAL_MODE) > 0) // init del normal mode
           zscInit(0x00);  // spengo 
           return readValue;
       }
    }
   }
   zscInit(0x00);  // spengo 
   return readValue=0xFFFF; // non ho letto correttamente  
}

/* address = command byte          *
 *                                 *
 * buffer = third and fourth bytes */
byte writeAddressZsc(uint16 valueWrite, byte address)
{ 
   uint8 transmitBuffer[4]={zscWriteAddress,(address+0x40),HI_UINT16(valueWrite),LO_UINT16(valueWrite)};
   
   resetReceiveBuff();
  
   zscInit(0x00);
   halWait(10); // aspetto che sia stabile
   zscInit(0x01);  // spengo e attivo lo zsc per attivarmi in 6ms
   
   if ( startModeZsc(COMMAND_MODE) > 0 ) // init del command mode
   {
    if ( (zscI2cReceive( transmitBuffer, 0x04, receiveBuffer, 0x03)) > 0 )
    {
     if (receiveBuffer[0] == 0x5A )
        if (startModeZsc(NORMAL_MODE)>0) // init del normal mode
        zscInit(0x00);  // spengo 
        return 0x5A;  
    }
   }
   zscInit(0x00);  // spengo 
   return 0;
}



/******************************************************************************
* See eeprom.h for a description of this function.
* length must be less or equal 32 ( 128 )
******************************************************************************/
byte writeZscBlock(byte* buffer, int8 length, word address)
{
   uint8 i = 0;
   uint8 j;
   byte transmitBuffer[BUFFER_SIZE_ZSC];

   transmitBuffer[i++] = zscWriteAddress;
   transmitBuffer[i++] = (byte) (address >> 8);
   transmitBuffer[i++] = (byte) (address);

   for(j = 0; j < length; j++)
   {
      transmitBuffer[i++] = buffer[j];
   }

   smbSend(transmitBuffer, i);
   return TRUE;
}

uint16  readTempZsc()
{
 
 uint16 valueCamp;
 uint16 valueConv=0;
 uint8 buffer[4];

 uint8 ret=readZsc(buffer); // leggo lo Zsc ( 4 byte default )   
  
 if (ret==0x5A) 
 {
  valueCamp= BUILD_UINT16(buffer[3], buffer[2]);
  
  valueConv =((valueCamp>>=5) * ( VREF_TEMP / LIVELLI_ZSC_TEMP ) - OFFSET_TEMP); // �C - non converto lo fa il sw
  
  return valueConv;
  
 }  
  return 0x00;
}

uint16 readBridgeZsc()
{
 uint16 valueCamp;
 //uint16 valueConv=0;
 uint8 buffer[4];
   
 uint8 ret=readZsc(buffer); // leggo lo Zsc ( 4 byte default )   
 
 //zscInit(0x00); // spengo lo zsc ( lo spengo alla fine delle rilevazioni totali inutile spegnere per poi riaccendere )
  
 if (ret==0x5A) 
 {
  valueCamp= BUILD_UINT16(buffer[1], buffer[0]) & 0x3FFF; 
  
  //valueConv =(uint16)((valueCamp * ( VREF / (LIVELLI_ZSC*GAIN))) - OFFSET); // mVolt non converto perch� lo fa il sw
  
  return valueCamp;
 }  
 
  return 0x00;
}

uint8 setZscParameters()
{
  
  selectZsc(0x02); 
  
  // imposto i parametri per lettura del sensore ( risposta deve essere 0x5A )
  respVal=writeAddressZsc(0x1F88,0x0F);
    
  selectZsc(0x02); 
  
  respVal1=writeAddressZsc(0x1F88,0x0F); 
 
  if ( (respVal==0x5A) && (respVal1==0x5A) ) return 0x5A;
  else return 0x00;
  
}

void setZscPhyAddress()
{
  
  // ATTENZIONE - imposta nello zscInit il blocco sul secondo Zsc
  setZscAddress=TRUE; // procedura di set
  
  selectZsc(0x03); // valori di default che all'inizio sono su entrambi zsc 
    
  // imposto l'indirizzo per lo zsc1  ( risposta deve essere 0x5A )
  respVal1=writeAddressZsc(0x0D38,0x02); 
  
  setZscAddress=FALSE; // termine procedura di set
  
  asm("NOP");
  
}




#endif