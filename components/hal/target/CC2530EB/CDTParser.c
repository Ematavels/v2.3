/******************************************************************************
*                                                                             *
*        **********                                                           *
*       ************                                                          *
*      ***        ***                                                         *
*     ***    ++    ***                                                        *
*     ***   +  +   ***                      CHIPCON                           *
*     ***   +                                                                 *
*     ***   +  +   ***                                                        *
*     ***    ++    ***                                                        *
*      ***        ***                                                         *
*       ************                                                          *
*        **********                                                           *
*                                                                             *
*******************************************************************************

Filename:     GS3Parser.c
Target:       cc2430
Author:       EFU
Revised:      1/3-2007
Revision:     1.1
******************************************************************************/
#if defined DECCDT
#include "SdiCore.h"
#include "CDTParser.h"
#include "ioCC2530.h"
#include "hal_types.h"
#include "hal_sleep.h"
#include "OSAL_Timers.h"
#include "mac_api.h"
#include "astronomic.h"

void CDTParser(byte *buffer2,int16 *resbuf);

void hexCDTMultiplier (byte *databuf,uint16 dotpos,uint8 TYPE,int16 *resbuf);

uint8 ipb_CDT;    //indice di buffCDTPars
//uint8 aft,bef;  //contatori caratteri prima e dopo il punto
uint8 split_CDT[2]; //vettore contenten prima e dopo da passare a hexmultiplier
uint16 somma_CDT;
uint32 sommaExt_CDT;
bool negtemp_CDT=false;   //tiene traccia del segno "-" nella temperatura


uint16 d1_CDT;    //risultato posizione del punto nel primo dato ( PROFONDITA per CDT), unisce "bef" e "aft"
uint16 d2_CDT;    //risultato posizione del punto nel secondo dato (TEMP per CDT)  unisce "bef" e "aft"
uint16 d3_CDT;    //risultato posizione del punto nel terzo dato (EC per CDT) unisce "bef" e "aft"


//uint8 pi;

uint8 buffCDTPars[4]={0xFF,0xFF,0xFF,0xFF}; // vettore degli indici posizioni "+" e "-"
uint8 dotn_CDT=0;
uint8 idn_CDT;    // indice per copiare valori nei vari data1_CDT,data2_CDT,data3_CDT

uint8 data1_CDT[5]={0xFF,0xFF,0xFF,0xFF,0xFF};       // DEP  (0-10000)mm  [0,01]
uint8 data2_CDT[5]={0xFF,0xFF,0xFF,0xFF,0xFF};       // TEMP (-11 , +50) �C [0,1]
uint8 data3_CDT[6]={0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};       // EC  (0 - 120000) uS/cm

uint16 test1_CDT;   //Variabili di appoggio per BUILD risultato finale misura
uint16 test2_CDT;
uint16 test3_CDT;
// Il valore di conducibilita espresso in uS/cm � troppo grande per uint16, quindi creo delle variabili di appoggio a 32 bit
uint32 test1Ext_CDT;
uint32 test2Ext_CDT;
uint32 test3Ext_CDT;


void CDTParser(byte *buffer2,int16 *resbuf)
{
  //pi=0; // indice del for e if
  uint8 u=0;
  ipb_CDT=0;
 
  
  negtemp_CDT=false;
  //Ricavo i pivot dei dati ( le posizioni dei segni "+" e "-" della stringa
  for(u=0;u<SDI_BUFFER_LENGTH;u++)
  {
    
    //Se uguale a "+", "-" , o "CR" quando la posizione � diversa da 0 ( comincia sempre per CR)
    if(buffer2[u]==0x2B || buffer2[u]==0x2D || (buffer2[u]==0x0D && u!=0))
    {
      buffCDTPars[ipb_CDT]=u;     //Salvo le posizioni in un buffer per il parsing  
      ipb_CDT++;
      if(buffer2[u]==0x2D)negtemp_CDT=true;
      
    }
  }
  
  /* Anche la profondita pu� andare in negativo, trucco per non sballare TEMO */
  if(buffer2[1]==0x2D)negtemp_CDT=false;
  
  
  //Resetto i buffer
  for(u=0;u<sizeof(data1_CDT);u++)
  {
    data1_CDT[u]=0xFF;
  }
  
  for(u=0;u<sizeof(data2_CDT);u++)
  {
    data2_CDT[u]=0xFF;
  }
  
  for(u=0;u<sizeof(data3_CDT);u++)
  {
    data3_CDT[u]=0xFF;
  }
  
  //Separo i 3 diversi valori in 2 buffer separati
  //data1_CDT= PROFONDITA
  idn_CDT=0;
  for(u=buffCDTPars[0]+1;u<buffCDTPars[1];u++){
    data1_CDT[idn_CDT]=buffer2[u];
    idn_CDT++;
  }
  
  idn_CDT=0;
  //data2_CDT=TEMPERATURA
  for(u=buffCDTPars[1]+1;u<buffCDTPars[2];u++){
    data2_CDT[idn_CDT]=buffer2[u];
    idn_CDT++;
  }
  
  
  idn_CDT=0;
  //data3_CDT= CONDUCIBILITA
  for(u=buffCDTPars[2]+1;u<buffCDTPars[3];u++){
    data3_CDT[idn_CDT]=buffer2[u];
    idn_CDT++;
  }
  
  
  //Falsifico i risultati per test
  asm("NOP");
  
  // Trovo il punto  per i 3 buffer dati
  d1_CDT=finddot(data1_CDT,0x05);
  // Lo converto in un valore esadecimale buono per essere spedito via radio
  hexCDTMultiplier(data1_CDT,d1_CDT,DEP,resbuf);
  
  d2_CDT=finddot(data2_CDT,0x05);
  hexCDTMultiplier(data2_CDT,d2_CDT,TEMP,resbuf);
  
  d3_CDT=finddot(data3_CDT,0x06);
  hexCDTMultiplier(data3_CDT,d3_CDT,EC,resbuf);
  asm("NOP");
  
}


void hexCDTMultiplier (byte *databuf,uint16 dotpos, uint8 TYPE,int16 *resbuf)
{
  
  //Spezzo il valore uint16 del contatore caratteri in prima e dopo.
  split_CDT[0] = (dotpos >> 8) & 0xff;  //Parte alta
  split_CDT[1] = dotpos & 0xff;         //Parte Bassa
  
  
  asm("NOP");
  
  
  //2:1  solo per Temperatura, moltiplico tutto per 10.
  if (split_CDT[0]==0x02 && split_CDT [1]==0x01)
  {
  
        asm("NOP");
        test1_CDT=BUILD_UINT8(databuf[0],databuf[1]);
        test1_CDT=HextoBCD(test1_CDT);
        test2_CDT=BUILD_UINT8(0x00,databuf[3]);
        test2_CDT=HextoBCD(test2_CDT);
        test1_CDT=test1_CDT*10;
        somma_CDT=test1_CDT + test2_CDT;
        asm("NOP");
  }
  
  //2:0 Per tutti i valori
  else if (split_CDT[0]==0x02 && split_CDT [1]==0x00)
  {
    if(TYPE==TEMP){
        asm("NOP");
        test1_CDT=BUILD_UINT8(databuf[0],databuf[1]);
        test1_CDT=HextoBCD(test1_CDT);
        test1_CDT=test1_CDT*10;
        somma_CDT=test1_CDT;
        asm("NOP");
    }
   else
   {    
        test1_CDT=BUILD_UINT8(databuf[0],databuf[1]);
        test1_CDT=HextoBCD(test1_CDT);
        somma_CDT=test1_CDT;
        if(TYPE==EC) somma_CDT=somma_CDT/10;
   }       
  
  }
  
 
  //1:1 Solo Temperatura moltiplico tutto per 10
  else if(split_CDT[0]==0x01 && split_CDT [1]==0x01)
  {
    
        asm("NOP");
        test1_CDT=BUILD_UINT8(databuf[0],databuf[2]);
        test1_CDT=HextoBCD(test1_CDT);
    
        somma_CDT=test1_CDT;
        asm("NOP"); 
      
  }
  
  //1:0 per tutti i valori
  else if(split_CDT[0]==0x01 && split_CDT [1]==0x00)
  { 
    if(TYPE==TEMP){
      test1_CDT=BUILD_UINT8(0x00,databuf[0]);
      test1_CDT=HextoBCD(test1_CDT);
      test1_CDT=test1_CDT*0x0A;
      somma_CDT=test1_CDT;
    }
    else{
      test1_CDT=BUILD_UINT8(0x00,databuf[0]);
      test1_CDT=HextoBCD(test1_CDT);
      somma_CDT=test1_CDT;
      if(TYPE==EC) somma_CDT=somma_CDT/10;
    }
      asm("NOP");
  
  }
  
  
  //3:0 per tutti i valori tranne temp
  else if (split_CDT[0]==0x03 && split_CDT[1]==0x00)
  {
    test1_CDT=BUILD_UINT8(0x00,databuf[0]);
    test1_CDT=HextoBCD(test1_CDT);
    test2_CDT=BUILD_UINT8(databuf[1],databuf[2]);
    test2_CDT=HextoBCD(test2_CDT);
    test1_CDT=test1_CDT*0x64;
    somma_CDT=test1_CDT + test2_CDT + test3_CDT;
    if(TYPE==EC) somma_CDT=somma_CDT/10;
  }
  
  else if (split_CDT[0]==0x04 && split_CDT[1]==0x00)
  {
    test1_CDT=BUILD_UINT8(databuf[0],databuf[1]);
    test1_CDT=HextoBCD(test1_CDT);
    test2_CDT=BUILD_UINT8(databuf[2],databuf[3]);
    test2_CDT=HextoBCD(test2_CDT);
    test1_CDT=test1_CDT*0x64;
    somma_CDT=test1_CDT + test2_CDT + test3_CDT;
    if(TYPE==EC) somma_CDT=somma_CDT/10;
  }
  
  else if (split_CDT[0]==0x05 && split_CDT[1]==0x00)
  {
    test1Ext_CDT=BUILD_UINT8(0x00,databuf[0]);
    test1Ext_CDT=HextoBCD(test1Ext_CDT);
    test2Ext_CDT=BUILD_UINT8(databuf[1],databuf[2]);
    test2Ext_CDT=HextoBCD(test2Ext_CDT);
    test3Ext_CDT=BUILD_UINT8(databuf[3],databuf[4]);
    test3Ext_CDT=HextoBCD(test3Ext_CDT);
    test1Ext_CDT=test1Ext_CDT*0x2710;
    test2Ext_CDT=test2Ext_CDT*0x64;
    sommaExt_CDT=test1Ext_CDT + test2Ext_CDT + test3Ext_CDT;
    
    if(TYPE==EC) somma_CDT=sommaExt_CDT/10;
    else{
      //Controllo che la Profondita non vada fuori scala
      if(sommaExt_CDT>10000) sommaExt_CDT=10000;
      somma_CDT=sommaExt_CDT;
    } 
    
    
  }
  
   else if (split_CDT[0]==0x06 && split_CDT[1]==0x00)
  {
    test1Ext_CDT=BUILD_UINT8(0x00,databuf[0]);
    test1Ext_CDT=HextoBCD(test1Ext_CDT);
    test2Ext_CDT=BUILD_UINT8(databuf[1],databuf[2]);
    test2Ext_CDT=HextoBCD(test2Ext_CDT);
    test3Ext_CDT=BUILD_UINT8(databuf[3],databuf[4]);
    test3Ext_CDT=HextoBCD(test3Ext_CDT);
    test1Ext_CDT=test1Ext_CDT*0x186A0;
    test2Ext_CDT=test2Ext_CDT*0x3E8;
    sommaExt_CDT=test1Ext_CDT + test2Ext_CDT + test3Ext_CDT;
    //Controllo che la Conducibilit� non vada fuori scala
    if(sommaExt_CDT>=120000)sommaExt_CDT=120000;
    somma_CDT=sommaExt_CDT/10;
  }
 
  
 // Assegnazione dei risultati ai rispettivi buffer 
 if(TYPE==DEP) resbuf[0]=somma_CDT;
 else if (TYPE==TEMP)
 { 
   if(negtemp_CDT==false)
   resbuf[1]=somma_CDT;
   else
   {
    somma_CDT=-somma_CDT;
    resbuf[1]=somma_CDT;
   }
 }
 else if (TYPE==EC) resbuf[2]=somma_CDT;
 
}

#endif