/**************************************************************************************************
  Filename:       hal_spi.h

  Description:    Describe the purpose and contents of the file.

**************************************************************************************************
 **************************************************************************************************/

#ifdef __cplusplus
extern "C"
{
#endif

/**************************************************************************************************
 * INCLUDES
 **************************************************************************************************/

#include "hal_types.h"

#define U0_RXBYTE 0x04
#define U0_TXBYTE 0x02
#define URX0IF 0x80     // Bit mask for USART1 RX interrupt flag (TCON)

#define SSN P0_4
#define LOW 0
#define HIGH 1
#define TRUE 1
#define FALSE 0
  
#define SDcmd_SIZE 6
#define MMC_TIMEOUT  0xFF
#define MMC_BLOCK_SIZE 512

/* SPI settings */
#define HAL_SPI_CLOCK_POL_LO       0x00
#define HAL_SPI_CLOCK_PHA_0        0x00
#define HAL_SPI_TRANSFER_MSB_FIRST 0x20
  
  

/***************************************
        SD Card PARAMETERS
**************************************/ 
  
#define blockSize 512
 

/* Init the SPI port */

extern void SpiInit(void);
uint8 SPIWriteByte(uint8 byte);
uint8 SPIReadByte(void);
void  SPIWrite(uint8* data, uint16 length);
void  SPIRead(uint8* data, uint16 length);

