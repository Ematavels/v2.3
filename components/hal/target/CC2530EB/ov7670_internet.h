/******************************************************************************
*                                                                             *
*        **********                                                           *
*       ************                                                          *
*      ***        ***                                                         *
*     ***    ++    ***                                                        *
*     ***   +  +   ***                      CHIPCON                           *
*     ***   +                                                                 *
*     ***   +  +   ***                                                        *
*     ***    ++    ***                                                        *
*      ***        ***                                                         *
*       ************                                                          *
*        **********                                                           *
*                                                                             *
*******************************************************************************

Filename:     astronomic.h
Target:       cc2430
Author:       EFU
Revised:      1/3-2007
Revision:     1.1

Description:
Function declarations for common astronomic functions for use with the CC2430DB.

All functions defined here are implemented in astronomic.c.

******************************************************************************/

#ifdef __cplusplus
extern "C"
{
#endif


  /**************************************************************************************************
 * INCLUDES
 **************************************************************************************************/
#include "ZComDef.h"
#ifndef OV7670_H_
#define OV7670_H_


/* Includes -------------------------------------------------- ----------------*/

/* Exported typedef -------------------------------------------------- ---------*/
// Direction configuration of GPIO SIO_D pin
typedef enum
{
	OUT = 0,
	IN = 1,
}Direction;

/* Exported define -------------------------------------------------- ----------*/
#define Error			0
#define Success			1

// Number of register to change value when initialize Camera Module
#define RegNum		134
#define CHANGE_REG	167

// If Verify is defined, initialize of Camera Module registers is verified with
// data stored in InitBuffer
#define Camera_Verify		ENABLE
// Choose between normal and debug camera operation
//#define	Camera_Debug		ENABLE

// Camera Module (OV7670/OV7171) Register address
#define OV7670_COM7		0x12

/* Exported macro -------------------------------------------------- -----------*/


/* Exported variables -------------------------------------------------- -------*/


/* Exported constants ---------------------------------------------------------*/

// The period of the delay will depend on the system operating frequency. The following
// value has been set for system running at 168 MHz.
#define DCMI_TIMEOUT_MAX         10000
// maximal timeout
#define TIMEOUT_MAX              10000
// Define startup delay for HSI (High speed internal RC oscillator)
#define HSI_STARTUP_TIMEOUT		 10000


// Na pokusy
#define BuffSize		48000


/* Exported function prototypes -----------------------------------------------*/
uint8 SCCB_Read(void);
uint8 Camera_Init(void);
uint8 SCCB_Write(uint8 Data);
uint8 Camera_WriteReg(uint8 Address, uint8 Value);
uint8 Camera_ReadReg(uint8 Address);
void Camera_Reset(void);

/* Exported functions -------------------------------------------------- -------*/

#endif /* OV7670_H_ */
