/******************************************************************************
*                                                                             *
*        **********                                                           *
*       ************                                                          *
*      ***        ***                                                         *
*     ***    ++    ***                                                        *
*     ***   +  +   ***                      CHIPCON                           *
*     ***   +                                                                 *
*     ***   +  +   ***                                                        *
*     ***    ++    ***                                                        *
*      ***        ***                                                         *
*       ************                                                          *
*        **********                                                           *
*                                                                             *
*******************************************************************************

Filename:     GS3Parser.c
Target:       cc2430
Author:       EFU
Revised:      1/3-2007
Revision:     1.1
******************************************************************************/
#if defined DEC5TE_P04 || defined DEC5TE_P04_2

#include "SdiCore.h"
#include "5TEParser.h"
#include "ioCC2530.h"
#include "hal_types.h"
#include "hal_sleep.h"
#include "OSAL_Timers.h"
#include "mac_api.h"
#include "astronomic.h"

void Te5Parser(byte *buffer2,int16 *resbuf);

void hex5teMultiplier (byte *databuf,uint16 dotpos,uint8 TYPE,int16 *resbuf);

uint8 ipb_5TE;    //indice di buff5TEPars
//uint8 aft,bef;  //contatori caratteri prima e dopo il punto
uint8 split_5TE[2]; //vettore contenten prima e dopo da passare a hexmultiplier
uint16 somma_5TE;
bool negtemp_5TE=false;   //tiene traccia del segno "-" nella temperatura


uint16 d1_5TE;    //risultato posizione del punto nel primo dato (VWC per GS3), unisce "bef" e "aft"
uint16 d2_5TE;    //risultato posizione del punto nel secondo dato (TEMP per GS3)  unisce "bef" e "aft"
uint16 d3_5TE;    //risultato posizione del punto nel terzo dato (EC per GS3) unisce "bef" e "aft"


//uint8 pi;

uint8 buff5TEPars[4]={0xFF,0xFF,0xFF,0xFF}; // vettore degli indici posizioni "+" e "-"
uint8 dotn_5TE=0;
uint8 idn_5TE;    // indice per copiare valori nei vari data1_5TE,data2_5TE,data3_5TE

uint8 data1_5TE[5]={0xFF,0xFF,0xFF,0xFF,0xFF};       // VWC  (1-80) � [0,01]
uint8 data2_5TE[5]={0xFF,0xFF,0xFF,0xFF,0xFF};            // TEMP (-40 , +50) �C [0,1]
uint8 data3_5TE[4]={0xFF,0xFF,0xFF,0xFF};      // EC  (0 - 25000) ms/cm

uint16 test1_5TE;   //Variabili di appoggio per BUILD risultato finale misura
uint16 test2_5TE;
uint16 test3_5TE;


void Te5Parser(byte *buffer2,int16 *resbuf)
{
  //pi=0; // indice del for e if
  uint8 u=0;
  ipb_5TE=0;
 
  
  //Inserire reset delle misure
  
  negtemp_5TE=false;
  //Ricavo i pivot dei dati ( le posizioni dei segni "+" e "-" della stringa
  for(u=0;u<SDI_BUFFER_LENGTH;u++)
  {
    
    //Se uguale a "+", "-" , o "CR" quando la posizione � diversa da 0 ( comincia sempre per CR)
    if(buffer2[u]==0x2B || buffer2[u]==0x2D || (buffer2[u]==0x0D && u!=0))
    {
      buff5TEPars[ipb_5TE]=u;     //Salvo le posizioni in un buffer per il parsing  
      ipb_5TE++;
      if(buffer2[u]==0x2D)negtemp_5TE=true;
    }
  }
  
  for(u=0;u<sizeof(data1_5TE);u++)
  {
    data1_5TE[u]=0xFF;
  }
  
  for(u=0;u<sizeof(data2_5TE);u++)
  {
    data2_5TE[u]=0xFF;
  }
  
  for(u=0;u<sizeof(data3_5TE);u++)
  {
    data3_5TE[u]=0xFF;
  }
  
  //Separo i 3 diversi valori in 2 buffer separati
  //data1_5TE= VWC
  idn_5TE=0;
  for(u=buff5TEPars[0]+1;u<buff5TEPars[1];u++){
    data1_5TE[idn_5TE]=buffer2[u];
    idn_5TE++;
  }
  
  idn_5TE=0;
  //data2_5TE=TEMPERATURA
  for(u=buff5TEPars[1]+1;u<buff5TEPars[2];u++){
    data2_5TE[idn_5TE]=buffer2[u];
    idn_5TE++;
  }
  
  
  idn_5TE=0;
  //data3_5TE= EC
  for(u=buff5TEPars[2]+1;u<buff5TEPars[3];u++){
    data3_5TE[idn_5TE]=buffer2[u];
    idn_5TE++;
  }
  
  
  //Falsifico i risultati per test
  asm("NOP");
  
  // Trovo il punto  per i 3 buffer dati
  d1_5TE=finddot(data1_5TE,0x05);
  // Lo converto in un valore esadecimale buono per essere spedito via radio
  hex5teMultiplier(data1_5TE,d1_5TE,VWC,resbuf);
  
  d2_5TE=finddot(data2_5TE,0x05);
  hex5teMultiplier(data2_5TE,d2_5TE,EC,resbuf);
  
  d3_5TE=finddot(data3_5TE,0x04);
  hex5teMultiplier(data3_5TE,d3_5TE,TEMP,resbuf);
  asm("NOP");
  
}


void hex5teMultiplier (byte *databuf,uint16 dotpos, uint8 TYPE,int16 *resbuf)
{
  
  //Spezzo il valore uint16 del contatore caratteri in prima e dopo.
  split_5TE[0] = (dotpos >> 8) & 0xff;  //Parte alta
  split_5TE[1] = dotpos & 0xff;         //Parte Bassa
  
  
  // 2:2  
  if(split_5TE[0]==0x02 && split_5TE [1]==0x02){
    test1_5TE=BUILD_UINT8(databuf[0],databuf[1]);
    test1_5TE=HextoBCD(test1_5TE);
    test2_5TE=BUILD_UINT8(databuf[3],databuf[4]);
    test2_5TE=HextoBCD(test2_5TE);
    test1_5TE=test1_5TE*0x64;
    somma_5TE=test1_5TE + test2_5TE;
    asm("NOP");
  }
  
  //2:1
  else if (split_5TE[0]==0x02 && split_5TE [1]==0x01)
  {
    test1_5TE=BUILD_UINT8(databuf[0],databuf[1]);
    test1_5TE=HextoBCD(test1_5TE);
    test2_5TE=BUILD_UINT8(databuf[3],0x00);
    test2_5TE=HextoBCD(test2_5TE);
    test1_5TE=test1_5TE*0x64;
    somma_5TE=test1_5TE + test2_5TE;
    asm("NOP");
  }
  
  //2:0
  else if (split_5TE[0]==0x02 && split_5TE [1]==0x00)
  {
   
    test1_5TE=BUILD_UINT8(databuf[0],databuf[1]);
    test1_5TE=HextoBCD(test1_5TE);
    test1_5TE=test1_5TE*0x64;
    test2_5TE=BUILD_UINT16(0x00,0x00);
    somma_5TE=test1_5TE+test2_5TE;
   
  
  }
  
  //1:2
  else if(split_5TE[0]==0x01 && split_5TE [1]==0x02)
  {
    test1_5TE=BUILD_UINT8(0x00,databuf[0]);
    test1_5TE=HextoBCD(test1_5TE);
    test2_5TE=BUILD_UINT8(databuf[2],databuf[3]);
    test2_5TE=HextoBCD(test2_5TE);
    test1_5TE=test1_5TE*0x64;
    somma_5TE=test1_5TE + test2_5TE;
  }
  
  
  //1:1
  else if(split_5TE[0]==0x01 && split_5TE [1]==0x01)
  {
    
      test1_5TE=BUILD_UINT8( 0x00,databuf[0]);
      test1_5TE=HextoBCD(test1_5TE);
      test2_5TE=BUILD_UINT8(databuf[2],0x00);
      test2_5TE=HextoBCD(test2_5TE);
      test1_5TE=test1_5TE*0x64;
      somma_5TE=test1_5TE+test2_5TE;
  }
  
  //1:0
  else if(split_5TE[0]==0x01 && split_5TE [1]==0x00)
  {
    
   
      test1_5TE=BUILD_UINT8(0x00,databuf[0]);
      test1_5TE=HextoBCD(test1_5TE);
      test1_5TE=test1_5TE*0x64;
      somma_5TE=test1_5TE;
      asm("NOP");
  
  }
  
  /*
  //3:0
  else if (split_5TE[0]==0x03 && split_5TE[1]==0x00)
  {
    test1_5TE=BUILD_UINT8(0x00,databuf[0]);
    test1_5TE=HextoBCD(test1_5TE);
    test2_5TE=BUILD_UINT8(databuf[1],databuf[2]);
    test2_5TE=HextoBCD(test2_5TE);
    test1_5TE=test1_5TE*0x64;
    somma_5TE=test1_5TE + test2_5TE + test3_5TE;
  }*/
  
 if(TYPE==VWC) resbuf[0]=somma_5TE;
 else if (TYPE==TEMP)
 { 
   if(negtemp_5TE==false)
   resbuf[1]=somma_5TE;
   else
   {
    somma_5TE=-somma_5TE;
    resbuf[1]=somma_5TE;
   }
 }
 else if (TYPE==EC) resbuf[2]=somma_5TE;
 
}

#endif