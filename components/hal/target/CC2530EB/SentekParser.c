/******************************************************************************
*                                                                             *
*        **********                                                           *
*       ************                                                          *
*      ***        ***                                                         *
*     ***    ++    ***                                                        *
*     ***   +  +   ***                      CHIPCON                           *
*     ***   +                                                                 *
*     ***   +  +   ***                                                        *
*     ***    ++    ***                                                        *
*      ***        ***                                                         *
*       ************                                                          *
*        **********                                                           *
*                                                                             *
*******************************************************************************

Filename:     GS3Parser.c
Target:       cc2430
Author:       EFU
Revised:      1/3-2007
Revision:     1.1
******************************************************************************/
#if defined SENTEK_60 || defined SENTEK_90 || defined SENTEK_120 || defined SENTEK_10A || defined SENTEK_10B || defined SENTEK_10C || defined SENTEK_10D || defined SENTEK_10E || defined SENTEK_10F || defined SENTEK_30 || defined SENTEK_30_B

#include "SdiCore.h"
#include "string.h"
#include "SentekParser.h"
#include "ioCC2530.h"
#include "hal_types.h"
#include "hal_sleep.h"
#include "OSAL_Timers.h"
#include "mac_api.h"
#include "astronomic.h"
#include "stdlib.h"
#include "stdio.h"

uint8 ipb_Sentek;    //indice di buffSentekPars
//uint8 aft,bef;  //contatori caratteri prima e dopo il punto
uint8 split_Sentek[2]; //vettore contenten prima e dopo da passare a hexmultiplier
uint16 somma_Sentek=0;
bool negtemp_Sentek=false;   //tiene traccia del segno "-" nella temperatura


uint16 d1_Sentek=0;    //risultato posizione del punto nel primo dato (VWC per GS3), unisce "bef" e "aft"
uint16 d2_Sentek=0;    //risultato posizione del punto nel secondo dato (TEMP per GS3)  unisce "bef" e "aft"
uint16 d3_Sentek=0;    //risultato posizione del punto nel terzo dato (EC per GS3) unisce "bef" e "aft"


//uint8 pi;

uint8 buffSentekPars[4]={0xFF,0xFF,0xFF,0xFF}; // vettore degli indici posizioni "+" e "-"
uint8 dotn_Sentek=0;
uint8 idn_Sentek=0;    // indice per copiare valori nei vari data1_Sentek,data2_Sentek,data3_Sentek

uint8 data1_Sentek[5]={0xFF,0xFF,0xFF,0xFF,0xFF};       // VWC  (1-80) � [0,01]
uint8 data2_Sentek[5]={0xFF,0xFF,0xFF,0xFF,0xFF};            // TEMP (-40 , +50) �C [0,1]
uint8 data3_Sentek[5]={0xFF,0xFF,0xFF,0xFF,0xFF};      // EC  (0 - 25000) ms/cm

uint16 test1_Sentek;   //Variabili di appoggio per BUILD risultato finale misura
uint16 test2_Sentek;
uint16 test3_Sentek;


void hexSentekMultiplier(byte *databuf,uint16 dotpos,int16 *resbuf,uint8 internalIndex,uint8 extIndex,bool negtemp);
void SenParser(byte *buffer1,int16 *resbuf,uint8 index);
void SenParser10(byte *buffer1, int16 *resbuf,uint8 index);
void hexSentekMultiplier10 (byte *databuf,uint16 dotpos,int16 *resbuf,uint8 tenIndex,bool negtemp);


bool ParserSentek(byte *inputbuffer,int16 *resbuffer,uint8 size,uint8 idxofbuffer,uint8 type);


uint8 pivotcount=0;
uint8 tempBuffer1[10];
uint8 tempBuffer2[10];
uint8 tempBuffer3[10];
uint8 u1=0;
uint8 u2=0;
uint8 u3=0;
uint8 d1Sen=0;
uint8 d2Sen=0;
uint8 d3Sen=0;
bool negtemp1;
bool negtemp2;
bool negtemp3;

void SenParser(byte *buffer1,int16 *resbuf,uint8 index){
      
      uint8 u1=0;
      uint8 u2=0;
      uint8 u3=0;
      negtemp1=0;
      negtemp2=0;
      negtemp3=0;
      pivotcount=0;
      asm("NOP");
      for(uint8 k=0;k<30;k++){
      
      if(buffer1[k]=='+'|| buffer1[k]=='-') pivotcount++;
      
      asm("NOP");
      if(pivotcount>0 && pivotcount<2){
        if(buffer1[k]!='+'&& buffer1[k]!='-')
        {
          tempBuffer1[u1]=buffer1[k];
          u1++;
        }
        else if(buffer1[k]=='-')negtemp1=true;
      }
      if(pivotcount>=2 && pivotcount<3){
        if(buffer1[k]!='+'&& buffer1[k]!='-')
        {
          tempBuffer2[u2]=buffer1[k];
          u2++;
        }
        else if(buffer1[k]=='-')negtemp2=true;
        
      }
      if(pivotcount>=3)
      {
        if(buffer1[k]==0x0D) break;
        if(buffer1[k]!='+'&& buffer1[k]!='-')
        {
          tempBuffer3[u3]=buffer1[k];
          u3++;
        }
        else if(buffer1[k]=='-')negtemp3=true;
        
      }
      
    }
    
    
     asm("NOP");
     d1_Sentek=finddot(tempBuffer1,0x05);
     asm("NOP");
     hexSentekMultiplier(tempBuffer1,d1_Sentek,resbuf,1,index,negtemp1);
     asm("NOP");
     d2_Sentek=finddot(tempBuffer2,0x05);
     hexSentekMultiplier(tempBuffer2,d2_Sentek,resbuf,2,index,negtemp2);
     asm("NOP");
     d3_Sentek=finddot(tempBuffer3,0x05);
     hexSentekMultiplier(tempBuffer3,d3_Sentek,resbuf,3,index,negtemp3);
     asm("NOP");
     
     
      
}



void hexSentekMultiplier (byte *databuf,uint16 dotpos,int16 *resbuf,uint8 internalIndex,uint8 extIndex,bool negtemp)
{
  
  //Spezzo il valore uint16 del contatore caratteri in prima e dopo.
  split_Sentek[0] = (dotpos >> 8) & 0xff;  //Parte alta
  split_Sentek[1] = dotpos & 0xff;         //Parte Bassa
  asm("NOP");
  
  //4:0
  if (split_Sentek[0]==0x04 && split_Sentek[1]==0x00)
  {
    test1_Sentek=BUILD_UINT8(databuf[0],databuf[1]);
    test1_Sentek=HextoBCD(test1_Sentek);
    test2_Sentek=BUILD_UINT8(databuf[2],databuf[3]);
    test2_Sentek=HextoBCD(test2_Sentek);
    test1_Sentek=test1_Sentek*0x64;
    somma_Sentek=test1_Sentek + test2_Sentek;
  }
  
  //3:+
  else if (split_Sentek[0]==0x03 && split_Sentek[1]>=0x01)
  {
    test1_Sentek=BUILD_UINT8(0x00,databuf[0]);
    test1_Sentek=HextoBCD(test1_Sentek);
    test2_Sentek=BUILD_UINT8(databuf[1],databuf[2]);
    test2_Sentek=HextoBCD(test2_Sentek);
    test1_Sentek=test1_Sentek*0x64;
    somma_Sentek=test1_Sentek + test2_Sentek + test3_Sentek;
  }
  
  
  // 2:2  
  else if(split_Sentek[0]==0x02 && split_Sentek [1]==0x02){
    test1_Sentek=BUILD_UINT8(databuf[0],databuf[1]);
    test1_Sentek=HextoBCD(test1_Sentek);
    test2_Sentek=BUILD_UINT8(databuf[3],databuf[4]);
    test2_Sentek=HextoBCD(test2_Sentek);
    test1_Sentek=test1_Sentek*0x64;
    somma_Sentek=test1_Sentek + test2_Sentek;
    asm("NOP");
  }
  
  //1:3
  else if(split_Sentek[0]==0x01 && split_Sentek [1]==0x03)
  {
    asm("NOP");
    test1_Sentek=BUILD_UINT8(0x00,databuf[0]);
    test1_Sentek=HextoBCD(test1_Sentek);
    test2_Sentek=BUILD_UINT8(databuf[2],databuf[3]);
    test2_Sentek=HextoBCD(test2_Sentek);
    test1_Sentek=test1_Sentek*0x64;
    somma_Sentek=test1_Sentek + test2_Sentek;
  }
  
  if(extIndex==0x01){
    if(negtemp==true)somma_Sentek=-somma_Sentek;
    if(internalIndex==0x01)resbuf[0]=somma_Sentek;
    else if (internalIndex==0x02)resbuf[1]=somma_Sentek;
    else if (internalIndex==0x03)resbuf[2]=somma_Sentek;
  }
  else if(extIndex==0x02){
    if(negtemp==true)somma_Sentek=-somma_Sentek;
    if(internalIndex==0x01)resbuf[3]=somma_Sentek;
    else if (internalIndex==0x02)resbuf[4]=somma_Sentek;
    else if (internalIndex==0x03)resbuf[5]=somma_Sentek;
  }
  split_Sentek[0]=0x00;
 split_Sentek[1]=0x00;
}



void SenParser10(byte *buffer1,int16 *resbuf,uint8 tenIndex){
      
      uint8 u1=0;
      negtemp1=0;
      pivotcount=0;
      asm("NOP");
      uint8 k=0;
      for(k=0;k<30;k++){
      
      if(buffer1[k]=='+'|| buffer1[k]=='-') pivotcount++;
      
      asm("NOP");
      if(pivotcount>0 && pivotcount<2){
        if(buffer1[k]!='+'&& buffer1[k]!='-')
        {
          tempBuffer1[u1]=buffer1[k];
          u1++;
          if(k>= sizeof(tempBuffer1)) break;
          
        }
        else if(buffer1[k]=='-')negtemp1=true;
        
      
      }
    
     asm("NOP");
     d1_Sentek=finddot(tempBuffer1,0x05);
     asm("NOP");
     hexSentekMultiplier10(tempBuffer1,d1_Sentek,resbuf,tenIndex,negtemp1);
     asm("NOP");     
    }
}



void hexSentekMultiplier10 (byte *databuf,uint16 dotpos,int16 *resbuf,uint8 tenIndex,bool negtemp)
{
  
  //Spezzo il valore uint16 del contatore caratteri in prima e dopo.
  split_Sentek[0] = (dotpos >> 8) & 0xff;  //Parte alta
  split_Sentek[1] = dotpos & 0xff;         //Parte Bassa
  asm("NOP");
  
  //4:0
  if (split_Sentek[0]==0x04 && split_Sentek[1]==0x00)
  {
    test1_Sentek=BUILD_UINT8(databuf[0],databuf[1]);
    test1_Sentek=HextoBCD(test1_Sentek);
    test2_Sentek=BUILD_UINT8(databuf[2],databuf[3]);
    test2_Sentek=HextoBCD(test2_Sentek);
    test1_Sentek=test1_Sentek*0x64;
    somma_Sentek=test1_Sentek + test2_Sentek;
  }
  
  //3:+
  else if (split_Sentek[0]==0x03 && split_Sentek[1]>=0x01)
  {
    test1_Sentek=BUILD_UINT8(0x00,databuf[0]);
    test1_Sentek=HextoBCD(test1_Sentek);
    test2_Sentek=BUILD_UINT8(databuf[1],databuf[2]);
    test2_Sentek=HextoBCD(test2_Sentek);
    test1_Sentek=test1_Sentek*0x64;
    somma_Sentek=test1_Sentek + test2_Sentek + test3_Sentek;
  }
  
  
  // 2:2  
  else if(split_Sentek[0]==0x02 && split_Sentek [1]==0x02){
    test1_Sentek=BUILD_UINT8(databuf[0],databuf[1]);
    test1_Sentek=HextoBCD(test1_Sentek);
    test2_Sentek=BUILD_UINT8(databuf[3],databuf[4]);
    test2_Sentek=HextoBCD(test2_Sentek);
    test1_Sentek=test1_Sentek*0x64;
    somma_Sentek=test1_Sentek + test2_Sentek;
    asm("NOP");
  }
  
  //1:3
  else if(split_Sentek[0]==0x01 && split_Sentek [1]==0x03)
  {
    asm("NOP");
    test1_Sentek=BUILD_UINT8(0x00,databuf[0]);
    test1_Sentek=HextoBCD(test1_Sentek);
    test2_Sentek=BUILD_UINT8(databuf[2],databuf[3]);
    test2_Sentek=HextoBCD(test2_Sentek);
    test1_Sentek=test1_Sentek*0x64;
    somma_Sentek=test1_Sentek + test2_Sentek;
  }
  
  if(negtemp==true)somma_Sentek=-somma_Sentek;
  if(tenIndex==0x01){
    resbuf[0]=somma_Sentek;
  }
  else if (tenIndex==0x02){
    resbuf[1]=somma_Sentek;
  }
  else if (tenIndex==0x03){
    resbuf[2]=somma_Sentek;
  }
  asm("NOP");
  split_Sentek[0]=0x00;
  split_Sentek[1]=0x00;
}


uint8 signArray[4] = {1,1,1,0x00};
double nato=0;
uint16 appoggio=0;
double number=0;
int8 pipos=0;
int8 mipos=0;
const char *pi;
const char *mi;
//Buffer da analizzare, buffer di uscita, indicatore su dove riempire il buffer di uscita.
bool ParserSentek(byte *inputbuffer,int16 *resbuffer,uint8 size,uint8 idxofbuffer,uint8 type){
    
    pipos=0;mipos=0;
    //("%s\r\n",inputbuffer);
    int shift=0;
    uint8 i=0,j=0;
    uint8 cc=0;
    char * pointer;
    pi=NULL;
    mi=NULL;
    
    pi = memchr(inputbuffer,'+',size);
    mi = memchr(inputbuffer,'-',size);

    //Stringa errata
    if(pi==NULL && mi==NULL){
        //printf("Nessun segno trovato, stringa errata\r\n");
        return 0;
    }
    asm("NOP");
    pipos= pi-(char const*)inputbuffer;
    mipos= mi-(char const*)inputbuffer;
    if(pipos<0 && mipos<0)
    {
        //printf("Nessun segno trovato, stringa mal formattata\r\n");
        return 0;
    }
    asm("NOP");
    //printf("mipos: %d\r\n",mipos);
    //printf("pipos: %d\r\n",pipos);
    if(pipos>=0 && mipos>=0)
    shift = pipos<mipos?pipos:mipos;
    else if (pipos>=0 && mipos<0) shift = pipos;
    else if (mipos>=0 && pipos<0) shift = mipos;
    
    //printf("shift: %d\r\n",shift);
    memmove(inputbuffer,inputbuffer+shift,size);
    //printf("%s\r\n",inputbuffer);
    memset(signArray,0x00,sizeof(signArray));
    
    asm("NOP");
    for(i=0,j=0;i<size && j<sizeof(signArray)-1;i++)
    {
        if(inputbuffer[i]=='+') signArray[j++]=true;
        else if(inputbuffer[i]=='-') signArray[j++]=false;
    }
    i=0;j=0;
    
    //for(int k=0;k<sizeof(signArray); k++) printf("%d,",signArray[k]);
    //printf("\r\n");
    
    if(idxofbuffer==1)cc=0;
    else if (idxofbuffer==2)cc=3;
    
    pointer = strtok ((char*)inputbuffer,"+-");
    while(pointer!=NULL){
        nato=atof(pointer);
        if(type==SAL) appoggio = (uint16)nato;
        else appoggio=(uint16)(nato*100);
        if(signArray[j]==0)appoggio=-appoggio;
        //printf("VALORE PARSATO: %d\r\n",appoggio);
        resbuffer[cc]=appoggio;
        //printf("risultato in vettore [%d]: %d\r\n",cc,appoggio);
        asm("NOP");
        j++;
        cc++;
        pointer = strtok (NULL,"+-");
        asm("NOP");
    }
   j=0; 
   pointer=NULL;
   asm("NOP");
   return 1;
}

#endif