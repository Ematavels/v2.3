/******************************************************************************
*                                                                             *
*        **********                                                           *
*       ************                                                          *
*      ***        ***                                                         *
*     ***    ++    ***                                                        *
*     ***   +  +   ***                      CHIPCON                           *
*     ***   +                                                                 *
*     ***   +  +   ***                                                        *
*     ***    ++    ***                                                        *
*      ***        ***                                                         *
*       ************                                                          *
*        **********                                                           *
*                                                                             *
*******************************************************************************

Filename:     eeprom.h
Target:       cc2430
Author:       EFU
Revised:      1/3-2007
Revision:     1.1

Description:
Function declarations for common EEPROM functions for use with the CC2430DB.

All functions defined here are implemented in eeprom.c.

******************************************************************************/

#ifdef __cplusplus
extern "C"
{
#endif



/**************************************************************************************************
 * INCLUDES
 **************************************************************************************************/
#include "ZComDef.h"

/* General Define */

#if defined EPROM
#define PAGE_SIZE 32
// 64 k bit = 8192 byte = 8K Bytes
#define PROM_SIZE (0x2000)
#define BUFFER_SIZE_EEPROM 35
#endif

#if defined EPROM512
#define PAGE_SIZE 128
// 512 k bit =  65536 byte = 64K Bytes
//#define PROM_SIZE (0x10000)
#define PROM_SIZE (0xFFFF)  // tengo questa come dimensione per evitare di mettere double word per 1 byte
#define BUFFER_SIZE_EEPROM 131
#endif

#define EEPROM_WRITEADDRESS   0xA8
#define EEPROM_READADDRESS    0xA9

#define WAIT_FOR_WRITE_DELAY 0x0A  
#define EEPROM_RETRIES 0x03

/* Impostazioni divisione memoria EEPROM */

#define N_PARAMETRI  2      // paramentri che ho nel file impostazioni

#if defined WINETTX
 #define MSA_READ_LENGHT           0x0000 // 0x0000 e 0x0001 sono riservati alla lunghezza
 #define MSA_READ_ADDRESS          0x0002 // inzio dei dati da leggere
#else  
 #define MSA_SAVE_LEVEL            0x0000 // salvo il livello
 #define MSA_READ_LENGHT           0x0001 // 0x0001 e 0x0002 sono riservati alla lunghezza
 #define MSA_READ_ADDRESS          0x0003 // inzio dei dati da leggere
#endif

/******************************************************************************
* @fn  writeEEProm
*
* @brief
*      This function writes _length_ bytes of data starting at the _buffer_
*      location to the EEPROM address _address_.
*
* Parameters:
*
* @param  BYTE*    buffer
* 	  Pointer to the first byte to be written to EEPROM. _length_ bytes
*	  starting from this address will be written to EEPROM address _address_.
*
* @param  WORD	   address
*	  Target EEPROM address for write operation.
*
* @param  UINT8    length
*	  Number of bytes to be written to EEPROM.
*
* @return BYTE
*         TRUE: Write operation successful.
*         FALSE: Write operation NOT successful.
*
******************************************************************************/
extern byte writeEEProm(byte* buffer, word address, int8 length);


/******************************************************************************
* @fn  readEEProm
*
* @brief
*      Initiates sequential read of EEPROM content from the given address. The
*      read operation transfer _length_ bytes to _buffer_, starting at EEPROM
*      address _address_, ending at _adress_ + _length_.
*
* Parameters:
*
* @param  BYTE*    buffer
*	  Pointer to buffer for storing read values.
*	
* @param  WORD     address
*	  Start address for read operation. Valid range: 0 - 0xFFF.
*
* @param  UINT8    length
*	  Number of bytes to be read.
*
* @return BOOL
*         TRUE: Read operation successful.
*         FALSE: Read operation NOT successful.
*
******************************************************************************/
extern bool readEEProm(byte* buffer, word address, uint8 length);

extern void eraseSelectedEprom(word address, uint16 length);


