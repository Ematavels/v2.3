/******************************************************************************
*                                                                             *
*        **********                                                           *
*       ************                                                          *
*      ***        ***                                                         *
*     ***    ++    ***                                                        *
*     ***   +  +   ***                      CHIPCON                           *
*     ***   +                                                                 *
*     ***   +  +   ***                                                        *
*     ***    ++    ***                                                        *
*      ***        ***                                                         *
*       ************                                                          *
*        **********                                                           *
*                                                                             *
*******************************************************************************

Filename:     astronomic.h
Target:       cc2430
Author:       EFU
Revised:      1/3-2007
Revision:     1.1

Description:
Function declarations for common astronomic functions for use with the CC2430DB.

All functions defined here are implemented in astronomic.c.

******************************************************************************/

#ifdef __cplusplus
extern "C"
{
#endif

#if defined WINETHP

  /**************************************************************************************************
 * INCLUDES
 **************************************************************************************************/
#include "ZComDef.h"

/* General define */
#define ADC1115_WRITEADDRESS_1   0x90
#define ADC1115_READADDRESS_1    0x91  // default address for first ADC

#define ADC1115_WRITEADDRESS_2   0x92
#define ADC1115_READADDRESS_2    0x93  // default address for second ADC

#define ADC1115_BUFFER_SIZE 18

#define CONV_REG   0x00
#define CONFIG_REG 0x01
#define LO_THRESH  0x02
#define HI_THRESH  0x03


extern uint16 readSensorsAdc1115(uint8 selAdc, uint16 configAdc );

extern void selectAdc(uint8 numbers);

extern bool readAdc1115(byte* buffer, byte address, uint8 length);
extern byte writeAdc1115(byte* buffer, byte address, int8 length);

#endif