/**************************************************************************************************
  Filename:       _hal_spi.c
  Revised:        $Date: 2009-05-11 10:58:41
  Revision:       $Revision: 1 $

  Description: This file contains the interface to the H/W SPI driver written by Emanuele

***************************************************************************************************/

/* SPI on USART0 Alt.2 */
/*--------------------------------------------------------------------------------
Master Slave
------------- -------------
| | | |
|P1_2 SSN |--------->|SSN P1_2|
| | | |
|P1_3 SCK |--------->|SCK P1_3|
| | | |
|P1_4 MISO|<---------|MISO P1_4|
| | | |
|P1_5 MOSI|--------->|MOSI P1_5|
| | | |
------------- -------------
--------------------------------------------------------------------------------*/


/*********************************************************************
 * INCLUDES
 */

#include "hal_types.h"
#include "hal_board.h"
#include "hal_defs.h"
#include "hal_spi.h"
#include "hal_sleep.h"
#include "hal_i2c.h"


/*********************************************************************
 * LOCAL FUNCTIONS
 */


uint8 resp;




/******************************************************************************
                                SPI INITIALIZATION
*******************************************************************************/

void SpiInit()
{
  PERCFG = 0x00; // PERCFG.U0CFG = 0
  P0SEL |= 0x2C;  // P0_5, P0_3, and P0_2 are peripherals
  P0SEL &= ~0x10; // P0_4 output
  P0DIR |= 0x10;  // P0_4 is set as output

  uint8 baud_exponent = 0x0D;  // per impostare 300Khz
  uint8 baud_mantissa = 0x3B;

  /* Configure SPI */
  U0UCR  = 0x80;      /* Flush and goto IDLE state. 8-N-1. */
  U0CSR  = 0x00;      /* SPI mode, master. */
  U0GCR  = HAL_SPI_TRANSFER_MSB_FIRST | HAL_SPI_CLOCK_PHA_0 | HAL_SPI_CLOCK_POL_LO | baud_exponent;
  U0BAUD = baud_mantissa;

  // USART0 Control and Status
  U0CSR |= 0x40; // receiver enable
}

/******************************************************************************
                                SPI COMMANDS
*******************************************************************************/

uint8 SPIWriteByte(uint8 byte)
{
  
  U0CSR &= ~(BV(2) | BV(1));
  U0DBUF = byte;
  //Si blocca qui se problemi con sd
  //while ( !(U0CSR & BV(1)) && (spit<100) )spit++; // wait TX
  while( !(U0CSR & BV(1)))// wait TX
  
  asm("NOP");
  asm("NOP");
  asm("NOP");
  asm("NOP");
  asm("NOP");
  asm("NOP");
  asm("NOP");
  asm("NOP");
  
//if (spit>=99) return 0xFF;
//else return U0DBUF;
return U0DBUF;

}

uint8 SPIReadByte(void){

  uint8 byte = 0;

  U0CSR &= ~(BV(2) | BV(1));
  U0DBUF = 0xFF;
  while( !(U0CSR & BV(1)) ); // wait TX

  /* Read the received byte */
  byte = U0DBUF;
  
  return byte;
}

void SPIWrite(uint8* data, uint16 length)
{
  for(uint8 i = 0; i < length; i++)
    SPIWriteByte(data[i]);
}

void SPIRead(uint8* data, uint16 length)
{
  for(uint8 i = 0; i < length; i++)
    data[i]=SPIReadByte();
}

