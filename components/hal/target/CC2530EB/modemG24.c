/******************************************************************************
*                                                                             *
*        **********                                                           *
*       ************                                                          *
*      ***        ***                                                         *
*     ***    ++    ***                                                        *
*     ***   +  +   ***                      CHIPCON                           *
*     ***   +                                                                 *
*     ***   +  +   ***                                                        *
*     ***    ++    ***                                                        *
*      ***        ***                                                         *
*       ************                                                          *
*        **********                                                           *
*                                                                             *
*******************************************************************************

Filename:     eeprom.c
Target:       cc2430
Author:       EFU
Revised:      1/3-2007
Revision:     1.1


******************************************************************************/
#if defined WINETTX

#include "modem.h"
#include "ioCC2530.h"
#include "hal_timer.h"
#include "ZComDef.h"
#include "hal_uart.h"
#include "hal_drivers.h"
#include "hal_led.h"
#include "OSAL_Memory.h"
#include "OSAL_Timers.h"
#include "OSAL.h"
#include "hal_mcu.h"
#include "hal_defs.h"
#include "hal_types.h"
#include "eeprom.h"
#include "astronomic.h"
#include "hal_i2c.h"
#include "CoordFrane.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>


// local functions
void resetBuffer(void);
void resetTempBuffer(void);
void uartResponse(void);
void StrToHex(char* string);
bool storeAndSendData(void);
void saveDataFromFile(void);
void saveDataOra(void);
void BytesToChar(uint16 lenghtBytes,uint8 startAdd);
void CharToBytes (int* bufferToConv,uint8 p);
uint8 searchOk(void);
uint8 searchResp(char* codeResp);
void modemOnOff(uint8 setOnOff);

// response code
char userRsp[6]="333331";
char passRsp[6]="323330";
char storeRsp[6]="313530";
char close2[6]="323236";
char ftpCode[6]="323230";
char passCode[6]="323237";
char push1[6]="USH: 1";
char push2[6]="USH: 2";
char mipOpen[6]="PEN: 1";
char mipOpen2[6]="PEN: 2";
char mipSend2[6]="END: 2";
char mipSend1[6]="END: 1";
char mipCall[6]="MIPCAL";
char mipClose[6]="MIPCLO";
char closeConn[6]="NO CAR";
char downloadCode[6]=": 2,0,";
char downloadCode2[6]=": 1,0,";
char endFile[6]={0x0D,0x0A,0x0D,0x0A,0x2B,0x4D};
uint8 startOfFile;

// operatore
char windRsp[6]="I WIND";
char vodaRsp[6]="vodafo";
char timRsp[6]={0x22,0x49,0x20,0x54,0x49,0x4D}; // I TIM con "

// variabili per l'acquisizione seriale
uint16 rxLen=0,rxLenTemp=0;
uint8 rxBufModem[RX_CNT];
uint8 sendModem=0;
uint8 firstPart=0,secondPart=0;
uint8 readFailed=0;

// li creo dopo avere convertito il testo in valori adatti al modem
char SENDBUFFER[SIZE+MAX_CMD_LENGHT]; // lo utilizzo per tutte gli altri comandi, AT+MIPSEND=1,AT+MIPSEND=2,OPENSOCK2,.. visto che il comando � sempre lo stesso
char tempBuffer[SIZE];  // utilizzato nella strtohex per memorizzare il valore convertito

char base[4], offset[4];

// porta su cui aprire la socket 2
uint16 porta;

// Ip Address del sito collegato
//uint8 IpAddress[12];

// variabili per i retry e per l'avanzamento comandi
uint8 cmdSent=0xFF,pushType=0x00;

bool noMoreData=FALSE;
bool downloadFile=FALSE;
bool respOk=FALSE;
bool firstOn=TRUE;
uint8 errorRetry=0; // conta errori 

uint16 readAddress=MSA_READ_ADDRESS; // init dell'indirizzo da dove leggere i dati

uint8 contRead=1; // variabile che mi conta quante send successive faccio prima del push
uint16 lenghtData,totLenghtData=0; // memorizzo la lunghezza dei dati da spedire
uint8 nRetries=N_RETRIES;
uint8 position=0;

uint8 clearBuffer[1];
uint8 watchDog[2]={0x7F,0x00};


uint8 Modem_TaskID;

/**************************************************************************************************************/
// VALORI DA IMPOSTARE

// gestore telefonico e sito ftp dove caricare
char CONNECTVODA[]="AT+MIPCALL=1,\"web.omnitel.it\"\r";
char CONNECTWBIZ[]="AT+MIPCALL=1,\"internet.wind.biz\"\r";
char CONNECTW[]="AT+MIPCALL=1,\"internet.wind\"\r";
char CONNECTTIM[]="AT+MIPCALL=1,\"ibox.tim.it\"\r";
char CONNECT[35];

// ATTENZIONE - cambiare anche a riga 489 - per il push del pasv
//char OPENSOCK[]="AT+MIPOPEN=1,3348,\"www.winetsrl.com\",21,0\r";
//char OPENSOCK[]="AT+MIPOPEN=1,3348,\"icoworld.serveftp.com\",21,0\r";
//char OPENSOCK[]="AT+MIPOPEN=1,3348,\"213.188.197.38\",21,0\r"; // volterra
char OPENSOCK[]="AT+MIPOPEN=1,3348,\"winetsrl.serveftp.com\",21,0\r";


// user e password
//char USER[]="USER winetsrl.com";
//char USER[]="USER winet";
//char USER[]="USER ricasoli";
//char USER[]="USER apofruit";
//char USER[]="USER emanueleHK";
//char USER[]="USER marcoHK";
//char USER[]="USER castello";
char USER[]="USER marco.barla";
//char USER[]="USER pollaccia";
//char USER[]="USER apofruit2";


//char PASS[]="PASS uuraekac";
//char PASS[]="PASS 21Giugno03";
//char PASS[]="PASS ricasoli14!"; // dati_10
//char PASS[]="PASS pietrasanta14!";  // dati_12
//char PASS[]="PASS marcoHK!";
//char PASS[]="PASS emanueleHK!"; // Dati_7.txt
//char PASS[]="PASS apofruit";  // Dati_5.txt
char PASS[]="PASS marcoB!"; // Dati_13.txt
//char PASS[]="PASS pollaccia14!"; // Dati_11.txt
//char PASS[]="PASS apofruit2!";  // Dati_9.txt

char FILE[]="STOR dati_13.txt";
char FILE2[]="RETR dataora.txt";
char FILE3[]="RETR impostazioni.txt";

// directory
//char CWD[]="CWD /htdocs/provaEma";
//char CWD[]="CWD /htdocs";
char CWD[]="CWD /";

// lasciare stare cosi
char PWD[]="PWD";
char PASV[]="PASV";

  // Comandi Modem
//char INIT1[] = "AT\r"; // valuto se modem risponde e chiedo anche orario all'operatore
//char TIMEMS[]="ATS102=0\r"; // tempo in ms da wake up a tx comando
char CSQ[]="AT+CSQ\r";
// verifica la qualit� del GPRS - I paramentro � strenght va da 0 (-113dbm) a 31 (-5dbm), avere un 20 indicativamente.
// II parametro � la ber, va da 0 a 7, 99 significa che la rete gsm non lo comunica

char DATAORA[]="AT+CCLK?\r";
char ECHO[]="ATE0\r";
//char ECHO[]="AT+IPR=115200\r";
char ATH[]="ATH\r";
char GPRS[]="AT+CGPRS\r";
char CHECKOPER[]="AT+COPS?\r";
char CHECKSOCK[] = "AT+MIPOPEN?\r";
char CLOSESOCK [] = "AT+MIPCLOSE=1\r";
char CLOSESOCK2[]="AT+MIPCLOSE=2\r";
char PUSH[]="AT+MIPPUSH=1\r";
char PUSH2[]="AT+MIPPUSH=2\r";
char CLOSECONN[]="AT+MIPCALL=0\r";
char RESET[]="AT+MRST\r";


/* Funzione per accendere/spegnere il modem
 * se invio 1 = modem ON *
 * se invio 0 = modem OFF *
 **************************/
void modemOnOff(uint8 setOnOff)
{

if (setOnOff==1)
 {
  ENABLE_MODEM();  // alimento il modem
  halWait(200);
  // devo impostare uscita alta e poi tenerla bassa per un tempo maggiore di 500ms e minore di 1500ms
  MODEM_ON_N=0x01;
  // attendo che sia stabile 500ms come da datasheet G30
  halWait(250);
  halWait(250);
  MODEM_ON_N=0x00;
  // attendo 1 secondo e modem on
  halWait(250);
  halWait(250);
  halWait(250);
  halWait(250);
  // la ripristino ad 1 ed il modem si accende ( devo tenerla a 1 per comunicare con modem e non SD card )
  MODEM_ON_N=0x01;
  // attendo 1.5 secondi altrimenti non � pronto il modem
  halWait(250);
  halWait(250);
  halWait(250);
  halWait(250);
  halWait(250);
  halWait(250);
  halWait(250);
  halWait(250);
 }
 else if (setOnOff==0)
 {
   MODEM_ON_N=0x00;

   // attendo 3 secondi e modem off
   halWait(250);
   halWait(250);
   halWait(250);
   halWait(250);
   halWait(250);
   halWait(250);
   halWait(250);
   halWait(250);
   halWait(250);
   halWait(250);
   halWait(250);
   halWait(250);
   // la ripristino ad 1 ed il modem si spegne
   MODEM_ON_N=0x01;
   // attendo come da datasheet 2.5 sec affinch� faccia tutte le chiusure
   halWait(250);
   halWait(250);
   halWait(250);
   halWait(250);
   halWait(250);
   halWait(250);
   halWait(250);
   halWait(250);
   halWait(250);
   halWait(250);
   halWait(250);
   halWait(250);
   //MODEM_ON_N=0x00;
   //DISABLE_MODEM();  // spengo alimentazione del modem
   // faccio nello sleep
 }
}


void Modem_Init( uint8 task_id )
{
  /* Register task ID */
  Modem_TaskID = task_id;

  /*Setting the connection on port1*/
  halUARTCfg_t uartConfig;

  uartConfig.configured           = TRUE;         // 2430 don't care.
  uartConfig.baudRate             = UART_BAUD;
  uartConfig.flowControl          = TRUE;
  uartConfig.flowControlThreshold = UART_THRESH;
  uartConfig.rx.maxBufSize        = UART_RX_MAX;
  uartConfig.tx.maxBufSize        = UART_TX_MAX;
  uartConfig.idleTimeout          = UART_IDLE; // 2430 don't care.
  uartConfig.intEnable            = TRUE;      // 2430 don't care.

  #if defined WINETTX
  uartConfig.callBackFunc         = rxCB;    // callback for serial rx event
  #else
  uartConfig.callBackFunc         = NULL;      // callback for serial rx event
  #endif

  // open UART
  HalUARTOpen(HAL_UART_PORT_0,&uartConfig);

  resetBuffer(); // pulisco il buffer

}

void resetTempBuffer()
{
  for (uint16 i=0;i<(SIZE+MAX_CMD_LENGHT);i++) {
   SENDBUFFER[i]='\0';
  }
  for (uint16 i=0;i<(SIZE);i++) {
   tempBuffer[i]='\0';
  }
}


void resetBuffer()
{
 rxLen=0;
 rxLenTemp=0;

  for (uint16 i=0;i<RX_CNT;i++)
   rxBufModem[i]='\0';

}

uint16 Modem_ProcessEvent( uint8 task_id, uint16 events )
{
  
if (events & ERROR_EVENT)
  {
   // disabilito il collegamento seriale in modo che se dovessi ricevere ora al limite del timer non lo processo
   MODEM_ON_N=0x00;
   cmdSent=0xFF; // init
  
   resetTempBuffer(); // resetto il buffer
   resetBuffer(); // resetto anche buffer seriale

   noMoreData=FALSE;
   nRetries=N_RETRIES; // reinit della nRetries
   contRead=1; // init delle variabili per il push dati
   downloadFile=FALSE; // re init delle variabile per download
   readAddress=MSA_READ_ADDRESS; // rinit del readAddress

   // se entro qui 10 volte nonostante i vari reinit errore grave e resetto il nodo ( seriale non recuperabile )
   // tranne se GPRS non prende
   if (cmdSent!=0x03) errorRetry++;
   
    if (errorRetry<10)
         osal_start_timerEx(MSA_TaskId, MSA_SLEEP_EVENT, 10); // lasciare 10ms per evitare problemi cambio task
   else HAL_SYSTEM_RESET();
   
  return events ^ ERROR_EVENT;
  }


  if (events & RETRY_EVENT )
  {
     // effettuo n tentativi di connessione GPRS
    cmdSent=0x03;
    osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
  return events ^ RETRY_EVENT;
  }

  if(events & DATA_RX )
  {
  uartResponse();
  return events ^ DATA_RX;
  }

  if(events &  NEXT_CMD_MODEM )
  {
    
    // Set Watch-dog procedure
    readAstronomic(clearBuffer,0x0F,1); 
    writeAstronomic(watchDog,0x09,2);
       
 switch(cmdSent)
 {
  /* case 0x01:  // comando AT per verificare che il modem risponda
  osal_start_timerEx ( Modem_TaskID, ERROR_EVENT, TIME_ERROR); // faccio partire un timer entro il quale devo avere risposta
  cmdSent=0x01;
  HalUARTWrite( HAL_UART_PORT_0, (uint8*)INIT1, strlen((char*)INIT1) );
  break; */

 case 0x02:  // comando AT per disabilitare l'echo
  modemOnOff(1);   // accendo il modem
  osal_start_timerEx ( Modem_TaskID, ERROR_EVENT, TIME_ERROR);
  HalUARTWrite( HAL_UART_PORT_0, (uint8*)ECHO, strlen((char*)ECHO) );
 break;

 case 0x03:  // comando AT per fare un check del segnale GPRS
  if (nRetries==N_RETRIES) osal_start_timerEx ( Modem_TaskID, ERROR_EVENT, TIME_ERROR);

  // faccio partire un timer solo la 1 volta entro il quale devo avere risposta
  // � da 20 secondi quindi superiore al tempo dei retries
  HalUARTWrite( HAL_UART_PORT_0, (uint8*)GPRS, strlen((char*)GPRS));
 break;

 case 0x3A:  // comando AT per vedere l'operatore
  osal_start_timerEx ( Modem_TaskID, ERROR_EVENT, TIME_ERROR);
  HalUARTWrite( HAL_UART_PORT_0, (uint8*)CHECKOPER, strlen((char*)CHECKOPER));
  //HalUARTWrite( HAL_UART_PORT_0, (uint8*)CSQ, strlen((char*)CSQ));
 break;

 case 0x04:  // comando AT per creare il link
  osal_start_timerEx ( Modem_TaskID, ERROR_EVENT, TIME_ERROR);
  HalUARTWrite( HAL_UART_PORT_0, (uint8*)CONNECT, strlen((char*)CONNECT));
 break;

 /*
 case 0x05:  // comando AT per fare check della socket
  osal_start_timerEx ( Modem_TaskID, ERROR_EVENT, TIME_ERROR);
  HalUARTWrite( HAL_UART_PORT_0, (uint8*)CHECKSOCK, strlen((char*)CHECKSOCK));
 break;

 case 0x06:  // comando AT per chiudere socket
  osal_start_timerEx ( Modem_TaskID, ERROR_EVENT, TIME_ERROR);
  HalUARTWrite( HAL_UART_PORT_0, (uint8*)CLOSESOCK,  strlen((char*)CLOSESOCK));
 break;
*/
 case 0x07:  // comando AT per aprire la socket
  osal_start_timerEx ( Modem_TaskID, ERROR_EVENT, TIME_ERROR);
  HalUARTWrite( HAL_UART_PORT_0, (uint8*)OPENSOCK, strlen((char*)OPENSOCK));
 break;

 case 0x08:  // comando AT per inviare l'username
  osal_start_timerEx ( Modem_TaskID, ERROR_EVENT, TIME_ERROR);  // timer sia per username che per password
  StrToHex(USER);
  // inserisco il comando convertito nel buffer
  sprintf((char*)&SENDBUFFER,(char const*)"AT+MIPSEND=1,\"%s0d0a\"\r",tempBuffer);
  HalUARTWrite( HAL_UART_PORT_0, (uint8*)SENDBUFFER, strlen((char*)SENDBUFFER));
 break;

  case 0x09:  // comando AT per inviare la password
  StrToHex(PASS);
   // inserisco il comando convertito nel buffer
  sprintf((char*)&SENDBUFFER,(char const*)"AT+MIPSEND=1,\"%s0d0a\"\r",tempBuffer);
  HalUARTWrite( HAL_UART_PORT_0, (uint8*)SENDBUFFER, strlen((char*)SENDBUFFER));
 break;

 /* case 0x0A:  // comando AT per inviare il CWD
  StrToHex(CWD);
   // inserisco il comando convertito nel buffer
  sprintf((char*)&SENDBUFFER,(char const*)"AT+MIPSEND=1,\"%s0d0a\"\r",tempBuffer);
  osal_start_timerEx ( Modem_TaskID, ERROR_EVENT, TIME_ERROR);
  HalUARTWrite( HAL_UART_PORT_0, (uint8*)SENDBUFFER, strlen((char*)SENDBUFFER));
 break;

 case 0x0B:  // comando AT per inviare il PWD
  StrToHex(PWD);
  // inserisco il comando convertito nel buffer
  sprintf((char*)&SENDBUFFER,(char const*)"AT+MIPSEND=1,\"%s0d0a\"\r",tempBuffer);
  osal_start_timerEx ( Modem_TaskID, ERROR_EVENT, TIME_ERROR);
  HalUARTWrite( HAL_UART_PORT_0, (uint8*)SENDBUFFER, strlen((char*)SENDBUFFER));
 break; */

 case 0x0C:  // comando AT per inviare il Passive Mode
  osal_start_timerEx ( Modem_TaskID, ERROR_EVENT, TIME_ERROR); // timer che fermer� dopo il push del passive mode
  StrToHex(PASV);
   // inserisco il comando convertito nel buffer
  sprintf((char*)&SENDBUFFER,(char const*)"AT+MIPSEND=1,\"%s0d0a\"\r",tempBuffer);
  HalUARTWrite( HAL_UART_PORT_0, (uint8*)SENDBUFFER, strlen((char*)SENDBUFFER));
 break;

 case 0x0D:  // comando AT per inviare i dati su server ( passive mode )
  HalUARTWrite( HAL_UART_PORT_0, (uint8*)PUSH, strlen((char*)PUSH));
 break;

 case 0x0E: // comando per aprire la socket2
  osal_start_timerEx ( Modem_TaskID, ERROR_EVENT, TIME_ERROR);
  HalUARTWrite( HAL_UART_PORT_0, (uint8*)SENDBUFFER, strlen((char*)SENDBUFFER));
  break;

  case 0x0F: // comando per fare lo stor del nome file
  osal_start_timerEx ( Modem_TaskID, ERROR_EVENT, TIME_ERROR);
  // timer che fermer� dopo il push del nome file
  StrToHex(FILE);
  // inserisco il comando convertito nel buffer
  sprintf((char*)&SENDBUFFER,(char const*)"AT+MIPSEND=1,\"%s0d0a\"\r",tempBuffer);
  HalUARTWrite( HAL_UART_PORT_0, (uint8*)SENDBUFFER, strlen((char*)SENDBUFFER));
  break;

  case 0x10:  // dati
   osal_start_timerEx ( Modem_TaskID, ERROR_EVENT, TIME_ERROR); // faccio partire qui cosi se ho problemi nelle fasi successive sono protetto
   if (storeAndSendData()==0) // se � tutto ok ( valore ritornato � 0 )
   {
   StrToHex(tempBuffer);
   // inserisco il comando convertito nel buffer
   sprintf((char*)&SENDBUFFER,(char const*)"AT+MIPSEND=2,\"%s\"\r",tempBuffer);
   HalUARTWrite( HAL_UART_PORT_0, (uint8*)SENDBUFFER, strlen((char*)SENDBUFFER));
   }
   else
   {
     if (readFailed==3) // se per 3 volte consecutive leggo FFFF non riprovo e vado a chiudere
     {
      noMoreData=FALSE; // la renit
      contRead=1; // init delle variabili per il push dati
      cmdSent=0x14;
      osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
     }
     else  // riprovo la lettura dei dati
     {
       readFailed++;
       storeAndSendData();
     }
   }
  break;

  case 0x11:  // comando AT per inviare i dati su server ( push dati )
  osal_start_timerEx ( Modem_TaskID, ERROR_EVENT, TIME_ERROR);
  HalUARTWrite( HAL_UART_PORT_0, (uint8*)PUSH2, strlen((char*)PUSH2));
 break;

  case 0x12:  // comando AT per il download del file impostazioni
  osal_start_timerEx ( Modem_TaskID, ERROR_EVENT, TIME_ERROR);
  // timer che fermer� dopo il push del file
  StrToHex(FILE3);
  // inserisco il comando convertito nel buffer
  sprintf((char*)&SENDBUFFER,(char const*)"AT+MIPSEND=1,\"%s0d0a\"\r",tempBuffer);
  HalUARTWrite( HAL_UART_PORT_0, (uint8*)SENDBUFFER, strlen((char*)SENDBUFFER));
 break;

 case 0x13:  // comando AT per chiudere connessione dati - socket 2
  osal_start_timerEx ( Modem_TaskID, ERROR_EVENT, TIME_ERROR);
  HalUARTWrite( HAL_UART_PORT_0, (uint8*)CLOSESOCK2, strlen((char*)CLOSESOCK2));
 break;

 case 0x14:  // comando AT per chiudere connessione socket 1
  osal_start_timerEx ( Modem_TaskID, ERROR_EVENT, TIME_ERROR);
  // timer che fermo direttamente dopo la disconnessione con operatore
  HalUARTWrite( HAL_UART_PORT_0, (uint8*)CLOSESOCK, strlen((char*)CLOSESOCK));
 break;

 case 0x15:  // comando AT per il download del file impostazioni
  osal_start_timerEx ( Modem_TaskID, ERROR_EVENT, TIME_ERROR);
  // timer che fermer� dopo il push del file
  StrToHex(FILE2);
  // inserisco il comando convertito nel buffer
  sprintf((char*)&SENDBUFFER,(char const*)"AT+MIPSEND=1,\"%s0d0a\"\r",tempBuffer);
  HalUARTWrite( HAL_UART_PORT_0, (uint8*)SENDBUFFER, strlen((char*)SENDBUFFER));
 break;

 case 0x16:  // comando AT per chiudere connessione socket 1
  HalUARTWrite( HAL_UART_PORT_0, (uint8*)CLOSECONN, strlen((char*)CLOSECONN));
 break;

 case 0xAA:  // comando AT per inviare i dati ( push su socket 1)
  HalUARTWrite( HAL_UART_PORT_0, (uint8*)PUSH, strlen((char*)PUSH));
 break;

  default:
   break;
 }

  resetTempBuffer(); // resetto il buffer

  return events ^ NEXT_CMD_MODEM;
 }

 return 0;

}



void rxCB( uint8 port, uint8 event )
{
  if (( rxLen = HalUARTRead( HAL_UART_PORT_0, &rxBufModem[rxLenTemp], RX_CNT-rxLenTemp ) ) != 0 ) // ho ricevuto qualcosa
  {
  if (rxLenTemp==0) osal_start_timerEx(Modem_TaskID,DATA_RX,120);  // parte il timer seriale di 120ms sto in ascolto e salvo i dati
  rxLenTemp=rxLen+rxLenTemp; // salvo dove sono arrivato
  if (rxLenTemp>RX_CNT) 
  { // se sforo i 300 bytes vado in errore
      asm("NOP");
      osal_stop_timerEx(Modem_TaskID,DATA_RX); 
      osal_start_timerEx(Modem_TaskID,ERROR_EVENT,10); // error event 
   }
 }
}


void uartResponse()
{
  uint8 j=0,k=0;
  uint8 virgola=0,uno=0,due=0;

  asm("NOP");
  
 switch(cmdSent)
 {
  case 0x01:  // at ok
    if ( searchOk() ) // ho trovato l'ok proseguo
     {
        osal_stop_timerEx (Modem_TaskID, ERROR_EVENT);
        cmdSent=0x02;
        osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
     }
   break;

  case 0x02: // ate0 ok
    if ( searchOk() ) // ho trovato l'ok proseguo
    {
    osal_stop_timerEx (Modem_TaskID, ERROR_EVENT);

    cmdSent=0x03;
    osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, TIME_OUT_GPRS);  
    }
    break;

    case 0x03: // verifica gprs ( fa 3 tentativi da 4 secondi )
      if ( (rxBufModem[10]=='1') && (rxBufModem[15]=='O') && (rxBufModem[16]=='K') )
      {
        osal_stop_timerEx (Modem_TaskID, ERROR_EVENT); // risposta da modem arrivata

        nRetries=N_RETRIES;
       
        if (firstOn==TRUE)
        {
          cmdSent=0x3A;
          // verifico l'operatore dopo 15 secondi altirmenti non � pronto
          osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, 15000);
        }
        else
        {
          cmdSent=0x04;
          osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, TIME_OUT_GPRS);
        }
      }
      else
      {
        if ( nRetries-- > 0 )
        {
        // se nRetries = 0 scadr� il error event
        // non fermo ERROR_EVENT perch� tanto i 3 tentativi impiegano meno del time error
        osal_start_timerEx ( Modem_TaskID, RETRY_EVENT, TIME_OUT_GPRS);
        }
      }
    break;

   // operatore - lo faccio solo la prima volta che accendo
   case 0x3A:
    if ( searchResp(windRsp) ) // ho trovato l'operatore wind business
    {
     strncpy(CONNECT,CONNECTWBIZ, strlen((char*)CONNECTWBIZ) );
     respOk=TRUE;
    }
    else
    {
      if ( searchResp(vodaRsp) )  // ho trovato l'operatore vodafone
      {
      strncpy(CONNECT,CONNECTVODA, strlen((char*)CONNECTVODA));
      respOk=TRUE;
      }
      else
      {
       if ( searchResp(timRsp) )  // ho trovato l'operatore tim
        {
         strncpy(CONNECT,CONNECTTIM, strlen((char*)CONNECTTIM));
         respOk=TRUE;
        }
      }
    }
    
    if (respOk==TRUE)  // operatore riconosciuto proseguo
    {

      osal_stop_timerEx (Modem_TaskID, ERROR_EVENT);

      // ok operatore allora vado a connettermi gprs
      cmdSent=0x04;

      osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
     }
    else
    {
      HalLedSet (HAL_LED_1, HAL_LED_MODE_ON); // segnalo che il GPRS non prende
      for(uint8 i=0;i<20;i++) halWait(250);  // con led fisso 5 secondi e poi resetto
      HAL_SYSTEM_RESET();
    }
    break;


   case 0x04:  // connect to server
    asm("NOP");
    if(searchResp(mipCall))
    {
    osal_stop_timerEx (Modem_TaskID, ERROR_EVENT);

     // vado direttamente alla opensocket ( server winet la chiude in auto dopo tot tempo )
     cmdSent=0x07;
     osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
    }
    break;


 /* case 0x05:  // check socket e retry
   if ( (rxBufModem[8]=='E')&&(rxBufModem[9]=='N')&&(rxBufModem[12]=='1'))
   {
      osal_stop_timerEx (Modem_TaskID, ERROR_EVENT);

      cmdSent=0x07;
    osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
    }
    else // provo a chiudere la socket 1 ed a riaprirla
    {
      // cmd 0x06 close e reopen socket
      osal_stop_timerEx (Modem_TaskID, ERROR_EVENT);

     cmdSent=0x06;
    osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
    }
    break;


  case 0x06:
     if((rxBufModem[3]=='M')&&(rxBufModem[4]=='I')&&(rxBufModem[8]=='O')&&(rxBufModem[9]=='S')&&(rxBufModem[13]=='1'))
     {
       osal_stop_timerEx (Modem_TaskID, ERROR_EVENT);

        cmdSent=0x07;
    osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
     }
    break; */

   case 0x07:  // connessione ftp
    if(searchResp(ftpCode))
     {
      osal_stop_timerEx (Modem_TaskID, ERROR_EVENT);

      cmdSent=0x08;
      osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);

   }
    break;

  case 0x08:  // invio username al modem
    if ( searchOk() ) // ho trovato l'ok proseguo
     {

      pushType=0x01; // pushtype user
      cmdSent=0xAA;
      osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
    }
    break;

   case 0x09:  // invio password al modem
   if ( searchOk() ) // ho trovato l'ok proseguo
   {

      pushType=0x02; // pushtype password
      cmdSent=0xAA;
      osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
   }
    break;

 /*case 0x0A:  // invio cwd al modem
    if ( searchOk() ) // ho trovato l'ok proseguo
     {
      osal_stop_timerEx (Modem_TaskID, ERROR_EVENT);

      pushType=0x03; // pushtype cwd
     cmdSent=0xAA;
    osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
    }
  break;

   case 0x0B:  // invio pwd al modem
     if ( searchOk() ) // ho trovato l'ok proseguo
     {
      osal_stop_timerEx (Modem_TaskID, ERROR_EVENT);

      pushType=0x04; // pushtype pwd
      cmdSent=0xAA;
    osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
    }
    break; */


  case 0x0C:  // invio passive mode al modem
    if (searchResp(mipSend1))
    {
      cmdSent=0x0D;
      osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
    }
    break;

  case 0x0D: // push del passive mode
   if(searchResp(passCode))
    {

   for(uint8 i=0;i<4;i++)
    {
    base[i]='\0';
    offset[i]='\0';
    }

    for(uint16 i=0; i<RX_CNT-1; i++)
        {
        if((rxBufModem[i]=='2' && rxBufModem[i+1]=='C') || (rxBufModem[i]=='2' && rxBufModem[i+1]=='9'))
        virgola++;

        if(virgola==4)
            {
            i++;
            base[j]=rxBufModem[i+2];
            j++;
            }

        if(virgola==5)
            {
            i++;
            offset[k]=rxBufModem[i+2];
            k++;
            }
        }//for


    // Scrittura dei due numeri letti dal pacchetto: Base = P1 e Offset = P2
    base[j-1]='\0';
    offset[k-1]='\0';

    // Conversione da tipo stringa a tipo intero
    uno=atoi(base);
    due=atoi(offset);

    // Calcolo del numero di PORTA a cui connettersi
    porta=uno*256+due;

    // inserisco il comando nel buffer
    sprintf((char*)&SENDBUFFER,(char const*)"AT+MIPOPEN=2,3349,\"winetsrl.serveftp.com\",%u,0\r",porta);

    osal_stop_timerEx (Modem_TaskID, ERROR_EVENT); // fermo il timer qui cosi sono sicuro che ha fatto tutte le operazioni
    cmdSent=0x0E;
    osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
    }
   break;

  case 0x0E: // apertura socket dati ( socket 2 )
    if(searchResp(mipOpen2))
     {
      osal_stop_timerEx (Modem_TaskID, ERROR_EVENT);

      if (firstOn==TRUE)
      {
        cmdSent=0x15;
        osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
      }
      else
      {
        if (downloadFile)
        {
        cmdSent=0x12;
        osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
        }  // se sono al download devo dire al server quale � il file da scaricare
        else {
        cmdSent=0x0F;
        osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND); }
        }
     }
    // N.B. se non va vuol dire che server � impallato!!
    break;

   case 0x0F:  // stor del nome del file
    if ( searchOk() ) // ho trovato l'ok proseguo
    {
      // salvo la lunghezza dei dati che voglio trasmettere, mi servir� nell'erase eeprom
      totLenghtData=lenghtData;

      pushType=0x05; // pushtype stor
      cmdSent=0xAA;
      osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
    }
    break;


   case 0x10:  // invio dati
     if (searchResp(mipSend2) ||  (searchOk()) )
     {
      osal_stop_timerEx (Modem_TaskID, ERROR_EVENT);

      if ( (noMoreData==TRUE) || (contRead>=MAX_PACKET_READ) )  // ho finito i dati da inviare o ho inviato il massimo
       {
        cmdSent=0x11;
        osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
       }
       else {
         contRead++;
         cmdSent=0x10;
         osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
       }

    }
    break;

  case 0x11: // push dei dati
    if (searchResp(push2))
    {
     osal_stop_timerEx (Modem_TaskID, ERROR_EVENT);

     contRead=1; // la reinit

     if (noMoreData==TRUE)  // ho finito i dati da inviare
        {
         noMoreData=FALSE; // la renit
         downloadFile=TRUE; // imposto il download del file
         cmdSent=0x13;
         osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
        }
       else {
         cmdSent=0x10;
         osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
       }
    }
   break;

   case 0x12: // download del file di impostazioni
    if ( searchOk() ) // ho trovato l'ok proseguo
    {

     pushType=0x06; // pushtype stor
     cmdSent=0xAA;
     osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
    }
    break;

     case 0x13: // close  socket 2 dati
     if (searchResp(close2)) // closing data connection
     {
     osal_stop_timerEx (Modem_TaskID, ERROR_EVENT);

     if (downloadFile) {
       cmdSent=0x0C;
       osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
        }  // vado a fare il download altrimenti fine
     else {
       cmdSent=0x14;
       osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
        }
    }
    break;

    case 0x14: // close socket 1
     if (searchResp(mipClose))
     {
     cmdSent=0x16;
     osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND); // non faccio il save data e ora durante invio dati
     }
     break;

     case 0x15:  // data e ora
     if ( searchOk() ) // ho trovato l'ok proseguo
     {
      pushType=0x07; // pushtype data ora file
      cmdSent=0xAA;
      osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
     }
    break;

   case 0x16: // close connection con operatore
     asm("NOP");
     if (searchResp(closeConn))
     {
     // disabilito il collegamento seriale in modo che se dovessi ricevere ora al limite del timer non lo processo
      MODEM_ON_N=0x00;
      nRetries=N_RETRIES;
      cmdSent=0xFF; // init 
  
      resetTempBuffer(); // resetto il buffer
      resetBuffer(); // resetto anche buffer seriale

      readAddress=MSA_READ_ADDRESS; // rinit del readAddress
      eraseSelectedEprom(MSA_READ_LENGHT,totLenghtData+2);  // erase dei dati + 2byte per la lunghezza

      errorRetry=0; // OK no errori
      osal_stop_timerEx (Modem_TaskID, ERROR_EVENT);
     
      osal_start_timerEx(MSA_TaskId, MSA_SLEEP_EVENT, 10);  // lasciare 10ms per evitare problemi cambio task
     }
    break;

  case 0xAA:  // push command su socket 1
   if (searchResp(push1))
    {
     rxLenTemp=0; // resetto solo l'indice del buffer
    }
    else {
    switch (pushType)
    {
    case 0x01: // username
    if (searchResp(userRsp)) // username ok
    {
    cmdSent=0x09;
    osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
    }
    break;

    case 0x02: // password
    if (searchResp(passRsp)) // pass ok
    {
     osal_stop_timerEx (Modem_TaskID, ERROR_EVENT);  // fermo il timer che avevo fatto partire con username

    cmdSent=0x0C;  // vado direttamente al pasv
    osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
    }
    break;

   /* case 0x03:  // cwd
    if ((rxBufModem[16]=='3')&&(rxBufModem[17]=='2')&&(rxBufModem[18]=='3')&&(rxBufModem[19]=='5')&& (rxBufModem[20]=='3')&&(rxBufModem[21]=='0')) // cwd ok
    {
     osal_stop_timerEx (Modem_TaskID, ERROR_EVENT);

   cmdSent=0x0C;
    osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
    }
    break;

    case 0x04:  //pwd
    if ((rxBufModem[16]=='3')&&(rxBufModem[17]=='2')&& (rxBufModem[18]=='3')&&(rxBufModem[19]=='5')&& (rxBufModem[20]=='3')&&(rxBufModem[21]=='7')) // pwd ok
    {
     osal_stop_timerEx (Modem_TaskID, ERROR_EVENT);

     cmdSent=0x0C;
    osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
    }
    break; */

    case 0x05: // store nome file
    if (searchResp(storeRsp)) // store ok
    {
     osal_stop_timerEx (Modem_TaskID, ERROR_EVENT);

     cmdSent=0x10;
     osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
    }
    break;

    case 0x06: // download file
    if (searchResp(storeRsp)) // file status ok
    {
    
     saveDataFromFile(); // salvo i dati letti dal file

     osal_stop_timerEx (Modem_TaskID, ERROR_EVENT);

     downloadFile=FALSE; //reinit del download
     cmdSent=0x14;
     osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
    }
    break;

   case 0x07: // download file data ora
   asm("NOP");
    if (searchResp(storeRsp)) // file status ok
    {
    
     saveDataOra();

     osal_stop_timerEx (Modem_TaskID, ERROR_EVENT);

     firstOn=FALSE;
       
     cmdSent=0x14;
     osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
    }
    break;

    default:
     break;
   
    } // end switch
  } // end else
  break;

  case 0xFF:
   // Disable flow control
   U0UCR  &= ~0x40;
   // Flush del buffer seriale 
   U0UCR |= 0x80;  
   // Enable flow control
   U0UCR |= 0x40;
  break;
  
   default:
   break;

 } // end switch totale

 resetBuffer(); // pulisco il buffer
}

void StrToHex(char* string)
{
  char x[SIZE]; // deve contenere i valori convertiti

  char *p=string; // Assegna al puntatore l'indirizzo iniziale dell'area di memoria di string
  char *d=x; // Stessa cosa....
  while(*p)
  {
          sprintf(d, "%02X", *p++); // Copia nell'array x, il valore hex di s[0]
          d+=2; // vengono occupati 2 byte, quindi si sposta di 2 il puntatore nell'array x
  }
sprintf(tempBuffer,"%s", x);
p=0;
}

void saveDataOra()
{
  uint8 i=0,k=0;
  uint8 readDateHour[8];
  uint8 DataOraBuffer[10];  // metto 10 come margine
  uint8 startFile=0;
  int tempDataOra[35];

  //: 2,0,
  
 if ( searchResp(downloadCode) || searchResp(downloadCode2)) 
 {
  if (position<120) // ulteriore controllo per evitare overflow
  {  
    // salvo lo start dei parametri
    startFile=position+1;
        
        // ciclo per convertire i char in hex
        for (i=startFile;i<startFile+34;i++)
        {
         tempDataOra[k]=(int)(rxBufModem[i]);
         CharToBytes(tempDataOra,k);
         k++;
         i++;
         tempDataOra[k]=(int)(rxBufModem[i]); // converto da char a hex la parte che mi interessa
         CharToBytes(tempDataOra,k);
         tempDataOra[k-1]=BUILD_UINT8(tempDataOra[k-1],tempDataOra[k]);  // salvo come un bytes hex
        }

        // devo convertire in bytes i dati letti dal file e salvarli in eeprom

        k=0;
        for (uint8 i=0;i<16;i++)
        {
           if  (tempDataOra[i]!=0x2C)
           {
               DataOraBuffer[k]=BUILD_UINT8(tempDataOra[i],tempDataOra[i+1]);  // se � formato da 2 char es. numero 10
               i=i+2;
               k++;
           }
        }

        // verifico di avere letto bene, mese e anno non possono essere 0 (char)
        if(DataOraBuffer[1]!=0x00 && DataOraBuffer[0]!=0x00)
        {

         // verifio che non vi sia gia la data e ora impostata in modo corretto
         if (readAstronomic(readDateHour,0x0000,0x08)==0xAA)
         {
          
          // salvo orario nell'astronomico
          //START,SECONDI,MINUTI,ORE,GIORNO,DATA,MESE,ANNO
          setAstronomic(START,DataOraBuffer[5],(uint8)DataOraBuffer[4],(uint8)DataOraBuffer[3],
                      GIORNO,(uint8)DataOraBuffer[2],(uint8)DataOraBuffer[1],(uint8)DataOraBuffer[0]);
         }
         else restoreTime();
        
       // segnalo ok con 5 lampeggi veloci del led
      for (uint8 i=0;i<5;i++)
      {
       HalLedSet (HAL_LED_1, HAL_LED_MODE_ON);
       halWait(250);
       HalLedSet (HAL_LED_1, HAL_LED_MODE_OFF);
       halWait(250);
      }
     }
  }
  else
  {
    HalLedSet (HAL_LED_1, HAL_LED_MODE_ON); // segnalo che il GPRS non prende o errore sull'ora
    for(uint8 i=0;i<20;i++) halWait(250);  // con led fisso 5 secondi e poi resetto
    HAL_SYSTEM_RESET();
  }
 }
}


void saveDataFromFile()
{
  int tempConv[10];  // devo farlo int per convertire i char
  uint16 impostBuffer[10];  // metto 10 come margine
  uint8 k=0;
  uint8 endOfFile;

 if ( searchResp(downloadCode) )
 {
    // salvo lo start dei parametri
    startOfFile=position+1;

    // cerco la fine del file in modo da sapere quanti parametri salvare
    searchResp(endFile);
    endOfFile=position-3;
    
 if ( (endOfFile>startOfFile) && ((endOfFile-startOfFile)<20) )
 {   
  for (uint8 j=startOfFile;j<endOfFile;j++)  // devo fare la conversione da char a hex
  {
    tempConv[k]=(int)(rxBufModem[j]); // converto da char a hex la parte che mi interessa

    CharToBytes(tempConv,k);

    k++;
    j++;

    tempConv[k]=(int)(rxBufModem[j]); // converto da char a hex la parte che mi interessa
    CharToBytes(tempConv,k);

    tempConv[k-1]=BUILD_UINT8(tempConv[k-1],tempConv[k]);  // salvo come un bytes hex
  }

  k=0;

   // devo convertire in bytes i dati letti dal file e salvarli in eeprom
 for (uint8 i=0;i<((endOfFile-startOfFile)/2);i++)
 {
   if ( (tempConv[i]!=0xDA) || (tempConv[i]!=0x0A) || (tempConv[i]!=0x0D) )   // se non sono arrivato alla fine ( 0xDA o 0x0A o 0x0D )
   {
     if  (tempConv[i]!=0x2C)
     {
       if ( (tempConv[i+1]!=0x2C) && (tempConv[i+1]!=0xDA) && (tempConv[i+1]!=0x0D) && (tempConv[i+1]!=0x0A) && 
           (tempConv[i+2]!=0x2C) && (tempConv[i+2]!=0xDA) && (tempConv[i+2]!=0x0D) && (tempConv[i+2]!=0x0A) )// ho tre char ( es. 120 minuti )
       {
         firstPart=HextoBCD(BUILD_UINT8(tempConv[i],tempConv[i+1]));
         secondPart=HextoBCD(BUILD_UINT8(0x00,tempConv[i+2]));

         impostBuffer[k]=Hex16toBCD((BUILD_UINT16((secondPart<<4),firstPart))>>4);

         i=i+3;
       }
       else if ( (tempConv[i+1]!=0x2C) && (tempConv[i+1]!=0xDA) && (tempConv[i+1]!=0x0D) && (tempConv[i+1]!=0x0A) )// ho due char ( es. 12 minuti )
       {
         impostBuffer[k]=HextoBCD(BUILD_UINT8(tempConv[i],tempConv[i+1]));  // se � formato da 2 char es. numero 10
         i=i+2;
       }
       else
       // ho un char devo fare build con 0x00
       {
        impostBuffer[k]=HextoBCD(BUILD_UINT8(0x00,tempConv[i]));  // se � formato da solo 1 char es. numero 5
        i=i+1;
       }
      k++;
     }
   }
   else i=endOfFile; // esco
 }
  if ( (impostBuffer[0]!=0x00) && (impostBuffer[0]!=0xFF)
      && (impostBuffer[1]!=0x00) && (impostBuffer[1]!=0xFF)
       &&  (impostBuffer[0]<=480) && (impostBuffer[1]<=480) 
        && (impostBuffer[1]>=impostBuffer[0]) && (impostBuffer[1]%impostBuffer[0]==0) ) // massimo impostabile � 8 ore e multipli
    // controllo che il parametro sia !=0 e da 0xFF per evitare errori di lettura parametri
  {

    nextMinToSleep=impostBuffer[0]; // memorizzo il tempo di campionamento ( risveglio )

    sendModem=impostBuffer[1]/impostBuffer[0]; // calcolo quanti cicli devo fare prima di spedire

  }
 }
}

}

bool storeAndSendData()
{

  // controllo se i dati da spedire ci stanno in una sola spedizione
  if (lenghtData<=MAX_BYTES)
  {
    /* Read EEPROM - leggo tutti i dati */
    if ((readEEProm((byte*)&tempBuffer[0],readAddress,lenghtData))>0)
    {
      BytesToChar(lenghtData,0x0000); // converto in char
      lenghtData=0;
      noMoreData=TRUE;
    }
    else return 1; // errore lettura i2c non proseguo con invio dati
  }
  else
  {
   /* Read EEPROM - leggo il massimo dei dati */
   if ((readEEProm((byte*)&tempBuffer[0],readAddress,MAX_BYTES))>0)
   { 
   BytesToChar(MAX_BYTES,0x0000); // converto in char
   // aggiorno lenght e readAddress
   lenghtData=lenghtData-MAX_BYTES;
   readAddress=readAddress+MAX_BYTES;
   noMoreData=FALSE;
   }
   else return 1;  // errore lettura i2c non proseguo con invio dati
  }
  return 0;
}


void CharToBytes (int* bufferToConv,uint8 p)
{

  if(bufferToConv[p]>=0x30 && bufferToConv[p]<=0x39){
    bufferToConv[p]=bufferToConv[p]-48;
  }
  if(bufferToConv[p]>=0x41 && bufferToConv[p]<=0x46){
    bufferToConv[p]=bufferToConv[p]-55;
  }
  if(bufferToConv[p]>=0x61 && bufferToConv[p]<=0x66){
    bufferToConv[p]=bufferToConv[p]-87;
  }


/*  if ((bufferToConv[p]!=0x0A) && (bufferToConv[p]!=0x0B) && (bufferToConv[p]!=0x0C)
   && (bufferToConv[p]!=0x0D) && (bufferToConv[p]!=0x0E) && (bufferToConv[p]!=0x0F))
   bufferToConv[p]=bufferToConv[p]-48;  // per avere il codice ascii del valore memorizzato
   else bufferToConv[p]=bufferToConv[p]-55; */
}

void BytesToChar(uint16 lenghtBytes,uint8 startAdd)
{
  uint8 k=0;
  char tempSupport[2];
  uint8 supportBuffer[MAX_BYTES*2];  // qui non mi serve SIZE perch� da 80bytes faccio 160char

  for(uint8 i=startAdd;i<(startAdd+lenghtBytes);i++)
  {
   tempSupport[0]=HI_UINT8(tempBuffer[i]);
   tempSupport[1]=LO_UINT8(tempBuffer[i]);

   if ((tempSupport[0]!=0x0A) && (tempSupport[0]!=0x0B) && (tempSupport[0]!=0x0C)
   && (tempSupport[0]!=0x0D) && (tempSupport[0]!=0x0E) && (tempSupport[0]!=0x0F))
   supportBuffer[k++]=tempSupport[0]+48;  // per avere il codice ascii del valore memorizzato
   else supportBuffer[k++]=tempSupport[0]+55;

   if ((tempSupport[1]!=0x0A) && (tempSupport[1]!=0x0B) && (tempSupport[1]!=0x0C)
   && (tempSupport[1]!=0x0D) && (tempSupport[1]!=0x0E) && (tempSupport[1]!=0x0F))
   supportBuffer[k++]=tempSupport[1]+48;  // per avere il codice ascii del valore memorizzato
   else supportBuffer[k++]=tempSupport[1]+55;
  }

  osal_memcpy(&tempBuffer[startAdd],supportBuffer,k);
}

uint8 searchResp(char* codeResp)
{
  uint16 i=0,k=0;

  // se sto cercando la fine del file per i parametri parto da startFile che ho rilevato
  // ci sono diverse "fine file" quindi devo leggere quella corretta
  if ( (codeResp[0]==0x0D) && (codeResp[1]==0x0A)&& (codeResp[2]==0x0D) && (codeResp[3]==0x0A) )
    k=startOfFile;

 for (i=k;i<RX_CNT-5;i++)
 {
   if ( (rxBufModem[i]==codeResp[0]) && (rxBufModem[i+1]==codeResp[1]) && (rxBufModem[i+2]==codeResp[2]) && (rxBufModem[i+3]==codeResp[3])
       && (rxBufModem[i+4]==codeResp[4]) && (rxBufModem[i+5]==codeResp[5]) )
   {
     position=i+5;
     i=RX_CNT;
     return 1;
   }
 }
 return 0;
}

uint8 searchOk()
{
 for (uint16 i=0;i<RX_CNT-1;i++)
 {
   if ( (rxBufModem[i]=='O') && (rxBufModem[i+1]=='K') )
   {
     return 1;
   }
 }
 return 0;
}


/* promemoria per i valori convertiti
uint8 SENDUSER[49] = "AT+MIPSEND=1,\"555345522073656e65742e69740d0a\"\r"; // USER senet.it
uint8 SENDPASS[49] = "AT+MIPSEND=1,\"504153532073656e65744654500d0a\"\r"; // PASS senetFTP
uint8 SENDCWD[60] = "AT+MIPSEND=1,\"435744202f6874646f63730d0a\"\r"; // CWD /htdocs
uint8 SENDPWD[29] = "AT+MIPSEND=1,\"5057440D0A\"\r"; // PWD
uint8 SENDPASV[31]= "AT+MIPSEND=1,\"504153560d0a\"\r"; // PASV
uint8 STOR[53]="AT+MIPSEND=1,\"53544F522070726F7661342E6373760D0A\"\r"; // STOR prova4.csv
uint8 SENDROW[40]="AT+MIPSEND=2,\"696e76696f20646174690d0a\"\r"; // Per ora invio un file con scritto solo: "invio dati"
uint8 SENDROW2[60]="AT+MIPSEND=2,\"50726f766120456d616e75656c65\"\r"; // "Prova Emanuele" */

#endif //modem
