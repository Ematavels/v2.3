/******************************************************************************
*                                                                             *
*        **********                                                           *
*       ************                                                          *
*      ***        ***                                                         *
*     ***    ++    ***                                                        *
*     ***   +  +   ***                      CHIPCON                           *
*     ***   +                                                                 *
*     ***   +  +   ***                                                        *
*     ***    ++    ***                                                        *
*      ***        ***                                                         *
*       ************                                                          *
*        **********                                                           *
*                                                                             *
*******************************************************************************

Filename:     MODEM G30
Target:       cc2430
Author:       EFU
Revised:      1/3-2007
Revision:     1.1


******************************************************************************/
#if defined WINETTX

#include "modem.h"
#include "ioCC2530.h"
#include "hal_i2c.h"
#include "hal_timer.h"
#include "ZComDef.h"
#include "hal_uart.h"
#include "hal_drivers.h"
#include "hal_led.h"
#include "OSAL_Memory.h"
#include "OSAL_Timers.h"
#include "OSAL.h"
#include "hal_mcu.h"
#include "hal_defs.h"
#include "hal_types.h"
#include "eeprom.h"
#include "astronomic.h"
#include "hal_i2c.h"
#include "CoordFrane.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#if defined SPI
#include "hal_spi.h"
#endif

// local functions
void resetBuffer(void);
void resetTempBuffer(void);
void uartResponse(void);
void StrToHex(char* string);
bool storeAndSendData(void);
void saveDataFromFile(void);
void saveDataOra(void);
void CharToBytes (int* bufferToConv,uint8 p);
uint8 searchOk(void);
uint8 searchResp(char* codeResp);
void modemOnOff(uint8 setOnOff);
void initVar(void);
bool checkSizeFile(void);
bool setOperatorT(void);
bool setOperator=FALSE;
int power2 (int x, unsigned int y);

// response code
char userRsp[]="333331";
char passRsp[]="323330";
char storeRsp[]="313530";
char sizeRsp[]="323133";
char close2[]="323236";
char ftpCode[]="323230";
char passCode[]="323237";
char push1[]="USH: 1";
char push2[]="H: 2,0";
char mipStat[]="TAT: 1";
char mipOpen2[]="PEN: 2";
char mipOpen3[]="PEN: 3";
char mipSend2[]="D: 2,0";
char mipSend1[]="END: 1";
char mipCall[]="MIPCAL";
char closeSock[]="E: 1,0";
char closeConn[]="ALL: 0";
char downloadCode[]=": 2,0,";
char downloadCode2[]=": 3,0,";
char sizeCode[]=": 1,0,";
char endFile[]={0x0D,0x0A,0x0D,0x0A,0x2B,0x4D};
char endFileSize[]={0x30,0x44,0x30,0x41,0x0D,0x0A};
uint8 startOfFile,endOfFile;
uint8 startOfFileSize,endOfFileSize;

// variabili per l'acquisizione seriale
uint16 rxLen=0,rxLenTemp=0; // ok uint16
uint8 rxBufModem[RX_CNT];
uint8 sendModem=0;
uint8 dataRead[MAX_BYTES+1];  // buffer letttura da memoria eeprom 
char xConv[SIZE]; // deve contenere i valori convertiti
uint16 setBuf[SIZE_SAVE];  // metto 10 come margine posso salvare 10 parametri
char tempConv[SIZE_SAVE+1];  //+terminatore
char convArray[2]={0x00,0x00};  // 1 + terminatore
char MccBuffer[4]={0x00,0x00,0x00,0x00};
char MncBuffer[4]={0x00,0x00,0x00,0x00};
int MccVal=0;
int MncValRead=0;
char supVal[SIZE_SUP]; //4 + terminatore
char sizeConv[SIZE_DIM+1];  // 1 + terminatore
char base[4],offset[4];
uint8 writeBuffer[2]={0x00,0x00};

// li creo dopo avere convertito il testo in valori adatti al modem
char SENDBUFFER[SIZE+MAX_CMD_LENGHT]; // lo utilizzo per tutte gli altri comandi, AT+MIPSEND=1,AT+MIPSEND=2,OPENSOCK2,.. visto che il comando � sempre lo stesso

// variabili per i retry e per l'avanzamento comandi
uint8 cmdSent=0xFF,pushType=0x00;

bool noMoreData=FALSE;
bool allDataSend=FALSE;
bool downloadFile=FALSE;
bool respOk=FALSE;
bool firstOn=TRUE;
bool firstServer=TRUE;

uint8 setOk;
uint16 readAddress=MSA_READ_ADDRESS; // init dell'indirizzo da dove leggere i dati

uint16 sendTimeGprs=SEND_TIME;  // 4 secondi
uint8 sendTimeRetries=0;

uint8 contRead=1; // variabile che mi conta quante send successive faccio prima del push
uint16 lenghtData,totLenghtData=0; // memorizzo la lunghezza dei dati da spedire
uint16 sizeCalc=0; 
uint8 nRetries=N_RETRIES;
uint16 timeCarrier=TIME_CARRIER; // dafault
uint8 position=0;
uint8 socket=2;
uint16 indSock=3348; // socket di partenza

uint8 Modem_TaskID;
/**************************************************************************************************************/
// VALORI DA IMPOSTARE

// gestore telefonico e sito ftp dove caricare
// multi
char CONNECT_EM[]="AT+MIPCALL=1,\"em\",\"\",\"\"\r"; // APN EMNIFY
char CONNECT_1NCE[]="AT+MIPCALL=1,\"iot.1nce.net\"\r"; // APN 1NCE
char CONNECT_THMOB[]="AT+MIPCALL=1,\"TM\",\"\",\"\"\r"; // APN Things Mobile


// ITALY
char CONNECT_VODA[]="AT+MIPCALL=1,\"web.omnitel.it\"\r";
char CONNECT_WBIZ[]="AT+MIPCALL=1,\"internet.wind.biz\"\r";
char CONNECT_W[]="AT+MIPCALL=1,\"internet.wind\"\r";
char CONNECT_TIM[]="AT+MIPCALL=1,\"ibox.tim.it\"\r";

// BELGIO
char CONNECT_BASE[]="AT+MIPCALL=1,\"gprs.base.be\",\"base\",\"base\"\r"; 
char CONNECT_ORANGE_BG[]="AT+MIPCALL=1,\"mworld.be\",\"\",\"\"\r";
char CONNECT_PROX[]="AT+MIPCALL=1,\"internet.proximus.be\",\"\",\"\"\r"; 

// UNITED STATES
char CONNECT_ATT[]="AT+MIPCALL=1,\"NXTGENPHONE\",\"\",\"\"\r"; 
char CONNECT_TMOBILE_US[]="AT+MIPCALL=1,\"fast.t-mobile.com\",\"\",\"\"\r"; 

uint8 apnChange=0x00;

#if defined MODEM_FORCE_RESET
 uint8 hardfaultCounter=0;
#endif

char CONNECT[65];

//char OPENSOCK[]="AT+MIPOPEN=1,3348,\"winetsrl.serveftp.com\",21,0\r";
//char OPENSOCK2[]="AT+MIPOPEN=1,3348,\"winetsrl.serveftp.org\",21,0\r";

// user e password
//char USER[]="USER ortofruit24";
//char USER[]="USER kiwigold28";
//char USER[]="USER roncovetro";
//char USER[]="USER demofiera";
//char USER[]="USER winet21";
//char USER[]="USER marco.barla";
//char USER[]="USER iacco";
//char USER[]="USER ricasoli";
//char USER[]="USER gigi";
//char USER[]="USER apofruit2";
//char USER[]="USER marcoHK";
//char USER[]="USER castello";
//char USER[]="USER emanueleHK";
//char USER[]="USER apofruit";
//char USER[]="USER curie";
//char USER[]="USER horta_7";
//char USER[]="USER riomarina";
//char USER[]="USER beardo71";
//char USER[]="USER demo105";
//char USER[]="USER pariana";
//char USER[]="USER mazzoni39";
//char USER[]="USER magalotti40";
//char USER[]="USER cabria41";
//char USER[]="USER pradazzo42";
//char USER[]="USER quercia43";
//char USER[]="USER bassi45";
//char USER[]="USER bombardi46";
//char USER[]="USER zanchin47";
//char USER[]="USER gentilini48";
//char USER[]="USER lungarno49";
//char USER[]="USER marconi50";
//char USER[]="USER golinucci51";
//char USER[]="USER giuliani52";
//char USER[]="USER assirelli53";
//char USER[]="USER babini54";
//char USER[]="USER melandri55";
//char USER[]="USER alac57";
//char USER[]="USER fmf58";
//char USER[]="USER vecchiarocca61";
//char USER[]="USER firenzuola62";
//char USER[]="USER melini64";
//char USER[]="USER zespri83";
//char USER[]="USER valle65";
//char USER[]="USER cornino66";
//char USER[]="USER pasquini67";
//char USER[]="USER cecilia68";
//char USER[]="USER sarche116";
//char USER[]="USER candiracci143";
//char USER[]="USER pollaccia11";
//char USER[]="USER bardonecchia112";
//char USER[]="USER zespri376";
//char USER[]="USER santini74";
//char USER[]="USER franchini89";
//char USER[]="USER demo90";
//char USER[]="USER memento91";
//char USER[]="USER ferlini93";
//char USER[]="USER qsi187";
//char USER[]="USER ufficio";
//char USER[]="USER sassi107";
//char USER[]="USER trentina109";
//char USER[]="USER demo95";
//char USER[]="USER nobili122";
//char USER[]="USER meneghini123";
//char USER[]="USER giusti124";
//char USER[]="USER querce125";
//char USER[]="USER geoapp190";
//char USER[]="USER gestecno132";
//char USER[]="USER speranza130";
//char USER[]="USER dipsa137";
//char USER[]="USER agronica144";
//char USER[]="USER grigno146";
//char USER[]="USER canossa156";
//char USER[]="USER valentini89";
//char USER[]="USER polito180";
//char USER[]="USER dsa188";
//char USER[]="USER niccolai189";
//char USER[]="USER crea101";
//char USER[]="USER trento192";
//char USER[]="USER cenova193";
//char USER[]="USER bertaccini134";
//char USER[]="USER carrizales159";
//char USER[]="USER ippolito164";
//char USER[]="USER terremerse185";
//char USER[]="USER capacologna197";
//char USER[]="USER agronica307";
//char USER[]="USER bernardini199";
//char USER[]="USER tanganelli200";
//char USER[]="USER jingold244";
//char USER[]="USER puntofrutta228";
//char USER[]="USER susybest237";
//char USER[]="USER padane271";
//char USER[]="USER cesenate260";
//char USER[]="USER govoni267";
//char USER[]="USER focaccia268";
//char USER[]="USER benzoni120";
//char USER[]="USER minzoni265";
//char USER[]="USER cappelli266";
//char USER[]="USER guzzon299b";
//char USER[]="USER zespri306";
//char USER[]="USER firenze310";
//char USER[]="USER zangari311";
//char USER[]="USER camogli312";
//char USER[]="USER dipsa314";
//char USER[]="USER farolfi318";
//char USER[]="USER balducci319";
//char USER[]="USER amadori320";
//char USER[]="USER castellari315a";
//char USER[]="USER gaetti401";
//char USER[]="USER vernocchi321";
//char USER[]="USER nonni322";
//char USER[]="USER gaia428";
//char USER[]="USER abbo452";
//char USER[]="USER muscedere329";
//char USER[]="USER gervasi331";
//char USER[]="USER piceno332";
//char USER[]="USER dichiara333";
//char USER[]="USER perica334";
//char USER[]="USER colatosti336";
//char USER[]="USER gentilini337";
//char USER[]="USER landi340";
//char USER[]="USER miani341";
//char USER[]="USER rivalta342";
//char USER[]="USER crpv338";
//char USER[]="USER crea344";
//char USER[]="USER tti352";
//char USER[]="USER farolfi351";
//char USER[]="USER bellelli355";
//char USER[]="USER becchi357";
//char USER[]="USER bragato358";
//char USER[]="USER mauro359";
//char USER[]="USER chiarli360a";
//char USER[]="USER tigli361";
//char USER[]="USER agribologna362";
//char USER[]="USER geoapp364";
//char USER[]="USER apoconerpo365";
//char USER[]="USER baldini373";
//char USER[]="USER luppi374";
//char USER[]="USER bellini375";
//char USER[]="USER steffanini377";
//char USER[]="USER allegro378";
//char USER[]="USER guidotti379";
//char USER[]="USER dimeo380";
//char USER[]="USER cornell383";
//char USER[]="USER memo413";
//char USER[]="USER chillon381";
//char USER[]="USER giuzzo382";
//char USER[]="USER univpm385";
//char USER[]="USER sciotti386";
//char USER[]="USER gio387";
//char USER[]="USER cjo390";
//char USER[]="USER ballardini399";
//char USER[]="USER unipg403";
//char USER[]="USER pizzuto404";
//char USER[]="USER utili405";
//char USER[]="USER memo408";
//char USER[]="USER ragazzini409";
//char USER[]="USER amadei410";
//char USER[]="USER quercia411";
//char USER[]="USER zattoni412";
//char USER[]="USER guerrini414";
//char USER[]="USER budellazzi415";
//char USER[]="USER zauli416";
//char USER[]="USER ferlini417";
//char USER[]="USER ricci418";
//char USER[]="USER bassi419";
//char USER[]="USER davitti420";
//char USER[]="USER cer421";
//char USER[]="USER farolfi422";
//char USER[]="USER villa423";
//char USER[]="USER collina424";
//char USER[]="USER laviola425";
//char USER[]="USER geoapp429";
//char USER[]="USER sogno430";
//char USER[]="USER melandri431";
//char USER[]="USER camorani432";
//char USER[]="USER gallegati433";
//char USER[]="USER ancarani434";
//char USER[]="USER zardi435";
//char USER[]="USER energia436";
//char USER[]="USER marchetti437";
//char USER[]="USER ballardini438";
//char USER[]="USER rossi439";
//char USER[]="USER zauli440";
//char USER[]="USER memo441";
//char USER[]="USER carloni442";
//char USER[]="USER calderoni443";
//char USER[]="USER zama444";
//char USER[]="USER camagri445";
//char USER[]="USER gallina446";
//char USER[]="USER cembali449";
//char USER[]="USER castellari450";
//char USER[]="USER fieramonti448";
//char USER[]="USER renzetti451";
//char USER[]="USER mirasole453";
char USER[]="USER corzani456";
//char USER[]="USER winet33";

//char PASS[]="PASS kiwigold28!"; // dati_28.txt e impostazioni_28.txt
//char PASS[]="PASS ortofruit24!"; // dati_26.txt e impostazioni_26.txt
//char PASS[]="PASS roncovetro25!"; // dati_25.txt e impostazioni_25.txt
//char PASS[]="PASS demofiera22!"; // dati_22.txt e impostazioni_22.txt
//char PASS[]="PASS winet21!";
//char PASS[]="PASS marcoB!"; // Dati_13.txt
//char PASS[]="PASS iacco14!"; // dati_16
//char PASS[]="PASS ricasoli14!"; // dati_10
//char PASS[]="PASS pietrasanta14!";  // dati_12
//char PASS[]="PASS marcoHK!";
//char PASS[]="PASS gigi14!"; // dati_14
//char PASS[]="PASS apofruit2!"; // dati_9
//char PASS[]="PASS emanueleHK!"; // dati_7
//char PASS[]="PASS apofruit"; // dati_5
//char PASS[]="PASS 21Giugno03"; // dati_5
//char PASS[]="PASS horta_7!"; // dati_7
//char PASS[]="PASS curie_15!";
//char PASS[]="PASS riomarina_15!"; // dati_30.txt e impostazioni_30.txt
//char PASS[]="PASS melini64!"; // dati_64.txt e impostazioni_64.txt
//char PASS[]="PASS zespri83!"; // dati_77.txt e impostazioni_32.txt
//char PASS[]="PASS demo105!"; // dati_35.txt e impostazioni_35.txt
//char PASS[]="PASS pariana_34!"; // dati_34.txt e impostazioni_34.txt
//char PASS[]="PASS mazzoni_39!"; // dati_39.txt e impostazioni_39.txt
//char PASS[]="PASS magalotti_40!"; // dati_40.txt e impostazioni_40.txt
//char PASS[]="PASS cabria41!"; // dati_41.txt e impostazioni_41.txt
//char PASS[]="PASS pradazzo42!"; // dati_42.txt e impostazioni_42.txt
//char PASS[]="PASS quercia43!"; // dati_43.txt e impostazioni_43.txt
//char PASS[]="PASS bassi45!"; // dati_45.txt e impostazioni_45.txt
//char PASS[]="PASS bombardi46!"; // dati_46.txt e impostazioni_46.txt
//char PASS[]="PASS zanchin47!"; // dati_47.txt e impostazioni_47.txt
//char PASS[]="PASS gentilini48!"; // dati_48.txt e impostazioni_48.txt
//char PASS[]="PASS lungarno49!"; // dati_49.txt e impostazioni_49.txt
//char PASS[]="PASS marconi50!"; // dati_50.txt e impostazioni_50.txt
//char PASS[]="PASS golinucci51!"; // dati_51.txt e impostazioni_51.txt
//char PASS[]="PASS giuliani52!"; // dati_52.txt e impostazioni_52.txt
//char PASS[]="PASS babini54!"; // dati_54.txt e impostazioni_54.txt
//char PASS[]="PASS melandri55!"; // dati_46.txt e impostazioni_46.txt
//char PASS[]="PASS alac57!"; // dati_57.txt e impostazioni_57.txt
//char PASS[]="PASS fmf58!";
//char PASS[]="PASS vecchiarocca61!"; // dati_61.txt e impostazioni_61.txt
//char PASS[]="PASS firenzuola62!"; // dati_62.txt e impostazioni_62.txt
//char PASS[]="PASS bardonecchia112!"; // dati_75.txt e impostazioni_75.txt
//char PASS[]="PASS valle65!"; // dati_65 e impostazioni_65
//char PASS[]="PASS cornino66!"; // dati_66 e impostazioni_66
//char PASS[]="PASS pasquini67!"; // dati_67 e impostazioni_67
//char PASS[]="PASS cecilia68!"; // dati_68 e impostazioni_68
//char PASS[]="PASS sarche116!"; // dati_69 e impostazioni_69
//char PASS[]="PASS candiracci143!"; // dati_70 e impostazioni_70
//char PASS[]="PASS canossa156!"; // dati_70 e impostazioni_70
//char PASS[]="PASS pollaccia11!"; // dati_71 e impostazioni_71
//char PASS[]="PASS beardo71!"; // dati_97 e impostazioni_97
//char PASS[]="PASS demo73!"; // dati_73 e impostazioni_73
//char PASS[]="PASS assirelli53!"; // dati_73 e impostazioni_73
//char PASS[]="PASS santini74!"; // dati_74 e impostazioni_74
//char PASS[]="PASS franchini89!"; // dati_89 e impostazioni_89
//char PASS[]="PASS demo95!"; // dati_90 e impostazioni_90
//char PASS[]="PASS memento91!"; // dati_91 e impostazioni_91
//char PASS[]="PASS ferlini93!"; // dati_93 e impostazioni_93
//char PASS[]="PASS qsi187!"; // dati_102 e impostazioni_102
//char PASS[]="PASS ufficio!"; // dati_105 e impostazioni_105
//char PASS[]="PASS sassi107!"; // dati_32.txt e impostazioni_32.txt
//char PASS[]="PASS trentina109!"; // dati_109 e impostazioni_109
//char PASS[]="PASS demo90!"; // dati_109 e impostazioni_109
//char PASS[]="PASS nobili122!"; // dati_122 e impostazioni_122
//char PASS[]="PASS meneghini123!"; // dati_123 e impostazioni_123
//char PASS[]="PASS giusti124!"; // dati_124 e impostazioni_124
//char PASS[]="PASS querce125!"; // dati_124 e impostazioni_124
//char PASS[]="PASS geoapp190!"; // dati_127.txt e impostazioni_127.txt
//char PASS[]="PASS speranza113!"; // dati_42.txt e impostazioni_42.txt
//char PASS[]="PASS gestecno132!"; // dati_42.txt e impostazioni_42.txt
//char PASS[]="PASS dipsa137!"; 
//char PASS[]="PASS agronica307!";
//char PASS[]="PASS grigno146!";
//char PASS[]="PASS valentini89!";
//char PASS[]="PASS ippolito164!";
//char PASS[]="PASS polito180!";
//char PASS[]="PASS dsa188!"; // dati_48.txt e impostazioni_48.txt
//char PASS[]="PASS niccolai189!"; // dati_48.txt e impostazioni_48.txt
//char PASS[]="PASS crea101!";
//char PASS[]="PASS trento192!"; // dati_48.txt e impostazioni_48.txt
//char PASS[]="PASS cenova193!"; // dati_48.txt e impostazioni_48.txt
//char PASS[]="PASS bertaccini134!"; // dati_48.txt e impostazioni_48.txt
//char PASS[]="PASS carrizales159!";
//char PASS[]="PASS terremerse185!";
//char PASS[]="PASS agronica195!"; // dati_90 e impostazioni_90
//char PASS[]="PASS capacologna197!"; // dati_90 e impostazioni_90
//char PASS[]="PASS bernardini199!";
//char PASS[]="PASS tanganelli200!";
//char PASS[]="PASS jingold244!"; 
//char PASS[]="PASS puntofrutta228!"; 
//char PASS[]="PASS susybest237!";
//char PASS[]="PASS padane271!";
//char PASS[]="PASS cesenate260!";
//char PASS[]="PASS govoni267!";
//char PASS[]="PASS focaccia268!";
//char PASS[]="PASS benzoni120!";
//char PASS[]="PASS minzoni265!";
//char PASS[]="PASS cappelli266!";
//char PASS[]="PASS guzzon299b!";
//char PASS[]="PASS zespri376!";
//char PASS[]="PASS firenze310!";
//char PASS[]="PASS zangari311!";
//char PASS[]="PASS camogli312!";
//char PASS[]="PASS dipsa314!";
//char PASS[]="PASS farolfi318!";
//char PASS[]="PASS balducci319!";
//char PASS[]="PASS amadori320!";
//char PASS[]="PASS castellari315a!";
//char PASS[]="PASS gaetti401!";
//char PASS[]="PASS vernocchi321!";
//char PASS[]="PASS nonni322!";
//char PASS[]="PASS gaia428!";
//char PASS[]="PASS abbo452!";
//char PASS[]="PASS muscedere329!";
//char PASS[]="PASS gervasi331!";
//char PASS[]="PASS piceno332!";
//char PASS[]="PASS dichiara333!";
//char PASS[]="PASS perica334!";
//char PASS[]="PASS colatosti336!";
//char PASS[]="PASS gentilini337!";
//char PASS[]="PASS landi340!";
//char PASS[]="PASS miani341!";
//char PASS[]="PASS rivalta342!";
//char PASS[]="PASS crpv338!";
//char PASS[]="PASS crea344!";
//char PASS[]="PASS tti352!";
//char PASS[]="PASS farolfi351!";
//char PASS[]="PASS bellelli355!";
//char PASS[]="PASS becchi357!";
//char PASS[]="PASS bragato358!";
//char PASS[]="PASS mauro359!";
//char PASS[]="PASS chiarli360a!";
//char PASS[]="PASS tigli361!";
//char PASS[]="PASS agribologna362!";
//char PASS[]="PASS geoapp364!";
//char PASS[]="PASS apoconerpo365!";
//char PASS[]="PASS baldini373!";
//char PASS[]="PASS luppi374!";
//char PASS[]="PASS bellini375!";
//char PASS[]="PASS steffanini377!";
//char PASS[]="PASS allegro378!";
//char PASS[]="PASS guidotti379!";
//char PASS[]="PASS dimeo380!";
//char PASS[]="PASS memo413!";
//char PASS[]="PASS chillon381!";
//char PASS[]="PASS giuzzo382!";
//char PASS[]="PASS cornell383!";
//char PASS[]="PASS univpm385!";
//char PASS[]="PASS sciotti386!";
//char PASS[]="PASS gio387!";
//char PASS[]="PASS cjo390!";
//char PASS[]="PASS ballardini399!";
//char PASS[]="PASS unipg403!";
//char PASS[]="PASS pizzuto404!";
//char PASS[]="PASS utili405!";
//char PASS[]="PASS memo408!";
//char PASS[]="PASS ragazzini409!";
//char PASS[]="PASS amadei410!";
//char PASS[]="PASS quercia411!";
//char PASS[]="PASS zattoni412!";
//char PASS[]="PASS guerrini414!";
//char PASS[]="PASS budellazzi415!";
//char PASS[]="PASS zauli416!";
//char PASS[]="PASS ferlini417!";
//char PASS[]="PASS ricci418!";
//char PASS[]="PASS bassi419!";
//char PASS[]="PASS davitti420!";
//char PASS[]="PASS cer421!";
//char PASS[]="PASS farolfi422!";
//char PASS[]="PASS villa423!";
//char PASS[]="PASS collina424!";
//char PASS[]="PASS laviola425!";
//char PASS[]="PASS geoapp429!";
//char PASS[]="PASS sogno430!";
//char PASS[]="PASS melandri431!";
//char PASS[]="PASS camorani432!";
//char PASS[]="PASS gallegati433!";
//char PASS[]="PASS ancarani434!";
//char PASS[]="PASS zardi435!";
//char PASS[]="PASS energia436!";
//char PASS[]="PASS marchetti437!";
//char PASS[]="PASS ballardini438!";
//char PASS[]="PASS rossi439!";
//char PASS[]="PASS zauli440!";
//char PASS[]="PASS memo441!";
//char PASS[]="PASS carloni442!";
//char PASS[]="PASS calderoni443!";
//char PASS[]="PASS zama444!";
//char PASS[]="PASS camagri445!";
//char PASS[]="PASS gallina446!";
//char PASS[]="PASS cembali449!";
//char PASS[]="PASS castellari450!";
//char PASS[]="PASS fieramonti448!";
//char PASS[]="PASS renzetti451!";
//char PASS[]="PASS mirasole453!";
char PASS[]="PASS corzani456!";
//char PASS[]="PASS winet33!";

char FILE[]="STOR dati_456.txt";
char FILE3[]="RETR impostazioni_456.txt";
char FILE4[]="SIZE dati_456.txt";

char FILE2[]="RETR dataora.txt";
// directory
//char CWD[]="CWD /htdocs/provaEma";
//char CWD[]="CWD /htdocs";
char CWD[]="CWD /";

// lasciare stare cosi
char PWD[]="PWD";
char PASV[]="PASV";

  // Comandi Modem
//char INIT1[] = "AT\r"; // valuto se modem risponde e chiedo anche orario all'operatore
//char TIMEMS[]="ATS102=0\r"; // tempo in ms da wake up a tx comando
//char CSQ[]="AT+CSQ\r";
//char GPRS[]="AT+CPIN?\r";
// verifica la qualit� del GPRS - I paramentro � strenght va da 0 (-113dbm) a 31 (-5dbm), avere un 20 indicativamente.
// II parametro � la ber, va da 0 a 7, 99 significa che la rete gsm non lo comunica

char DATAORA[]="AT+CCLK?\r";
char ECHO[]="ATE0\r";
//char ECHO[]="AT+IPR=115200\r";
char ATH[]="ATH\r";
char GPRS[]="AT+CGPRS\r";
char CHECKOPER[]="AT+COPS?\r";
char CHECKCODE[]="AT+COPS=3,2\r";
char CHECKSOCK[] = "AT+MIPOPEN?\r";
char CLOSESOCK [] = "AT+MIPCLOSE=1\r";
char CLOSESOCK2[]="AT+MIPCLOSE=2\r";
char PUSH[]="AT+MIPPUSH=1\r";
char PUSH2[]="AT+MIPPUSH=2\r";
char CLOSECONN[]="AT+MIPCALL=0\r";
char RESET[]="AT+MRST\r";


/* Funzione per accendere/spegnere il modem
 * se invio 1 = modem ON *
 * se invio 0 = modem OFF *
 **************************/
void modemOnOff(uint8 setOnOff)
{

if (setOnOff==1)
 {
  ENABLE_MODEM();  // alimento il modem
  halWait(200);
  // devo impostare uscita alta e poi tenerla bassa per un tempo maggiore di 500ms e minore di 1500ms
  MODEM_ON_N=0x01;
  // attendo che sia stabile 500ms come da datasheet
  halWait(250);
  halWait(250);
  MODEM_ON_N=0x00;
  // attendo 1 secondo e modem on
  halWait(250);
  halWait(250);
  halWait(250);
  halWait(250);
  // la ripristino ad 1 ed il modem si accende ( devo tenerla a 1 per comunicare con modem e non SD card )
  MODEM_ON_N=0x01;
  // attendo 2 secondi altrimenti non � pronto il modem - self system test � 1.6 secondi da datasheet
  halWait(250);
  halWait(250); //500
  halWait(250);
  halWait(250); //1000
  halWait(250);
  halWait(250); //1500
  halWait(250);
  halWait(250); //2000
  halWait(250);
 }
 else if (setOnOff==0)
 {
   MODEM_ON_N=0x00;
   // attendo 3 secondi e modem off
   halWait(250);
   halWait(250);
   halWait(250);
   halWait(250);
   halWait(250);
   halWait(250);
   halWait(250);
   halWait(250);
   halWait(250);
   halWait(250);
   halWait(250);
   halWait(250);
   // la ripristino ad 1 ed il modem si spegne
   MODEM_ON_N=0x01;
   // attendo come da datasheet 2.5 sec affinch� faccia tutte le chiusure
   halWait(250);
   halWait(250);
   halWait(250);
   halWait(250);
   halWait(250);
   halWait(250);
   halWait(250);
   halWait(250);
   halWait(250);
   halWait(250);
   halWait(250);
   halWait(250);
   //MODEM_ON_N=0x00;
   //DISABLE_MODEM();  // spengo alimentazione del modem
   // faccio nello sleep
 }
}


void Modem_Init( uint8 task_id )
{
  /* Register task ID */
  Modem_TaskID = task_id;

  /*Setting the connection on port1*/
  halUARTCfg_t uartConfig;

  uartConfig.configured           = TRUE;         // 2430 don't care.
  uartConfig.baudRate             = UART_BAUD;
  uartConfig.flowControl          = TRUE;
  uartConfig.flowControlThreshold = UART_THRESH;
  uartConfig.rx.maxBufSize        = UART_RX_MAX;
  uartConfig.tx.maxBufSize        = UART_TX_MAX;
  uartConfig.idleTimeout          = UART_IDLE; // 2430 don't care.
  uartConfig.intEnable            = TRUE;      // 2430 don't care.

  #if defined WINETTX
  uartConfig.callBackFunc         = rxCB;    // callback for serial rx event
  #else
  uartConfig.callBackFunc         = NULL;      // callback for serial rx event
  #endif

  // open UART
  HalUARTOpen(HAL_UART_PORT_1,&uartConfig);

  resetBuffer(); // pulisco il buffer
  

}

void resetTempBuffer()
{
  for (uint8 i=0;i<(SIZE+MAX_CMD_LENGHT);i++) {
   SENDBUFFER[i]='\0';
  }
  for (uint8 i=0;i<(SIZE);i++) {
   tempBuffer[i]='\0';
  }
}


void resetBuffer()
{
 rxLen=0;
 rxLenTemp=0;

  for (uint8 i=0;i<RX_CNT;i++)
   rxBufModem[i]='\0';
}

void initVar()
{
   // disabilito il collegamento seriale in modo che se dovessi ricevere ora al limite del timer non lo processo
   MODEM_ON_N=0x00;
   nRetries=N_RETRIES;
   cmdSent=0xFF; // init 
   
   resetTempBuffer(); // resetto il buffer
   resetBuffer(); // resetto anche buffer seriale
   
   // init della socket se necessario
   if (indSock>MAX_SOCKET) {
     indSock=3348;
   }
   
   socket=2; // reinit
   firstServer=TRUE; 
   totLenghtData=0;
   noMoreData=FALSE;
   allDataSend=FALSE; // init del check tutti dati inviati
   contRead=1; // init delle variabili per il push dati
   downloadFile=FALSE; // re init delle variabile per download
   readAddress=MSA_READ_ADDRESS; // rinit del readAddress
   
}

uint16 Modem_ProcessEvent( uint8 task_id, uint16 events )
{

if (events & ERROR_EVENT)
  {
    
    if (cmdSent==0x07)
    {
     if (firstOn) // entro qui perch� non ho accesso FTP e sono prima accensione
     {
       memset(CONNECT,0x00,sizeof(CONNECT)); 
       
       switch (apnChange) // provo i vari apn non standard sim tim/vodafone/wind
       {
        case 0x00: // emnify
         strncpy(CONNECT,CONNECT_EM, strlen((char*)CONNECT_EM) );
        break;
        
        case 0x01:  // 1nce
         strncpy(CONNECT,CONNECT_1NCE, strlen((char*)CONNECT_1NCE) );
        break;
        
        case 0x02: // thinghs mobile
         strncpy(CONNECT,CONNECT_THMOB, strlen((char*)CONNECT_THMOB) );
        break;
       
        case 0x03: // no operatore
         HalLedSet (HAL_LED_2, HAL_LED_MODE_ON); // segnalo che il GPRS o operatore non prende
         initVar();
         halWaitSec(5);
         err=15;
         resetBoard();
        break;
       }
       
       apnChange++;
       cmdSent=0x04;
       osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, SEND_TIME); // metto comunque 4 secodni per nuovo check
     }
      else
      {
       if (timeCarrier<20000) // parto da 20 default 
        timeCarrier=timeCarrier+2000;
      }
     }
      
     // se ho chiuso la connessione gia vado in sleep anche se non ho ricevuto risposta altrimenti vado in loop  
     // se � la prima accensione invece vado a cercare APN
     if (!firstOn)
     {  
      if (cmdSent!=0x16) 
      {
       cmdSent=0x16;
       osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND); // chiudo connessione con gestore
      }
      else 
      {
       initVar();
       osal_start_timerEx(MSA_TaskId, MSA_SLEEP_EVENT,10);  // lasciare 10ms per evitare problemi cambio task
       }
     }
    
  return events ^ ERROR_EVENT;
  }

 if (events & RETRY_EVENT )
  {
     // effettuo n tentativi di connessione GPRS
   cmdSent=0x03;
   osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
   
  return events ^ RETRY_EVENT;
  }


  if(events & DATA_RX )
  {

  uartResponse();
  return events ^ DATA_RX;
  }

  if(events &  NEXT_CMD_MODEM )
  {    
  switch(cmdSent)
  {
   /* case 0x01:  // comando AT per verificare che il modem risponda
   osal_start_timerEx ( Modem_TaskID, ERROR_EVENT, TIME_ERROR); // faccio partire un timer entro il quale devo avere risposta
   cmdSent=0x01;
   HalUARTWrite( HAL_UART_PORT_1, (uint8*)INIT1, strlen((char*)INIT1) );
   break; */

 case 0x02:  // comando AT per disabilitare l'echo
  osal_start_timerEx ( Modem_TaskID, ERROR_EVENT, TIME_ERROR);
  modemOnOff(1);   // accendo il modem
  HalUARTWrite( HAL_UART_PORT_1, (uint8*)ECHO, strlen((char*)ECHO) );
 break;

 case 0x03:  // comando AT per fare un check del segnale GPRS
 osal_start_timerEx ( Modem_TaskID, ERROR_EVENT, TIME_ERROR);
 HalUARTWrite( HAL_UART_PORT_1, (uint8*)GPRS, strlen((char*)GPRS));
 break;

 case 0x3A:  // comando AT per vedere l'operatore
  osal_start_timerEx ( Modem_TaskID, ERROR_EVENT, TIME_ERROR);
  HalUARTWrite( HAL_UART_PORT_1, (uint8*)CHECKCODE, strlen((char*)CHECKCODE));
 break;
 
 case 0x3B:  // comando AT per vedere l'operatore
  osal_start_timerEx ( Modem_TaskID, ERROR_EVENT, TIME_ERROR);
  HalUARTWrite( HAL_UART_PORT_1, (uint8*)CHECKOPER, strlen((char*)CHECKOPER));
 break;

 case 0x04:  // comando AT per creare il link
  writeAstronomic(watchDogAbeAfe,0x09,1); // riattivo il WD cosi ho 124 secondi ( ogni volta che mando un burst di dati riattivo )
  // non imposto il WD hardware perch� la prima volta lo faccio impostare allo sleep
  // potrei non avere in memoria un orario corretto quindi non � impostabile
  osal_start_timerEx ( Modem_TaskID, ERROR_EVENT, TIME_ERROR);
  HalUARTWrite( HAL_UART_PORT_1, (uint8*)CONNECT, strlen((char*)CONNECT));
 break;

 /*
 case 0x05:  // comando AT per fare check della socket
  osal_start_timerEx ( Modem_TaskID, ERROR_EVENT, TIME_ERROR);
  HalUARTWrite( HAL_UART_PORT_1, (uint8*)CHECKSOCK, strlen((char*)CHECKSOCK));
 break;

 case 0x06:  // comando AT per chiudere socket
  osal_start_timerEx ( Modem_TaskID, ERROR_EVENT, TIME_ERROR);
  HalUARTWrite( HAL_UART_PORT_1, (uint8*)CLOSESOCK,  strlen((char*)CLOSESOCK));
 break;
*/
 case 0x07:  // comando AT per aprire la socket
  osal_start_timerEx ( Modem_TaskID, ERROR_EVENT, TIME_ERROR);
  
  // inserisco il comando nel buffer
  if (firstServer==TRUE)
  sprintf((char*)&SENDBUFFER,(char const*)"AT+MIPOPEN=1,%u,\"winetsrl.serveftp.org\",21,0\r",indSock);
  else sprintf((char*)&SENDBUFFER,(char const*)"AT+MIPOPEN=1,%u,\"winetsrl.serveftp.com\",21,0\r",indSock);

  // socket successiva
  indSock++;
 
  HalUARTWrite( HAL_UART_PORT_1, (uint8*)SENDBUFFER, strlen((char*)SENDBUFFER));
  break;

 case 0x08:  // comando AT per inviare l'username
  osal_start_timerEx ( Modem_TaskID, ERROR_EVENT, TIME_ERROR);  // timer sia per username che per password
  StrToHex(USER);
  // inserisco il comando convertito nel buffer
  sprintf((char*)&SENDBUFFER,(char const*)"AT+MIPSEND=1,\"%s0d0a\"\r",tempBuffer);
  HalUARTWrite( HAL_UART_PORT_1, (uint8*)SENDBUFFER, strlen((char*)SENDBUFFER));
 break;

  case 0x09:  // comando AT per inviare la password
  StrToHex(PASS);
   // inserisco il comando convertito nel buffer
  sprintf((char*)&SENDBUFFER,(char const*)"AT+MIPSEND=1,\"%s0d0a\"\r",tempBuffer);
  HalUARTWrite( HAL_UART_PORT_1, (uint8*)SENDBUFFER, strlen((char*)SENDBUFFER));
 break;

 case 0x0C:  // comando AT per inviare il Passive Mode
  osal_start_timerEx ( Modem_TaskID, ERROR_EVENT, TIME_ERROR); 
  StrToHex(PASV);
   // inserisco il comando convertito nel buffer
  sprintf((char*)&SENDBUFFER,(char const*)"AT+MIPSEND=1,\"%s0d0a\"\r",tempBuffer);
  HalUARTWrite( HAL_UART_PORT_1, (uint8*)SENDBUFFER, strlen((char*)SENDBUFFER));
 break;

 case 0x0D:  // comando AT per inviare i dati su server ( passive mode )
  HalUARTWrite( HAL_UART_PORT_1, (uint8*)PUSH, strlen((char*)PUSH));
 break;

 case 0x0E: // comando per aprire la socket2
  osal_start_timerEx ( Modem_TaskID, ERROR_EVENT, TIME_ERROR);
  HalUARTWrite( HAL_UART_PORT_1, (uint8*)SENDBUFFER, strlen((char*)SENDBUFFER));
  break;

  case 0x0F: // comando per fare lo stor del nome file
  osal_start_timerEx ( Modem_TaskID, ERROR_EVENT, TIME_ERROR);
  // timer che fermer� dopo il push del nome file
  StrToHex(FILE);
  // inserisco il comando convertito nel buffer
  sprintf((char*)&SENDBUFFER,(char const*)"AT+MIPSEND=1,\"%s0d0a\"\r",tempBuffer);
  HalUARTWrite( HAL_UART_PORT_1, (uint8*)SENDBUFFER, strlen((char*)SENDBUFFER));
  break;

  case 0x10:  // dati
   osal_start_timerEx ( Modem_TaskID, ERROR_EVENT, TIME_ERROR); // faccio partire qui cosi se ho problemi nelle fasi successive sono protetto
   if (storeAndSendData()==0) // se � tutto ok ( valore ritornato � 0 )
   {
   StrToHex(tempBuffer);
   // inserisco il comando convertito nel buffer
   sprintf((char*)&SENDBUFFER,(char const*)"AT+MIPSEND=2,\"%s\"\r",tempBuffer);
   HalUARTWrite( HAL_UART_PORT_1, (uint8*)SENDBUFFER, strlen((char*)SENDBUFFER));
   }
   else
   {
    osal_start_timerEx ( Modem_TaskID, ERROR_EVENT, NEXT_COMMAND);  // vado in errore riprover� la volta successiva
   }
  break;

  case 0x11:  // comando AT per inviare i dati su server ( push dati )
  osal_start_timerEx ( Modem_TaskID, ERROR_EVENT, TIME_ERROR);
  HalUARTWrite( HAL_UART_PORT_1, (uint8*)PUSH2, strlen((char*)PUSH2));
 break;
 
  case 0x1A:  // comando AT per verifica della size del file caricato
  osal_start_timerEx ( Modem_TaskID, ERROR_EVENT, TIME_ERROR);
  // timer che fermer� dopo il push del file
  StrToHex(FILE4);
  // inserisco il comando convertito nel buffer
  sprintf((char*)&SENDBUFFER,(char const*)"AT+MIPSEND=1,\"%s0d0a\"\r",tempBuffer);
  HalUARTWrite( HAL_UART_PORT_1, (uint8*)SENDBUFFER, strlen((char*)SENDBUFFER));
 break;

  case 0x12:  // comando AT per il download del file impostazioni
  osal_start_timerEx ( Modem_TaskID, ERROR_EVENT, TIME_ERROR);
  // timer che fermer� dopo il push del file
  StrToHex(FILE3);
  // inserisco il comando convertito nel buffer
  sprintf((char*)&SENDBUFFER,(char const*)"AT+MIPSEND=1,\"%s0d0a\"\r",tempBuffer);
  HalUARTWrite( HAL_UART_PORT_1, (uint8*)SENDBUFFER, strlen((char*)SENDBUFFER));
 break;

/* case 0x13:  // comando AT per chiudere connessione dati - socket 2
  osal_start_timerEx ( Modem_TaskID, ERROR_EVENT, TIME_ERROR);
  HalUARTWrite( HAL_UART_PORT_1, (uint8*)CLOSESOCK2, strlen((char*)CLOSESOCK2));
 break; */

 case 0x14:  // comando AT per chiudere connessione socket 1
  osal_start_timerEx ( Modem_TaskID, ERROR_EVENT, TIME_ERROR);
  // timer che fermo direttamente dopo la disconnessione con operatore
  HalUARTWrite( HAL_UART_PORT_1, (uint8*)CLOSESOCK, strlen((char*)CLOSESOCK));
 break;

 case 0x15:  // comando AT per il download del file impostazioni
  osal_start_timerEx ( Modem_TaskID, ERROR_EVENT, TIME_ERROR);
  // timer che fermer� dopo il push del file
  StrToHex(FILE2);
  // inserisco il comando convertito nel buffer
  sprintf((char*)&SENDBUFFER,(char const*)"AT+MIPSEND=1,\"%s0d0a\"\r",tempBuffer);
  HalUARTWrite( HAL_UART_PORT_1, (uint8*)SENDBUFFER, strlen((char*)SENDBUFFER));
 break;

 case 0x16:  // comando AT per chiudere connessione socket 1
   osal_start_timerEx ( Modem_TaskID, ERROR_EVENT, TIME_ERROR);
   HalUARTWrite( HAL_UART_PORT_1, (uint8*)CLOSECONN, strlen((char*)CLOSECONN));
 break;

 case 0xAA:  // comando AT per inviare i dati ( push su socket 1)
  HalUARTWrite( HAL_UART_PORT_1, (uint8*)PUSH, strlen((char*)PUSH));
 break;
 
  default:
   break;
 }

  resetTempBuffer(); // resetto il buffer

  return events ^ NEXT_CMD_MODEM;
 }

 return 0;

}

void rxCB( uint8 port, uint8 event )
{
  if (( rxLen = HalUARTRead( HAL_UART_PORT_1, &rxBufModem[rxLenTemp], RX_CNT-rxLenTemp ) ) != 0 ) // ho ricevuto qualcosa
  {
  if (rxLenTemp==0) osal_start_timerEx(Modem_TaskID,DATA_RX,120);  // parte il timer seriale di 120ms sto in ascolto e salvo i dati
  rxLenTemp=rxLen+rxLenTemp; // salvo dove sono arrivato
  if (rxLenTemp>RX_CNT) 
   { 
      // se sforo i 255 bytes-margine vado in errore ( rxLenTemp � uint16 quindi posso permettermelo )
      rxLenTemp=0; // riparto da inizio buffer
      osal_stop_timerEx(Modem_TaskID,DATA_RX); 
      osal_start_timerEx(Modem_TaskID,ERROR_EVENT,10); // error event 
   }
 }
}

void uartResponse()
{
  uint8 jb=0;
  uint8 ko=0;  // usate in base e offset
  uint8 virgola=0;
  uint16 uno=0,due=0;
  uint16 porta;  
  
  asm("NOP");
  
  
 switch(cmdSent)
 {
   
  case 0x01:  // at ok
    if ( searchOk() ) // ho trovato l'ok proseguo
     {
        osal_stop_timerEx (Modem_TaskID, ERROR_EVENT);
        cmdSent=0x02;
        osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
     }
   break;

  case 0x02: // ate0 ok
    if ( searchOk() ) // ho trovato l'ok proseguo
    {
    osal_stop_timerEx (Modem_TaskID, ERROR_EVENT);

    // salvo la lunghezza dei dati che voglio trasmettere, mi servir� nell'erase eeprom
    totLenghtData=lenghtData;
   
    cmdSent=0x03;
   
    osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, TIME_OUT_GPRS);  // aspetto in modo che GPRS sia pronto
    }
    break;

    case 0x03: // verifica gprs ( fa 3 tentativi da 4 secondi )
      if ( (rxBufModem[10]=='1') && (rxBufModem[15]=='O') && (rxBufModem[16]=='K') )
      {
       osal_stop_timerEx (Modem_TaskID, ERROR_EVENT); // risposta da modem arrivata

        nRetries=N_RETRIES;
        
        if (firstOn==TRUE)
        {
         cmdSent=0x3A;
         osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, 3000); // 3 secondi margine per il COPS
        }
        else
        {
        cmdSent=0x04;
        osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM,timeCarrier); // 10 secondi default poi incremento in caso
        }
      }
      else
      {
        if ( --nRetries > 0 )
        {
        osal_stop_timerEx (Modem_TaskID, ERROR_EVENT); // risposta da modem arrivata
        writeAstronomic(watchDogAbeAfe,0x09,1); // faccio ripartire WD - 124 secondi
        osal_start_timerEx ( Modem_TaskID, RETRY_EVENT, TIME_OUT_GPRS);
        }
        else
        { 
          osal_stop_timerEx (Modem_TaskID, ERROR_EVENT); // risposta da modem arrivata
        
          HalLedSet (HAL_LED_2, HAL_LED_MODE_ON); // segnalo che il GPRS non prende
          initVar();
          halWaitSec(5);
          
          if (firstOn) 
          {
           err=9;
           #if !defined SPI
            resetBoard();
           #else
            osal_start_timerEx(MSA_TaskId, MSA_SLEEP_EVENT,10); 
           #endif
          }
          else osal_start_timerEx(MSA_TaskId, MSA_SLEEP_EVENT,10); 
        }
      }
    break;
    
   case 0x3A:  // cops per avere il codice operatore
     if ( searchOk() ) // ho trovato l'ok proseguo
     {
        osal_stop_timerEx (Modem_TaskID, ERROR_EVENT);
        cmdSent=0x3B;
        osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, 20000);
     }
   break;
    
   // operatore - lo faccio solo la prima volta che accendo
   case 0x3B:
    respOk=setOperatorT();
 
    if (respOk==TRUE)  // operatore riconosciuto proseguo
    {

      osal_stop_timerEx (Modem_TaskID, ERROR_EVENT);

      // ok operatore allora vado a connettermi gprs
      cmdSent=0x04;

      osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
    }
    break;


   case 0x04:  // connect to server
    if(searchResp(mipCall))
    {
      osal_stop_timerEx (Modem_TaskID, ERROR_EVENT);
     
     // vado direttamente alla opensocket ( server winet la chiude in auto dopo tot tempo )
     cmdSent=0x07;
     osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
    }
    break;

   case 0x07:  // connessione ftp
     if(searchResp(ftpCode))
     {
      osal_stop_timerEx (Modem_TaskID, ERROR_EVENT);

      cmdSent=0x08;
      osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
     }
    else if ( (firstServer==TRUE) && (searchResp(mipStat)) ) 
    {
      osal_stop_timerEx (Modem_TaskID, ERROR_EVENT);
      firstServer=FALSE; // provo con il secondo indirizzo 
      writeAstronomic(watchDogAbeAfe,0x09,1); // faccio ripartire WD - 124 secondi   
      cmdSent=0x07;
      osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
    }   
    break;

  case 0x08:  // invio username al modem
    if ( searchOk() ) // ho trovato l'ok proseguo
     {

      pushType=0x01; // pushtype user
      cmdSent=0xAA;
      osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
    }
    break;

   case 0x09:  // invio password al modem
   if ( searchOk() ) // ho trovato l'ok proseguo
   {

      pushType=0x02; // pushtype password
      cmdSent=0xAA;
      osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
   }
    break;

  case 0x0C:  // invio passive mode al modem
    if (searchResp(mipSend1))
    {
      cmdSent=0x0D;
      osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
    }
    break;

  case 0x0D: // push del passive mode
   if(searchResp(passCode))
    {

    for(uint8 i=0;i<4;i++)
    {
    base[i]='\0';
    offset[i]='\0';
    }

    for(uint8 i=0; i<RX_CNT-1; i++)
    {
        if((rxBufModem[i]=='2' && rxBufModem[i+1]=='C') || (rxBufModem[i]=='2' && rxBufModem[i+1]=='9'))
        virgola++;

        if(virgola==4)
        {
          i++;
          base[jb]=rxBufModem[i+2];
          jb++;
        }

        if(virgola==5)
        {
          i++;
          offset[ko]=rxBufModem[i+2];
          ko++;
       }
    }//for

    if ( (jb>0) && (ko>0) ) // check lettura corretta
    {
     // Scrittura dei due numeri letti dal pacchetto: Base = P1 e Offset = P2
     base[jb-1]='\0';
     offset[ko-1]='\0';

     // Conversione da tipo stringa a tipo intero
     uno=atoi(base);
     due=atoi(offset);
     
     // Calcolo del numero di PORTA a cui connettersi
     if ( (uno<=255) && (due<=255) ) // ulteriore check di non avere letto male
     {
       porta=uno*256+due;

       // inserisco il comando nel buffer
       if (firstServer==FALSE)
       sprintf((char*)&SENDBUFFER,(char const*)"AT+MIPOPEN=%u,%u,\"winetsrl.serveftp.org\",%u,0\r",socket,indSock,porta);
       else sprintf((char*)&SENDBUFFER,(char const*)"AT+MIPOPEN=%u,%u,\"winetsrl.serveftp.com\",%u,0\r",socket,indSock,porta);

       // socket successiva
       indSock++;
       
       osal_stop_timerEx (Modem_TaskID, ERROR_EVENT); // fermo il timer qui cosi sono sicuro che ha fatto tutte le operazioni
       cmdSent=0x0E;
       osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
     }
    }
   }
   break;

  case 0x0E: // apertura socket dati ( socket 2 )
    if ( searchResp(mipOpen2) || searchResp(mipOpen3) )
     {
      osal_stop_timerEx (Modem_TaskID, ERROR_EVENT);
      if (socket==3) socket=2;
  
       if (firstOn==TRUE)
       {
        cmdSent=0x15;
        osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
       }
       else
       {
        if (downloadFile)
        {
        cmdSent=0x12;
        osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
        }  // se sono al download devo dire al server quale � il file da scaricare
        else {
        cmdSent=0x0F;
        osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND); }
        }
     }
    // N.B. se non va vuol dire che server � impallato!!
    break;

   case 0x0F:  // stor del nome del file
    if ( searchOk() ) // ho trovato l'ok proseguo
    {
      pushType=0x05; // pushtype stor
      cmdSent=0xAA;
      osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
    }
    break;


   case 0x10:  // invio dati
     if (searchResp(mipSend2))
     {
      osal_stop_timerEx (Modem_TaskID, ERROR_EVENT);

      if ( (noMoreData==TRUE) || (contRead>=MAX_PACKET_READ) )  // ho finito i dati da inviare o ho inviato il massimo
      {
         writeAstronomic(watchDogAbeAfe,0x09,1); // riattivo il WD cosi ho 124 secondi ( ogni volta che mando un burst di dati riattivo )
       
	#if defined WD_AQ22
         setWDTime(WD_TIME_RESET);  // imposto ogni volta 10 minuti per invio del blocco dati
        #endif
	  
        cmdSent=0x11;
        osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, sendTimeGprs);
       }
       else 
       {
         contRead++;
         cmdSent=0x10;
         osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
       }

    }
    break;

  case 0x11: // push dei dati
    if (searchResp(push2))
    {
     osal_stop_timerEx (Modem_TaskID, ERROR_EVENT);

     contRead=1; // la reinit

     if (noMoreData==TRUE)  // ho finito i dati da inviare
        {
         downloadFile=TRUE; // imposto il download del file
         //apro una socket 3 per velocizzare le cose al posto che chiudere e aprire la 2
         //cmdSent=0x13;
          if (downloadFile) 
          {
           socket=3;
           cmdSent=0x0C; 
           osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
          }  // vado a fare il download altrimenti fine
          else 
          {
           cmdSent=0x14;
           osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
          }
        }
       else {
         cmdSent=0x10;
         osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, sendTimeGprs);  
       }
    }
   break;
   
  case 0x1A:  // size del file caricato
   if ( searchOk() ) // ho trovato l'ok proseguo
     {
      pushType=0x08; // pushtype size file
      cmdSent=0xAA;
      osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
     }
    break;
 

   case 0x12: // download del file di impostazioni
    if ( searchResp(mipSend1) ) // ho trovato l'ok proseguo
    {
      cmdSent=0xAA;
      pushType=0x06; // pushtype stor
     osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
    }
    break;

/*     case 0x13: // close  socket 2 dati
     if (searchResp(close2)) // closing data connection
     {
     osal_stop_timerEx (Modem_TaskID, ERROR_EVENT);

     if (downloadFile) {
       cmdSent=0x0C;
       osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
        }  // vado a fare il download altrimenti fine
     else {
       cmdSent=0x14;
       osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
        }

    }
    break; */

   case 0x14: // close socket 1
    if (searchResp(closeSock)) // ci pu� mettere anche qualche secondo
    {    
     
     osal_stop_timerEx (Modem_TaskID, ERROR_EVENT); 
     cmdSent=0x16;
     osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
     }
     break;

     case 0x15:  // data e ora
     if ( searchOk() ) // ho trovato l'ok proseguo
     {
      pushType=0x07; // pushtype data ora file
      cmdSent=0xAA;
      osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
     }
    break;

   case 0x16: // close connection con operatore
    if ( searchResp(closeConn))
    {
      #if defined MODEM_FORCE_RESET
       hardfaultCounter=0;
      #endif
     
     if (firstOn==TRUE) firstOn=FALSE; // se al primo avvio non faccio erase perch� i dati non li ho inviati
      else if ( allDataSend==TRUE ) // se ho inviato tutti i dati
      { 
       //eraseSelectedEprom(MSA_READ_LENGHT,totLenghtData+2);
       // non serve cancellare la eeprom ma mi basta cancellare le prime due locazioni dove salvo la lunghezza dei dati in memoria
       eraseSelectedEprom(MSA_READ_LENGHT,0x02); 
      
       timeCarrier=TIME_CARRIER; // Ok imposto default time carrier
      } // erase dei dati + 2byte per la lunghezza
     
     
     initVar();
   
     osal_stop_timerEx (Modem_TaskID, ERROR_EVENT);
   
     osal_start_timerEx(MSA_TaskId, MSA_SLEEP_EVENT, 10);  // lasciare 10ms per evitare problemi cambio task
    }
    break;

  case 0xAA:  // push command su socket 1
   if (searchResp(push1))
    {
     rxLenTemp=0; // resetto solo l'indice del buffer
    }
    else switch (pushType)
    {
    case 0x01: // username
    if (searchResp(userRsp)) // username ok
    {
    cmdSent=0x09;
    osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
    }
    break;

    case 0x02: // password
    if (searchResp(passRsp)) // pass ok
    {
     osal_stop_timerEx (Modem_TaskID, ERROR_EVENT);  // fermo il timer che avevo fatto partire con username

    cmdSent=0x0C;  // vado al pasv mode
    osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
    }
    break;

    case 0x05: // store nome file
    if (searchResp(storeRsp)) // store ok
    {
     osal_stop_timerEx (Modem_TaskID, ERROR_EVENT);

     cmdSent=0x10;
     osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
    }
    break;

    case 0x06: // download file
    if (searchResp(storeRsp)) // file status ok
    {
    
     saveDataFromFile(); // salvo i dati letti dal file

     osal_stop_timerEx (Modem_TaskID, ERROR_EVENT); 
        
     cmdSent=0x1A; // era 0x14 - mettere 0x1A per il checksize EMANUELE
     //cmdSent=0x14;
     osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
    }
   break;

   case 0x07: // download file data ora
    if (searchResp(storeRsp)) // file status ok
    {

     saveDataOra();

     osal_stop_timerEx (Modem_TaskID, ERROR_EVENT);

    
     cmdSent=0x14;
     osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
    
    }
    break;
    
   case 0x08: // check size file
   if (searchResp(sizeRsp)) // file status ok
    {
     
    if (checkSizeFile()!=0) // not ok - devo cmq uscire per evitare di far esplodere la memoria
     {
      if (sendTimeRetries==3) // ho raggiunto il limite ragiono come fosse andata bene  
      {
        // caso limite raggiunto ragiono come se avesse inviato per fare erase eeprom
       sendTimeRetries=0; // init dei retries
       allDataSend=TRUE; // Ok tutti i dati inviati
       sendTimeGprs=SEND_TIME; // init del send time gprs 
      }
     }
      
     osal_stop_timerEx (Modem_TaskID, ERROR_EVENT); 
     
     cmdSent=0x14;
     osal_start_timerEx ( Modem_TaskID, NEXT_CMD_MODEM, NEXT_COMMAND);
    }
    break;
   
    default:
    break;
   
  } // end switch/else
 
  case 0xFF:  // arriva qualcosa di sballato
   // Disable flow control
   U1UCR  &= ~0x40;
   // Flush del buffer seriale 
   U1UCR |= 0x80;  
   // Enable flow control
   U1UCR |= 0x40;
  break;
 
   default:
   break;

 } // end switch totale

 resetBuffer(); // pulisco il buffer
}

void StrToHex(char* string)
{
  char *p=string; // Assegna al puntatore l'indirizzo iniziale dell'area di memoria di string
  char *d=xConv; // Stessa cosa....
  
  while(*p)
  {
          sprintf(d, "%02X", *p++); // Copia nell'array x, il valore hex di s[0]
          d+=2; // vengono occupati 2 byte, quindi si sposta di 2 il puntatore nell'array x
  }
  sprintf(tempBuffer,"%s", xConv);
  p=0; 
}

void saveDataOra()
{
  uint8 i=0,k=0;
  uint8 readDateHour[8];
  uint8 DataOraBuffer[6];  // metto 10 come margine
  uint8 startFile=0;
  int tempDataOra[35];

 if ( searchResp(downloadCode) )
 {
  if (position<120) // ulteriore controllo per evitare overflow
  {  
    // salvo lo start dei parametri
    startFile=position+1;
        
        // ciclo per convertire i char in hex
        for (i=startFile;i<startFile+34;i++)
        {
         tempDataOra[k]=(int)(rxBufModem[i]);
         CharToBytes(tempDataOra,k);
         k++;
         i++;
         tempDataOra[k]=(int)(rxBufModem[i]); // converto da char a hex la parte che mi interessa
         CharToBytes(tempDataOra,k);
         tempDataOra[k-1]=BUILD_UINT8(tempDataOra[k-1],tempDataOra[k]);  // salvo come un bytes hex
        }

        // devo convertire in bytes i dati letti dal file e salvarli in eeprom

        k=0;
        for (uint8 i=0;i<16;i++)
        {
           if  (tempDataOra[i]!=0x2C)
           {
               DataOraBuffer[k]=BUILD_UINT8(tempDataOra[i],tempDataOra[i+1]);  // se � formato da 2 char es. numero 10
               i=i+2;
               k++;
           }
        }

        // verifico di avere letto bene, mese e anno non possono essere 0 (char)
        if(DataOraBuffer[1]!=0x00 && DataOraBuffer[0]!=0x00)
        {

          // verifio che non vi sia gia la data e ora impostata in modo corretto
         setOk=readAstronomic(readDateHour,0x0000,0x08);
           
         if (setOk==0xAA)
         {
          // salvo orario nell'astronomico
          //START,SECONDI,MINUTI,ORE,GIORNO,DATA,MESE,ANNO
          setAstronomic(START,DataOraBuffer[5],(uint8)DataOraBuffer[4],(uint8)DataOraBuffer[3],
                      GIORNO,(uint8)DataOraBuffer[2],(uint8)DataOraBuffer[1],(uint8)DataOraBuffer[0]);
          
          
         }
         
       // segnalo ok con 2 secondo luce verde
      for (uint8 i=0;i<5;i++)
      {
       HalLedSet (HAL_LED_1, HAL_LED_MODE_ON);
       halWaitSec(2);
       HalLedSet (HAL_LED_1, HAL_LED_MODE_OFF);
      }
     }
  }
  else
  {
    HalLedSet (HAL_LED_2, HAL_LED_MODE_ON); // segnalo che il GPRS non prende o errore sull'ora
    initVar();
    halWaitSec(5); // con led fisso 5 secondi e poi resetto
    err=10;
    resetBoard();
  }
 }
}


void saveDataFromFile()
{
  uint8 k=0,sv=0;
  uint8 pCount=0;
  
  memset(tempConv,'\0',sizeof(tempConv));
  memset(setBuf,0xFFFF,sizeof(setBuf));
  memset(supVal,'\0',sizeof(supVal));
 
 if (searchResp(downloadCode2)) 
 {
  if (position<230) // check di sicurezza
  {  
    // salvo lo start dei parametri
    startOfFile=position+1;

    // cerco la fine del file in modo da sapere quanti parametri salvare
    if (searchResp(endFile)) endOfFile=position-2;
    else endOfFile=startOfFile; // condizione per non entrare nell'if successivo ( non ho trovato la fine corretta )

  // check di sicurezza
  if ( (endOfFile>startOfFile) && ((endOfFile-startOfFile)<=(SIZE_SAVE*2)) )   
  {
   
   for (uint8 j=startOfFile;j<endOfFile;j++)  // devo fare la conversione da char a hex
   {
     if ((j%2)!=0) 
     {
       if ((rxBufModem[j]==0x43) || (rxBufModem[j]==0x0A)) pCount++;  // se trovo 0x43 ovvero la parte alta di ascii , conto come parametro
       tempConv[k]=rxBufModem[j]; 
       k++; 
     }
   }

  k=0;
  
 if (pCount==N_PARAMETRI_M) // ho scaricato bene tutto?
  {
   for (uint8 s=0;s<=SIZE_SAVE;s++)
   {
    if ( (tempConv[s]!=0x00) && (tempConv[s]!=0x43) && (tempConv[s]!=0x0A) ) 
    {
     supVal[sv]=tempConv[s];
     sv++;
    }
    else
    {
     sscanf(supVal,"%u",&setBuf[k]);
     k++;
     sv=0;
     memset(supVal,'\0',sizeof(supVal));
     if (tempConv[s]==0x0A) s=SIZE_SAVE+1; // esco ho finito
    }
  }
 
 
  /************* OPERAZIONI SUI VALORI SCARICATI *************/ 
   for (uint8 j=0;j<N_PARAMETRI_M;j++)
   {
    if (setBuf[j]==0xFFFF) return; // se uno solo dei campi � 0xFFFF esci
   }
  
   /*** FORZATURA AL RESET - WINET **/
   if ( (setBuf[0]==0x00) && (setBuf[1]==0x00) )
   {
    osal_stop_timerEx (Modem_TaskID, ERROR_EVENT);
    
    /* Setto orario fake in modo che poi scarica data e ora */
    //writeBuffer[0]=STOP|0x88;
    //writeAstronomic(writeBuffer,0x01,0x01);
    // setto il Century bit in modo da renderlo "fake" per capire se effettuare il setAstronomic nuova dataora
    writeBuffer[0]=0x88;
    writeAstronomic(writeBuffer,0x03,0x01);
    initVar();
    err=14;
    resetBoard();
   }
   else
   {
     // Imposto i tempi di risvelgio e di trasmissione
     if ( (setBuf[0]!=0x00) && (setBuf[0]!=0xFF)
      && (setBuf[1]!=0x00) && (setBuf[1]!=0xFF)
       &&  (setBuf[0]<=480) && (setBuf[1]<=480)
         && (setBuf[1]>=setBuf[0]) && (setBuf[1]%setBuf[0]==0) ) // massimo impostabile � 8 ore
      // controllo che il parametro sia !=0 e da 0xFF per evitare errori di lettura parametri
      {
      nextMinToSleep=setBuf[0]; // memorizzo il tempo di campionamento ( risveglio )

      sendModem=setBuf[1]/setBuf[0]; // calcolo quanti cicli devo fare prima di spedire

      coordinatorMode=setBuf[2];
      
      #if defined WEBCAM
       doPhotoNow=setBuf[3]; // se ho la webcam il sistema mi inserisce un terzo valore 1 o 0 per lo scatto
      #endif
     
      }
     }
    }
   }
  } 
 }
}

bool setOperatorT(void)
{
  uint8 mcSet=0;
  uint8 rVal=0;
  
  memset(CONNECT,0x00,sizeof(CONNECT)); 
  
  #if defined EMNIFY
     strncpy(CONNECT,CONNECT_EM, strlen((char*)CONNECT_EM) );
     setOperator=TRUE;
  #else
  
   memset(MccBuffer,0x00,sizeof(MccBuffer));
   memset(MncBuffer,0x00,sizeof(MncBuffer));
     
     for (rVal=14;rVal<=16;rVal++)  
     {
       if ( (rxBufModem[rVal]>=0x30) && (rxBufModem[rVal]<=0x39) )
        { 
          MccBuffer[mcSet]=rxBufModem[rVal]; 
          mcSet++; 
        }
     }
     
     mcSet=0;
     MccVal=(int16)atol(MccBuffer);  // valore Country
     
     for (rVal=17;rVal<=19;rVal++)  
     {
       if ( (rxBufModem[rVal]>=0x30) && (rxBufModem[rVal]<=0x39) )
        { 
          MncBuffer[mcSet]=rxBufModem[rVal]; 
          mcSet++; 
        }
     }
     
     MncValRead=(int16)atol(MncBuffer);  // valore Operatore
     
     if (MccVal!=0)
     {
      switch(MccVal)
      {
      case 222:  // Italy
        switch(MncValRead)
        {
          case 10:  // Vodafone
          case 6:
            strncpy(CONNECT,CONNECT_VODA, strlen((char*)CONNECT_VODA));
            setOperator=TRUE;
          break;
          
          case 1:
          case 43:
          case 48:  // TIM
            strncpy(CONNECT,CONNECT_TIM, strlen((char*)CONNECT_TIM));
            setOperator=TRUE;
          break;
          
          case 88:  // WIND
             strncpy(CONNECT,CONNECT_WBIZ, strlen((char*)CONNECT_WBIZ) );
             setOperator=TRUE;
          break;
          
         default:
           setOperator=FALSE;
           cmdSent=0x07;
          break;
        }
       break;
       
      case 206: // Belgium
        switch(MncValRead)
        {
          case 20:  // BASE
            strncpy(CONNECT,CONNECT_BASE, strlen((char*)CONNECT_BASE));
            setOperator=TRUE;
          break;
          
          case 10: // Orange Belgium / Mobistar
            strncpy(CONNECT,CONNECT_ORANGE_BG, strlen((char*)CONNECT_ORANGE_BG));
            setOperator=TRUE;
          break;
          
          case 1:  // Proximus
             strncpy(CONNECT,CONNECT_PROX, strlen((char*)CONNECT_PROX) );
             setOperator=TRUE;
          break;
          
         default:
           setOperator=FALSE;
           cmdSent=0x07;
          break;
        }
       break;
       
        case 310: // United States
        switch(MncValRead)
        {
          case 30: 
          case 70:
          case 80:
          case 90:
          case 150:  
          case 170:
          case 410:
          case 560:  
          case 680:  
            // AT&T
            strncpy(CONNECT,CONNECT_ATT, strlen((char*)CONNECT_ATT));
            setOperator=TRUE;
          break;
          
          case 160: 
          case 260:   //T-Mobile
            strncpy(CONNECT,CONNECT_TMOBILE_US, strlen((char*)CONNECT_TMOBILE_US));
            setOperator=TRUE;
          break;
          
         default:
           setOperator=FALSE;
           cmdSent=0x07;
          break;
        }
       break;
       
       
       default:
          setOperator=FALSE;
          cmdSent=0x07;
        break;
      }
     }   
     
  #endif
  
  return setOperator;
    
}


bool checkSizeFile()
{
  uint8 k=0;
  bool okCheck=FALSE;
  sizeCalc=0;
  memset(sizeConv,0x00,sizeof(sizeConv));
   
 if ( searchResp(sizeCode) )
 {
    // salvo lo start dei parametri ( con check sia nella posizione corretta)
   if (position<230) startOfFileSize=position+9;
   else return 1;

    // cerco la fine del file in modo da sapere quanti parametri salvare
    if (searchResp(endFileSize)) endOfFileSize=position-5;
    else return 1; // errore - non ho trovato la fine

  // check di sicurezza
  if ( (endOfFileSize>startOfFileSize) && ((endOfFileSize-startOfFileSize)<=(SIZE_DIM*2)) )   
  {
   for (uint8 j=startOfFileSize;j<endOfFileSize;j++)  // devo fare la conversione da char a hex
   {
     if ((j%2)!=0) {
       if ( (rxBufModem[j]>=0x30) && (rxBufModem[j]<=0x39) )  // check che sia un numero
       sizeConv[k]=rxBufModem[j]; 
       k++; 
     }
   }
   
   for(uint8 ck=0;ck<k;ck++)
   {
     if (isdigit(sizeConv[ck])) okCheck=TRUE;
     else okCheck=FALSE;
   }
         
   if (okCheck)
   {  
   sizeCalc=(int16)atol(sizeConv); 
   
   // se tutto ok 
   if ( (sizeCalc/2) >= totLenghtData )
   { 
     sendTimeRetries=0; // init dei retries
     allDataSend=TRUE; // Ok tutti i dati inviati
     sendTimeGprs=SEND_TIME; // init del send time gprs
   }
   else 
   {
     if (sendTimeRetries<=3) 
     { 
       sendTimeRetries++; 
       sendTimeGprs=SEND_TIME+(sendTimeRetries*1000); // terzo tentativo incremento dinamicamente il timeout
     }
     else 
     { 
      // caso limite raggiunto ragiono come se avesse inviato per fare erase eeprom
      sendTimeRetries=0; // init dei retries
      allDataSend=TRUE; // Ok tutti i dati inviati
      sendTimeGprs=SEND_TIME; // init del send time gprs 
     }
    }
     return 0; // ok check
   }
   else return 1; // errore check
  }
  else return 1; // errore check
 }
 else return 1; // errore check
}


bool storeAndSendData()
{

  memset(dataRead,0x00,sizeof(dataRead));
  
  // controllo se i dati da spedire ci stanno in una sola spedizione
  if (lenghtData<=MAX_BYTES)
  {
    /* Read EEPROM - leggo tutti i dati */
    if ((readEEProm((byte*)&dataRead[0],readAddress,lenghtData))>0)
    {
      BytesToChar((byte*)&dataRead[0],lenghtData); // converto in char
      lenghtData=0;
      noMoreData=TRUE;
    }
    else return 1; // errore lettura i2c non proseguo con invio dati
  }
  else
  {
   /* Read EEPROM - leggo il massimo dei dati */
   if ((readEEProm((byte*)&dataRead[0],readAddress,MAX_BYTES))>0)
   {
     BytesToChar((byte*)&dataRead[0],MAX_BYTES); // converto in char
     // aggiorno lenght e readAddress
     lenghtData=lenghtData-MAX_BYTES;
     readAddress=readAddress+MAX_BYTES;
     noMoreData=FALSE;
   }
   else return 1;  // errore lettura i2c non proseguo con invio dati
  }
  return 0;
}


void CharToBytes (int* bufferToConv,uint8 p)
{

  if(bufferToConv[p]>=0x30 && bufferToConv[p]<=0x39){
    bufferToConv[p]=bufferToConv[p]-48;
  }
  if(bufferToConv[p]>=0x41 && bufferToConv[p]<=0x46){
    bufferToConv[p]=bufferToConv[p]-55;
  }
  if(bufferToConv[p]>=0x61 && bufferToConv[p]<=0x66){
    bufferToConv[p]=bufferToConv[p]-87;
  }
}

uint8 searchResp(char* codeResp)
{
  uint8 i=0,k=0;

  // se sto cercando la fine del file per i parametri parto da startFile che ho rilevato
  // ci sono diverse "fine file" quindi devo leggere quella corretta
  if ( (codeResp[0]==0x0D) && (codeResp[1]==0x0A)&& (codeResp[2]==0x0D) && (codeResp[3]==0x0A) )
    k=startOfFile;
  
  if ( (codeResp[0]==0x30) && (codeResp[1]==0x44)&& (codeResp[2]==0x30) && (codeResp[3]==0x41) )
    k=startOfFileSize;

 for (i=k;i<RX_CNT-5;i++)
 {
   if ( (rxBufModem[i]==codeResp[0]) && (rxBufModem[i+1]==codeResp[1]) && (rxBufModem[i+2]==codeResp[2]) && (rxBufModem[i+3]==codeResp[3])
       && (rxBufModem[i+4]==codeResp[4]) && (rxBufModem[i+5]==codeResp[5]) )
   {
     position=i+5;
     i=RX_CNT;
     return 1;
   }
 }
 return 0;
}

uint8 searchOk()
{
 for (uint8 i=0;i<RX_CNT-1;i++)
 {
   if ( (rxBufModem[i]=='O') && (rxBufModem[i+1]=='K') )
   {
     i=RX_CNT;
     return 1;
   }
 }
 return 0;
}

int power2 (int x, unsigned int y)
{
    int temp;
    if (y == 0)
        return 1;

    temp = power2 (x, y / 2);
    if ((y % 2) == 0)
        return temp * temp;
    else
        return x * temp * temp;
}



/* promemoria per i valori convertiti
uint8 SENDUSER[49] = "AT+MIPSEND=1,\"555345522073656e65742e69740d0a\"\r"; // USER senet.it
uint8 SENDPASS[49] = "AT+MIPSEND=1,\"504153532073656e65744654500d0a\"\r"; // PASS senetFTP
uint8 SENDCWD[60] = "AT+MIPSEND=1,\"435744202f6874646f63730d0a\"\r"; // CWD /htdocs
uint8 SENDPWD[29] = "AT+MIPSEND=1,\"5057440D0A\"\r"; // PWD
uint8 SENDPASV[31]= "AT+MIPSEND=1,\"504153560d0a\"\r"; // PASV
uint8 STOR[53]="AT+MIPSEND=1,\"53544F522070726F7661342E6373760D0A\"\r"; // STOR prova4.csv
uint8 SENDROW[40]="AT+MIPSEND=2,\"696e76696f20646174690d0a\"\r"; // Per ora invio un file con scritto solo: "invio dati"
uint8 SENDROW2[60]="AT+MIPSEND=2,\"50726f766120456d616e75656c65\"\r"; // "Prova Emanuele" */

#endif //modem
