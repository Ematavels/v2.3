/**************************************************************************************************
  Filename:       i2c.h
  Revised:        $Date: 2007-09-11 10:58:41 -0700 (Tue, 11 Sep 2007) $
  Revision:       $Revision: 15371 $

  Description:    Describe the purpose and contents of the file.

**************************************************************************************************
 **************************************************************************************************/

#ifdef __cplusplus
extern "C"
{
#endif

/**************************************************************************************************
 * INCLUDES
 **************************************************************************************************/
#include "ioCC2530.h"
#include "hal_types.h"
#include "ZComDef.h"
#include "hal_board_cfg.h"

extern uint8 i2cRetries;  // retry per la spedizione o lettura su i2c
extern uint8 i2cAck;
extern uint8 Peripheral_TaskID;
extern uint8 err;

#if defined SPI || defined COORDINATOR // buffer che uso sia nel modem che nel SD Card
extern char tempBuffer[205];  // utilizzato nella strtohex per memorizzare il valore convertito e nella SD CARD packet_lenght*2 + 1 terminatore
#endif

#define I2C_ERROR_TIME  2000
// eventi I2C
#define I2C_ERROR_EVENT 0x0001

// the default cofiguration below uses P1.2 for SDA and P1.3 for SCL.
// change these as needed.
#ifndef CLK_PORT
  #define CLK_PORT  1
#endif

#ifndef DATA_PORT
  #define DATA_PORT   1
#endif

#ifndef CLK_PIN
  #define CLK_PIN   3
#endif

#ifndef DATA_PIN
  #define DATA_PIN    2
#endif

#define I2C_RETRIES  3

  // General I/O definitions
#define IO_GIO  0  // General purpose I/O
#define IO_PER  1  // Peripheral function
#define IO_IN   0  // Input pin
#define IO_OUT  1  // Output pin
#define IO_PUD  0  // Pullup/pulldn input
#define IO_TRI  1  // Tri-state input
#define IO_PUP  0  // Pull-up input pin
#define IO_PDN  1  // Pull-down input pin

    /* I/O PORT CONFIGURATION */
#define CAT1(x,y) x##y  // Concatenates 2 strings
#define CAT2(x,y) CAT1(x,y)  // Forces evaluation of CAT1

// OCM port I/O defintions
// Builds I/O port name: PNAME(1,INP) ==> P1INP
#define PNAME(y,z) CAT2(P,CAT2(y,z))
// Builds I/O bit name: BNAME(1,2) ==> P1_2
#define BNAME(port,pin) CAT2(CAT2(P,port),CAT2(_,pin))

// OCM port I/O defintions
#define SCL BNAME(CLK_PORT, CLK_PIN)
#define SDA BNAME(DATA_PORT, DATA_PIN)

#define IO_DIR_PORT_PIN(port, pin, dir) \
{\
  if ( dir == IO_OUT ) \
    PNAME(port,DIR) |= (1<<(pin)); \
  else \
    PNAME(port,DIR) &= ~(1<<(pin)); \
}

#define DATA_HIGH()\
{ \
  IO_DIR_PORT_PIN(DATA_PORT, DATA_PIN, IO_IN); \
  SDA = 1;\
}

#define DATA_LOW() \
{ \
  IO_DIR_PORT_PIN(DATA_PORT, DATA_PIN, IO_OUT); \
  SDA = 0;\
}

#define DATA_OUT_HIGH() \
{ \
  IO_DIR_PORT_PIN(DATA_PORT, DATA_PIN, IO_OUT); \
  SDA = 1;\
}

#define IO_FUNC_PORT_PIN(port, pin, func) \
{ \
  if( port < 2 ) \
  { \
    if ( func == IO_PER ) \
      PNAME(port,SEL) |= (1<<(pin)); \
    else \
      PNAME(port,SEL) &= ~(1<<(pin)); \
  } \
  else \
  { \
    if ( func == IO_PER ) \
      P2SEL |= (1<<(pin>>1)); \
    else \
      P2SEL &= ~(1<<(pin>>1)); \
  } \
}

#define IO_IMODE_PORT_PIN(port, pin, mode) \
{ \
  if ( mode == IO_TRI ) \
    PNAME(port,INP) |= (1<<(pin)); \
  else \
    PNAME(port,INP) &= ~(1<<(pin)); \
}

#define IO_PUD_PORT(port, dir) \
{ \
  if ( dir == IO_PDN ) \
    P2INP |= (1<<(port+5)); \
  else \
    P2INP &= ~(1<<(port+5)); \
}

// Clock Speed
#define CLKSPEED ( CLKCONSTA & 0x40 )

/**************************************************************************************************
* @fn  halWait
*
* @brief
*      This function waits approximately a given number of m-seconds
*      regardless of main clock speed.
*
* Parameters:
*
* @param  uint8	 wait
*         The number of m-seconds to wait.
*
* @return void
*
******************************************************************************/
extern void halWait(uint8 wait);

extern void halWaitUs(uint16 microSecs);

extern void halWaitSec(uint16 wait);

extern void halWaitms(uint16 wait);

#if defined SPI || defined COORDINATOR // buffer che uso sia nel modem che nel SD Card
extern void BytesToChar(uint8* buffer,uint16 lenghtBytes);
#endif

/**************************************************************************************************
* @fn  InitI2C
*
*
*
******************************************************************************/
/* I2C Internal Functions */

extern void Peripheral_Init( uint8 task_id );

extern uint16 Peripheral_ProcessEvent( uint8 task_id, uint16 events );

extern void initI2C (void);

extern uint8 smbSend(byte *buffer, const uint8 n);

extern bool smbSendbyte(byte b);

extern void smbWrite(bool value);

extern uint8 smbReceive(byte address, uint16 length, byte *buffer);

extern byte smbReceivebyte(void);

extern void smbClock(bool value);

extern void smbStart(void);

extern void smbStop(void);

extern void wait(void);

extern void resetBoard(void);

#if defined (PIOGGIA) || defined (VENTO) || defined (PRESS0) || defined (PRESS1) || defined (PRESS2) || defined (PRESS3)

extern uint8 smbReceiveLongAtTiny(byte addressDevice,byte addressByte, byte *buffer);

#endif