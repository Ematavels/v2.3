/******************************************************************************
*                                                                             *
*        **********                                                           *
*       ************                                                          *
*      ***        ***                                                         *
*     ***    ++    ***                                                        *
*     ***   +  +   ***                      CHIPCON                           *
*     ***   +                                                                 *
*     ***   +  +   ***                                                        *
*     ***    ++    ***                                                        *
*      ***        ***                                                         *
*       ************                                                          *
*        **********                                                           *
*                                                                             *
*******************************************************************************

Filename:     eeprom.h
Target:       cc2430
Author:       EFU
Revised:      1/3-2007
Revision:     1.1

Description:
Function declarations for common EEPROM functions for use with the CC2430DB.

All functions defined here are implemented in eeprom.c.

******************************************************************************/

#ifdef __cplusplus
extern "C"
{
#endif

#if defined RS485


/**************************************************************************************************
 * INCLUDES
 **************************************************************************************************/
#include "hal_board.h"

extern uint8 RS485_TaskID;
extern uint8 dstAddress;
extern uint8 reqSent;
extern bool doRS485Read;

extern uint16 saveMeasure1,saveTemperature1; // Interfaccia1
extern uint16 saveMeasure2,saveTemperature2; // Interfaccia2

extern void RS485_Init( uint8 task_id );
extern uint16 RS485_ProcessEvent( uint8 task_id, uint16 events );
extern void rxRS485( uint8 port, uint8 event );

/* UART Settings */
#define RS485_BAUD  HAL_UART_BR_9600
#define RS485_THRESH  64
#define RS485_RX_MAX  20
#define RS485_TX_MAX  20
#define RS485_IDLE  100

#define NO_RSP_TIME  2000
#define MAX_CONVERTER 2
#define RS_RETRIES 1  // 1 retry

#define RX_CNT_BUFFER  RS485_RX_MAX    // dimensione buffer ricezione modem - lo metto come max uart

// eventi rs485
#define NEXT_CMD_RS485 0x0001
#define DATA_RX_RS485  0x0002
#define NO_RSP_EVENT   0x0004

/******************************/

#endif //modem

