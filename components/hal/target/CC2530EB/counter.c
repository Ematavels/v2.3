/******************************************************************************
*                                                                             *
*        **********                                                           *
*       ************                                                          *
*      ***        ***                                                         *
*     ***    ++    ***                                                        *
*     ***   +  +   ***                      CHIPCON                           *
*     ***   +                                                                 *
*     ***   +  +   ***                                                        *
*     ***    ++    ***                                                        *
*      ***        ***                                                         *
*       ************                                                          *
*        **********                                                           *
*                                                                             *
*******************************************************************************

Filename:     counter.c
Target:       cc2430
Author:       EFU
Revised:      1/3-2007
Revision:     1.1

Description:
Function implementations for common counter functions for use with the CC2430DB.

******************************************************************************/
#if defined (COUNTER) || defined (PLUVIOMETRO)

#include "counter.h"
#include "ioCC2530.h"
#include "hal_timer.h"
#include "ZComDef.h"
#include "hal_i2c.h"
#include "hal_led.h"
#include "OSAL_Timers.h"
#include "OSAL.h"

uint8 retCount;

bool resetCounter(void)
{
  uint8 resetByte[2]={0x00,0x00};

  retCount=writeCounter(resetByte,0x0008,0x02);
  halWait(25);  // tempo per la eeprom 

  return retCount;
}


bool selectCounter(uint8 select)
{
   byte selectBuffer[2];

   // init the retries
   i2cRetries=I2C_RETRIES;

   /* Scelgo il counter */
   selectBuffer[0] = SELECT_COUNTER_WR;

   if (select==0x00)  selectBuffer[1] = CHANNEL_0;
   else selectBuffer[1] = CHANNEL_1;

   if (smbSend(selectBuffer, 2) > 0 )   retCount=TRUE;
   else retCount=FALSE;

   halWait(25);

   return retCount;
}


/******************************************************************************
* See counter.h for a description of this function.
******************************************************************************/
bool readCounter( byte* buffer, byte address, uint8 length)
{
   byte i;
   byte transmitBuffer[2];
   uint8 dummy[1]={0x01};

   // timer error avviato
  if (systemStart) 
   osal_start_timerEx(Peripheral_TaskID, I2C_ERROR_EVENT, I2C_ERROR_TIME);
 
   // init the retries
   i2cRetries=I2C_RETRIES;

   // dummy write to move index
   writeCounter(dummy,0x00,0x01);

   // imposto il buffer del counter scelto
   transmitBuffer[0] = COUNTER_WRITEADDRESS;
   transmitBuffer[1] = address;

   for(i = 0; i < 2; i++)
   {
     i2cAck=0; // init della variabile
     while ( (i2cAck==0) && (i2cRetries>0)) //Keep sending until ACK received
     smbSendbyte(transmitBuffer[i]);
   }

   smbClock(0);
   wait();
   DATA_HIGH();
   smbClock(1);

   retCount=smbReceive(COUNTER_READADDRESS, length, buffer); // se >0 Ok

   if (retCount==0) HalLedSet (HAL_LED_1, HAL_LED_MODE_ON);  // led rosso ON per segnalare malfunzionamento

   // stop timer error
  if (systemStart) 
   osal_stop_timerEx(Peripheral_TaskID,I2C_ERROR_EVENT);
   
   return retCount;
}


byte writeCounter( byte* buffer, byte address, int8 length)
{
   uint8 i = 0;
   uint8 j;
   byte transmitBuffer[COUNTER_SIZE];

   // timer error avviato
  if (systemStart) 
   osal_start_timerEx(Peripheral_TaskID, I2C_ERROR_EVENT, I2C_ERROR_TIME);

   // init the retries
   i2cRetries=I2C_RETRIES;

   transmitBuffer[i++] = COUNTER_WRITEADDRESS;
   transmitBuffer[i++] = (byte) (address);

   for(j = 0; j < length; j++)
   {
      transmitBuffer[i++] = buffer[j];
   }

    // scrivo sul counter scelto
   if (smbSend(transmitBuffer, i) > 0 )  retCount=TRUE;
   else retCount=FALSE;

  if (retCount==FALSE) HalLedSet (HAL_LED_1, HAL_LED_MODE_ON);  // led rosso ON per segnalare malfunzionamento

   // stop timer error
  if (systemStart) 
   osal_stop_timerEx(Peripheral_TaskID,I2C_ERROR_EVENT);

  return retCount;
}

#endif
