#if defined SPI

#include "hal_types.h"
#include "hal_board.h"
#include "hal_defs.h"
#include "hal_sleep.h"
#include "sdcard.h"
#include "hal_spi.h"
#include "hal_i2c.h"
#include <string.h>

/***********************************
    Variabili
***********************************/
uint8    response[50];
uint8    SDcmd[10];
bool     CardType=0;
uint16   timeout8;
uint16   timeout16;
uint32   arg;

/***********************************
    Comandi SD
***********************************/

uint8 SD_Command(uint8 cmd, uint32 arg, uint8 crc)
{ 
  int count = 10;
  uint8 reply;
  SDcmd[0] =  0x40 | cmd;
  SDcmd[1] = arg>>24;
  SDcmd[2] = arg>>16;
  SDcmd[3] = arg>>8;
  SDcmd[4] = arg;
  SDcmd[5] = crc;
  SSN=LOW;
  SPIWrite(SDcmd, 6);
  while(count > 0)
  {
    reply = SPIReadByte();
    if (reply != 0xFF)
    {
      SSN=HIGH;
      return reply;
    }
    count--;
  }
  SSN=HIGH;
  return 0xCC;
}


/******************************************************************************
                                SD INITIALIZATION
*******************************************************************************/
  
bool SDInit(SD_CARD *sd_card)
{     
  uint32 c_size=0;
  uint8 c_size_mult=0;
  uint8 block_len=0;
  
  memset(response,0x00,sizeof(response));
  
    /* Init the card in SPI mode by sending 80 clks */
    for(uint8 i = 0; i < 10; i++)
    {
      SSN=HIGH;
      SPIWriteByte(0xFF);
      SSN=LOW;
    }


SSN=HIGH;
SSN=LOW;

/*****************************************
               RESET CARD
******************************************/

//Nicolas 15/12/2017
// timeout8 � diventato uint16 , il timeout iniziale era 200
// inoltre non era considerato nel ciclo.
timeout8 = 500; 
do
{
 response[0] = SD_Command(CMD_GO_IDLE_STATE, 0x00000000,0x95);                 //CMD0
 timeout8--;
}
while((response[0]!=0x01) && (timeout8 != 0x00));

/**************************************************************************
**************************************************************************/
timeout8 = 100; 
do
{
  response[1] = SD_Command(CMD_SEND_IF_COND, 0x000001AA, 0x87);                 //CMD8
  timeout8--;
}
while((response[1] != 0x01) && (timeout8 != 0x00));

if (timeout8 == 0x00)
{
  sd_card->init_error1 = 1;
  return 1;
}

SSN=HIGH;         
SSN=LOW;

  for(int n=1;n<=4;n++)
    response[n+1] = SPIReadByte();
 
    if(response[5]!=0xAA)
      CardType=1; 

/**************************************************************************
**************************INIZIALIZZO LA SCHEDA***************************/
  
arg = CardType ? 0x40000000 : 0;
timeout8 = 250; 
do
{ 
  response[6] = SD_Command(CMD_APP_CMD, 0,0x65);                                //CMD55

  response[7] = SD_Command(ACMD_SEND_OP_COND, arg,0x77);                        //ACMD41
  timeout8--;
} 
while( (response[7] != 0x00) && (timeout8 != 0x00));

if (timeout8 == 0x00)
{
  sd_card->init_error2 = 1;
  return 1;
}


/**************************************************************************
********************************LEGGO OCR*********************************/

timeout8 = 30; 
do
{
  response[8] = SD_Command(CMD_READ_OCR, 0,0xFF);                               //CMD58
  timeout8--;
}while((response[8] != 0x00) && (timeout8 != 0x00));

if (timeout8 == 0x00)
{
  sd_card->init_error3 = 1;
  return 1;
}

SSN=HIGH;         
SSN=LOW;

  for(int n=1;n<=4;n++)
    response[n+8] = SPIReadByte();



if((response[9] & 0x40) == 0x40)
sd_card->hc=1;
else
{
  timeout8 = 100;
  sd_card->hc=0;
  do {
             response[13] = SD_Command(CMD_SEND_OP_COND, 0,0xFF);               // send op command with HCS = 0
             timeout8--;
      } while ((response[13] != 0x00) && (timeout8 != 0x00));
}

/**************************************************************************
**************************************************************************/
    for(uint8 i = 0; i < 10; i++)
    {
      SSN=HIGH;
      SPIWriteByte(0xFF);
      SSN=LOW;
    }
timeout8 = 30; 
do
{
  response[14] = SD_Command(CMD_SEND_CSD,0, 0xFF);                              //CMD9
  timeout8--;
}while((response[14] != 0x00)&& (timeout8 != 0x00));

if (timeout8 == 0x00)
{
  sd_card->init_error4 = 1;
  return 1;
}

SSN=HIGH;        
SSN=LOW;

timeout8 = 230;

do {                                                
         response[15] = SPIWriteByte(0xFF);                                    
         timeout8--;
     } while ((response[15] != 0xFE) && (timeout8 != 0x00));

if (timeout8 == 0x00)
{
  sd_card->init_error5 = 1;
  return 1;
}

  for(int n=1;n<18;n++)
    response[n+15] = SPIReadByte(); 
/**************************************************************************
**********************CALCOLO I PARAMETRI SCHEDA**************************/

if(response[16] & 0xC0) {                                                        //Check CSD_STRUCTURE field for v2+ struct device    
         //Must be a v2 device (or a reserved higher version, that doesn't currently exist)
         //Extract the C_SIZE field from the response.  It is a 22-bit number in bit position 69:48.  This is different from v1.  
         //It spans bytes 7, 8, and 9 of the response.
         c_size = (((uint32)response[23] & 0x3F) << 16) | ((uint8)response[24] << 8) | response[25];
         sd_card->size = ((uint32)(c_size + 1) * (uint8)(1024u)) - 1;
     }else{
         //Must be a v1 device.
         //Extract the C_SIZE field from the response.  It is a 12-bit number in bit position 73:62.  
         //Although it is only a 12-bit number, it spans bytes 6, 7, and 8, since it isn't byte aligned.
         c_size = ((uint32)response[22] << 16) | ((uint8)response[23] << 8) | response[24];    //Get the bytes in the correct positions
         c_size &= 0x0003FFC0;    //Clear all bits that aren't part of the C_SIZE
         c_size = c_size >> 6;    //Shift value down, so the 12-bit C_SIZE is properly right justified in the uint32.
         //Extract the C_SIZE_MULT field from the response.  It is a 3-bit number in bit position 49:47.
         c_size_mult = ((uint8)((response[25] & 0x03) << 1)) | ((uint8)((response[26] & 0x80) >> 7));
         //Extract the BLOCK_LEN field from the response. It is a 4-bit number in bit position 83:80.
         block_len = response[21] & 0x0F;
         block_len = 1 << (block_len - 9); //-9 because we report the size in sectors of 512 bytes each
         sd_card->size = ((uint32)(c_size + 1) * (uint8)((uint8)1 << (c_size_mult + 2)) * block_len) - 1;
     }

/**************************************************************************
*************************SETTO IL BLOCKLENGTH*****************************/
  
do {                                                
         response[34] = SD_Command(CMD_SET_BLOCKLEN, 512,0xFF);                                    
         timeout8--;
     } while ((response[34] != 0x00) && (timeout8 != 0x00));

if (timeout8 == 0x00)
{
  sd_card->init_error6 = 1;
  return 1;
}

do {                                                
         response[35] = SD_Command(CMD_CRC_ON_OFF, 0,0xFF);                                    
         timeout8--;
     } while ((response[35] != 0x00) && (timeout8 != 0x00));

if (timeout8 == 0x00)
{
  sd_card->init_error6 = 1;
  return 1;
}



SSN=HIGH;

//Alzo la velocit�

U0GCR  = HAL_SPI_TRANSFER_MSB_FIRST | HAL_SPI_CLOCK_PHA_0 | HAL_SPI_CLOCK_POL_LO | 0x0F;

return 0;   //  SD Card Initialized
}



/**************************************************************************
                            WRITE SECTOR
**************************************************************************/



uint8 SD_WriteSector ( uint32 SectorNumber, uint8 *Buffer) {
     // write 512 byte buffer to sd_card sector at address, return next available sector, 0 if error
     int                 i;
     uint8 tx_response;
     
     SSN = LOW;                                                                 // unassert card (because main code will expect this)    
     

     SPIWriteByte(0xFF); SPIWriteByte(0xFF);
     response[35] = SD_Command(CMD_WRITE_SINGLE_BLOCK,SectorNumber << 9,0xFF);  // send CMD24, leave card asserted                                                                 // command completed OK
          while(SPIReadByte()!=0x00);
          SSN = HIGH;
          SSN = LOW;  
          SPIWriteByte(0xFE);                                                   // send data start token
          for (i=0;i<512;i++)    SPIWriteByte(Buffer[i]);                       // send buffer                           // send crc (don't care)
              SPIWriteByte(0xFF); SPIWriteByte(0xFF);
          tx_response =SPIReadByte();

   while(SPIReadByte()!=0xFF);
     
     SSN = HIGH;                                                                // unassert card (because main code will expect this)    
     
     return tx_response;                                                        // return next address
 }


/**************************************************************************
                            READ SECTOR
**************************************************************************/

uint8 SD_ReadSector( uint32 SectorNumber, uint8 *Buffer )
{
   uint8 b;
                       
   SSN=LOW;
   
   /* send block-read command... */
   
   response[38] = SD_Command( CMD_READ_SINGLE_BLOCK, SectorNumber << 9,-1);

   SSN=HIGH;         
   SSN=LOW;
   
   /*----------Wait for Token----------*/
   timeout16 = 0xFFFF;
   while((SPIReadByte()!=0xFE) && (timeout16!= 0x0000)) timeout16--;
   
   if (timeout16 == 0x0000) {                                                   // check that command was accepted                                              // write command not accepted
         SSN=HIGH;                                                              // unassert (because main code will not expect to)
         return 1;
   }
   

  for(uint16 i = 0; i < 512; i++)  
    {  
      b = SPIReadByte();    
      *Buffer++ = b;  
    }
   
   timeout16 = 0xFFFF;
   while((SPIReadByte()!=0x00) && (timeout16!= 0x0000)) timeout16--;

   /* release the CS line... */
   
   SSN=HIGH;         

   return 0;
}

/**************************************************************************
                            MULTI READ SECTOR
**************************************************************************/

uint8 SD_readMultipleBlock (uint32 startBlock, uint32 totalBlocks, uint8 *buffer)
{
uint16 i=0;
uint16 retry=0;

SSN=LOW;
   
response[49] = SD_Command(CMD_READ_MULTIPLE_BLOCK, startBlock <<9, 0xFF);       //read a Block command
//block address converted to starting address of 512 byte Block

while( totalBlocks )
{ 
   SSN=HIGH;         
   SSN=LOW;
   
   retry = 0;
   timeout16 = 0xFFFF;
   while((SPIReadByte()!=0xFE) && (timeout16!= 0x0000)) timeout16--;

  for(i=0; i<512; i++)                                                          //read 512 bytes
  {
    buffer[i] = SPIReadByte();
  }
  SPIWriteByte(0xFF);                                                           //receive incoming CRC (16-bit), CRC is ignored here
  SPIWriteByte(0xFF);
  
  SPIWriteByte(0xFF);                                                           //extra 8 cycles
  
  totalBlocks--;
}

SD_Command(CMD_STOP_TRANSMISSION, 0, 0xFF);                                     //command to stop transmission

timeout16=40;
while((SPIReadByte()!=0xFF) && (timeout16!= 0x0000)) timeout16--;;

SSN=HIGH;

return 0;
}


/**************************************************************************
                            MULTI WRITE SECTOR
**************************************************************************/

uint8 SD_writeMultipleBlock(uint32 startBlock, uint32 totalBlocks, uint8 *buffer)
{
uint8 response;
unsigned int i, retry=0;
uint32 blockCounter=0;

SSN=LOW;

response = SD_Command(CMD_WRITE_MULTIPLE_BLOCK, startBlock<<9, 0xFF);           //write a Block command
if(response != 0x00)                                                            //check for SD status: 0x00 - OK (No flags set)
  return response;


   SSN=HIGH;         
   SSN=LOW;
   
while( blockCounter < totalBlocks )
{
   SPIWriteByte(0xfc);                                                          //Send start block token 0xfc 

   for(i=0; i<512; i++)                                                         //send 512 bytes data
 	SPIWriteByte( buffer[i]);

   SPIWriteByte(0xff);                                                          //transmit dummy CRC (16-bit), CRC is ignored here
   SPIWriteByte(0xff);

   response = SPIReadByte();
   if( (response & 0x1f) != 0x05)                                               
   {        	                                                                
      SSN=HIGH;                                                                 
      return response;
   }

   while(SPIReadByte()!=0xFF);

   SPIReadByte();                                                               //extra 8 bits
   blockCounter++;
}

SPIWriteByte(0xfd);                                                             //send 'stop transmission token'

retry = 0;

while(!SPIReadByte())                                                           //wait for SD card to complete writing and get idle
   if(retry++ > 0xfffe){SSN=HIGH; return 1;}

SSN=HIGH;
SPIWriteByte(0xff);                                                             //just spend 8 clock cycle delay before reasserting the CS signal
SSN=LOW;                                                                        //re assertion of the CS signal is required to verify if card is still busy

while(!SPIReadByte())                                                           //wait for SD card to complete writing and get idle
   if(retry++ > 0xfffe){SSN=HIGH; return 1;}
SSN=HIGH;

return 0;
}
#endif