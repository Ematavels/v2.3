/******************************************************************************
*                                                                             *
*        **********                                                           *
*       ************                                                          *
*      ***        ***                                                         *
*     ***    ++    ***                                                        *
*     ***   +  +   ***                      CHIPCON                           *
*     ***   +                                                                 *
*     ***   +  +   ***                                                        *
*     ***    ++    ***                                                        *
*      ***        ***                                                         *
*       ************                                                          *
*        **********                                                           *
*                                                                             *
*******************************************************************************

Filename:     GS3Parser.c
Target:       cc2430
Author:       EFU
Revised:      1/3-2007
Revision:     1.1
******************************************************************************/
#if defined (MPS6_P04)

#include "SdiCore.h"
#include "MPS6Parser.h"
#include "ioCC2530.h"
#include "hal_types.h"
#include "hal_sleep.h"
#include "OSAL_Timers.h"
#include "mac_api.h"
#include "astronomic.h"



void Mps6Parser(byte *buffer2,int16 *resbuf);

void hexMPS6Multiplier (byte *databuf,uint16 dotpos,uint8 TYPE,int16 *resbuf);

uint8 ipb_MPS6;    //indice di buffMPS6Pars
//uint8 aft,bef;  //contatori caratteri prima e dopo il punto
uint8 split_MPS6[2]; //vettore contenten prima e dopo da passare a hexmultiplier
uint16 somma_MPS6;
bool negtemp_MPS6=false;   //tiene traccia del segno "-" nella temperatura


uint16 d1_MPS6;    //risultato posizione del punto nel primo dato (TENS per GS3), unisce "bef" e "aft"
uint16 d2_MPS6;    //risultato posizione del punto nel secondo dato (TEMP per GS3)  unisce "bef" e "aft"
uint16 d3_MPS6;    //risultato posizione del punto nel terzo dato (EC per GS3) unisce "bef" e "aft"


//uint8 pi;

uint8 buffMPS6Pars[4]={0xFF,0xFF,0xFF,0xFF}; // vettore degli indici posizioni "+" e "-"
uint8 dotn_MPS6=0;
uint8 idn_MPS6;    // indice per copiare valori nei vari data1_MPS6,data2_MPS6,data3_MPS6

uint8 data1_MPS6[8]={0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};       // TENS  (1-80) € [0,01]
uint8 data2_MPS6[4]={0xFF,0xFF,0xFF,0xFF};            // TEMP (-40 , +50) °C [0,1]


uint16 test1_MPS6;   //Variabili di appoggio per BUILD risultato finale misura
uint16 test2_MPS6;
uint16 test3_MPS6;
uint32 testmax_MPS6;
uint32 sommamax_MPS6;


void Mps6Parser(byte *buffer2,int16 *resbuf)
{
  //pi=0; // indice del for e if
  uint8 u=0;
  ipb_MPS6=0;
  
  //Inserire reset delle misure
  
  negtemp_MPS6=false;
  //Ricavo i pivot dei dati ( le posizioni dei segni "+" e "-" della stringa
  for(u=0;u<SDI_BUFFER_LENGTH;u++)
  {
    
    //Se uguale a "+", "-" , o "CR" quando la posizione è diversa da 0 ( comincia sempre per CR)
    if(buffer2[u]==0x2B || buffer2[u]==0x2D || (buffer2[u]==0x0D && u!=0))
    {
      buffMPS6Pars[ipb_MPS6]=u;     //Salvo le posizioni in un buffer per il parsing  
      if(buffer2[u]==0x2D && ipb_MPS6!=0x00)negtemp_MPS6=true; // modificato
      ipb_MPS6++;    
    }
  }
  
  for(u=0;u<sizeof(data1_MPS6);u++)
  {
    data1_MPS6[u]=0xFF;
  }
  
  for(u=0;u<sizeof(data2_MPS6);u++)
  {
    data2_MPS6[u]=0xFF;
  }
  
  
  //Separo i 3 diversi valori in 2 buffer separati
  //data1_MPS6= TENS
  idn_MPS6=0;
  for(u=buffMPS6Pars[0]+1;u<buffMPS6Pars[1];u++){
    data1_MPS6[idn_MPS6]=buffer2[u];
    idn_MPS6++;
  }
  
  idn_MPS6=0;
  //data2_MPS6=TEMPERATURA
  for(u=buffMPS6Pars[1]+1;u<buffMPS6Pars[2];u++){
    data2_MPS6[idn_MPS6]=buffer2[u];
    idn_MPS6++;
  }
  
 
  //Falsifico i risultati per test
  asm("NOP");
  
  // Trovo il punto  per i 3 buffer dati
  d1_MPS6=finddot(data1_MPS6,0x08);
  // Lo converto in un valore esadecimale buono per essere spedito via radio
  hexMPS6Multiplier(data1_MPS6,d1_MPS6,TENS,resbuf);
  
  d2_MPS6=finddot(data2_MPS6,0x04);
  asm("NOP");
  hexMPS6Multiplier(data2_MPS6,d2_MPS6,TEMP,resbuf);
  
  
 
  
}





  

void hexMPS6Multiplier (byte *databuf,uint16 dotpos, uint8 TYPE,int16 *resbuf)
{
  
  //Spezzo il valore uint16 del contatore caratteri in prima e dopo.
  split_MPS6[0] = (dotpos >> 8) & 0xff;  //Parte alta
  split_MPS6[1] = dotpos & 0xff;         //Parte Bassa
  asm("NOP");
  
  if(split_MPS6[0]>0x03 )     //piu di 1000
  {
    somma_MPS6=9999;
  }
  //3:1
  else if (split_MPS6[0]==0x03 && split_MPS6 [1]==0x01)
  {
    test1_MPS6=BUILD_UINT8(databuf[0],databuf[1]);
    test1_MPS6=HextoBCD(test1_MPS6);
    test2_MPS6=BUILD_UINT8(databuf[2],databuf[4]);
    test2_MPS6=HextoBCD(test2_MPS6);
    test1_MPS6=test1_MPS6*0x64;
    somma_MPS6=test1_MPS6 + test2_MPS6;
    asm("NOP");
  }
  
  //3:0  
  else if (split_MPS6[0]==0x03 && split_MPS6 [1]==0x00)
  {
    test1_MPS6=BUILD_UINT8(databuf[0],databuf[1]);
    test1_MPS6=HextoBCD(test1_MPS6);
    test2_MPS6=BUILD_UINT8(databuf[2],0x00);
    test2_MPS6=HextoBCD(test2_MPS6);
    test1_MPS6=test1_MPS6*0x64;
    somma_MPS6=test1_MPS6 + test2_MPS6;
    asm("NOP");
  }
  
  
  //2:1
  else if (split_MPS6[0]==0x02 && split_MPS6 [1]==0x01)
  {
    test1_MPS6=BUILD_UINT8(databuf[0],databuf[1]);
    test1_MPS6=HextoBCD(test1_MPS6);
    test2_MPS6=BUILD_UINT8(0x00,databuf[3]);
    test2_MPS6=HextoBCD(test2_MPS6);
    test1_MPS6=test1_MPS6*0x0A;
    somma_MPS6=test1_MPS6 + test2_MPS6;
    asm("NOP");
  }
  
  //2:0
  else if (split_MPS6[0]==0x02 && split_MPS6 [1]==0x00)
  {
    test1_MPS6=BUILD_UINT8(databuf[0],databuf[1]);
    test1_MPS6=HextoBCD(test1_MPS6);
    test1_MPS6=test1_MPS6*0x0A;
    somma_MPS6=test1_MPS6;
    asm("NOP");
  }
  
  
   //1:1
  else if(split_MPS6[0]==0x01 && split_MPS6 [1]==0x01)
  {
    /*
      test1_MPS6=BUILD_UINT8( 0x00,databuf[0]);
      test1_MPS6=HextoBCD(test1_MPS6);
      test2_MPS6=BUILD_UINT8(0x00,databuf[2]);
      test2_MPS6=HextoBCD(test2_MPS6);
      test1_MPS6=test1_MPS6*0x64;
      somma_MPS6=test1_MPS6 + test2_MPS6;
    */
      test1_MPS6=BUILD_UINT8( 0x00,databuf[0]);
      test1_MPS6=HextoBCD(test1_MPS6);
      test2_MPS6=BUILD_UINT8(0x00,databuf[2]);
      test2_MPS6=HextoBCD(test2_MPS6);
      test1_MPS6=test1_MPS6*0x0A;
      somma_MPS6=test1_MPS6+test2_MPS6;
  }
  
  
  //2:0
  /*else if (split_MPS6[0]==0x02 && split_MPS6 [1]==0x00)
  {
    if(TYPE==TENS || TYPE==TEMP)
    {
    test1_MPS6=BUILD_UINT8(databuf[0],databuf[1]);
    test1_MPS6=HextoBCD(test1_MPS6);
    test1_MPS6=test1_MPS6*0x64;
    test2_MPS6=BUILD_UINT16(0x00,0x00);
    somma_MPS6=test1_MPS6+test2_MPS6;
   
    //asm("NOP");
    }
    else
    {
    test1_MPS6=BUILD_UINT8(databuf[0],databuf[1]);
    test1_MPS6=HextoBCD(test1_MPS6);
    somma_MPS6=test1_MPS6;
    //asm("NOP");
    }
  
  }*/
  
  
 
  //1:0
  else if(split_MPS6[0]==0x01 && split_MPS6 [1]==0x00)
  {
    
    //if(TYPE==TENS || TYPE==TEMP)
    //{
      test1_MPS6=BUILD_UINT8(0x00,databuf[0]);
      test1_MPS6=HextoBCD(test1_MPS6);
      test1_MPS6=test1_MPS6*0x0A;
      somma_MPS6=test1_MPS6;
    /*}
    else
    {
      test1_MPS6=BUILD_UINT8(0x00,databuf[0]);
      test1_MPS6=HextoBCD(test1_MPS6);
      somma_MPS6=test1_MPS6;
      //asm("NOP");
    }*/
  }
  
  
 if(TYPE==TENS) resbuf[0]=somma_MPS6;
 else if (TYPE==TEMP)
 { 
   if(negtemp_MPS6==false)
   resbuf[1]=somma_MPS6;
   else
   {
    somma_MPS6=-somma_MPS6;
    resbuf[1]=somma_MPS6;
   }
 }
 else if (TYPE==TENS) resbuf[2]=somma_MPS6;

}

#endif