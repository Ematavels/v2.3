/******************************************************************************
*                                                                             *
*        **********                                                           *
*       ************                                                          *
*      ***        ***                                                         *
*     ***    ++    ***                                                        *
*     ***   +  +   ***                      CHIPCON                           *
*     ***   +                                                                 *
*     ***   +  +   ***                                                        *
*     ***    ++    ***                                                        *
*      ***        ***                                                         *
*       ************                                                          *
*        **********                                                           *
*                                                                             *
*******************************************************************************

Filename:     sht21_winet.h
Target:       cc2430
Author:       EFU
Revised:      1/3-2007
Revision:     1.1

Description:
Function declarations for common sht21_winet functions for use with the CC2430DB.

All functions defined here are implemented in sht21_winet.c.

******************************************************************************/

#ifdef __cplusplus
extern "C"
{
#endif


  /**************************************************************************************************
 * INCLUDES
 **************************************************************************************************/
#include "ZComDef.h"

/* General define */
#define SHT21_WRITEADDRESS   0x80
#define SHT21_READADDRESS    0x81

#define TEMP_MEASURE 0xF3
#define HUM_MEASURE  0xF5
#define WRITE_REGISTER  0xE6
#define READ_REGISTER  0xE7
#define RESET_SHT21  0xFE


/******************************************************************************
* @fn  readSht21
*
* @brief
*      Initiates sequential read of Sht21 content from the given address. The
*      read operation transfer _length_ bytes to _buffer_, starting at Sht21
*      address _address_, ending at _adress_ + _length_.
*
* Parameters:
*
* @param  BYTE*    buffer
*	  Pointer to buffer for storing read values.
*	
* @param  WORD     address
*	  Start address for read operation. Valid range: 0 - 0xFFF.
*
* @param  UINT8    length
*	  Number of bytes to be read.
*
* @return BOOL
*         TRUE: Read operation successful.
*         FALSE: Read operation NOT successful.
*
******************************************************************************/
extern bool readSht21(byte* buffer, byte address, uint8 length);

extern byte writeSht21(byte* buffer, byte address, int8 length);

extern byte resetSht21(void);