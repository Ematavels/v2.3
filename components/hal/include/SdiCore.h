/**************************************************************************************************
  Filename:       Decagon_GS3.h
  Revised:        $Date: 2007-09-11 10:58:41 -0700 (Tue, 11 Sep 2007) $
  Revision:       $Revision: 15371 $

  Description:    Describe the purpose and contents of the file.

**************************************************************************************************
 **************************************************************************************************/

#ifdef __cplusplus
extern "C"
{
#endif

#if defined FLORA_1 || DEC5TE_P04 || defined SENTEK_10A || defined SENTEK_10B || defined SENTEK_10C || defined SENTEK_10D || defined SENTEK_10E || defined SENTEK_10F || defined SENTEK_60 || defined SENTEK_90 || defined SENTEK_120 || defined SENTEK_30 || defined MPS6_P04 || defined DECGS3_P04 || defined DECCDT || defined ACC_TDR || defined ACC_TDTP

/**************************************************************************************************
 * INCLUDES
 **************************************************************************************************/
#include "ioCC2530.h"
#include "hal_types.h"
#include "ZComDef.h"
#include "hal_board_cfg.h"
#define SOIL 0
#define SAL 1
#define TEMP 2  
      
extern void halWait(uint8 wait);

extern void halWaitUs(uint16 microSecs);

extern void halWaitSec(uint16 wait);

extern void halWaitms(uint16 wait);

  
extern void sdiClock(bool value);  
extern void sdiSendByte(byte b);
extern void sdiWrite(byte b);
extern void sdiStart(void);
extern void sdiStop(void);
extern uint8 reverse_bit8(uint8 x);
extern void markingSdi(void);
extern uint8 receiveChar(byte *buffer,float trim);
extern void processSdiResponse(void);
extern void resetSdiBuffer(byte *buffer);
extern bool readSdi(uint8 sdiAddress, byte *buffer,float trim);
extern bool readSentek(uint8 sdiAddress, byte *buffer,byte* buffer2,byte *buffer3,byte *buffer4,float trim,uint8 nprobes,uint8 readType);
extern uint8 askSdi(uint8 sdiAddress,byte *buffer,float trim);
extern void sentekJoiner(byte *buffer,byte *buffer2,byte *destionation);
extern uint8 askSentek(uint8 sdiAddress,byte *buffer,byte *buffer2, byte *buffer3,byte *buffer4,float trim,uint8 nprobes,uint8 readType);
extern uint8 SDI_TaskID;
extern bool sdi_read;
extern uint16 finddot(byte *databuff,uint8 len);


#define SDI_DEF_CLOCK 735
#define SDI_BUFFER_LENGTH 40   ///Nicolas 14/02/2018
#define TRIM 1.5
#define SDI_ERROR_TIME  5000
// eventi sdi
#define SDI_ERROR_EVENT 0x0A0A

// the default cofiguration below uses P0.5 for SDI
#define MAX_SDI_COUNTER 60000  //60000
#define SDI_DEFAULT_TRIM 2
#define SDI_TRIM_2_0 2
#define SDI_TRIM_1_8 1.8
#define SDI_TRIM_1_3 1.3

#define GS3_1 0x0033
#define GS3_2 0x43
#define GS3_3 0x53
#define GS3_4 0x63
#define GS3_5 0x73

#define MPS6_1 0x0036
#define MPS6_2 0x46
#define MPS6_3 0x56
#define MPS6_4 0x66
#define MPS6_5 0x76



#ifndef SDI_PORT
  #define SDI_PORT  0
#endif

#ifndef SDI_PIN
  #define SDI_PIN   4
#endif



#define SDI_RETRIES 3

// General I/O definitions
#define IO_GIO  0  // General purpose I/O
#define IO_PER  1  // Peripheral function
#define IO_IN   0  // Input pin
#define IO_OUT  1  // Output pin
#define IO_PUD  0  // Pullup/pulldn input
#define IO_TRI  1  // Tri-state input
#define IO_PUP  0  // Pull-up input pin
#define IO_PDN  1  // Pull-down input pin

    /* I/O PORT CONFIGURATION */
#define CAT1(x,y) x##y  // Concatenates 2 strings
#define CAT2(x,y) CAT1(x,y)  // Forces evaluation of CAT1

// OCM port I/O defintions
// Builds I/O port name: PNAME(1,INP) ==> P1INP
#define PNAME(y,z) CAT2(P,CAT2(y,z))
// Builds I/O bit name: BNAME(1,2) ==> P1_2
#define BNAME(port,pin) CAT2(CAT2(P,port),CAT2(_,pin))

// OCM port I/O defintions
#define SDI BNAME(SDI_PORT, SDI_PIN)




#define IO_DIR_PORT_PIN(port, pin, dir) \
{\
  if ( dir == IO_OUT ) \
    PNAME(port,DIR) |= (1<<(pin)); \
  else \
    PNAME(port,DIR) &= ~(1<<(pin)); \
}

#define SDI_DATA_HIGH()\
{ \
  IO_DIR_PORT_PIN(SDI_PORT, SDI_PIN, IO_IN); \
}

#define SDI_DATA_LOW() \
{ \
  IO_DIR_PORT_PIN(SDI_PORT, SDI_PIN, IO_OUT); \
  SDI = 0;\
}

#define SDI_DATA_OUT_HIGH() \
{ \
  IO_DIR_PORT_PIN(SDI_PORT, SDI_PIN, IO_OUT); \
  SDI = 1;\
}

#define IO_FUNC_PORT_PIN(port, pin, func) \
{ \
  if( port < 2 ) \
  { \
    if ( func == IO_PER ) \
      PNAME(port,SEL) |= (1<<(pin)); \
    else \
      PNAME(port,SEL) &= ~(1<<(pin)); \
  } \
  else \
  { \
    if ( func == IO_PER ) \
      P2SEL |= (1<<(pin>>1)); \
    else \
      P2SEL &= ~(1<<(pin>>1)); \
  } \
}

#define IO_IMODE_PORT_PIN(port, pin, mode) \
{ \
  if ( mode == IO_TRI ) \
    PNAME(port,INP) |= (1<<(pin)); \
  else \
    PNAME(port,INP) &= ~(1<<(pin)); \
}

#define IO_PUD_PORT(port, dir) \
{ \
  if ( dir == IO_PDN ) \
    P2INP |= (1<<(port+5)); \
  else \
    P2INP &= ~(1<<(port+5)); \
}

// Clock Speed
#define CLKSPEED ( CLKCONSTA & 0x40 )  

#endif
