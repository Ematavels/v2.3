/**************************************************************************************************
  Filename:       Decagon_5TE.h
  Revised:        $Date: 2007-09-11 10:58:41 -0700 (Tue, 11 Sep 2007) $
  Revision:       $Revision: 15371 $

  Description:    Describe the purpose and contents of the file.

**************************************************************************************************
 **************************************************************************************************/

#ifdef __cplusplus
extern "C"
{
#endif

#if defined SENTEK_10A || defined SENTEK_10B || defined SENTEK_10C || defined SENTEK_10D || defined SENTEK_10E || defined SENTEK_10F  || defined SENTEK_60 || defined SENTEK_30 || defined SENTEK_90 || defined SENTEK_120 || defined SENTEK_30_B
/**************************************************************************************************
 * INCLUDES
 **************************************************************************************************/
#include "ioCC2530.h"
#include "hal_types.h"
#include "ZComDef.h"
#include "hal_board_cfg.h"

extern void SenParser(byte *buffer2, int16 *resbuf,uint8 index);
extern void SenParser10(byte *buffer2, int16 *resbuf,uint8 index);
bool ParserSentek(byte *inputbuffer,int16 *resbuffer,uint8 size,uint8 idxofbuffer,uint8 type);

#endif