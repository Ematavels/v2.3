 /**************************************************************************************************/

#ifdef __cplusplus
extern "C"
{
#endif

#if defined FLORA_1
  
/**************************************************************************************************
 * INCLUDES
 **************************************************************************************************/
#include "ioCC2530.h"
#include "hal_types.h"
#include "ZComDef.h"
#include "hal_board_cfg.h"

extern void FloraParser(byte *buffer2, int16 *resbuf);
extern void hexFloraMultiplier (byte *databuf,uint16 dotpos, uint8 TYPE, int16 *resbuf);
extern uint16 findFloradot(byte *databuff,uint8 len);

#define FADD 1
#define FPOT 2
#define FMV 3
#define FRES 4


#endif