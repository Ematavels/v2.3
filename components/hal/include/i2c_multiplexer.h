/* 
Header file for i2c_multiplexer.h
*/

#ifdef __cplusplus
extern "C"
{
#endif
  
  
  
/**************************************************************************************************
 * INCLUDES
 **************************************************************************************************/
#include "ZComDef.h"
  
  
/* General Define */
//Indirizzo 0x70, shiftato per i2c 0x0E(scrittura), 0xE1(lettura)
#define TCA9548A_ADDRESS 0x70 // Datasheet with A0,A1,A2 to GND 
#define SELECT_TCA9548_W     TCA9548A_ADDRESS<<1
#define SELECT_TCA9548_R     (TCA9548A_ADDRESS<<1)|0x01


// MUX Channels
#define NO_CHANNEL 0x00  
#define CH_0  0x01
#define CH_1  0x02
#define CH_2  0x04
#define CH_3  0x08
#define CH_4  0x10
#define CH_5  0x20
#define CH_6  0x40
#define CH_7  0x80  
  

/* Global Functions */
  
extern uint8 countSht3x[4];  

extern uint8 checkMux;
  
extern bool multiplexerChannel(uint8 select);  

extern void checkSensorConnected(void);