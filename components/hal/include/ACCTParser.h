 /**************************************************************************************************/

#ifdef __cplusplus
extern "C"
{
#endif

#if defined ACC_TDT || defined ACC_TDTP || defined ACC_TDR
  
/**************************************************************************************************
 * INCLUDES
 **************************************************************************************************/
#include "ioCC2530.h"
#include "hal_types.h"
#include "ZComDef.h"
#include "hal_board_cfg.h"

extern void AcctParser(byte *buffer2, int16 *resbuf);
extern void hexACCTMultiplier (byte *databuf,uint16 dotpos, uint8 TYPE, int16 *resbuf);
extern uint16 findACCdot(byte *databuff,uint8 len);


#define AVWC 1
#define ATEMP 2
#define APERM 3
#define AEC 4
#define AECPORE 5


#endif