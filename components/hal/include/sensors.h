/******************************************************************************
*                                                                             *
*        **********                                                           *
*       ************                                                          *
*      ***        ***                                                         *
*     ***    ++    ***                                                        *
*     ***   +  +   ***                      CHIPCON                           *
*     ***   +                                                                 *
*     ***   +  +   ***                                                        *
*     ***    ++    ***                                                        *
*      ***        ***                                                         *
*       ************                                                          *
*        **********                                                           *
*                                                                             *
*******************************************************************************

Filename:     sensors.h
Target:       cc2430
Author:       EFU
Revised:      1/3-2007
Revision:     1.1

******************************************************************************/

#ifdef __cplusplus
extern "C"
{
#endif



/**************************************************************************************************
 * INCLUDES
 **************************************************************************************************/
#include "ZComDef.h"

extern uint8 readSensor (uint32 sensorId);

#if !defined SENTEK_120
extern uint16 readSensorM[10]; // imposto 0xFFFF che indica lettura errata ( es.i2c... )
#else
extern uint16 readSensorM[13]; // imposto 0xFFFF che indica lettura errata ( es.i2c... )
#endif

extern uint8 numSensTot;

#if defined (VENTO)
 extern uint8 vento[10];
#endif
 
#if defined (PIOGGIA)
 extern uint8 pioggia[2];
#endif
 
#if defined PRESS0
 extern uint8 press0[6];
#endif
#if defined PRESS1
 extern uint8 press1[6];
#endif
 #if defined PRESS2
 extern uint8 press2[6];
#endif
#if defined PRESS3
 extern uint8 press3[6];
#endif
 
#if defined (SENTEK_60) && (DECGS3_P04) || (SENTEK_60) && (ACC_TDTP) ||  (DECGS3_P04) 
  #error "Eh no caro! Non puoi definire sia GS3 che il secondo 5TE o Acclima perch� usano gli stessi indici!!"
#endif
 
#if defined (SENTEK_60) &&  (SENTEK_60_B) && (VENTO) && (MUX_TCA9584)
  #error "Eh no caro! Non puoi definire Sentek60 Sentek60B e vento perch� non c'� posto!! Maiala!"
#endif

#if defined (WINETHP) && (ACC_TDTP)
 #error "Eh no caro! Non puoi definire sia Acclima e WinetHP su questi ingressi!!"
#endif 
 
#if defined (WHPIN0_2048) && (WHPIN0_4096)
  #error "ERRORE! WinetHP Input0 � definito due volte"
#endif

#if defined (WHPIN0DIFF_2048) && (WHPIN0DIFF_4096)
  #error "ERRORE! WinetHP Input0 differenziale � definito due volte"
#endif

#if defined (WHPIN1_2048) && (WHPIN1_4096)
  #error "ERRORE! WinetHP Input1 � definito due volte"
#endif

#if defined (WHPIN1DIFF_2048) && (WHPIN1DIFF_4096)
  #error "ERRORE! WinetHP Input1 differenziale � definito due volte"
#endif

#if defined (WHPIN2_2048) && (WHPIN2_4096)
  #error "ERRORE! WinetHP Input2 � definito due volte"
#endif

#if defined (WHPIN2DIFF_2048) && (WHPIN2DIFF_4096)
  #error "ERRORE! WinetHP Input2 differenziale � definito due volte"
#endif

#if defined (WHPIN3_2048) && (WHPIN3_4096)
  #error "ERRORE! WinetHP Input3 � definito due volte"
#endif

#if defined (WHPIN3DIFF_2048) && (WHPIN3DIFF_4096)
  #error "ERRORE! WinetHP Input3 differenziale � definito due volte"
#endif

#if defined (WHPIN4_2048) && (WHPIN4_4096)
  #error "ERRORE! WinetHP Input4 � definito due volte"
#endif

#if defined (WHPIN5_2048) && (WHPIN5_4096)
  #error "ERRORE! WinetHP Input5 � definito due volte"
#endif

#if defined (WHPIN6_2048) && (WHPIN6_4096)
  #error "ERRORE! WinetHP Input6 � definito due volte"
#endif

#if defined (WHPIN7_2048) && (WHPIN7_4096)
  #error "ERRORE! WinetHP Input7 � definito due volte"
#endif

#if ( defined (WHPIN0_2048) || defined(WHPIN0_4096) || defined(WHPIN1_2048) || defined(WHPIN1_4096) ) && ( defined (WHPIN0DIFF_2048) || (WHPIN0DIFF_4096) )
  #error "ERRORE! WinetHP Input0 differenziale non pu� essere definito"
#endif

#if ( defined (WHPIN2_2048) || defined(WHPIN2_4096) || defined(WHPIN3_2048) || defined(WHPIN3_4096) ) && ( defined (WHPIN1DIFF_2048) || (WHPIN1DIFF_4096) )
  #error "ERRORE! WinetHP Input0 differenziale non pu� essere definito"
#endif

#if ( defined (WHPIN4_2048) || defined(WHPIN4_4096) || defined(WHPIN5_2048) || defined(WHPIN5_4096) ) && ( defined (WHPIN2DIFF_2048) || (WHPIN2DIFF_4096) )
  #error "ERRORE! WinetHP Input0 differenziale non pu� essere definito"
#endif

#if ( defined (WHPIN6_2048) || defined(WHPIN6_4096) || defined(WHPIN7_2048) || defined(WHPIN7_4096) ) && ( defined (WHPIN3DIFF_2048) || (WHPIN3DIFF_4096) )
  #error "ERRORE! WinetHP Input0 differenziale non pu� essere definito"
#endif