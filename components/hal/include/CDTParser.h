/**************************************************************************************************
  Filename:       Decagon_5TE.h
  Revised:        $Date: 2007-09-11 10:58:41 -0700 (Tue, 11 Sep 2007) $
  Revision:       $Revision: 15371 $

  Description:    Describe the purpose and contents of the file.

**************************************************************************************************
 **************************************************************************************************/

#ifdef __cplusplus
extern "C"
{
#endif

#if defined (DECCDT)
/**************************************************************************************************
 * INCLUDES
 **************************************************************************************************/
#include "SdiCore.h"
#include "ioCC2530.h"
#include "hal_types.h"
#include "ZComDef.h"
#include "hal_board_cfg.h"

extern void CDTParser(byte *buffer2, int16 *resbuf);
extern void hexCDTMultiplier (byte *databuf,uint16 dotpos, uint8 TYPE, int16 *resbuf);
//extern uint16 finddot(byte *databuff,uint8 len);

// Tipi di dato disponibili
#define DEP 1
#define TEMP 2
#define EC 3

#endif


#ifdef __cplusplus
}
#endif