/*-----------------------------------------------------------------------*/
/* Low level disk I/O module skeleton for FatFs     (C)ChaN, 2016        */
/*-----------------------------------------------------------------------*/
/* If a working storage control module is available, it should be        */
/* attached to the FatFs via a glue function rather than modifying it.   */
/* This is an example of glue functions to attach various exsisting      */
/* storage control modules to the FatFs module with a defined API.       */
/*-----------------------------------------------------------------------*/

#if defined SPI

#include "diskio.h"		/* FatFs lower layer API */
#include "hal_types.h"
#include "hal_spi.h"
#include "sdcard.h"

/* Definitions of physical drive number for each drive */
#define SD		0	/* Example: Map Ramdisk to physical drive 0 */

SD_CARD sd_card;


/*-----------------------------------------------------------------------*/
/* Get Drive Status                                                      */
/*-----------------------------------------------------------------------*/

DSTATUS disk_status (
	BYTE pdrv		/* Physical drive nmuber to identify the drive */
)
{
	DSTATUS stat=RES_OK;

        switch (pdrv) {
	case SD:
		return stat;
	}
	return STA_NOINIT;
	
}



/*-----------------------------------------------------------------------*/
/* Inidialize a Drive                                                    */
/*-----------------------------------------------------------------------*/

DSTATUS disk_initialize (
	BYTE pdrv				/* Physical drive nmuber to identify the drive */
)
{
	DSTATUS stat;
        uint8 deadlockcount=0;

	switch (pdrv) {
	case SD :
          do{          
          deadlockcount++;
          stat = SDInit(&sd_card);
          
          //Nicolas 15/12/2017 deadlockcount era 0xFF;
          //Se dopo 10 tentativi non trovo la scheda , vuol dire che non c'�
          }
          while( (stat != 0) && (deadlockcount != 0x10) );
          return stat;
	}
	return STA_NOINIT;
}



/*-----------------------------------------------------------------------*/
/* Read Sector(s)                                                        */
/*-----------------------------------------------------------------------*/

DRESULT disk_read (
	BYTE pdrv,		/* Physical drive nmuber to identify the drive */
	BYTE *buff,		/* Data buffer to store read data */
	DWORD sector,	/* Start sector in LBA */
	UINT count		/* Number of sectors to read */
)
{
	switch (pdrv) {
	case SD :
          return SD_readMultipleBlock (sector, count, buff);

	}
	return RES_PARERR;
}



/*-----------------------------------------------------------------------*/
/* Write Sector(s)                                                       */
/*-----------------------------------------------------------------------*/

DRESULT disk_write (
	BYTE pdrv,			/* Physical drive nmuber to identify the drive */
	uint8 *buff,	/* Data to be written */
	DWORD sector,		/* Start sector in LBA */
	UINT count			/* Number of sectors to write */
)
{
	switch (pdrv) {
	case SD :
          return SD_writeMultipleBlock(sector, count, buff);

	}
	return RES_PARERR;

}



/*-----------------------------------------------------------------------*/
/* Miscellaneous Functions                                               */
/*-----------------------------------------------------------------------*/

DRESULT disk_ioctl (
	BYTE pdrv,		/* Physical drive nmuber (0..) */
	BYTE cmd,		/* Control code */
	void *buff		/* Buffer to send/receive control data */
)
{
  return RES_OK;
}

#endif